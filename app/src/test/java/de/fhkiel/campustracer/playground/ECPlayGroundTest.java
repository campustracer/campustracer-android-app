package de.fhkiel.campustracer.playground;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.generators.ECKeyPairGenerator;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECKeyGenerationParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.spec.ECPrivateKeySpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;
import org.bouncycastle.math.ec.ECPoint;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.exceptions.CryptoDecryptionException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;


public class ECPlayGroundTest {
	@BeforeClass
	public static void setUp() {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Test
	public void genKeyPairWithD()
		throws CryptoEncryptionException, CryptoDecryptionException, NoSuchAlgorithmException,
		InvalidKeySpecException, NoSuchProviderException {
		// Generate a SecretKey with PBKDF2
		byte[] seed = "TEST".getBytes(StandardCharsets.UTF_8);
		System.out.println("seed: " + Base64.getEncoder().encodeToString(seed));
		SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
		PBEKeySpec pbekeyspec =
			new PBEKeySpec("secret pass".toCharArray(), seed, 1000, 256 /* in bit = 32 byte*/);
		SecretKey pbkdf2Key = skf.generateSecret(pbekeyspec);

		// make secretKey = D for Privatekey
		BigInteger D = new BigInteger(pbkdf2Key.getEncoded());

		//  Configure Curve Parameters
		String curveName = "secp256k1";
		X9ECParameters ecp = SECNamedCurves.getByName(curveName);
		ECParameterSpec ecParameterSpec = new ECNamedCurveParameterSpec(curveName,
			ecp.getCurve(),
			ecp.getG(),
			ecp.getN(),
			ecp.getH(),
			ecp.getSeed()
		);

		System.out.println("N bitLength: " + ecp.getN().bitLength());
		System.out.println("N hex: " + ecp.getN().toString(16));

		ECDomainParameters domainParams = new ECDomainParameters(ecp.getCurve(),
			ecp.getG(),
			ecp.getN(),
			ecp.getH(),
			ecp.getSeed()
		);

		// generate PrivKey
		ECPrivateKeyParameters ecPrivateKey = new ECPrivateKeyParameters(D, domainParams);
		assertThat("d of generated privkey matches",
			Arrays.equals(ecPrivateKey.getD().toByteArray(), D.toByteArray()),
			equalTo(true)
		);
		// generate Q for Public Key
		ECPoint Q = domainParams.getG().multiply(D);

		// generate PubKey
		ECPublicKeyParameters ecPublicKey = new ECPublicKeyParameters(Q, domainParams);
		assertThat("Q of generated PubKey matches",
			Arrays.equals(ecPublicKey.getQ().getEncoded(false), Q.getEncoded(false)),
			equalTo(true)
		);

		// transform to KeyPair of PrivateKey + PublicKey
		ECPrivateKeySpec ecprivkeyspec = new ECPrivateKeySpec(ecPrivateKey.getD(),
			ecParameterSpec);
		KeyFactory keyFactory = KeyFactory.getInstance("EC", "BC");
		PrivateKey privateKeyObj = keyFactory.generatePrivate(ecprivkeyspec);

		ECPublicKeySpec ecpubkeyspec = new ECPublicKeySpec(ecPublicKey.getQ(), ecParameterSpec);
		PublicKey publicKeyObj = keyFactory.generatePublic(ecpubkeyspec);
		KeyPair keyPairObj = new KeyPair(publicKeyObj, privateKeyObj);

		String teststr = "HI TEST! HELLO WORLD! WOW IT WORKS!";
		assertThat("Encryption and Decryption works with generated KeyPair",
			CryptoWrapper.decryptToString(CryptoWrapper.encrypt(teststr, keyPairObj.getPublic()),
				keyPairObj.getPrivate()
			),
			equalTo(teststr)
		);
		//assertThat(Arrays.equals(, publicKey.getQ().getEncoded(false)), equalTo(true));
	}

	@Test
	public void testtest3() {
		KeyPair kp = CryptoWrapper.generateKeypair();

		byte[] message = "HALLO WELT!!!".getBytes(StandardCharsets.UTF_8);
		byte[] signature = CryptoWrapper.sign(message, kp.getPrivate());

		System.out.println("PrivKey: " + Base64.getEncoder()
			.encodeToString(CryptoWrapper.serializePrivKey(kp.getPrivate())));
		System.out.println("PrivKey: " + Base64.getEncoder()
			.encodeToString(CryptoWrapper.serializePrivKey(kp.getPrivate())));
		System.out.println("PubKey:  " + Base64.getEncoder()
			.encodeToString(CryptoWrapper.serializePubKey(kp.getPublic())));
		System.out.println("Message: " + Base64.getEncoder().encodeToString(message));

		System.out.println("Signtr: " + Base64.getEncoder().encodeToString(signature));
	}

	@Test
	public void testtest4() throws CryptoKeyDeserializationException {

		byte[] message = "HALLO WELT!!!".getBytes(StandardCharsets.UTF_8);
		String signatureB64 =
			"MEUCIEYY3bPTyDu1k/puZOnx0CHiTaXDALFJu1qUsDSYB11oAiEAqPbLlC9K4R4F0aWtJlBU2Z3NijoVyI"
				+ "/5pbx66gNiBbE=";
		String pubKeyB64 = "AhS/bp6pN554xoHqo1Xcv09SIi1h709tGvRUlOfeKol1";
		PublicKey pubKey = CryptoWrapper.deserializePubKey(Base64.getDecoder().decode(pubKeyB64));
		byte[] signatureBytes = Base64.getDecoder().decode(signatureB64);
		if (CryptoWrapper.verify(message, signatureBytes, pubKey)) {
			System.out.println("SIGNATURE OK!");
		} else {
			System.out.printf("SIGNATURE FAILED !!!");
		}
	}

	@Test
	public void testtest1() {
		// Get domain parameters for example curve secp256k1
		X9ECParameters ecp = SECNamedCurves.getByName("secp256k1");
		ECDomainParameters domainParams = new ECDomainParameters(ecp.getCurve(),
			ecp.getG(),
			ecp.getN(),
			ecp.getH(),
			ecp.getSeed()
		);

		// Generate a private key and a public key
		ECKeyGenerationParameters keyGenParams =
			new ECKeyGenerationParameters(domainParams, new SecureRandom());
		ECKeyPairGenerator generator = new ECKeyPairGenerator();
		generator.init(keyGenParams);
		AsymmetricCipherKeyPair keyPair = generator.generateKeyPair();

		ECPrivateKeyParameters privkey = (ECPrivateKeyParameters) keyPair.getPrivate();
		ECPublicKeyParameters publicKey = (ECPublicKeyParameters) keyPair.getPublic();

		System.out.println("D length: " + privkey.getD().toByteArray().length);

		BigInteger d = privkey.getD();
		ECPoint q = privkey.getParameters().getG().multiply(d);
		ECPublicKeyParameters pubkeyRecreated =
			new ECPublicKeyParameters(q, privkey.getParameters());
		AsymmetricCipherKeyPair keyPairRecreated =
			new AsymmetricCipherKeyPair(pubkeyRecreated, privkey);

		byte[] privateKeyBytes = privkey.getD().toByteArray();

		// First print our generated private key and public key

		// Then calculate the public key only using domainParams.getG() and private key
		ECPoint Q = domainParams.getG().multiply(new BigInteger(privateKeyBytes));
		assertThat(Arrays.equals(Q.getEncoded(false), publicKey.getQ().getEncoded(false)),
			equalTo(true)
		);
	}

	@Test
	public void testtest2() throws NoSuchAlgorithmException, NoSuchProviderException {
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC", "BC");
		kpg.initialize(239, new SecureRandom());
		KeyPair kp = kpg.generateKeyPair();
		PrivateKey kppriv = kp.getPrivate();
		System.out.println(kppriv.getClass().getName());
		BCECPrivateKey kppriv_k = (BCECPrivateKey) kppriv;

		//var ks = new PKCS8EncodedKeySpec(kppriv.getEncoded());
		//var x = KeyFactory.getInstance("EC", "BC").translateKey();
		//System.out.println(kppriv.getEncoded().length); // -> 144

/*
		var key2 = skf.generateSecret(pbekeyspec);
		System.out.println(key1.getClass().getName());
		System.out.println((key1.getEncoded().length));
		System.out.println("key: " + Base64.getEncoder().encodeToString(key1.getEncoded()));


		assertThat(Arrays.equals(key1.getEncoded(), key2.getEncoded()), equalTo(true));
		//var kf = KeyFactory.getInstance("EC", "BC");
		//kf.generatePrivate(pbekeyspec);


		//new ECKeyGenerationParameters()

		//return kpg.generateKeyPair();
*/
	}
}
