package de.fhkiel.campustracer.entities;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.junit.MatcherAssert.assertThat;
import static org.junit.Assert.assertThrows;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.javatuples.Pair;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;

import de.fhkiel.campustracer.api.MockDatabase;
import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.cryptowrapper.ThresholdCryptoSystem;
import de.fhkiel.campustracer.entities.qrcode.Lecture;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoDecryptionException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;
import de.fhkiel.campustracer.model.CheckInEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;
import de.fhkiel.campustracer.tracing.ContactTracing;


public class ContactTracingTest {
	private static final Boolean SHOW_QR_CODE = false;
	private List<Student> studentObjectsList;
	private List<Lecturer> lecturerObjectsList;
	private List<Lecture> lecturesObjectsList;
	private List<KeyPair> trustedPartyKeyPairs;

	private University university = University.getInstance();
	private final MockDatabase mockDatabase = MockDatabase.getInstance();

	private final long LECTURER = 1;
	private final long STUDENT1 = 1001;
	private final long STUDENT2 = 1002;
	private final long STUDENT3 = 1003;
	private final long STUDENT4 = 1004;
	private final long STUDENT5 = 1005;
	private final long STUDENT6_NO_APP = 1006;
	private final long STUDENT7_NO_APP = 1007;
	private final long STUDENT8_SOME_APP = 1008;

	public ContactTracingTest() {
	}

	@BeforeClass
	public static void beforeClass() {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Before
	public void beforeEach() throws Exception {

		MockDatabase.clearInstance();

		// initialize lists
		this.studentObjectsList = new ArrayList<>();
		this.lecturerObjectsList = new ArrayList<>();
		this.lecturesObjectsList = new ArrayList<>();
		this.trustedPartyKeyPairs = new ArrayList<>();

		// create University
		this.university = University.getInstance();
		this.university.generateKeyPair();

		// create TrustedParties
		this.trustedPartyKeyPairs.add(CryptoWrapper.generateKeypair());
		this.trustedPartyKeyPairs.add(CryptoWrapper.generateKeypair());
		this.trustedPartyKeyPairs.add(CryptoWrapper.generateKeypair());

		List<PublicKey> trustedPartiesPubKeysList =
			this.trustedPartyKeyPairs.stream().map(KeyPair::getPublic).collect(Collectors.toList());

		// CREATE LECTURER
		Lecturer lecturer1 = new Lecturer(this.LECTURER);
		lecturer1.registerLecturer(LocalDateTime.of(2021, 11, 13, 11, 12, 13),
			this.university.getPublicKey(),
			trustedPartiesPubKeysList
		);
		this.lecturerObjectsList.add(lecturer1);

		// CREATE STUDENTS
		for (long sID : Arrays.asList(this.STUDENT1,
			this.STUDENT2,
			this.STUDENT3,
			this.STUDENT4,
			this.STUDENT5,
			this.STUDENT6_NO_APP,
			this.STUDENT7_NO_APP,
			this.STUDENT8_SOME_APP
		)) {
			this.studentObjectsList.add(new Student(sID));
		}

		//register students
		for (Student stud : this.studentObjectsList) {
			stud.registerStudent(LocalDateTime.of(2021, 11, 13, 11, 12, 13),
				this.university.getPublicKey(),
				trustedPartiesPubKeysList
			);
		}

		// NEW LECTURE
		Lecture lecture1 = new Lecture(this.university.getPublicKey(),
			LocalDate.of(2021, 11, 14),
			lecturer1.getCheckInAuthKey()
		);
		lecturer1.checkInToLecture(lecture1, this.university.getPublicKey());
		this.lecturesObjectsList.add(lecture1);
		// Student Check-In with Student App + QR-Code
		for (Student attendee : this.filterStudentObjectList(this.STUDENT1,
			this.STUDENT2
		)) //filter list for students
		{
			attendee.checkInWithQRCode(lecture1.getNextLectureQRCode().toBytes(),
				this.university.getPublicKey(),
				lecture1.getLectureDate()
			);
		}
		// No-App Check In
		for (Student noAppAttendee : this.filterStudentObjectList(this.STUDENT6_NO_APP)) {
			lecturer1.checkInStudentWithoutApp(noAppAttendee.getStudentId(),
				lecture1,
				this.university.getPublicKey()
			);
		}

		if (SHOW_QR_CODE || true) {
			System.out.println("LECTURE1QRCODE: " + Base64.getEncoder()
				.encodeToString(lecture1.getNextLectureQRCode().toBytes()));
		}

		// NEW LECTURE
		Lecture lecture2 = new Lecture(this.university.getPublicKey(),
			LocalDate.of(2021, 11, 15),
			lecturer1.getCheckInAuthKey()
		);
		this.lecturesObjectsList.add(lecture2);
		lecturer1.checkInToLecture(lecture2, this.university.getPublicKey());

		// Student Check-In with Student App + QR-Code
		for (Student attendee : this.filterStudentObjectList(this.STUDENT1,
			this.STUDENT4,
			this.STUDENT5,
			this.STUDENT8_SOME_APP
		)) //filter list for students
		{
			attendee.checkInWithQRCode(lecture2.getNextLectureQRCode().toBytes(),
				this.university.getPublicKey(),
				lecture2.getLectureDate()
			);
		}
		// No-App Check In
		for (Student noAppAttendee : this.filterStudentObjectList(this.STUDENT7_NO_APP)) {
			lecturer1.checkInStudentWithoutApp(noAppAttendee.getStudentId(),
				lecture2,
				this.university.getPublicKey()
			);
		}

		// NEW LECTURE
		Lecture lecture3 = new Lecture(this.university.getPublicKey(),
			LocalDate.of(2021, 11, 15),
			lecturer1.getCheckInAuthKey()
		);
		this.lecturesObjectsList.add(lecture3);
		lecturer1.checkInToLecture(lecture3, this.university.getPublicKey());

		// Student Check-In with Student App + QR-Code
		for (Student attendee : this.filterStudentObjectList(this.STUDENT3,
			this.STUDENT5
		)) //filter list for students
		{
			attendee.checkInWithQRCode(lecture3.getNextLectureQRCode().toBytes(),
				this.university.getPublicKey(),
				lecture3.getLectureDate()
			);
		}
		// No-App Check In
		for (Student noAppAttendee : this.filterStudentObjectList(this.STUDENT8_SOME_APP)) {
			lecturer1.checkInStudentWithoutApp(noAppAttendee.getStudentId(),
				lecture3,
				this.university.getPublicKey()
			);
		}
	}

	public void printIds(Iterable<Long> ids) {
		System.out.println("FOUND THE FOLLOWING STUDENT IDS:");
		for (Long l : ids) {
			System.out.println(l);
		}
	}

	@Test
	// Tests:
	// CheckInKeyPair Lookup
	// Recreate Keys from MasterKeyPrivKey
	public void student0001InfectedLecture12AndTracingViaMasterPrivKey()
		throws CryptoKeyDeserializationException, CryptoDecryptionException,
		CryptoDataDeserializationException, ApiHttpException {
		List<Long> contactStudentIDs =
			this.traceContactsOfUserFromMasterPrivKey(this.STUDENT1, LocalDate.of(2021, 11, 14),
				// SO
				LocalDate.of(2021, 11, 15)
				// MO
			);
		this.printIds(contactStudentIDs);

		assertThat(contactStudentIDs, containsInAnyOrder(this.STUDENT1,
			this.STUDENT2,
			this.STUDENT4,
			this.STUDENT5,
			this.STUDENT6_NO_APP,
			this.STUDENT7_NO_APP,
			this.STUDENT8_SOME_APP,
			this.LECTURER
		)); //matches length and items
	}

	@Test
	// Tests:
	// CheckInKeyPair Lookup
	// TPs decrypt MasterPrivKey
	// Recreate Keys from MasterKeyPrivKey
	public void student0001InfectedLecture12AndTracingViaMasterPrivKeyWithTrustedPartyDecryption()
		throws CryptoDecryptionException, CryptoKeyDeserializationException, IOException,
		CryptoDataDeserializationException, ApiHttpException {

		long infectedStudentId = this.STUDENT1;
		List<LocalDate> dates = Arrays.asList(
			LocalDate.of(2021, 11, 14), // SO
			LocalDate.of(2021, 11, 15)
		);

		ArrayList<UserMasterKeyEntry> studentMasterKeyEntries =
			new ArrayList<UserMasterKeyEntry>();
		for (LocalDate date : dates) {
			studentMasterKeyEntries.addAll(this.mockDatabase.getStudentMasterKeyEntriesForStudentIdAndDate(infectedStudentId,
				date
			));
		}

		TreeSet<Long> contactStudentIDs = new TreeSet<Long>(); //auto-deduplicating
		for (UserMasterKeyEntry studentMasterKeyEntry : studentMasterKeyEntries) {
			byte[] thresholdCryptoSysRaw =
				CryptoWrapper.decrypt(studentMasterKeyEntry.getEncryptedMasterPrivKey(),
					this.university.getPrivateKey()
				);
			ThresholdCryptoSystem threshCryptoSys =
				ThresholdCryptoSystem.fromBytes(thresholdCryptoSysRaw);

			for (KeyPair tpKP : this.trustedPartyKeyPairs) {
				Boolean ep = threshCryptoSys.decryptPartial(tpKP.getPublic(), tpKP.getPrivate());
				System.out.println(ep ? "TRUE" : "FALSE");
				//assertThat(ep, equalTo(true));
			}
			assertThat(threshCryptoSys.isDecryptable(), equalTo(true));
			Optional<byte[]> masterPrivKeyOptional = threshCryptoSys.decryptAll();
			assertThat(masterPrivKeyOptional.isPresent(), equalTo(true));
			PrivateKey masterPrivKey =
				CryptoWrapper.deserializePrivKey(masterPrivKeyOptional.get());
			KeyPair masterKeyPair =
				new KeyPair(CryptoWrapper.calculatePubKeyFromPrivKey(masterPrivKey),
					masterPrivKey);
			ContactTracing contactTracing = ContactTracing.fromMasterPrivKey(infectedStudentId,
				masterKeyPair,
				dates,
				new byte[]{
					1, 2, 3
				}
			);

			contactStudentIDs.addAll(contactTracing.performTracing(this.university.getPrivateKey()));
		}

		this.printIds(contactStudentIDs);

		assertThat(contactStudentIDs, containsInAnyOrder(this.STUDENT1,
			this.STUDENT2,
			this.STUDENT4,
			this.STUDENT5,
			this.STUDENT6_NO_APP,
			this.STUDENT7_NO_APP,
			this.STUDENT8_SOME_APP,
			this.LECTURER
		)); //matches length and items
	}

	@Test
	// Tests:
	// CheckInKeyPair Lookup
	// Surrender CheckInKeyPairs
	public void student0001InfectedLecture12AndTracingViaSurrenderedKeys()
		throws CryptoKeyDeserializationException, CryptoDecryptionException,
		CryptoDataDeserializationException, ApiHttpException {
		List<Long> contactStudentIDs =
			this.traceContactsOfUserFromSurrenderedKeys(this.STUDENT1, LocalDate.of(2021, 11, 14),
				// SO
				LocalDate.of(2021, 11, 15)
				// MO
			);
		this.printIds(contactStudentIDs);

		assertThat(contactStudentIDs, containsInAnyOrder(this.STUDENT1,
			this.STUDENT2,
			this.STUDENT4,
			this.STUDENT5,
			this.STUDENT6_NO_APP,
			this.STUDENT7_NO_APP,
			this.STUDENT8_SOME_APP,
			this.LECTURER
		)); //matches length and items
	}

	@Test
	// Tests:
	// Infected Lecturer
	// CheckInKeyPair Lookup
	// Surrender CheckInKeyPairs
	public void lecturer0001InfectedAllAndTracingViaSurrenderedKeys()
		throws CryptoKeyDeserializationException, CryptoDecryptionException,
		CryptoDataDeserializationException, ApiHttpException {
		List<Long> contactStudentIDs =
			this.traceContactsOfUserFromSurrenderedKeys(this.LECTURER, LocalDate.of(2021, 11, 14),
				// SO
				LocalDate.of(2021, 11, 15)
				// MO
			);
		this.printIds(contactStudentIDs);

		assertThat(contactStudentIDs, containsInAnyOrder(this.STUDENT1,
			this.STUDENT2,
			this.STUDENT3,
			this.STUDENT4,
			this.STUDENT5,
			this.STUDENT6_NO_APP,
			this.STUDENT7_NO_APP,
			this.STUDENT8_SOME_APP,
			this.LECTURER
		)); //matches length and items
	}

	@Test
	// Tests:
	// Infected Lecturer
	// CheckInKeyPair Lookup
	// from MasterPrivKey
	public void lecturer0001InfectedAllAndTracingViaMasterPrivKey()
		throws CryptoKeyDeserializationException, CryptoDecryptionException,
		CryptoDataDeserializationException, ApiHttpException {
		List<Long> contactStudentIDs =
			this.traceContactsOfUserFromMasterPrivKey(this.LECTURER, LocalDate.of(2021, 11, 14),
				// SO
				LocalDate.of(2021, 11, 15)
				// MO
			);
		this.printIds(contactStudentIDs);

		assertThat(contactStudentIDs, containsInAnyOrder(this.STUDENT1,
			this.STUDENT2,
			this.STUDENT3,
			this.STUDENT4,
			this.STUDENT5,
			this.STUDENT6_NO_APP,
			this.STUDENT7_NO_APP,
			this.STUDENT8_SOME_APP,
			this.LECTURER
		)); //matches length and items
	}

	@Test
	// Tests:
	// PreGeneratedKeyPair Encryption Lookup
	// Recreate Keys from MasterKeyPrivKey
	public void student0006NoAppInfectedLecture1StudentsAndTracingViaMasterKey()
		throws CryptoKeyDeserializationException, CryptoDecryptionException,
		CryptoDataDeserializationException, ApiHttpException {
		List<Long> contactStudentIDs =
			this.traceContactsOfUserFromMasterPrivKey(this.STUDENT6_NO_APP,
				LocalDate.of(2021, 11, 14),
				// SO
				LocalDate.of(2021, 11, 15)
				// MO
			);
		this.printIds(contactStudentIDs);

		assertThat(contactStudentIDs, containsInAnyOrder(this.STUDENT1,
			this.STUDENT2,
			this.STUDENT6_NO_APP,
			this.LECTURER
		)); //matches length and items
	}

	@Test
	// Tests:
	// PreGeneratedKeyPair Encryption Lookup
	// Surrender PreGeneratedKeyPairs
	public void student0006NoAppInfectedLecture1StudentdAndTracingViaSurrenderedKeys()
		throws CryptoKeyDeserializationException, CryptoDecryptionException,
		CryptoDataDeserializationException, ApiHttpException {
		List<Long> contactStudentIDs =
			this.traceContactsOfUserFromSurrenderedKeys(this.STUDENT6_NO_APP,
				LocalDate.of(2021, 11, 14),
				// SO
				LocalDate.of(2021, 11, 15)
				// MO
			);
		this.printIds(contactStudentIDs);

		assertThat(contactStudentIDs,
			containsInAnyOrder(this.STUDENT1, this.STUDENT2, this.STUDENT6_NO_APP, this.LECTURER)
		); //matches length and items
	}

	@Test
	// Tests:
	// PreGeneratedKeyPair Encryption Lookup
	// Surrender PreGeneratedKeyPairs
	public void student0003NoAppInfectedLecture3StudentsAndTracingViaSurrenderedKeys()
		throws CryptoKeyDeserializationException, CryptoDecryptionException,
		CryptoDataDeserializationException, ApiHttpException {
		List<Long> contactStudentIDs =
			this.traceContactsOfUserFromSurrenderedKeys(this.STUDENT3, LocalDate.of(2021, 11, 14),
				// SO
				LocalDate.of(2021, 11, 15)
				// MO
			);
		this.printIds(contactStudentIDs);

		assertThat(contactStudentIDs,
			containsInAnyOrder(this.STUDENT3, this.STUDENT5, this.STUDENT8_SOME_APP, this.LECTURER)
		); //matches length and items
	}

	@Test
	public void detectForgedCheckInSignature() throws Exception {
		// Student 0001 forges CheckIn signatures for Student 0003

		Optional<User> student3o = this.getFromStudentObjectList(this.STUDENT3);
		Optional<User> student1o = this.getFromStudentObjectList(this.STUDENT1);
		if (!student3o.isPresent() || !student1o.isPresent()) {
			throw new IllegalStateException("ERROR");
		}
		User student3 = student3o.get();
		User student1 = student1o.get();

		Lecture lecture2 = this.lecturesObjectsList.get(1);

		CheckInEntry checkInEntry = new CheckInEntry(student3.getUserId(),
			lecture2.getLecturePubKey(),
			Optional.empty(),
			student3.getUserId(),
			student1.getMasterKeyPair().getPrivate(),
			lecture2.getLecturePubKey(),
			lecture2.getLecturePrivKeyEncryptedWithUniPubKey(),
			lecture2.getNextLecturePrivKeyHashRandomized(),
			lecture2.getLectureDate(),
			this.university.getPublicKey()
		);

		this.mockDatabase.addCheckInEntry(checkInEntry);

		LocalDate exDate = LocalDate.of(2021, 11, 15);
		Exception exception = assertThrows(IllegalStateException.class, () -> {
			this.traceContactsOfUserFromMasterPrivKey(
				this.STUDENT1,
				//LocalDate.of(2021, 11, 14), // SO
				exDate // MO
			);
		});

		assertThat(exception.getMessage(), equalTo("COULD NOT VERIFY SIGNATURE"));
	}

	// ---------------  END OF TESTS ------------------

	private List<Student> filterStudentObjectList(long... studentIDs) {
		//List<Long> sidList = Arrays.stream(studentIDs).collect(Collectors.toList());
		List<Long> sidList = Arrays.stream(studentIDs).boxed().collect(Collectors.toList());
		return this.studentObjectsList.stream()
			.filter(x -> sidList.contains(x.getStudentId()))
			.collect(Collectors.toList());
	}

	private List<Long> traceContactsOfUserFromMasterPrivKey(
		long userId, LocalDate... contagiousDates
	) throws CryptoKeyDeserializationException, CryptoDecryptionException,
		CryptoDataDeserializationException, ApiHttpException {
		return this.traceContactsOfUser(userId, true, Arrays.asList(contagiousDates));
	}

	private List<Long> traceContactsOfUserFromSurrenderedKeys(
		long studentID, LocalDate... contagiousDates
	) throws CryptoKeyDeserializationException, CryptoDecryptionException,
		CryptoDataDeserializationException, ApiHttpException {
		return this.traceContactsOfUser(studentID, false, Arrays.asList(contagiousDates));
	}

	private List<Long> traceContactsOfUser(
		long userId, Boolean fromMasterPrivKeyOrSurrenderedKeys, List<LocalDate> contagiousDates
	) throws CryptoKeyDeserializationException, CryptoDecryptionException,
		CryptoDataDeserializationException, ApiHttpException {
		Optional<User> infUserOptional = this.getFromStudentObjectList(userId);
		if (!infUserOptional.isPresent()) {
			infUserOptional = this.getFromLecturerObjectList(userId);
			if (!infUserOptional.isPresent()) {
				throw new IllegalArgumentException(
					"PERSON ID NEITHER IN STUDENTS NOR LECTURERS LIST!");
			}
		}
		User userObj = infUserOptional.get();
		ContactTracing contactTracing;
		if (fromMasterPrivKeyOrSurrenderedKeys) {
			contactTracing = ContactTracing.fromMasterPrivKey(userObj.getUserId(),
				userObj.getMasterKeyPair(),
				contagiousDates,
				new byte[]{
					1, 2, 3
				}
			);
		} else {
			List<KeyPair> surCheckInKeys =
				userObj.surrenderCheckInKeyPairsForDates(contagiousDates);

			Map<Pair<LocalDateTime, LocalDateTime>, KeyPair> surPreGenKeys = new HashMap<>();
			if (userObj.isStudent()) {
				surPreGenKeys =
					((Student) userObj).surrenderPreGeneratedKeyPairsForDates(contagiousDates);
			}
			contactTracing = new ContactTracing(userObj.getUserId(),
				userObj.getMasterPublicKey(),
				surCheckInKeys,
				surPreGenKeys,
				new byte[]{
					1, 2, 3
				}
			);
		}
		// return contact studentIDs
		return contactTracing.performTracing(this.university.getPrivateKey());
	}

	private Optional<User> getFromStudentObjectList(long studentID) {

		List<Student> sobjl = this.studentObjectsList.stream()
			.filter(x -> studentID == x.getStudentId())
			.collect(Collectors.toList());
		if (!sobjl.isEmpty()) {
			return Optional.of(sobjl.get(0));
		} else {
			return Optional.empty();
		}
	}

	private Optional<User> getFromLecturerObjectList(long lecturerID) {

		List<Lecturer> sobjl = this.lecturerObjectsList.stream()
			.filter(x -> lecturerID == x.getLecturerId())
			.collect(Collectors.toList());
		if (!sobjl.isEmpty()) {
			return Optional.of(sobjl.get(0));
		} else {
			return Optional.empty();
		}
	}
}
