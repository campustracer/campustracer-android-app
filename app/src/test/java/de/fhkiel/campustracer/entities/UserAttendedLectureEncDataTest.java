package de.fhkiel.campustracer.entities;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.junit.MatcherAssert.assertThat;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.security.KeyPair;
import java.security.Security;
import java.time.LocalDate;
import java.util.Arrays;

import de.fhkiel.campustracer.api.MockDatabase;
import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoDecryptionException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.model.UserAttendedLectureEntry;
import de.fhkiel.campustracer.model.encdata.UserAttendedLectureEncData;


public class UserAttendedLectureEncDataTest {
	private static MockDatabase mockDatabase = MockDatabase.getInstance();

	@BeforeClass
	public static void beforeEach() {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Test
	public void signEncryptDecryptVerifyTest()
		throws CryptoEncryptionException, CryptoDecryptionException,
		CryptoDataDeserializationException {
		long studentId = 1001;
		long signedById = 2;
		LocalDate lectureDate = LocalDate.of(2021, 12, 1);
		KeyPair studentMasterKeypair = CryptoWrapper.generateKeypair();
		KeyPair studentDerivedKeyPair =
			CryptoWrapper.deriveCheckInKeypair(studentMasterKeypair.getPrivate(), lectureDate, 0);
		KeyPair lecturerMasterKeyPair = CryptoWrapper.generateKeypair();
		KeyPair universityKeyPair = CryptoWrapper.generateKeypair();
		KeyPair lectureKeyPair = CryptoWrapper.generateKeypair();

		byte[] encLecturePrivKey =
			CryptoWrapper.encrypt(CryptoWrapper.serializePrivKey(lectureKeyPair.getPrivate()),
				universityKeyPair.getPublic()
			);

		UserAttendedLectureEncData UALEncData =
			new UserAttendedLectureEncData(encLecturePrivKey, studentId, signedById, lectureDate);
		byte[] encUALEncData = UALEncData.signAndEncryptToBytes(studentDerivedKeyPair.getPublic(),
			universityKeyPair.getPublic(),
			lecturerMasterKeyPair.getPrivate()
		);
		byte[] signature = UALEncData.getSignature();

		System.out.println("EncData Length: " + encUALEncData.length);

		// reverse encryption, verify

		UserAttendedLectureEncData UALEncData2 =
			UserAttendedLectureEncData.fromBytes(encUALEncData,
			studentDerivedKeyPair.getPrivate(),
			universityKeyPair.getPrivate(),
			studentId,
			lectureDate
		);

		Boolean verifyOk = UALEncData2.verifySignature(lecturerMasterKeyPair.getPublic());
		assertThat(verifyOk, equalTo(true));

		// Compare Fields
		assertThat(Arrays.equals(UALEncData2.getEncLecturePrivKey(), encLecturePrivKey),
			equalTo(true)
		);
		assertThat(UALEncData2.getUserId(), equalTo(studentId));
		assertThat(UALEncData2.getSignedById(), equalTo(signedById));
		assertThat(UALEncData2.getLectureDate().compareTo(lectureDate), equalTo(0));
		assertThat(Arrays.equals(UALEncData2.getSignature(), signature), equalTo(true));
	}

	@Ignore("Too slow in Gitlab-CI/CD")
	//@Test
	public void iterativeDecryptionSpeedTest() throws CryptoEncryptionException {
		int ITERATIONS = 100000;
		KeyPair universityKeyPair = CryptoWrapper.generateKeypair();
		System.out.println("GENERATING " + ITERATIONS + " ENTRIES, PLEASE WAIT ...");
		for (int i = 0; i < ITERATIONS; i++) {

			long studentId = 1001;
			long signedById = 2;
			LocalDate lectureDate = LocalDate.of(2021, 12, 1);
			KeyPair studentMasterKeypair = CryptoWrapper.generateKeypair();
			KeyPair studentDerivedKeyPair =
				CryptoWrapper.deriveCheckInKeypair(studentMasterKeypair.getPrivate(),
					lectureDate,
					0
				);
			byte[] studentDerivedPrivKey =
				CryptoWrapper.hash(CryptoWrapper.serializePrivKey(studentDerivedKeyPair.getPrivate()));
			KeyPair lecturerMasterKeyPair = CryptoWrapper.generateKeypair();

			KeyPair lectureKeyPair = CryptoWrapper.generateKeypair();

			byte[] encLecturePrivKey =
				CryptoWrapper.encrypt(CryptoWrapper.serializePrivKey(lectureKeyPair.getPrivate()),
					universityKeyPair.getPublic()
				);

			UserAttendedLectureEncData UALEncData =
				new UserAttendedLectureEncData(encLecturePrivKey,
					signedById,
					studentId,
					lectureDate
				);
			byte[] encSalEncData =
				UALEncData.signAndEncryptToBytes(studentDerivedKeyPair.getPublic(),
					universityKeyPair.getPublic(),
					lecturerMasterKeyPair.getPrivate()
				);

			/* TODO: Replace with checkInEntry
			UserAttendedLectureEntry UALEntry =
				new UserAttendedLectureEntry(lectureDate, encSalEncData, studentDerivedPrivKey);
			mockDatabase.addUserAttendedLectureEntry(UALEntry, "1234");
			if (i == 1) {
				System.out.println("Size of UserAttendedLecture, base64 encoded, in bytes");
				System.out.println("CreatedAt: " + Base64.getEncoder()
					.encode(DateHelper.getISODateString(UALEntry.getCreatedAt())
						.getBytes(StandardCharsets.UTF_8)).length);
				System.out.println("EncData: " + Base64.getEncoder()
					.encode(UALEntry.getEncData()).length);
				System.out.println("DerPrivKeyHash: " + Base64.getEncoder()
					.encode(UALEntry.getId()).length);
				System.out.println("TOTAL: " + (Base64.getEncoder()
					.encode(DateHelper.getISODateString(UALEntry.getCreatedAt())
						.getBytes(StandardCharsets.UTF_8)).length + Base64.getEncoder()
					.encode(UALEntry.getEncData()).length + Base64.getEncoder()
					.encode(UALEntry.getId()).length));
			}
			*/
		}

		System.out.println("STARTING DECRYPTION ...");

		Boolean sec = null;
		long start = System.currentTimeMillis();
		for (UserAttendedLectureEntry UALEntry : mockDatabase.getUserAttendedLectureTable()) {

			sec = UserAttendedLectureEncData.isDecryptableWithPrivKey(UALEntry.getEncData(),
				universityKeyPair.getPrivate()
				// intentionally wrong key
			);
			System.out.println(sec ? "OK" : "FAIL");
		}
		long end = System.currentTimeMillis();
		System.out.println(ITERATIONS + " failed Decryptions in " + (end - start) + "ms (" + ((end
			- start) / (float) ITERATIONS) + "ms/Decryption) (outer layer only)");
	}
}
