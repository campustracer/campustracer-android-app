package de.fhkiel.campustracer.entities;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.BeforeClass;
import org.junit.Test;

import java.security.KeyPair;
import java.security.Security;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.entities.qrcode.Lecture;


public class StudentTest {
	@BeforeClass
	public static void setUp() {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Test
	public void surrenderCheckInKeyPairsForDates() throws Exception {
		Lecturer lecturer = new Lecturer(1);
		lecturer.registerLecturer(LocalDateTime.of(2021, 11, 13, 11, 12, 13),
			(CryptoWrapper.generateKeypair().getPublic()),
			Collections.emptyList()
		);

		KeyPair uniKeyPair = CryptoWrapper.generateKeypair();
		Student stud = new Student(1001);

		Lecture lectureA = new Lecture(uniKeyPair.getPublic(),
			LocalDate.of(2021, 11, 14),
			lecturer.getCheckInAuthKey()
		);
		byte[] lectureQrCodeA = lectureA.getNextLectureQRCode().toBytes();

		Lecture lectureB = new Lecture(uniKeyPair.getPublic(),
			LocalDate.of(2021, 11, 15),
			lecturer.getCheckInAuthKey()
		);
		byte[] lectureQrCodeB = lectureB.getNextLectureQRCode().toBytes();

		Lecture lectureC = new Lecture(uniKeyPair.getPublic(),
			LocalDate.of(2021, 11, 16),
			lecturer.getCheckInAuthKey()
		);
		byte[] lectureQrCodeC = lectureC.getNextLectureQRCode().toBytes();

		// Date A
		stud.checkInWithQRCode(lectureQrCodeA, uniKeyPair.getPublic(), lectureA.getLectureDate());
		stud.checkInWithQRCode(lectureQrCodeA, uniKeyPair.getPublic(), lectureA.getLectureDate());
		// Date B
		stud.checkInWithQRCode(lectureQrCodeB, uniKeyPair.getPublic(), lectureB.getLectureDate());
		// Date C
		stud.checkInWithQRCode(lectureQrCodeC, uniKeyPair.getPublic(), lectureC.getLectureDate());

		List<KeyPair> keysA =
			stud.surrenderCheckInKeyPairsForDates(Arrays.asList(LocalDate.of(2021, 11, 14)));
		assertThat(keysA.size(), equalTo(2));

		List<KeyPair> keysB =
			stud.surrenderCheckInKeyPairsForDates(Arrays.asList(LocalDate.of(2021, 11, 15)));
		assertThat(keysB.size(), equalTo(1));

		List<KeyPair> keysC =
			stud.surrenderCheckInKeyPairsForDates(Arrays.asList(LocalDate.of(2021, 11, 16)));
		assertThat(keysC.size(), equalTo(1));

		List<KeyPair> keysNone =
			stud.surrenderCheckInKeyPairsForDates(Arrays.asList(LocalDate.of(2021, 11, 30)));
		assertThat(keysNone.size(), equalTo(0));

		List<KeyPair> keysABC =
			stud.surrenderCheckInKeyPairsForDates(Arrays.asList(LocalDate.of(2021, 11, 14),
				LocalDate.of(2021, 11, 15),
				LocalDate.of(2021, 11, 16)
			));
		assertThat(keysABC.size(), equalTo(4));

		List<KeyPair> keysACNone =
			stud.surrenderCheckInKeyPairsForDates(Arrays.asList(LocalDate.of(2021, 11, 14),
				LocalDate.of(2021, 11, 16),
				LocalDate.of(2021, 11, 30)
			));
		assertThat(keysACNone.size(), equalTo(3));

		List<KeyPair> keysBC =
			stud.surrenderCheckInKeyPairsForDates(Arrays.asList(LocalDate.of(2021, 11, 16),
				LocalDate.of(2021, 11, 15)
			));
		assertThat(keysBC.size(), equalTo(2));
	}
}
