package de.fhkiel.campustracer.cryptowrapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;
import org.bouncycastle.util.encoders.Hex;
import org.junit.BeforeClass;
import org.junit.Test;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoDecryptionException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;


public class CryptoWrapperTest {
	@BeforeClass
	public static void setUp() {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Test
	public void asymEncryptDecrypt() throws CryptoEncryptionException, CryptoDecryptionException {
		KeyPair kp = CryptoWrapper.generateKeypair();
		System.out.println("Keys generated");
		PublicKey pubkey = kp.getPublic();
		PrivateKey privkey = kp.getPrivate();

		String message = "Hallo Welt Hier!!";
		byte[] ciphertxt = CryptoWrapper.encrypt(message, pubkey);
		String cleartxt = CryptoWrapper.decryptToString(ciphertxt, privkey);

		assertThat(cleartxt, equalTo(message));
	}

	@Test
	public void asymEncryptDecryptWithDerivedKeys()
		throws CryptoEncryptionException, CryptoDecryptionException {
		KeyPair masterKeyPair = CryptoWrapper.generateKeypair();
		String message = "Hallo Welt Hier!!";

		KeyPair checkInKeyPair = CryptoWrapper.deriveCheckInKeypair(masterKeyPair.getPrivate(),
			LocalDate.of(2021, 11, 20),
			0
		);
		byte[] ciphertxt = CryptoWrapper.encrypt(message, checkInKeyPair.getPublic());
		String cleartxt = CryptoWrapper.decryptToString(ciphertxt, checkInKeyPair.getPrivate());
		assertThat(cleartxt, equalTo(message));

		KeyPair preGeneratedKeyPair =
			CryptoWrapper.derivePreGeneratedKeypair(masterKeyPair.getPrivate(),
				LocalDateTime.of(2021, 11, 20, 11, 12, 13),
				LocalDateTime.of(2021, 11, 21, 11, 12, 13)
			);
		byte[] ciphertxt2 = CryptoWrapper.encrypt(message, preGeneratedKeyPair.getPublic());
		String cleartxt2 =
			CryptoWrapper.decryptToString(ciphertxt2, preGeneratedKeyPair.getPrivate());
		assertThat(cleartxt2, equalTo(message));
	}

	@Test
	public void asymCiphertextVariance() throws CryptoEncryptionException {
		KeyPair kp = CryptoWrapper.generateKeypair();
		System.out.println("Keys generated");
		PublicKey pubkey = kp.getPublic();

		String message = "Hallo Welt Hier!!";
		String ciphertxt1 = Hex.toHexString(CryptoWrapper.encrypt(message, pubkey));
		String ciphertxt2 = Hex.toHexString(CryptoWrapper.encrypt(message, pubkey));
		System.out.println("C1: " + ciphertxt1);
		System.out.println("C2: " + ciphertxt2);

		assertThat(
			"Ciphertexts of same message/pubkey combinations should differ due to padding",
			ciphertxt1,
			not(equalTo(ciphertxt2))
		);
	}

	@Test
	public void asymSignVerify() {
		KeyPair skp = CryptoWrapper.generateKeypair();
		PublicKey s_pubkey = skp.getPublic();
		PrivateKey s_privkey = skp.getPrivate();
		String s_message = "Dies ist eine sehr sehr sehr geheime Nachricht!";
		byte[] signature = CryptoWrapper.sign(s_message, s_privkey);
		System.out.println("SLEN: " + signature.length);
		Boolean isValid = CryptoWrapper.verify(s_message, signature, s_pubkey);
		System.out.println((isValid) ? "SIGNATURE OK" : "SIGNATURE FAILED!!!");
		assertThat(isValid, comparesEqualTo(true));
	}

	@Test
	public void asymSameSignatureTwice() {
		KeyPair skp = CryptoWrapper.generateKeypair();
		PrivateKey s_privkey = skp.getPrivate();
		String s_message = "Dies ist eine sehr sehr sehr geheime Nachricht!";
		String signature1 = Hex.toHexString(CryptoWrapper.sign(s_message, s_privkey));
		String signature2 = Hex.toHexString(CryptoWrapper.sign(s_message, s_privkey));
		System.out.println("Sig1: " + signature1);
		System.out.println("Sig2: " + signature2);
		if (CryptoWrapper.SIGNATURE_ALGORITHM.endsWith("ECDSA")) {
			assertThat("Two signatures of the same message should be different in ECIES",
				signature1,
				not(equalTo(signature2))
			);
		} else {
			assertThat("Two signatures of the same message should be the same",
				signature1,
				equalTo(signature2)
			);
		}
	}

	@Test
	public void asymSerializeDeserializePrivKey()
		throws CryptoEncryptionException, CryptoKeyDeserializationException,
		CryptoDecryptionException {
		// keygen
		KeyPair xkp = CryptoWrapper.generateKeypair();
		PrivateKey xpriv = xkp.getPrivate();
		PublicKey xpub = xkp.getPublic();
		String message = "Hallo Test String";
		// encrypt with pubkey
		byte[] xciph = CryptoWrapper.encrypt(message, xpub);

		// serialize privkey
		byte[] xprivbyte = CryptoWrapper.serializePrivKey(xpriv);

		// deserialize privkey
		PrivateKey ypriv = CryptoWrapper.deserializePrivKey(xprivbyte);

		// try decryption with deserialized privkey
		String yclear = CryptoWrapper.decryptToString(xciph, ypriv);
		System.out.println("YCLEAR: " + yclear);
		assertThat("Decryption works with deserialized PrivKey", message, equalTo(yclear));
	}

	@Test
	public void asymSerializeDeserializeDerivedPrivKey()
		throws CryptoDecryptionException, CryptoKeyDeserializationException,
		CryptoEncryptionException {
		// keygen
		KeyPair okp = CryptoWrapper.generateKeypair();
		PrivateKey opriv = okp.getPrivate();
		KeyPair dkp = CryptoWrapper.deriveCheckInKeypair(opriv, LocalDate.of(2021, 11, 17), 0);
		PublicKey xpub = dkp.getPublic();
		PrivateKey xpriv = dkp.getPrivate();

		String message = "Hallo Test String";
		// encrypt with pubkey
		byte[] xciph = CryptoWrapper.encrypt(message, xpub);

		// serialize privkey
		byte[] xprivbyte = CryptoWrapper.serializePrivKey(xpriv);

		// deserialize privkey
		PrivateKey ypriv = CryptoWrapper.deserializePrivKey(xprivbyte);

		// try decryption with deserialized privkey
		String yclear = CryptoWrapper.decryptToString(xciph, ypriv);
		System.out.println("YCLEAR: " + yclear);
		assertThat("Decryption works with deserialized derived PrivKey", message, equalTo(yclear));
	}

	@Test
	public void asymSerializeDeserializePubKey()
		throws CryptoKeyDeserializationException, CryptoEncryptionException,
		CryptoDecryptionException {
		// keygen
		KeyPair xkp = CryptoWrapper.generateKeypair();
		PrivateKey xpriv = xkp.getPrivate();
		PublicKey xpub = xkp.getPublic();

		System.out.println("PUB:  ALGO: '"
			+ xpub.getAlgorithm()
			+ "' FORMAT: '"
			+ xpub.getFormat()
			+ "'");
		System.out.println("PRIV: ALGO: '"
			+ xpriv.getAlgorithm()
			+ "' FORMAT: '"
			+ xpriv.getFormat()
			+ "'");

		String message = "Hallo PubKey!";
		// serialize pubkey
		byte[] xpubbytes = CryptoWrapper.serializePubKey(xpub);

		// deserialize pubkey
		PublicKey ypub = CryptoWrapper.deserializePubKey(xpubbytes);

		// encrypt with deserialized pubkey
		byte[] xciph = CryptoWrapper.encrypt(message, ypub);

		// decrypt with privkey
		String yclear = CryptoWrapper.decryptToString(xciph, xpriv);
		System.out.println("YCLEAR: " + yclear);
		assertThat("Encryption works with deserialized PubKey", message, equalTo(yclear));
	}

	@Test
	public void asymSerializeDeserializeDerivedPubKey()
		throws CryptoKeyDeserializationException, CryptoEncryptionException,
		CryptoDecryptionException {
		// keygen
		KeyPair okp = CryptoWrapper.generateKeypair();
		PrivateKey opriv = okp.getPrivate();
		KeyPair dkp = CryptoWrapper.deriveCheckInKeypair(opriv, LocalDate.of(2021, 11, 17), 0);
		PublicKey xpub = dkp.getPublic();
		PrivateKey xpriv = dkp.getPrivate();

		System.out.println("PUB:  ALGO: '"
			+ xpub.getAlgorithm()
			+ "' FORMAT: '"
			+ xpub.getFormat()
			+ "'");
		System.out.println("PRIV: ALGO: '"
			+ xpriv.getAlgorithm()
			+ "' FORMAT: '"
			+ xpriv.getFormat()
			+ "'");

		String message = "Hallo PubKey!";
		// serialize pubkey
		byte[] xpubbytes = CryptoWrapper.serializePubKey(xpub);

		// deserialize pubkey
		PublicKey ypub = CryptoWrapper.deserializePubKey(xpubbytes);

		// encrypt with deserialized pubkey
		byte[] xciph = CryptoWrapper.encrypt(message, ypub);

		// decrypt with privkey
		String yclear = CryptoWrapper.decryptToString(xciph, xpriv);
		System.out.println("YCLEAR: " + yclear);
		assertThat("Encryption works with deserialized PubKey", message, equalTo(yclear));
	}

	@Test
	public void asymDerivedCheckInKeysDiffer() {
		KeyPair masterKeyPair = CryptoWrapper.generateKeypair();
		PublicKey masterPubKey = masterKeyPair.getPublic();
		PrivateKey masterPrivKey = masterKeyPair.getPrivate();

		KeyPair checkInKeyPair0 =
			CryptoWrapper.deriveCheckInKeypair(masterPrivKey, LocalDate.of(2021, 11, 14), 0);
		PublicKey checkInKeyPair0PubKey = checkInKeyPair0.getPublic();
		PrivateKey checkInKeyPair0PrivKey = checkInKeyPair0.getPrivate();

		KeyPair checkInKeyPair1 =
			CryptoWrapper.deriveCheckInKeypair(masterPrivKey, LocalDate.of(2021, 11, 14), 1);
		PublicKey checkInKeyPair1PubKey = checkInKeyPair1.getPublic();
		PrivateKey checkInKeyPair1PrivKey = checkInKeyPair1.getPrivate();

		System.out.println("masterPubKey:        " + Hex.toHexString(CryptoWrapper.serializePubKey(
			masterPubKey)));
		System.out.println("checkInKeyPair0PubKey: "
			+ Hex.toHexString(CryptoWrapper.serializePubKey(checkInKeyPair0PubKey)));
		System.out.println("checkInKeyPair1PubKey: "
			+ Hex.toHexString(CryptoWrapper.serializePubKey(checkInKeyPair1PubKey)));
		System.out.println("");
		System.out.println("masterPrivKey:        "
			+ Hex.toHexString(CryptoWrapper.serializePrivKey(masterPrivKey)));
		System.out.println("checkInKeyPair0PrivKey: "
			+ Hex.toHexString(CryptoWrapper.serializePrivKey(checkInKeyPair0PrivKey)));
		System.out.println("checkInKeyPair1PrivKey: "
			+ Hex.toHexString(CryptoWrapper.serializePrivKey(checkInKeyPair1PrivKey)));

		assertThat("Derived PubKey differs from MasterPubKey",
			Hex.toHexString(CryptoWrapper.serializePubKey(masterPubKey)),
			not(equalTo(Hex.toHexString(CryptoWrapper.serializePubKey(checkInKeyPair0PubKey))))
		);

		assertThat("Derived PrivKey differs from MasterPrivKey",
			Hex.toHexString(CryptoWrapper.serializePrivKey(masterPrivKey)),
			not(equalTo(Hex.toHexString(CryptoWrapper.serializePrivKey(checkInKeyPair0PrivKey))))
		);

		assertThat("Derived PubKeys differ",
			Hex.toHexString(CryptoWrapper.serializePubKey(checkInKeyPair0PubKey)),
			not(equalTo(Hex.toHexString(CryptoWrapper.serializePubKey(checkInKeyPair1PubKey))))
		);

		assertThat("Derived PrivKeys differ",
			Hex.toHexString(CryptoWrapper.serializePrivKey(checkInKeyPair0PrivKey)),
			not(equalTo(Hex.toHexString(CryptoWrapper.serializePrivKey(checkInKeyPair1PrivKey))))
		);
	}

	@Test
	public void asymDerivedPreGeneratedKeysDiffer() {
		KeyPair masterKeyPair = CryptoWrapper.generateKeypair();
		PublicKey masterPubKey = masterKeyPair.getPublic();
		PrivateKey masterPrivKey = masterKeyPair.getPrivate();

		KeyPair preGeneratedKeyPair0 = CryptoWrapper.derivePreGeneratedKeypair(masterPrivKey,
			LocalDateTime.of(2021, 10, 14, 11, 12, 13),
			LocalDateTime.of(2021, 10, 15, 11, 12, 13)
		);
		PublicKey preGeneratedKeyPair0PubKey = preGeneratedKeyPair0.getPublic();
		PrivateKey preGeneratedKeyPair0PrivKey = preGeneratedKeyPair0.getPrivate();

		KeyPair preGeneratedKeyPair1 = CryptoWrapper.derivePreGeneratedKeypair(masterPrivKey,
			LocalDateTime.of(2021, 10, 21, 11, 12, 13),
			LocalDateTime.of(2021, 10, 22, 11, 12, 13)
		);
		PublicKey preGeneratedKeyPair1PubKey = preGeneratedKeyPair1.getPublic();
		PrivateKey preGeneratedKeyPair1PrivKey = preGeneratedKeyPair1.getPrivate();

		System.out.println("masterPubKey:         " + Hex.toHexString(CryptoWrapper.serializePubKey(
			masterPubKey)));
		System.out.println("preGeneratedKeyPair0PubKey: "
			+ Hex.toHexString(CryptoWrapper.serializePubKey(preGeneratedKeyPair0PubKey)));
		System.out.println("preGeneratedKeyPair1PubKey: "
			+ Hex.toHexString(CryptoWrapper.serializePubKey(preGeneratedKeyPair1PubKey)));
		System.out.println("");
		System.out.println("masterPrivKey:         "
			+ Hex.toHexString(CryptoWrapper.serializePrivKey(masterPrivKey)));
		System.out.println("preGeneratedKeyPair0PrivKey: "
			+ Hex.toHexString(CryptoWrapper.serializePrivKey(preGeneratedKeyPair0PrivKey)));
		System.out.println("preGeneratedKeyPair1PrivKey: "
			+ Hex.toHexString(CryptoWrapper.serializePrivKey(preGeneratedKeyPair1PrivKey)));

		assertThat("Derived PubKey differs from MasterPubKey",
			Hex.toHexString(CryptoWrapper.serializePubKey(masterPubKey)),
			not(equalTo(Hex.toHexString(CryptoWrapper.serializePubKey(preGeneratedKeyPair0PubKey))))
		);

		assertThat("Derived PrivKey differs from MasterPrivKey",
			Hex.toHexString(CryptoWrapper.serializePrivKey(masterPrivKey)),
			not(equalTo(Hex.toHexString(CryptoWrapper.serializePrivKey(preGeneratedKeyPair0PrivKey))))
		);

		assertThat("Derived PubKeys differ",
			Hex.toHexString(CryptoWrapper.serializePubKey(preGeneratedKeyPair0PubKey)),
			not(equalTo(Hex.toHexString(CryptoWrapper.serializePubKey(preGeneratedKeyPair1PubKey))))
		);

		assertThat("Derived PrivKeys differ",
			Hex.toHexString(CryptoWrapper.serializePrivKey(preGeneratedKeyPair0PrivKey)),
			not(equalTo(Hex.toHexString(CryptoWrapper.serializePrivKey(preGeneratedKeyPair1PrivKey))))
		);
	}

	@Test
	public void asymDerivedKeysPseudoRandom() {
		KeyPair masterKeyPair = CryptoWrapper.generateKeypair();
		PrivateKey masterPrivKey = masterKeyPair.getPrivate();

		// checkIn KEYPAIR
		KeyPair checkInKeyPair0 =
			CryptoWrapper.deriveCheckInKeypair(masterPrivKey, LocalDate.of(2021, 10, 31), 0);
		PublicKey checkInKeyPair0PubKey = checkInKeyPair0.getPublic();
		PrivateKey checkInKeyPair0PrivKey = checkInKeyPair0.getPrivate();

		KeyPair checkInKeyPair1 =
			CryptoWrapper.deriveCheckInKeypair(masterPrivKey, LocalDate.of(2021, 10, 31), 0);
		PublicKey checkInKeyPair1PubKey = checkInKeyPair1.getPublic();
		PrivateKey checkInKeyPair1PrivKey = checkInKeyPair1.getPrivate();

		assertThat("Derived checkIn PubKeys are the same",
			Hex.toHexString(CryptoWrapper.serializePubKey(checkInKeyPair0PubKey)),
			(equalTo(Hex.toHexString(CryptoWrapper.serializePubKey(checkInKeyPair1PubKey))))
		);

		assertThat("Derived checkIn PrivKeys are the same",
			Hex.toHexString(CryptoWrapper.serializePrivKey(checkInKeyPair0PrivKey)),
			(equalTo(Hex.toHexString(CryptoWrapper.serializePrivKey(checkInKeyPair1PrivKey))))
		);

		// PREGENERATED KEYPAIR
		KeyPair preGeneratedKeyPair0 = CryptoWrapper.derivePreGeneratedKeypair(masterPrivKey,
			LocalDateTime.of(2021, 10, 30, 11, 12, 13),
			LocalDateTime.of(2021, 10, 31, 11, 12, 13)
		);
		PublicKey preGeneratedKeyPair0PubKey = preGeneratedKeyPair0.getPublic();
		PrivateKey preGeneratedKeyPair0PrivKey = preGeneratedKeyPair0.getPrivate();

		KeyPair preGeneratedKeyPair1 = CryptoWrapper.derivePreGeneratedKeypair(masterPrivKey,
			LocalDateTime.of(2021, 10, 30, 11, 12, 13),
			LocalDateTime.of(2021, 10, 31, 11, 12, 13)
		);
		PublicKey preGeneratedKeyPair1PubKey = preGeneratedKeyPair1.getPublic();
		PrivateKey preGeneratedKeyPair1PrivKey = preGeneratedKeyPair1.getPrivate();

		assertThat("Derived preGenerated PubKeys are the same",
			Hex.toHexString(CryptoWrapper.serializePubKey(preGeneratedKeyPair0PubKey)),
			(equalTo(Hex.toHexString(CryptoWrapper.serializePubKey(preGeneratedKeyPair1PubKey))))
		);

		assertThat("Derived preGenerated PrivKeys are the same",
			Hex.toHexString(CryptoWrapper.serializePrivKey(preGeneratedKeyPair0PrivKey)),
			(equalTo(Hex.toHexString(CryptoWrapper.serializePrivKey(preGeneratedKeyPair1PrivKey))))
		);
	}

	@Test
	public void asymVerifySerializedKeySizes()
		throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException {
		// Key Size Explanation: https://stackoverflow.com/a/6687080
		KeyPair kp = CryptoWrapper.generateKeypair();

		KeyFactory keyFactory =
			KeyFactory.getInstance(CryptoWrapper.KEYPAIR_ALGORITHM,
				CryptoWrapper.KEYPAIR_PROVIDER);
		ECPublicKeySpec kspec = keyFactory.getKeySpec(kp.getPublic(), ECPublicKeySpec.class);
		int fieldSize = (kspec.getParams().getCurve().getFieldSize() + 7)
			/ 8; // integer math trick to convert bits to full bytes

		assertThat("Serialized Public Key (compressed) has fieldSize+1 bytes",
			CryptoWrapper.serializePubKey(kp.getPublic()).length,
			equalTo(fieldSize + 1)
		);

		assertThat("Serialized Key has length of fieldSize",
			CryptoWrapper.serializePrivKey(kp.getPrivate()).length,
			equalTo(fieldSize)
		);
	}

	@Test
	public void asymDerivedKeysStable() throws CryptoKeyDeserializationException {

		//Generate new Keys like this in case crypto parameters change:
		KeyPair keyPair = CryptoWrapper.generateKeypair();
		PublicKey pubKey = keyPair.getPublic();
		PrivateKey privKey = keyPair.getPrivate();
		byte[] sPubKey = CryptoWrapper.serializePubKey(pubKey);
		byte[] sPrivKey = CryptoWrapper.serializePrivKey(privKey);
		System.out.println("PubKey: "
			+ Hex.toHexString(sPubKey)
			+ "  ("
			+ sPubKey.length
			+ " bytes)");
		System.out.println("PrivKey: "
			+ Hex.toHexString(sPrivKey)
			+ "   ("
			+ sPrivKey.length
			+ " bytes)");
		// Key Size Explanation: https://stackoverflow.com/a/6687080

		byte[] masterPubKeyRaw =
			Hex.decode("03bc2c5cf2d2b99cecd6e028f6a715bb0cfee5d2fa12f018aaa6ef0a38c586b119");
		PublicKey masterPubKey = CryptoWrapper.deserializePubKey(masterPubKeyRaw);

		byte[] masterPrivKeyRaw =
			Hex.decode("260958532acdf0218e20714ae6518fc5e98d63756530682ae54bc2af29b23c23");
		PrivateKey masterPrivKey = CryptoWrapper.deserializePrivKey(masterPrivKeyRaw);

		KeyPair checkInKeyPair1 =
			CryptoWrapper.deriveCheckInKeypair(masterPrivKey, LocalDate.of(2021, 7, 21), 0);
		PublicKey checkInKeyPair1PubKey = checkInKeyPair1.getPublic();
		PrivateKey checkInKeyPair1PrivKey = checkInKeyPair1.getPrivate();

		KeyPair preGeneratedKeyPair1 = CryptoWrapper.derivePreGeneratedKeypair(masterPrivKey,
			LocalDateTime.of(2021, 3, 10, 11, 12, 13),
			LocalDateTime.of(2021, 3, 11, 11, 12, 13)
		);
		PublicKey preGeneratedKeyPair1PubKey = preGeneratedKeyPair1.getPublic();
		PrivateKey preGeneratedKeyPair1PrivKey = preGeneratedKeyPair1.getPrivate();

		System.out.println("masterPubKey:    " + Hex.toHexString(CryptoWrapper.serializePubKey(
			masterPubKey)));
		System.out.println("checkInKeyPair1PubKey:        "
			+ Hex.toHexString(CryptoWrapper.serializePubKey(checkInKeyPair1PubKey)));
		System.out.println("preGeneratedKeyPair1PubKey: "
			+ Hex.toHexString(CryptoWrapper.serializePubKey(preGeneratedKeyPair1PubKey)));
		System.out.println("");
		System.out.println("masterPrivKey:         "
			+ Hex.toHexString(CryptoWrapper.serializePrivKey(masterPrivKey)));
		System.out.println("checkInKeyPair1PrivKey:  "
			+ Hex.toHexString(CryptoWrapper.serializePrivKey(checkInKeyPair1PrivKey)));
		System.out.println("preGeneratedKeyPair1PrivKey: "
			+ Hex.toHexString(CryptoWrapper.serializePrivKey(preGeneratedKeyPair1PrivKey)));

		assertThat("Derived CheckIn Public Key is stable deterministic",
			Hex.toHexString(CryptoWrapper.serializePubKey(checkInKeyPair1PubKey)),
			equalTo("03ab2a62f3204c485cc213ea39ec3eb01d4b084c04911e616ae600f89834a48c58")
		);

		assertThat("Derived CheckIn Private Key is stable deterministic",
			Hex.toHexString(CryptoWrapper.serializePrivKey(checkInKeyPair1PrivKey)),
			equalTo("9fc16d5c572a6194141aa0c805e63341a67613ea7305194533260dd3b14ad784")
		);

		assertThat("Derived PreGenerated Public Key is stable deterministic",
			Hex.toHexString(CryptoWrapper.serializePubKey(preGeneratedKeyPair1PubKey)),
			equalTo("03226be41aa88caaef0428b8ec440e74154da52ea3161e27c3196aa4d5ddd1bc0f")
		);

		assertThat("Derived PreGenerated Private Key is stable deterministic",
			Hex.toHexString(CryptoWrapper.serializePrivKey(preGeneratedKeyPair1PrivKey)),
			equalTo("3335996572de1d37c377382a53849978592aaa249521c3e8ccfef222e03418aa")
		);
	}

	@Test
	public void asymCurveSeedisNull() {
		// null = not available
		ECNamedCurveParameterSpec ecp =
			ECNamedCurveTable.getParameterSpec(CryptoWrapper.KEYPAIR_CURVENAME);
		ECDomainParameters domainParams = new ECDomainParameters(ecp.getCurve(),
			ecp.getG(),
			ecp.getN(),
			ecp.getH(),
			ecp.getSeed()
		);
		//System.out.println(Hex.toHexString(ecp.getSeed()));
		assertThat(ecp.getSeed(), equalTo(null));
	}

	@Test
	public void hashTest() {
		String msg = "Hallo Test 123";
		byte[] h1 = CryptoWrapper.hash(msg);
		byte[] h2 = CryptoWrapper.hash(msg);
		System.out.println("Hash: " + Hex.toHexString(h1));
		assertThat("Hashing twice, same result", h1, equalTo(h2));
	}

	@Test
	public void symEnryptDecrypt() throws CryptoEncryptionException, CryptoDecryptionException,
		CryptoDataDeserializationException {
		byte[] symKey = CryptoWrapper.generateSymKey();
		System.out.println(Hex.toHexString(symKey));
		String message1 =
			"Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt "
				+ "Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! "
				+ "Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo "
				+ "Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt "
				+ "Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! "
				+ "Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo "
				+ "Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt "
				+ "Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! "
				+ "Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo "
				+ "Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt "
				+ "Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! "
				+ "Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo "
				+ "Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt "
				+ "Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! "
				+ "Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo "
				+ "Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt "
				+ "Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! "
				+ "Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo "
				+ "Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt "
				+ "Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! "
				+ "Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo "
				+ "Welt Hier!! ";
		byte[] sym_ciphertxt = CryptoWrapper.encryptSymmetric(message1, symKey);
		String sym_plaintext = CryptoWrapper.decryptSymmetricToString(sym_ciphertxt, symKey);
		System.out.println("DECRYPTED: " + sym_plaintext);
		assertThat(sym_plaintext, equalTo(message1));
	}

	@Test
	public void symCiphertextVariance() throws CryptoEncryptionException {
		byte[] symKey = CryptoWrapper.generateSymKey();
		String message1 =
			"Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt "
				+ "Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! "
				+ "Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo "
				+ "Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt "
				+ "Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! "
				+ "Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo "
				+ "Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt "
				+ "Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! "
				+ "Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo "
				+ "Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt "
				+ "Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! "
				+ "Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo "
				+ "Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt "
				+ "Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! "
				+ "Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo "
				+ "Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt "
				+ "Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! "
				+ "Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo "
				+ "Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt "
				+ "Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! "
				+ "Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo Welt Hier!! Hallo "
				+ "Welt Hier!! ";
		String hex_ciphertxt1 = Hex.toHexString(CryptoWrapper.encryptSymmetric(message1, symKey));
		String hex_ciphertxt2 = Hex.toHexString(CryptoWrapper.encryptSymmetric(message1, symKey));
		System.out.println(hex_ciphertxt1);
		System.out.println(hex_ciphertxt2);
		assertThat("Ciphertexts are different for the same message/key combination",
			hex_ciphertxt1,
			not(equalTo(hex_ciphertxt2))
		);
	}

	@Test
	public void calculatePubKeyFromPrivKeyTest() {
		KeyPair keyPair = CryptoWrapper.generateKeypair();

		byte[] serPubKey = CryptoWrapper.serializePubKey(keyPair.getPublic());

		PublicKey calcPubKey = CryptoWrapper.calculatePubKeyFromPrivKey(keyPair.getPrivate());
		byte[] serPubKey2 = CryptoWrapper.serializePubKey(calcPubKey);

		assertThat(Arrays.equals(serPubKey, serPubKey2), equalTo(true));
	}
}
