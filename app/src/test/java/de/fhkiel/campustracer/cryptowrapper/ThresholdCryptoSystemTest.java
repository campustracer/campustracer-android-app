package de.fhkiel.campustracer.cryptowrapper;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.junit.MatcherAssert.assertThat;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.BeforeClass;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.Security;
import java.util.Arrays;
import java.util.Optional;


public class ThresholdCryptoSystemTest {
	@BeforeClass
	public static void setUp() {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Test
	public void encryptDecryptTest() throws Exception {
		KeyPair tp1 = CryptoWrapper.generateKeypair();
		KeyPair tp2 = CryptoWrapper.generateKeypair();
		KeyPair tp3 = CryptoWrapper.generateKeypair();
		String secret = "THIS IS VERY VERY SECRET!";

		byte[] encData = secret.getBytes(StandardCharsets.UTF_8);
		ThresholdCryptoSystem tc1 = new ThresholdCryptoSystem(encData,
			Arrays.asList(tp1.getPublic(), tp2.getPublic(), tp3.getPublic()),
			3
		);

		ThresholdCryptoSystem tc2 = ThresholdCryptoSystem.fromBytes(tc1.toBytes());
		tc2.decryptPartial(tp1.getPublic(), tp1.getPrivate());
		tc2.decryptPartial(tp3.getPublic(), tp3.getPrivate());

		assertThat(tc2.isDecryptable(), equalTo(false));
		Optional<byte[]> decFail = tc2.decryptAll();
		assertThat(decFail.isPresent(), equalTo(false));

		tc2.decryptPartial(tp2.getPublic(), tp2.getPrivate());

		assertThat(tc2.isDecryptable(), equalTo(true));
		Optional<byte[]> decryptOK = tc2.decryptAll();
		assertThat(decryptOK.isPresent(), equalTo(true));
		assertThat(new String(decryptOK.get(), StandardCharsets.UTF_8), equalTo(secret));
	}
}
