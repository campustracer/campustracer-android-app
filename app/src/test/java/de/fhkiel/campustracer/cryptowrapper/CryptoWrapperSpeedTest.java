package de.fhkiel.campustracer.cryptowrapper;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.Ignore;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Security;
import java.time.LocalDate;

import de.fhkiel.campustracer.exceptions.CryptoDecryptionException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;


@Ignore
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CryptoWrapperSpeedTest {

	private static final int ITERATION_COUNT = 100000;
	private static final int PLAYLOAD_SIZE = 508;

	private byte[] PAYLOAD = new byte[PLAYLOAD_SIZE];

	@Before
	void setUpA() {
		Security.addProvider(new BouncyCastleProvider());
		SecureRandom rnd = new SecureRandom();

		rnd.nextBytes(this.PAYLOAD);
	}

	//@Test
	void speedtestEncryption() throws CryptoEncryptionException {
		KeyPair kpValid = CryptoWrapper.generateKeypair();
		long start = System.currentTimeMillis();
		for (int i = 0; i < ITERATION_COUNT; i++) {
			byte[] message = CryptoWrapper.encrypt(this.PAYLOAD, kpValid.getPublic());
		}
		long end = System.currentTimeMillis();
		System.out.println(ITERATION_COUNT + " Encryptions in " + (end - start) + "ms (" + ((end
			- start) / (float) ITERATION_COUNT) + "ms/Enc)");
	}

	//@Test
	void speedtestOKDecryption() throws CryptoEncryptionException, CryptoDecryptionException {
		KeyPair kpValid = CryptoWrapper.generateKeypair();
		byte[] message = CryptoWrapper.encrypt(this.PAYLOAD, kpValid.getPublic());

		long start = System.currentTimeMillis();
		for (int i = 0; i < ITERATION_COUNT; i++) {
			CryptoWrapper.decrypt(message, kpValid.getPrivate());
		}
		long end = System.currentTimeMillis();
		System.out.println(ITERATION_COUNT + " failed Decryptions in " + (end - start) + "ms (" + ((
			end
				- start) / (float) ITERATION_COUNT) + "ms/Decryption)");
	}

	//@Test
	void speedtestFailDecryption() throws CryptoEncryptionException {
		KeyPair kpValid = CryptoWrapper.generateKeypair();
		KeyPair kpInvalid = CryptoWrapper.generateKeypair();
		byte[] message = CryptoWrapper.encrypt("Hallo dies ist eine Message!",
			kpValid.getPublic());

		long start = System.currentTimeMillis();
		for (int i = 0; i < ITERATION_COUNT; i++) {
			try {
				CryptoWrapper.decrypt(message, kpInvalid.getPrivate());
			} catch (Exception e) {

			}
		}
		long end = System.currentTimeMillis();
		System.out.println(ITERATION_COUNT + " failed Decryptions in " + (end - start) + "ms (" + ((
			end
				- start) / (float) ITERATION_COUNT) + "ms/Decryption)");
	}

	//@Test
	void speedtestKeyGen() {
		KeyPair keypair;

		long start = System.currentTimeMillis();
		for (int i = 0; i < ITERATION_COUNT; i++) {
			keypair = CryptoWrapper.generateKeypair();
		}
		long end = System.currentTimeMillis();
		System.out.println(ITERATION_COUNT + " KeyPairs in " + (end - start) + "ms (" + ((end
			- start) / (float) ITERATION_COUNT) + "ms/KP)");
	}

	//@Test
	void speedtestDerivedKeyGen() {
		KeyPair keypair0 = CryptoWrapper.generateKeypair();
		PrivateKey kp0priv = keypair0.getPrivate();
		LocalDate date = LocalDate.of(2021, 10, 10);
		int nonce = 1;
		KeyPair kp1;

		long start = System.currentTimeMillis();
		for (int i = 0; i < ITERATION_COUNT; i++) {
			kp1 = CryptoWrapper.deriveCheckInKeypair(kp0priv, date, nonce);
		}
		long end = System.currentTimeMillis();
		System.out.println(ITERATION_COUNT + " derived KeyPairs in " + (end - start) + "ms (" + ((
			end
				- start) / (float) ITERATION_COUNT) + "ms/dKP)");
	}

	//@Test
	void speedtestSign() {
		KeyPair kpValid = CryptoWrapper.generateKeypair();

		long start = System.currentTimeMillis();
		for (int i = 0; i < ITERATION_COUNT; i++) {
			byte[] signature = CryptoWrapper.sign(this.PAYLOAD, kpValid.getPrivate());
		}
		long end = System.currentTimeMillis();
		System.out.println(ITERATION_COUNT + " Signatures in " + (end - start) + "ms (" + ((end
			- start) / (float) ITERATION_COUNT) + "ms/Sig)");
	}

	//@Test
	void speedtestVerifyOK() {
		byte[] b = this.PAYLOAD;
		KeyPair kpValid = CryptoWrapper.generateKeypair();
		byte[] signature = CryptoWrapper.sign(b, kpValid.getPrivate());

		long start = System.currentTimeMillis();
		for (int i = 0; i < ITERATION_COUNT; i++) {
			Boolean isOk = CryptoWrapper.verify(b, signature, kpValid.getPublic());
		}
		long end = System.currentTimeMillis();
		System.out.println(ITERATION_COUNT + " Verifications in " + (end - start) + "ms (" + ((end
			- start) / (float) ITERATION_COUNT) + "ms/Ver)");
	}

	//@Test
	void speedtestVerifyFail() {
		byte[] b = this.PAYLOAD;
		KeyPair kpValid = CryptoWrapper.generateKeypair();
		KeyPair kpInvalid = CryptoWrapper.generateKeypair();
		byte[] signature = CryptoWrapper.sign(b, kpValid.getPrivate());

		long start = System.currentTimeMillis();
		for (int i = 0; i < ITERATION_COUNT; i++) {
			Boolean isOk = CryptoWrapper.verify(b, signature, kpInvalid.getPublic());
		}
		long end = System.currentTimeMillis();
		System.out.println(ITERATION_COUNT
			+ " failed Verifications in "
			+ (end - start)
			+ "ms ("
			+ ((end - start) / (float) ITERATION_COUNT)
			+ "ms/fVer)");
	}
}
