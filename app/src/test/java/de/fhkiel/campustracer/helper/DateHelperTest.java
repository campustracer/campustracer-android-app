package de.fhkiel.campustracer.helper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThrows;

import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class DateHelperTest {
	@Test
	public void ISODateConversionTest() {
		LocalDate t = LocalDate.of(2021, 4, 5);
		assertThat(DateHelper.getISODateString(t), equalTo("2021-04-05"));
		assertThat(t,
			equalTo(DateHelper.getLocalDateFromISODateString(DateHelper.getISODateString(t)))
		);
	}

	@Test
	public void ISODateTimeConversionTest() {
		LocalDateTime t = LocalDateTime.of(2021, 4, 5, 11, 12, 13);
		assertThat(DateHelper.getISODateTimeString(t), equalTo("2021-04-05T11:12:13"));
		System.out.println(DateHelper.getISODateTimeString(t));
		assertThat(t,
			equalTo(DateHelper.getLocalDateTimeFromISODateTimeString(DateHelper.getISODateTimeString(
				t)))
		);
	}

	@Test
	public void castLocalDateToLocalDateTime() {
		LocalDate d = LocalDate.of(2021, 4, 5);
		LocalDateTime morning = DateHelper.castLocalDateToLocalDateTime(d, true);
		LocalDateTime evening = DateHelper.castLocalDateToLocalDateTime(d, false);
		assertThat(morning, equalTo(LocalDateTime.of(2021, 4, 5, 0, 0, 0)));
		assertThat(evening, equalTo(LocalDateTime.of(2021, 4, 5, 23, 59, 59)));
		assertThat(LocalDate.from(morning), equalTo(d));
		assertThat(LocalDate.from(evening), equalTo(d));
	}

	@Test
	public void getDatesListFromDateRangeTest() {
		LocalDate start = LocalDate.of(2021, 11, 10);
		LocalDate end = LocalDate.of(2021, 11, 13);
		List<LocalDate> referenceList = Arrays.asList(LocalDate.of(2021, 11, 10),
			LocalDate.of(2021, 11, 11),
			LocalDate.of(2021, 11, 12),
			LocalDate.of(2021, 11, 13)
		);

		List<LocalDate> dtlist = DateHelper.getDatesListFromDateRange(start, end);
		assertThat(dtlist.size(), equalTo(4));
		System.out.println(dtlist.stream()
			.map(x -> DateHelper.getISODateString(x))
			.collect(Collectors.joining(", ")));

		for (int i = 0; i < dtlist.size(); i++) {
			assertThat(dtlist.get(i).isEqual(referenceList.get(i)), equalTo(true));
		}

		assertThrows(IllegalArgumentException.class,
			() -> DateHelper.getDatesListFromDateRange(end, start)
		);

		List<LocalDate> dtlist2 = DateHelper.getDatesListFromDateRange(start, start);
		assertThat(dtlist2.size(), equalTo(1));
		assertThat(dtlist2.get(0).isEqual(start), equalTo(true));
	}

	@Test
	public void isBeforeOrEqualTest() {
		LocalDateTime a1 = LocalDateTime.of(2021, 11, 12, 13, 14, 15);
		LocalDate b1 = LocalDate.of(2021, 11, 12); // same day
		LocalDate b2 = LocalDate.of(2021, 11, 11); // a isAfter b
		LocalDate b3 = LocalDate.of(2021, 11, 13); // a isBefore b

		assertThat("Same Day", DateHelper.isBeforeOrEqual(a1, b1), equalTo(true));
		assertThat("Same Day", DateHelper.isBeforeOrEqual(b1, a1), equalTo(true));

		assertThat("a not isBefore b2", DateHelper.isBeforeOrEqual(a1, b2), equalTo(false));

		assertThat("a isBefore b3", DateHelper.isBeforeOrEqual(a1, b3), equalTo(true));

		assertThat("b2 not isBefore a", DateHelper.isBeforeOrEqual(b2, a1), equalTo(true));

		assertThat("b3 not isBefore a", DateHelper.isBeforeOrEqual(b3, a1), equalTo(false));
	}

	@Test
	public void isAfterOrEqualTest() {
		LocalDateTime a1 = LocalDateTime.of(2021, 11, 12, 13, 14, 15);
		LocalDate b1 = LocalDate.of(2021, 11, 12); // same day
		LocalDate b2 = LocalDate.of(2021, 11, 11); // a isAfter b
		LocalDate b3 = LocalDate.of(2021, 11, 13); // a isBefore b

		assertThat("Same Day", DateHelper.isAfterOrEqual(a1, b1), equalTo(true));

		assertThat("Same Day", DateHelper.isAfterOrEqual(b1, a1), equalTo(true));

		assertThat("a isAfter b2", DateHelper.isAfterOrEqual(a1, b2), equalTo(true));

		assertThat("a3 not isAfter b", DateHelper.isAfterOrEqual(a1, b3), equalTo(false));

		assertThat("b2 not isAfter a", DateHelper.isAfterOrEqual(b2, a1), equalTo(false));

		assertThat("b3 isAfter a", DateHelper.isAfterOrEqual(b3, a1), equalTo(true));
	}
}
