package de.fhkiel.campustracer.helper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThrows;

import org.hamcrest.Matchers;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;


public class BytesPackageTest {
	@Test
	public void bytesPackUnpack() throws CryptoDataDeserializationException {

		byte[] a = {0, 1, 2};
		byte[] b = {3, 4, 5, 6};
		byte[] c = {7, 8, 9, 10, 11};
		BytesPackage ba = new BytesPackage(a, b, c);
		byte[] ba_raw = ba.toBytes();
		this.dump(ba_raw);

		BytesPackage ba2 = BytesPackage.fromRaw(ba_raw);

		this.dump(ba2.get(0));
		this.dump(ba2.get(1));
		this.dump(ba2.get(2));

		assertThat(ba2.getList().size(), equalTo(3));
		assertThat(ba2.get(0), equalTo(a));
		assertThat(ba2.get(1), equalTo(b));
		assertThat(ba2.get(2), equalTo(c));
	}

	@Test
	public void bytesPackUnpackOnlyOne() throws CryptoDataDeserializationException {

		byte[] a = {0, 1, 2};
		BytesPackage ba = new BytesPackage(a);
		byte[] ba_raw = ba.toBytes();
		this.dump(ba_raw);
		BytesPackage ba2 = BytesPackage.fromRaw(ba_raw);
		this.dump(ba2.get(0));
		assertThat(ba2.getList().size(), equalTo(1));
		assertThat(ba2.get(0), equalTo(a));
	}

	@Test
	public void bytesPackUnpackEmpty() throws CryptoDataDeserializationException {

		byte[] a = {};
		BytesPackage ba = new BytesPackage(a);
		byte[] ba_raw = ba.toBytes();
		this.dump(ba_raw);
		BytesPackage ba2 = BytesPackage.fromRaw(ba_raw);
		this.dump(ba2.get(0));
		assertThat(ba2.getList().size(), equalTo(1));
		assertThat(ba2.get(0), equalTo(new byte[]{}));
	}

	@Test
	public void bytesPackUnpackNone() {
		BytesPackage ba = new BytesPackage();
		byte[] ba_raw = ba.toBytes();
		this.dump(ba_raw);

		assertThat(ba_raw.length, equalTo(0));
	}

	@Test
	public void bytesPackNull() throws CryptoDataDeserializationException {
		byte[] a = "A".getBytes(StandardCharsets.UTF_8);
		byte[] b = "B".getBytes(StandardCharsets.UTF_8);

		byte[] nullPayload = null;

		// CASE 0: null value only
		// pack
		BytesPackage ba4 = new BytesPackage(nullPayload);
		byte[] ba4_raw = ba4.toBytes();
		this.dump(ba4_raw);
		assertThat(ba4_raw.length, equalTo(4));
		//unpack
		BytesPackage ba4u = BytesPackage.fromRaw(ba4_raw);
		assertThat(ba4u.get(0), Matchers.nullValue());

		// CASE 1: null value at the beginning
		// pack
		BytesPackage ba3 = new BytesPackage(nullPayload, a);
		byte[] ba3_raw = ba3.toBytes();
		this.dump(ba3_raw);
		assertThat(ba3_raw.length, equalTo(9));
		//unpack
		BytesPackage ba3u = BytesPackage.fromRaw(ba3_raw);
		assertThat(ba3u.get(0), Matchers.nullValue());
		assertThat(ba3u.get(1), equalTo(a));

		// CASE 2: null value in between
		// pack
		BytesPackage ba2 = new BytesPackage(a, nullPayload, b);
		byte[] ba2_raw = ba2.toBytes();
		this.dump(ba2_raw);
		assertThat(ba2_raw.length, equalTo(14));
		//unpack
		BytesPackage ba2u = BytesPackage.fromRaw(ba2_raw);
		assertThat(ba2u.get(0), equalTo(a));
		assertThat(ba2u.get(1), Matchers.nullValue());
		assertThat(ba2u.get(2), equalTo(b));

		// CASE 3: null value at the end
		BytesPackage ba1 = new BytesPackage(a, nullPayload);
		byte[] ba1_raw = ba1.toBytes();
		this.dump(ba1_raw);
		assertThat(ba1_raw.length, equalTo(9));

		BytesPackage ba1u = BytesPackage.fromRaw(ba1_raw);
		assertThat(ba1u.get(0), equalTo(a));
		assertThat(ba1u.get(1), Matchers.nullValue());
	}

	@Test
	public void bytesPackUnpackEmptyInbetween() throws CryptoDataDeserializationException {

		byte[] a = {0, 1, 2};
		byte[] b = {};
		byte[] c = {7, 8, 9, 10, 11};
		BytesPackage ba = new BytesPackage(a, b, c);
		byte[] ba_raw = ba.toBytes();
		this.dump(ba_raw);

		BytesPackage ba2 = BytesPackage.fromRaw(ba_raw);

		this.dump(ba2.get(0));
		this.dump(ba2.get(1));
		this.dump(ba2.get(2));

		assertThat(ba2.getList().size(), equalTo(3));
		assertThat(ba2.get(0), equalTo(a));
		assertThat(ba2.get(1), equalTo(b));
		assertThat(ba2.get(2), equalTo(c));
	}

	@Test
	public void bytesMalformedUnpack() throws CryptoDataDeserializationException {
		byte[] rawpack_ok = {
			0, 0, 0, 3,
			/* */
			0, 1, 2, 0, 0, 0, 4,
			/* */
			3, 4, 5, 6, 0, 0, 0, 5,
			/* */
			7, 8, 9, 10, 11
		};

		//Assertions.assertDoesNotThrow(() -> {
		BytesPackage.fromRaw(rawpack_ok);
		//});

		byte[] rawpack1 = Arrays.copyOfRange(rawpack_ok, 0, rawpack_ok.length);
		rawpack1[3] = 4; // len of first elem: 4 instead of 3 (too long)
		assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
			BytesPackage.fromRaw(rawpack1);
		});

		byte[] rawpack2 = Arrays.copyOfRange(rawpack_ok, 0, rawpack_ok.length);
		rawpack2[18] = 4; // len of last elem: 4 instead of 5 (too short at end)
		assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
			BytesPackage.fromRaw(rawpack1);
		});

		byte[] rawpack3 = Arrays.copyOfRange(rawpack_ok, 0, rawpack_ok.length);
		rawpack3[18] = 6; // len of last elem: 4 instead of 5 (too long at end)
		assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
			BytesPackage.fromRaw(rawpack1);
		});
	}

	private void dump(byte[] bytes) {
		System.out.print("DUMP: ");
		for (byte b : bytes) {
			System.out.print((b) + " ");
		}
		System.out.println("");
	}
}
