package de.fhkiel.campustracer.repository.sqlite;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import de.fhkiel.campustracer.helper.room_type_converters.LocalDateConverter;
import de.fhkiel.campustracer.helper.room_type_converters.LocalDateTimeConverter;
import de.fhkiel.campustracer.helper.room_type_converters.PrivateKeyConverter;
import de.fhkiel.campustracer.helper.room_type_converters.PublicKeyConverter;
import de.fhkiel.campustracer.helper.room_type_converters.RolesEnumConverter;
import de.fhkiel.campustracer.model.CheckInEntry;
import de.fhkiel.campustracer.model.InfectionCaseEntry;
import de.fhkiel.campustracer.model.KeystoreEntry;
import de.fhkiel.campustracer.model.LectureAttendedByUserEntry;
import de.fhkiel.campustracer.model.OptionEntry;
import de.fhkiel.campustracer.model.PreGeneratedEncryptionKeyEntry;
import de.fhkiel.campustracer.model.UserAttendedLectureEntry;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;


@Database(entities = {
	CheckInEntry.class,
	InfectionCaseEntry.class,
	KeystoreEntry.class,
	LectureAttendedByUserEntry.class,
	OptionEntry.class,
	PreGeneratedEncryptionKeyEntry.class,
	UserAttendedLectureEntry.class,
	UserEntry.class,
	UserMasterKeyEntry.class,
}, version = 1, exportSchema = false)
@TypeConverters({
	LocalDateConverter.class,
	LocalDateTimeConverter.class,
	PublicKeyConverter.class,
	PrivateKeyConverter.class,
	RolesEnumConverter.class,
})
public abstract class SqliteDatabase extends RoomDatabase {
	public abstract OptionRepository optionRepository();

	public abstract KeyRepository keyRepository();

	public abstract UserRepository userRepository();

	public abstract CheckInRepository checkInRepository();
}
