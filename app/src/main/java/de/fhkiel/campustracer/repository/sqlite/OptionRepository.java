package de.fhkiel.campustracer.repository.sqlite;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import de.fhkiel.campustracer.model.OptionEntry;
import de.fhkiel.campustracer.model.OptionsEnum;


@Dao
public interface OptionRepository {
	@Query("SELECT * FROM OptionEntry")
	List<OptionEntry> getAll();

	@Query("SELECT * FROM OptionEntry WHERE `key` = :key")
	OptionEntry getOption(OptionsEnum key);

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	void setOption(OptionEntry option);

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	void setOptions(List<OptionEntry> option);
}
