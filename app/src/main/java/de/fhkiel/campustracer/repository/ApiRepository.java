package de.fhkiel.campustracer.repository;

import java.time.LocalDateTime;
import java.util.List;

import de.fhkiel.campustracer.model.CheckInEntry;
import de.fhkiel.campustracer.model.InfectionCaseEntry;
import de.fhkiel.campustracer.model.LectureAttendedByUserEntry;
import de.fhkiel.campustracer.model.UserAttendedLectureEntry;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;
import de.fhkiel.campustracer.model.virtual.InfectionCaseCreateEntry;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiRepository {
	@GET("UserMasterKey/")
		// ?since=2021-10-20T14:10:53.123
	Call<List<UserMasterKeyEntry>> getAllUserMasterKeysAndPreGeneratedKeys(
		@Header("Authorization") String basicAuth, @Query("since") String sinceDateTime
	);

	@POST("UserMasterKey/")
	Call<UserMasterKeyEntry> addUserMasterKeyAndPreGeneratedKeys(
		@Body UserMasterKeyEntry userMasterKeyEntry, @Header("Authorization") String basicAuth
	);

	@POST("CheckIn/")
	Call<CheckInEntry> createCheckIn(@Body CheckInEntry checkInEntry);

	@GET("UserMasterKey/")
	Call<List<UserMasterKeyEntry>> getAllMasterKeyEntriesForAuthenticatedUser(
		@Header("Authorization") String basicAuth
	);

	@GET("UserMasterKey/")
		// ?userid=1234
	Call<List<UserMasterKeyEntry>> getAllMasterKeyEntriesByUserId(
		@Header("Authorization") String basicAuth, @Query("userid") Integer userId
	);

	@GET("UserMasterKey/?onlyValid=true")
		// ?userid=1234&onlyValid=true
	Call<List<UserMasterKeyEntry>> getCurrentlyValidMasterKeyEntriesByUserId(
		@Header("Authorization") String basicAuth, @Query("userid") Long userId
	);

	@GET("User/")
	Call<List<UserEntry>> getUserEntries(
		@Header("Authorization") String basicAuth, @Query("since") String sinceDateTime
	);

	@GET("User/{username}")
	Call<UserEntry> getUserEntryByName(
		@Path("username") String username, @Header("Authorization") String basicAuth
	);

	@PUT("User/{username}/")
	Call<UserEntry> putEncContactDataForUsername(
		@Path("username") String username,
		@Body RequestBody requestBodyWithEncContactData,
		@Header("Authorization") String basicAuth
	);

	// Get single Infection Case with Image
	@GET("InfectionCase/{caseId}/")
	Call<InfectionCaseEntry> getInfectionCaseEntryById(
		@Path("caseId") Long infectionCaseId, @Header("Authorization") String basicAuth
	);

	// Get Infection Cases w/o Image
	@GET("InfectionCase/")
	Call<List<InfectionCaseEntry>> getInfectionCaseEntriesSinceId(
		@Header("Authorization") String basicAuth, @Query("sinceId") Long sinceId
	);

	@POST("InfectionCase/")
	Call<InfectionCaseEntry> addInfectionCase(
		@Body InfectionCaseCreateEntry infectionCaseCreateEntry
	);

	// POST privkeyhash list to get LABU-Entries
	@POST("LectureAttendedByUser/")
	Call<List<LectureAttendedByUserEntry>> getLectureAttendedByUserEntriesByLecturePrivKeyHashes(
		@Header("Authorization") String basicAuth, @Body List<String> privKeyHashes
	);

	// POST privkeyhash list to get UAL-Entries
	@POST("UserAttendedLecture/")
	Call<List<UserAttendedLectureEntry>> getUserAttendedLectureEntriesByCheckInPrivateKeyHashes(
		@Header("Authorization") String basicAuth, @Body List<String> privKeyHashes
	);

	@GET("UserAttendedLecture/")
	Call<List<UserAttendedLectureEntry>> getUserAttendedLectureEntriesByDates(
		@Header("Authorization") String authtoken,
		@Query("from") LocalDateTime dateFrom,
		@Query("to") LocalDateTime dateTo
	);
}
