package de.fhkiel.campustracer.repository.sqlite;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.time.LocalDate;
import java.util.List;

import de.fhkiel.campustracer.model.CheckInEntry;
import de.fhkiel.campustracer.model.InfectionCaseEntry;
import de.fhkiel.campustracer.model.LectureAttendedByUserEntry;
import de.fhkiel.campustracer.model.UserAttendedLectureEntry;


@Dao
public interface CheckInRepository {
	@Insert
	long addCheckInEntry(CheckInEntry checkInEntry);

	@Query("SELECT * FROM UserAttendedLectureEntry WHERE id IN (:ids)")
	List<UserAttendedLectureEntry> getUserAttendedLectureEntriesByIds(List<byte[]> ids);

	@Query("SELECT * FROM UserAttendedLectureEntry WHERE createdAt BETWEEN"
		+ ":dateFrom AND :dateTo")
	List<UserAttendedLectureEntry> getUserAttendedLectureEntriesByDate(
		LocalDate dateFrom, LocalDate dateTo
	);

	@Query("SELECT * FROM LectureAttendedByUserEntry WHERE id IN (:ids)")
	List<LectureAttendedByUserEntry> getLectureAttendedByUserEntriesByIds(List<byte[]> ids);

	@Query("SELECT * FROM InfectionCaseEntry")
	List<InfectionCaseEntry> getAllInfectionCaseEntries();

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	long[] addNewInfectionCaseEntries(List<InfectionCaseEntry> infectionCaseEntries);

	@Delete
	void removeInfectionCaseEntry(InfectionCaseEntry infectionCaseEntry);
}
