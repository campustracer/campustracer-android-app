package de.fhkiel.campustracer.repository.sqlite;

import androidx.core.util.Pair;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import de.fhkiel.campustracer.helper.DateHelper;
import de.fhkiel.campustracer.helper.LongUtils;
import de.fhkiel.campustracer.model.KeysEnum;
import de.fhkiel.campustracer.model.KeystoreEntry;
import de.fhkiel.campustracer.model.PreGeneratedEncryptionKeyEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;
import de.fhkiel.campustracer.model.virtual.UserMasterKeyRelationEntry;
import de.fhkiel.campustracer.model.virtual.UserMasterKeyUpdateEntry;


@Dao
public abstract class KeyRepository {
	@Query("SELECT * FROM UserMasterKeyEntry")
	public abstract List<UserMasterKeyEntry> getAll();

	@Query("SELECT * FROM UserMasterKeyEntry WHERE id = :keyId")
	public abstract UserMasterKeyEntry getUserMasterKeyEntry(long keyId);

	@Query("SELECT MAX(createdAt) FROM UserMasterKeyEntry;")
	public abstract LocalDateTime getDateTimeOfLatestKeyEntry();

	/**
	 * Check which key ids are already in the database
	 *
	 * @param keyIds The keys to check for
	 * @return All existing keys
	 */
	@Query("SELECT id FROM UserMasterKeyEntry WHERE id IN (:keyIds)")
	public abstract long[] getUserMasterKeyEntryIds(List<Long> keyIds);

	/**
	 * Get a UserMasterKeyRelationEntry by UserMasterKey id
	 *
	 * @param keyId The key id
	 * @return The key for the id
	 */
	@Transaction
	@Query("SELECT * FROM UserMasterKeyEntry WHERE id = :keyId")
	public abstract UserMasterKeyRelationEntry getUserMasterKeyRelationEntry(long keyId);

	/**
	 * Get a UserMasterKeyRelationEntry by Base64-encoded String of MasterPubKey
	 *
	 * @param base64PubKey Base64-encoded String of serialized MasterPubKey
	 * @return The key for the id
	 */
	@Transaction
	@Query("SELECT * FROM UserMasterKeyEntry WHERE masterPubKey = :base64PubKey LIMIT 1")
	public abstract Optional<UserMasterKeyRelationEntry> getUserMasterKeyRelationEntryByMasterPubKey(
		String base64PubKey
	);

	/**
	 * Get the current valid UserMasterKeyRelationEntry for a user
	 *
	 * @param userId The user's id
	 * @return The valid key for the id
	 */
	@Transaction
	@Query("SELECT * FROM UserMasterKeyEntry WHERE userId = :userId AND invalidatedAt IS NULL"
		+ " ORDER BY createdAt DESC LIMIT 1")
	public abstract Optional<UserMasterKeyRelationEntry> getValidUserMasterKeyRelationEntryForUser(
		long userId
	);

	/**
	 * Get all UserMasterKeys that were valid for a certain day, delimited by morning and evening
	 * times.
	 * <p>
	 * Reason: Dates are stored in DB as seconds/LocalDateTime, hence the comparison must consider
	 * the start/end times of the day to include ALL keys of a day, even if the user changed keys
	 * multiple times within a day. (See equivalent method in MockDatabase.java)
	 *
	 * @param userId  The user's id
	 * @param morning The date where the key was valid, time 00:00:00
	 * @param evening The date where the key was valid, time 23:59:59
	 * @return All valid keys
	 */
	@Transaction
	@Query("SELECT * FROM UserMasterKeyEntry WHERE userId = :userId "
		+ "AND createdAt <= :evening "
		+ "AND (invalidatedAt IS NULL OR invalidatedAt >= :morning)")
	public abstract List<UserMasterKeyRelationEntry> getUserMasterKeyRelationEntriesForUserAndDateTime(
		long userId, LocalDateTime morning, LocalDateTime evening
	);

	/**
	 * Get all UserMasterKeys that where valid for a certain date.
	 *
	 * @param userId   The user's id
	 * @param dateTime The date where the key was valid
	 * @return All valid keys
	 */
	public List<UserMasterKeyRelationEntry> getUserMasterKeyRelationEntriesForUserAndDate(
		long userId, LocalDate dateTime
	) {
		return this.getUserMasterKeyRelationEntriesForUserAndDateTime(
			userId,
			DateHelper.castLocalDateToLocalDateTime(dateTime, true),
			DateHelper.castLocalDateToLocalDateTime(dateTime, false)

		);
	}

	@Query("SELECT * FROM UserMasterKeyEntry WHERE userId = :userId")
	public abstract UserMasterKeyEntry getUserMasterKeyEntryForUser(long userId);

	@Transaction
	@Query("SELECT * FROM UserMasterKeyEntry WHERE userId = :userId")
	public abstract UserMasterKeyRelationEntry getUserMasterKeyRelationEntryForUser(long userId);

	@Query("SELECT * FROM UserMasterKeyEntry WHERE userId = :userId")
	public abstract List<UserMasterKeyEntry> getUserMasterKeyEntriesForUser(long userId);

	@Query("SELECT * FROM PreGeneratedEncryptionKeyEntry WHERE userMasterKeyId = :masterKeyId")
	public abstract List<PreGeneratedEncryptionKeyEntry> getPreGeneratedEncryptionKeyEntries(
		long masterKeyId
	);

	@Transaction
	@Query("SELECT * FROM UserMasterKeyEntry WHERE userId = :userId")
	public abstract List<UserMasterKeyRelationEntry> getUserMasterKeyRelationEntriesForUser(
		long userId
	);

	/**
	 * Add UserMasterKey without PGEKs - for internal usage only
	 *
	 * @param userMasterKey The key to add to the DB
	 * @return The key id
	 */
	@Insert(onConflict = OnConflictStrategy.ABORT)
	protected abstract long addUserMasterKeyEntryOnly(UserMasterKeyEntry userMasterKey);

	/**
	 * Add UserMasterKeys without PGEKs - for internal usage only
	 *
	 * @param userMasterKeys The keys to add to the DB
	 * @return The key ids
	 */
	@Insert(onConflict = OnConflictStrategy.ABORT)
	protected abstract long[] addUserMasterKeyEntriesOnly(List<UserMasterKeyEntry> userMasterKeys);

	/**
	 * Add list of PGEKs to existing UserMasterKey
	 *
	 * @param preGeneratedEncryptionKeyEntries The PGEKs
	 * @return The key ids
	 */
	@Insert(onConflict = OnConflictStrategy.ABORT)
	protected abstract long[] addPreGeneratedEncryptionKeyEntriesOnly(
		List<PreGeneratedEncryptionKeyEntry> preGeneratedEncryptionKeyEntries
	);

	/**
	 * Add complete UserMasterKey with related PGEKs
	 *
	 * @param userMasterKeyEntry The key to add to the DB
	 * @return The key id
	 */
	@Transaction
	public long addUserMasterKeyEntry(UserMasterKeyEntry userMasterKeyEntry) {
		return this.addUserMasterKeyRelationEntry(userMasterKeyEntry.toUserMasterKeyRelationEntry());
	}

	/**
	 * Add complete UserMasterKeys with related PGEKs
	 *
	 * @param userMasterKeyEntries The keys to add to the DB
	 * @return The key ids
	 */
	@Transaction
	public long[] addUserMasterKeyEntries(List<UserMasterKeyEntry> userMasterKeyEntries) {
		return this.addUserMasterKeyRelationEntries(userMasterKeyEntries.stream()
			.map(UserMasterKeyEntry::toUserMasterKeyRelationEntry)
			.collect(Collectors.toList()));
	}

	@Transaction
	@Query("SELECT * FROM PreGeneratedEncryptionKeyEntry WHERE preGeneratedPubKey = :pubKey LIMIT"
		+ " 1")
	public abstract Optional<PreGeneratedEncryptionKeyEntry> getPreGenEncryptionKeyEntryByPubKey(
		String pubKey
	);

	/**
	 * Add UserMasterKeyRelation with related PGEKs
	 *
	 * @param userMasterKeyRelationEntry The key to add to the DB
	 * @return The key id
	 */
	@Transaction
	public long addUserMasterKeyRelationEntry(UserMasterKeyRelationEntry userMasterKeyRelationEntry) {
		long id =
			this.addUserMasterKeyEntryOnly(userMasterKeyRelationEntry.getUserMasterKeyEntry());
		this.addPreGeneratedEncryptionKeyEntriesOnly(userMasterKeyRelationEntry.getPreGeneratedEncryptionKeyEntries()
			.stream()
			.peek(k -> k.setUserMasterKeyId(id))
			.collect(Collectors.toList()));
		return id;
	}

	/**
	 * Add UserMasterKeyRelations with related PGEKs
	 *
	 * @param userMasterKeyRelationEntries The keys to add to the DB
	 * @return The key id
	 */
	@Transaction
	public long[] addUserMasterKeyRelationEntries(List<UserMasterKeyRelationEntry> userMasterKeyRelationEntries) {
		List<Long> ids = new ArrayList<>();
		for (UserMasterKeyRelationEntry userMasterKeyRelationEntry :
			userMasterKeyRelationEntries) {
			long currentId =
				this.addUserMasterKeyEntryOnly(userMasterKeyRelationEntry.getUserMasterKeyEntry());
			this.addPreGeneratedEncryptionKeyEntriesOnly(userMasterKeyRelationEntry.getPreGeneratedEncryptionKeyEntries()
				.stream()
				.peek(k -> k.setUserMasterKeyId(currentId))
				.collect(Collectors.toList()));
			ids.add(currentId);
		}
		return LongUtils.toArray(ids);
	}

	/**
	 * Add list of UserMasterKeys with related PGEKs or update invalidatedAt field, if key exists
	 *
	 * @param userMasterKeyEntries The keys to add to the DB
	 * @return Pair of new and updated key ids
	 */
	@Transaction
	public Pair<long[], long[]> addOrUpdateUserMasterKeyEntries(List<UserMasterKeyEntry> userMasterKeyEntries) {
		List<Long> existing =
			LongUtils.toList(this.getUserMasterKeyEntryIds(userMasterKeyEntries.stream()
				.map(k -> k.id)
				.collect(Collectors.toList())));
		this.updateUserMasterKeyEntries(userMasterKeyEntries.stream()
			.filter(k -> existing.contains(k.id))
			.map(UserMasterKeyEntry::toUserMasterKeyUpdateEntry)
			.collect(Collectors.toList()));
		long[] newKeys = this.addUserMasterKeyEntries(userMasterKeyEntries.stream()
			.filter(k -> !existing.contains(k.id))
			.collect(Collectors.toList()));
		return new Pair<>(newKeys, LongUtils.toArray(existing));
	}

	@Update(entity = UserMasterKeyEntry.class)
	public abstract void updateUserMasterKeyEntry(UserMasterKeyUpdateEntry userMasterKeyUpdateEntry);

	@Update(entity = UserMasterKeyEntry.class)
	public abstract void updateUserMasterKeyEntries(
		List<UserMasterKeyUpdateEntry> userMasterKeyUpdateEntries
	);

	@Delete
	public abstract void deleteUserMasterKeyEntry(UserMasterKeyEntry userMasterKey);

	@Delete
	public abstract void deleteUserMasterKeyEntries(List<UserMasterKeyEntry> userMasterKeys);

	/*
	 * KeyStore
	 */
	@Query("SELECT * FROM KeystoreEntry WHERE id = :id")
	public abstract KeystoreEntry getKeystoreEntryById(long id);

	@Query("SELECT * FROM KeystoreEntry WHERE type = :type")
	public abstract List<KeystoreEntry> getKeystoreEntriesByType(KeysEnum type);

	@Query("SELECT * FROM KeystoreEntry WHERE type = :type"
		+ " AND createdAt BETWEEN :dateFrom AND :dateTo")
	public abstract List<KeystoreEntry> getKeystoreEntriesByTypeAndDates(
		KeysEnum type, long dateFrom, long dateTo
	);

	@Insert
	public abstract long addToKeystore(KeystoreEntry keystoreEntry);

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	public abstract long addToOrUpdateKeystore(KeystoreEntry keystoreEntry);

	@Delete
	public abstract void deleteFromKeystore(KeystoreEntry keystoreEntry);
}
