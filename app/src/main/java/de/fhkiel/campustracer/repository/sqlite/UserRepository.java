package de.fhkiel.campustracer.repository.sqlite;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import de.fhkiel.campustracer.model.UserEntry;


@Dao
public interface UserRepository {
	@Query("SELECT * FROM UserEntry WHERE id = :userId")
	Optional<UserEntry> getUserEntryById(long userId);

	@Query("SELECT * FROM UserEntry WHERE username = :username")
	Optional<UserEntry> getUserEntryById(String username);

	@Query("SELECT * FROM UserEntry WHERE id IN (:userIds)")
	List<UserEntry> getUserEnties(long[] userIds);

	@Query("SELECT * FROM UserEntry WHERE username IN (:usernames)")
	List<UserEntry> getUserEntries(List<String> usernames);

	@Query("SELECT * FROM UserEntry")
	List<UserEntry> getAllUserEntries();

	@Query("SELECT MAX(changedAt) FROM UserEntry")
	LocalDateTime getDateTimeOfLatestUserEntry();

	@Delete
	void deleteUserEntry(UserEntry user);

	@Delete
	void deleteUserEntries(List<UserEntry> users);

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	long addUserEntry(UserEntry user);

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	long[] addUserEntries(List<UserEntry> users);
}
