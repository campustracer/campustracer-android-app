package de.fhkiel.campustracer.helper.room_type_converters;

import androidx.room.ProvidedTypeConverter;
import androidx.room.TypeConverter;

import de.fhkiel.campustracer.model.KeysEnum;


@ProvidedTypeConverter
public class KeysEnumConverter {
	@TypeConverter
	public static KeysEnum fromEnumString(String type) {
		return KeysEnum.valueOf(type);
	}

	@TypeConverter
	public static String toEnumString(KeysEnum type) {
		return type.name();
	}

	private KeysEnumConverter() {
		throw new IllegalStateException("Converter Class");
	}
}
