package de.fhkiel.campustracer.helper;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.IOException;


public class FileHelper {
	private FileHelper() {
	}

	public static String getFilename(Activity activity, Uri uri) throws IOException {
		try (
			Cursor cursor = activity.getContentResolver().query(uri, null, null, null)
		) {
			if (cursor != null && cursor.moveToFirst()) {
				int columnindex =
					cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
				return cursor.getString(columnindex);
			}
		}
		throw new IOException("Could not read filename");
	}
}
