package de.fhkiel.campustracer.helper.room_type_converters;

import androidx.room.ProvidedTypeConverter;
import androidx.room.TypeConverter;

import java.security.PrivateKey;
import java.util.Base64;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;


@ProvidedTypeConverter
public class PrivateKeyConverter {
	@TypeConverter
	public static PrivateKey fromBase64String(String privkey) {
		try {
			return CryptoWrapper.deserializePrivKey(Base64.getDecoder().decode(privkey));
		} catch (CryptoKeyDeserializationException e) {
			throw new IllegalArgumentException("Cannot deserialize private key");
		}
	}

	@TypeConverter
	public static String toBase64String(PrivateKey privkey) {
		return Base64.getEncoder().encodeToString(CryptoWrapper.serializePrivKey(privkey));
	}

	private PrivateKeyConverter() {
		throw new IllegalStateException("Converter Class");
	}
}
