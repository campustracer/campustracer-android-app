package de.fhkiel.campustracer.helper.room_type_converters;

import androidx.room.ProvidedTypeConverter;
import androidx.room.TypeConverter;

import java.time.LocalDate;


@ProvidedTypeConverter
public class LocalDateConverter {
	@TypeConverter
	public static LocalDate fromTimestamp(Long timestamp) {
		return timestamp == null ? null : LocalDate.ofEpochDay(timestamp);
	}

	@TypeConverter
	public static Long toTimestamp(LocalDate date) {
		return date == null ? null : date.toEpochDay();
	}

	private LocalDateConverter() {
		throw new IllegalStateException("Converter Class");
	}
}
