package de.fhkiel.campustracer.helper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


public class DateHelper {
	private static final Locale DATE_LOCALE = Locale.GERMANY;

	private DateHelper() {
	}

	/**
	 * @param date
	 * @return String "YYYY-MM-DD"
	 */
	public static String getISODateString(LocalDate date) {
		return date.format(DateTimeFormatter.ISO_LOCAL_DATE.withLocale(DATE_LOCALE));
	}

	public static LocalDate getLocalDateFromISODateString(String isoDateStr)
		throws DateTimeParseException {
		if (isoDateStr.contains("T")) {
			throw new IllegalArgumentException(
				"An ISODateTime String was provided! This method can only handle ISODate "
					+ "Strings!");
		}
		return LocalDate.parse(isoDateStr,
			DateTimeFormatter.ISO_LOCAL_DATE.withLocale(DATE_LOCALE)
		);
	}

	public static String getISODateTimeString(LocalDateTime date) {
		return date.truncatedTo(ChronoUnit.SECONDS)
			.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME.withLocale(DATE_LOCALE));
	}

	public static LocalDateTime getLocalDateTimeFromISODateTimeString(String isoDateTimeStr)
		throws DateTimeParseException {
		if (!isoDateTimeStr.contains("T")) {
			throw new IllegalArgumentException(
				"An ISODate String was provided! This method can only handle ISODateTime "
					+ "Strings!");
		}
		return LocalDateTime.parse(isoDateTimeStr,
			DateTimeFormatter.ISO_LOCAL_DATE_TIME.withLocale(DATE_LOCALE)
		)
			.truncatedTo(ChronoUnit.SECONDS);
	}

	/**
	 * Transforms a LocalDate Object to a LocalDateTime and sets the time either to the
	 * beginning or
	 * end of the day.
	 *
	 * @param date         the LocalDate Object
	 * @param toBeginOfDay if true, Time is set to 00:00:00, otherwise 23:59:59
	 * @return LocalDateTime Object
	 */
	public static LocalDateTime castLocalDateToLocalDateTime(
		LocalDate date, Boolean toBeginOfDay
	) {
		LocalTime time = toBeginOfDay ? LocalTime.of(0, 0, 0) : LocalTime.of(23, 59, 59);
		return LocalDateTime.of(date, time);
	}

	public static List<LocalDate> getDatesListFromDateRange(
		LocalDate startDate, LocalDate endDate
	) {
		if (startDate.isEqual(endDate)) {
			return Arrays.asList(startDate);
		} else if (startDate.isAfter(endDate)) {
			throw new IllegalArgumentException("startDate must be before or equal to endDate!");
		}
		ArrayList<LocalDate> resultDatesList = new ArrayList<LocalDate>();
		LocalDate curDate = LocalDate.from(startDate);
		do {
			resultDatesList.add(curDate);
			curDate = curDate.plusDays(1);
		} while (DateHelper.isBeforeOrEqual(curDate, endDate));
		return resultDatesList;
	}

	public static List<LocalDate> getDatesListFromDateTimeRange(
		LocalDateTime startDateTime, LocalDateTime endDateTime
	) {
		return getDatesListFromDateRange(LocalDate.from(startDateTime),
			LocalDate.from(endDateTime)
		);
	}

	public static Boolean isBeforeOrEqual(LocalDateTime a, LocalDate b) {
		return (a.isBefore(DateHelper.castLocalDateToLocalDateTime(b, false)) || a.isEqual(
			DateHelper.castLocalDateToLocalDateTime(b, false)));
	}

	public static Boolean isBeforeOrEqual(LocalDate a, LocalDate b) {
		return (a.isBefore(b) || a.isEqual(b));
	}

	public static Boolean isBeforeOrEqual(LocalDateTime a, LocalDateTime b) {
		return (a.isBefore(b) || a.isEqual(b));
	}

	public static Boolean isAfterOrEqual(LocalDateTime a, LocalDate b) {
		return (a.isAfter(DateHelper.castLocalDateToLocalDateTime(b, true))
			|| a.isEqual(DateHelper.castLocalDateToLocalDateTime(b, true)));
	}

	public static Boolean isAfterOrEqual(LocalDate a, LocalDateTime b) {
		return isBeforeOrEqual(b, a);
	}

	public static Boolean isBeforeOrEqual(LocalDate a, LocalDateTime b) {
		return isAfterOrEqual(b, a);
	}

	public static Boolean isAfterOrEqual(LocalDateTime a, LocalDateTime b) {
		return (a.isAfter(b) || a.isEqual(b));
	}

	public static Boolean isAfterOrEqual(LocalDate a, LocalDate b) {
		return (a.isAfter(b) || a.isEqual(b));
	}
}
