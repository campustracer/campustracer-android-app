/*
 * Copyright (C) 2015 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhkiel.campustracer.helper.moshiadapter;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.JsonReader;
import com.squareup.moshi.JsonWriter;

import java.io.IOException;
import java.security.PublicKey;
import java.util.Base64;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;


public final class PublicKeyAdapter extends JsonAdapter<PublicKey> {
	@Override
	public synchronized PublicKey fromJson(JsonReader reader) throws IOException {
		if (reader.peek() == JsonReader.Token.NULL) {
			return reader.nextNull();
		}
		String string = reader.nextString();
		try {
			return CryptoWrapper.deserializePubKey(Base64.getDecoder().decode(string));
		} catch (CryptoKeyDeserializationException e) {
			e.printStackTrace();
			// TODO: How to properly handle this exception from within the Adapter?!
		}
		return null;
	}

	@Override
	public synchronized void toJson(JsonWriter writer, PublicKey value) throws IOException {
		if (value == null) {
			writer.nullValue();
		} else {
			String string =
				Base64.getEncoder().encodeToString(CryptoWrapper.serializePubKey(value));
			writer.value(string);
		}
	}
}
