package de.fhkiel.campustracer.helper.room_type_converters;

import androidx.room.ProvidedTypeConverter;
import androidx.room.TypeConverter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;


@ProvidedTypeConverter
public class LocalDateTimeConverter {
	@TypeConverter
	public static LocalDateTime fromTimestamp(Long timestamp) {
		return timestamp == null ? null : LocalDateTime.ofEpochSecond(timestamp, 0,
			ZoneOffset.UTC);
	}

	@TypeConverter
	public static Long toTimestamp(LocalDateTime dateTime) {
		return dateTime == null ? null : dateTime.toEpochSecond(ZoneOffset.UTC);
	}

	private LocalDateTimeConverter() {
		throw new IllegalStateException("Converter Class");
	}
}
