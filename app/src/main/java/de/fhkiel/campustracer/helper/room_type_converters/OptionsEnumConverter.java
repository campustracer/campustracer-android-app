package de.fhkiel.campustracer.helper.room_type_converters;

import androidx.room.ProvidedTypeConverter;
import androidx.room.TypeConverter;

import de.fhkiel.campustracer.model.OptionsEnum;


@ProvidedTypeConverter
public class OptionsEnumConverter {
	@TypeConverter
	public static OptionsEnum fromEnumString(String option) {
		return OptionsEnum.valueOf(option);
	}

	@TypeConverter
	public static String toEnumString(OptionsEnum option) {
		return option.name();
	}

	private OptionsEnumConverter() {
		throw new IllegalStateException("Converter Class");
	}
}
