package de.fhkiel.campustracer.helper;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDate;
import java.util.Optional;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.model.LectureAttendedByUserEntry;
import de.fhkiel.campustracer.model.UserAttendedLectureEntry;
import de.fhkiel.campustracer.model.encdata.LectureAttendedByUserEncData;
import de.fhkiel.campustracer.model.encdata.UserAttendedLectureEncData;


public class CheckInHelper {
	private CheckInHelper() {
	}

	public static UserAttendedLectureEntry createUserAttendedLectureEntry(
		long userId,
		PublicKey studentDerivedPubKey,
		Optional<PrivateKey> studentDerivedPrivKey,
		long signedById,
		PrivateKey signerMasterPrivKey,
		byte[] lecturePrivKeyEncryptedWithUniPubKey,
		LocalDate checkInDate,
		PublicKey universityPubKey
	) throws CryptoEncryptionException {

		UserAttendedLectureEncData userAttendedLectureEncData = new UserAttendedLectureEncData(
			lecturePrivKeyEncryptedWithUniPubKey,
			userId,
			signedById,
			checkInDate
		);

		boolean isSignedByLecturer = !(userAttendedLectureEncData.isSignedBySameUser());

		if (studentDerivedPrivKey.isPresent() == isSignedByLecturer) {
			throw new IllegalArgumentException(
				"A CheckInKeyPair UAL Entry cannot be signed by a Lecturer!");
		}
		byte[] encryptedAndSignedStudentAttendedLectureEncData =
			userAttendedLectureEncData.signAndEncryptToBytes(studentDerivedPubKey,
				universityPubKey,
				signerMasterPrivKey
			);

		byte[] derivedKeyHash;
		if (studentDerivedPrivKey.isPresent()) {
			// regular case: student checks in with app -> derived check-in key -> hash
			derivedKeyHash =
				CryptoWrapper.hash(CryptoWrapper.serializePrivKey(studentDerivedPrivKey.get()));
		} else {
			// no-app-case: use preGenerated pub key for encryption, create random hash as there
			// is no preGenerated priv key available during check-in
			derivedKeyHash = CryptoWrapper.randomHash();
		}

		return new UserAttendedLectureEntry(0,
			derivedKeyHash,
			checkInDate,
			encryptedAndSignedStudentAttendedLectureEncData
		);
	}

	public static LectureAttendedByUserEntry createLectureAttendedByUserEntry(
		long studentId,
		PublicKey lecturePubKey,
		byte[] lecturePrivKeyHashRandomized,
		PrivateKey signerMasterPrivKey,
		long signedById,
		// empty if signed by student (regular check-in)
		LocalDate checkInDate
	) throws CryptoEncryptionException {

		// Prepare LectureAttendedByUser Table Entry
		LectureAttendedByUserEncData lectureAttendedByUserEncData =
			new LectureAttendedByUserEncData(studentId, signedById, checkInDate);
		byte[] encryptedAndSignedLectureAttendedByUserEncData =
			lectureAttendedByUserEncData.signAndEncryptToBytes(lecturePubKey, signerMasterPrivKey);

		return new LectureAttendedByUserEntry(0,
			lecturePrivKeyHashRandomized,
			checkInDate,
			encryptedAndSignedLectureAttendedByUserEncData
		);
	}
}
