package de.fhkiel.campustracer.helper;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class LongUtils {
	public static List<Long> toList(long[] longs) {
		return Arrays.stream(longs).boxed().collect(Collectors.toList());
	}

	public static long[] toArray(List<Long> longs) {
		return longs.stream().mapToLong(l -> l).toArray();
	}
}
