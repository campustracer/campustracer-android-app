package de.fhkiel.campustracer.helper.room_type_converters;

import androidx.room.ProvidedTypeConverter;
import androidx.room.TypeConverter;

import java.security.PublicKey;
import java.util.Base64;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;


@ProvidedTypeConverter
public class PublicKeyConverter {
	@TypeConverter
	public static PublicKey fromBase64String(String publicKey) {
		try {
			return CryptoWrapper.deserializePubKey(Base64.getDecoder().decode(publicKey));
		} catch (CryptoKeyDeserializationException e) {
			throw new IllegalArgumentException("Cannot deserialize public key");
		}
	}

	@TypeConverter
	public static String toBase64String(PublicKey publicKey) {
		return Base64.getEncoder().encodeToString(CryptoWrapper.serializePubKey(publicKey));
	}

	private PublicKeyConverter() {
		throw new IllegalStateException("Converter Class");
	}
}
