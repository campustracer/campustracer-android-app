package de.fhkiel.campustracer.helper.room_type_converters;

import androidx.room.ProvidedTypeConverter;
import androidx.room.TypeConverter;

import de.fhkiel.campustracer.model.RolesEnum;


@ProvidedTypeConverter
public class RolesEnumConverter {
	@TypeConverter
	public static RolesEnum fromEnumString(String role) {
		return RolesEnum.valueOf(role);
	}

	@TypeConverter
	public static String toEnumString(RolesEnum role) {
		return role.name();
	}

	private RolesEnumConverter() {
		throw new IllegalStateException("Converter Class");
	}
}
