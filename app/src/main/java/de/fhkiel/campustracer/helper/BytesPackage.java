package de.fhkiel.campustracer.helper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;


/**
 * Puts multiple byte[] arrays into a larger byte[] and prepends each with the length as integer (=4
 * byte). The resulting byte[] array can later be unpacked into a List by doing: read lenght
 * integer, read [length] bytes, continue Example: a = [1, 2, 3], b = [4, 5, 6, 7] Result: x = [0,
 * 0, 0, 3,  1, 2, 3,    0, 0, 0, 4,   4, 5, 6, 7 ]
 */
public class BytesPackage {
	private final List<byte[]> byteArrays = new ArrayList<>();
	private static final int INT_SIZE = 4;
	private static final int LONG_SIZE = 8;
	private static final int NULL_HEADER = -1;

	public BytesPackage(byte[]... byteArrays) {
		this.byteArrays.addAll(Arrays.asList(byteArrays));
	}

	public BytesPackage(List<byte[]> byteArrays) {
		this.byteArrays.addAll(byteArrays);
	}

	public static BytesPackage fromRaw(byte[] rawBytesPackage)
		throws CryptoDataDeserializationException {

		ArrayList<byte[]> byteArraysList = new ArrayList<>();
		int bytePos = 0;
		try {
			while (bytePos < rawBytesPackage.length) {
				if (bytePos + INT_SIZE > rawBytesPackage.length) {
					throw new ArrayIndexOutOfBoundsException("Malformed rawBytesPackage!");
				}
				Integer contentLength =
					bytesToInt(Arrays.copyOfRange(rawBytesPackage, bytePos, bytePos + INT_SIZE));
				bytePos += INT_SIZE;
				if (contentLength.equals(NULL_HEADER)) {
					byteArraysList.add(null);
					continue;
				}
				if (bytePos + contentLength > rawBytesPackage.length) {
					throw new ArrayIndexOutOfBoundsException("Malformed rawBytesPackage!");
				}
				byte[] content =
					Arrays.copyOfRange(rawBytesPackage, bytePos, bytePos + contentLength);
				bytePos += contentLength;
				byteArraysList.add(content);
			}
		} catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e) {
			throw new CryptoDataDeserializationException(e);
		}
		return new BytesPackage(byteArraysList);
	}

	public byte[] toBytes() {
		int totalLength = 0;
		for (byte[] ba : this.byteArrays) {
			int baLen = (ba == null) ? 0 : ba.length;
			totalLength += (baLen + INT_SIZE);
		}

		byte[] bytesPackage = (byte[]) Array.newInstance(byte.class, totalLength);

		int bytePos = 0;
		for (byte[] ba : this.byteArrays) {
			if (ba != null) {
				byte[] headerBytes = intToBytes(ba.length);
				System.arraycopy(headerBytes, 0, bytesPackage, bytePos, INT_SIZE);
				bytePos += INT_SIZE;
				System.arraycopy(ba, 0, bytesPackage, bytePos, ba.length);
				bytePos += ba.length;
			} else {
				// set -1 header to indicate NULL, no payload
				byte[] headerBytes = intToBytes(-1);
				System.arraycopy(headerBytes, 0, bytesPackage, bytePos, INT_SIZE);
				bytePos += INT_SIZE;
			}
		}
		return bytesPackage;
	}

	public byte[] get(Integer index) {
		return this.byteArrays.get(index);
	}

	public List<byte[]> getList() {
		return this.byteArrays;
	}

	public Integer size() {
		return this.byteArrays.size();
	}

	public static byte[] intToBytes(int intgr) {
		return ByteBuffer.allocate(INT_SIZE).order(ByteOrder.BIG_ENDIAN).putInt(intgr).array();
	}

	public static Integer bytesToInt(byte[] bytes) {
		return ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).getInt();
	}

	public static byte[] longToBytes(long value) {
		return ByteBuffer.allocate(LONG_SIZE).order(ByteOrder.BIG_ENDIAN).putLong(value).array();
	}

	public static long bytesToLong(byte[] bytes) {
		return ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).getLong();
	}

	public static byte[] stringToBytes(String value) {
		return value.getBytes(StandardCharsets.UTF_8);
	}

	public static String bytesToString(byte[] bytes) {
		return new String(bytes, StandardCharsets.UTF_8);
	}

	/**
	 * Turns private key list to byte array
	 *
	 * @param keys The keys
	 * @return The byte array
	 */
	public static byte[] privateKeyListToBytes(List<PrivateKey> keys) {
		return new BytesPackage(keys.stream()
			.map(CryptoWrapper::serializePrivKey)
			.collect(Collectors.toList())).toBytes();
	}

	/**
	 * Turns byte array to private key list
	 *
	 * @param rawArray The byte array
	 * @return The key list
	 * @throws CryptoDataDeserializationException ?
	 * @throws CryptoKeyDeserializationException  ?
	 */
	public static List<PrivateKey> bytesToPrivateKeyList(byte[] rawArray)
		throws CryptoDataDeserializationException, CryptoKeyDeserializationException {
		List<PrivateKey> pks = new ArrayList<>();
		for (byte[] bt : BytesPackage.fromRaw(rawArray).getList()) {
			pks.add(CryptoWrapper.deserializePrivKey(bt));
		}
		return pks;
	}

	// Source: https://stackoverflow.com/questions/2436385/android-getting-from-a-uri-to-an
	// -inputstream-to-a-byte-array/2436413#2436413
	public static byte[] inputStreamToBytes(InputStream inputStream) throws IOException {
		// this dynamically extends to take the bytes you read
		ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

		// this is storage overwritten on each iteration with bytes
		int bufferSize = 1024;
		byte[] buffer = new byte[bufferSize];

		// we need to know how may bytes were read to write them to the byteBuffer
		int len = 0;
		while ((len = inputStream.read(buffer)) != -1) {
			byteBuffer.write(buffer, 0, len);
		}

		// and then we can return your byte array.
		return byteBuffer.toByteArray();
	}
}
