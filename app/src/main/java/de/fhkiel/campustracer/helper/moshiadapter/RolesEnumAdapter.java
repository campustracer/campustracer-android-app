/*
 * Copyright (C) 2015 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhkiel.campustracer.helper.moshiadapter;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.JsonReader;
import com.squareup.moshi.JsonWriter;

import java.io.IOException;

import de.fhkiel.campustracer.model.RolesEnum;


public final class RolesEnumAdapter extends JsonAdapter<RolesEnum> {
	@Override
	public synchronized RolesEnum fromJson(JsonReader reader) throws IOException {
		if (reader.peek() == JsonReader.Token.NULL) {
			return reader.nextNull();
		}
		String string = reader.nextString();
		return RolesEnum.valueOf(string);
	}

	@Override
	public synchronized void toJson(JsonWriter writer, RolesEnum value) throws IOException {
		if (value == null) {
			writer.nullValue();
		} else {
			String string = value.name();
			writer.value(string);
		}
	}
}
