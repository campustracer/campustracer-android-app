package de.fhkiel.campustracer.helper;

import static java.io.File.separator;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import org.bouncycastle.util.encoders.Hex;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Base64;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;


public class QrCodeHelper {

	private QrCodeHelper() {
	}

	public static final Integer DIMENSIONS_PX = 1024;
	private static final String TAG = "QRCodeHelper";

	public static Bitmap createQrCodeBitmap(String base64Content) throws WriterException {
		QRCodeWriter writer = new QRCodeWriter();
		BitMatrix bitMatrix =
			writer.encode(base64Content, BarcodeFormat.QR_CODE, DIMENSIONS_PX, DIMENSIONS_PX);
		int width = bitMatrix.getWidth();
		int height = bitMatrix.getHeight();
		Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
			}
		}
		return bmp;
	}

	public static Bitmap createQrCodeBitmap(byte[] bytesContent) throws WriterException {
		return createQrCodeBitmap(Base64.getEncoder().encodeToString(bytesContent));
	}

	public static String getFingerprint(String stringPayload) {
		return Hex.toHexString(CryptoWrapper.hash(stringPayload));
	}

	// Source: https://stackoverflow.com/a/57265702
	public static String saveBitmapToFile(
		Bitmap bitmap, Context context, String folderName, String filename
	) throws FileNotFoundException {
		String outputFilename;
		if (android.os.Build.VERSION.SDK_INT >= 29) {
			Log.d(TAG, "SDK >= 29");
			ContentValues values = contentValues(filename);
			values.put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/" + folderName);
			values.put(MediaStore.Images.Media.IS_PENDING, true);

			// RELATIVE_PATH and IS_PENDING are introduced in API 29.
			Uri uri = context.getContentResolver()
				.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
			if (uri != null) {
				saveImageToStream(bitmap, context.getContentResolver().openOutputStream(uri));
				values.put(MediaStore.Images.Media.IS_PENDING, false);
				context.getContentResolver().update(uri, values, null, null);
			}
			outputFilename = uri.getPath();
		} else {
			Log.d(TAG, "SDK < 29");

			File directory = new File(Environment.getExternalStorageDirectory().toString()
				+ separator
				+ folderName);
			// getExternalStorageDirectory is deprecated in API 29

			if (!directory.exists()) {
				directory.mkdirs();
			}
			String fileName = filename + ".png";
			File file = new File(directory, fileName);
			saveImageToStream(bitmap, new FileOutputStream(file));
			//if (file.getAbsolutePath() != null) {
			ContentValues values = contentValues(filename);
			values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
			// .DATA is deprecated in API 29
			context.getContentResolver()
				.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
			//}
			outputFilename = file.getAbsolutePath();
		}
		return outputFilename;
	}

	private static ContentValues contentValues(String filename) {
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
		values.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis() / 1000);
		values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
		values.put(MediaStore.Images.Media.DISPLAY_NAME, filename + ".png");
		return values;
	}

	private static void saveImageToStream(Bitmap bitmap, OutputStream outputStream) {
		if (outputStream != null) {
			try {
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
				outputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
