package de.fhkiel.campustracer.entities.qrcode;

import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.time.LocalDate;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;
import de.fhkiel.campustracer.helper.BytesPackage;
import de.fhkiel.campustracer.helper.DateHelper;
import lombok.Getter;


public class LectureQRCode extends Lecture {
	@Getter
	private final byte[] lecturePrivKeyHashRandomized;

	public byte[] toBytes() {

		BytesPackage payload = new BytesPackage(DateHelper.getISODateString(this.lectureDate)
			.getBytes(StandardCharsets.UTF_8),
			this.lecturePrivKeyHashRandomized,
			CryptoWrapper.serializePubKey(this.lecturePubKey),
			this.lecturePrivKeyEncryptedWithUniPubKey,
			this.checkInAuthKey.getBytes(StandardCharsets.UTF_8)
		);
		return payload.toBytes();
	}

	// fromLecture Constructor
	protected LectureQRCode(Lecture fromLecture) {
		super(fromLecture.getLectureDate(),
			fromLecture.getLecturePubKey(),
			fromLecture.lecturePrivKeyHash,
			fromLecture.getLecturePrivKeyEncryptedWithUniPubKey(),
			fromLecture.checkInAuthKey
		);
		this.lecturePrivKeyHashRandomized = this.getNextLecturePrivKeyHashRandomized();
	}

	// fromBytes Constructor
	protected LectureQRCode(
		LocalDate lectureDate,
		PublicKey lecturePubKey,
		byte[] lecturePrivKeyEncryptedWithUniPubKey,
		byte[] lecturePrivKeyHashRandomized,
		String lectureCheckInAuthKey
	) {
		super(lectureDate,
			lecturePubKey,
			null,
			lecturePrivKeyEncryptedWithUniPubKey,
			lectureCheckInAuthKey
		);
		this.lecturePrivKeyHashRandomized = lecturePrivKeyHashRandomized;
	}

	public static LectureQRCode fromBytes(byte[] lectureQrCodeRaw)
		throws CryptoKeyDeserializationException, CryptoDataDeserializationException {
		BytesPackage lectureQrCodePayload = BytesPackage.fromRaw(lectureQrCodeRaw);
		LocalDate lectureDate = DateHelper.getLocalDateFromISODateString(new String(
			lectureQrCodePayload.get(0),
			StandardCharsets.UTF_8
		));
		byte[] lecturePrivKeyHashRandomized = lectureQrCodePayload.get(1);
		PublicKey lecturePubKey = CryptoWrapper.deserializePubKey(lectureQrCodePayload.get(2));
		byte[] lecturePrivKeyEncryptedWithUniPubKey = lectureQrCodePayload.get(3);
		String lectureCheckInAuthkey =
			new String(lectureQrCodePayload.get(4), StandardCharsets.UTF_8);

		return new LectureQRCode(lectureDate,
			lecturePubKey,
			lecturePrivKeyEncryptedWithUniPubKey,
			lecturePrivKeyHashRandomized,
			lectureCheckInAuthkey
		);
	}

	/*
	//TODO: METHODS FOR DISPLAYING QR CODE IMAGE
	public BufferedImage toQRCodeBufferedImage() throws WriterException {
		byte[] qrCodePayload = this.toBytes();
		System.out.println("raw qrPack is " + qrCodePayload.length + " byte long");
		String qrPackB64 = Base64.getEncoder().encodeToString(qrCodePayload);
		System.out.println("qrPackB64 is  " + qrPackB64.getBytes(StandardCharsets.UTF_8).length +
		" byte long");
		String qrPackHex = Hex.toHexString(qrCodePayload);
		System.out.println("qrPackHex is  " + qrPackHex.getBytes(StandardCharsets.UTF_8).length +
		" byte long");

		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix bitMatrix = qrCodeWriter.encode(qrPackB64, BarcodeFormat.QR_CODE, 600, 600);
		return MatrixToImageWriter.toBufferedImage(bitMatrix);
	}

	public void displayQRCodeImage() throws WriterException {
		BufferedImage bufImg = this.toQRCodeBufferedImage();
		JFrame frame = new JFrame();
		frame.getContentPane().setLayout(new FlowLayout());
		frame.getContentPane().add(new JLabel(new ImageIcon(bufImg)));
		frame.pack();
		frame.setVisible(true);
	}

	*/
}
