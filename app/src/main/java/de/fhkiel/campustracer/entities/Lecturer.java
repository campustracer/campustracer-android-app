package de.fhkiel.campustracer.entities;

import org.javatuples.Pair;

import java.security.KeyPair;
import java.security.PublicKey;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import de.fhkiel.campustracer.entities.qrcode.Lecture;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;
import de.fhkiel.campustracer.model.CheckInEntry;
import de.fhkiel.campustracer.model.PreGeneratedEncryptionKeyEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;


public class Lecturer extends User {

	private Pair<LocalDate, String> checkInAuthKey = null; // acquired from server

	public Lecturer(long lecturerID) {
		super(lecturerID);
	}

	// Construct from Local Storage:
	public Lecturer(
		long userId, KeyPair masterKeyPair, List<Pair<LocalDate, KeyPair>> derivedCheckInKeyPairs
	) {
		super(userId, masterKeyPair, derivedCheckInKeyPairs);
	}

	public long getLecturerId() {
		return this.userId;
	}

	public String getCheckInAuthKey() {
		/*
		LocalDate today = LocalDate.now();
		if (this.checkInAuthKey == null || !today.isEqual(this.checkInAuthKey.getValue0())) {
			String newAuthKey = this.cryptoWrapperService.getTodaysCheckInAuthKey(this.userId);
			this.checkInAuthKey = new Pair<>(today, newAuthKey);
		}
		return this.checkInAuthKey.getValue1();
		*/
		return "NULL";
	}

	public void checkInStudentWithoutApp(
		long studentId, Lecture lecture, PublicKey universityPubKey
	) throws CryptoEncryptionException, ApiHttpException {
		// Get Student's currently valid derived PreGenerated Public Key from DB or die
		Optional<PreGeneratedEncryptionKeyEntry> studentPreGeneratedKeyEntryOptional =
			this.cryptoWrapperService.getCurrentlyValidPreGeneratedKeyEntryForUserIdAndDate(
				studentId,
				lecture.getLectureDate()
			);
		if (!studentPreGeneratedKeyEntryOptional.isPresent()) {
			throw new IllegalStateException(
				"The Student has no currently valid PreGeneratedPubKey! Student must generate new "
					+ "Keys before Check-In!");
		}

		PublicKey studentPreGeneratedPubKey =
			studentPreGeneratedKeyEntryOptional.get().getPreGeneratedPubKey();

		// Generate & add UserAttendedLectureEntry

		this.appService.getApiService().addCheckInEntry(new CheckInEntry(studentId,
			studentPreGeneratedPubKey,
			Optional.empty(),
			this.userId,
			this.masterKeyPair.getPrivate(),
			lecture.getLecturePubKey(),
			lecture.getLecturePrivKeyEncryptedWithUniPubKey(),
			lecture.getNextLecturePrivKeyHashRandomized(),
			lecture.getLectureDate(),
			universityPubKey
		));
	}

	public void checkInToLecture(Lecture lecture, PublicKey universityPubKey)
		throws CryptoKeyDeserializationException, CryptoEncryptionException, ApiHttpException,
		CryptoDataDeserializationException {
		byte[] qrCodePayload = lecture.getNextLectureQRCode().toBytes();
		super.checkInWithQRCode(qrCodePayload, universityPubKey, lecture.getLectureDate());
	}

	public UserMasterKeyEntry registerLecturer(
		LocalDateTime registryDate,
		PublicKey universityPubKey,
		List<PublicKey> trustedPartiesPubKeys
	) throws CryptoEncryptionException, ApiHttpException {
		UserMasterKeyEntry userMasterKeyEntry =
			this.generateUserMasterKeyEntry(registryDate, universityPubKey, trustedPartiesPubKeys);
		return this.cryptoWrapperService.addUserMasterKeyAndPreGeneratedKeys(userMasterKeyEntry);
	}
}
