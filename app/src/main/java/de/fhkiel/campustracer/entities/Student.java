package de.fhkiel.campustracer.entities;

import static de.fhkiel.campustracer.CampusTracerParameters.PREGENERATED_KEYS_VALID_FOR_N_DAYS;

import org.javatuples.Pair;

import java.security.KeyPair;
import java.security.PublicKey;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.fhkiel.campustracer.CampusTracerParameters;
import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.helper.DateHelper;
import de.fhkiel.campustracer.model.PreGeneratedEncryptionKeyEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;
import lombok.Getter;


public class Student extends User {

	@Getter
	private final Map<Pair<LocalDateTime, LocalDateTime>, KeyPair> preGeneratedKeyPairs;
	//<<validFrom, validTo>, KeyPair>

	public Student(final long studentID) {
		super(studentID);
		this.preGeneratedKeyPairs = new HashMap<>();
	}

	// Construct from Local Storage
	public Student(
		long userId,
		KeyPair masterKeyPair,
		List<Pair<LocalDate, KeyPair>> checkInKeyPairs,
		Map<Pair<LocalDateTime, LocalDateTime>, KeyPair> preGeneratedKeyPairs
	) {
		super(userId, masterKeyPair, checkInKeyPairs);
		this.preGeneratedKeyPairs = preGeneratedKeyPairs;
	}

	public long getStudentId() {
		return this.userId;
	}

	public UserMasterKeyEntry registerStudent(
		LocalDateTime registryDate,
		PublicKey universityPubKey,
		List<PublicKey> trustedPartiesPubKeys
	) throws CryptoEncryptionException, ApiHttpException {
		//create StudentTable Entry
		UserMasterKeyEntry userMasterKeyEntry =
			this.generateUserMasterKeyEntry(registryDate, universityPubKey, trustedPartiesPubKeys);

		// create PreGeneratedKey Entry
		List<PreGeneratedEncryptionKeyEntry> preGeneratedEncryptionKeyEntriesList =
			new ArrayList<>();
		for (Pair<Pair<LocalDateTime, LocalDateTime>, KeyPair> preGenKeyPairObj :
			this.generatePreGeneratedKeypairs(registryDate,
			CampusTracerParameters.GENERATE_PREGENERATED_KEYS_FOR_N_WEEKS
		)) {
			LocalDateTime preGenValidFrom = preGenKeyPairObj.getValue0().getValue0();
			LocalDateTime preGenValidTo = preGenKeyPairObj.getValue0().getValue1();
			KeyPair preGenKeyPair = preGenKeyPairObj.getValue1();
			PublicKey preGenPubKey = preGenKeyPair.getPublic();
			Pair<LocalDateTime, LocalDateTime> preGenDatePair =
				new Pair<LocalDateTime, LocalDateTime>(preGenValidFrom, preGenValidTo);

			this.preGeneratedKeyPairs.put(preGenDatePair, preGenKeyPair);
			PreGeneratedEncryptionKeyEntry studentPreGeneratedKeyEntry =
				PreGeneratedEncryptionKeyEntry.createSigned(preGenPubKey,
					preGenValidFrom,
					preGenValidTo,
					this.masterKeyPair.getPrivate()
				);
			preGeneratedEncryptionKeyEntriesList.add(studentPreGeneratedKeyEntry);
		}
		userMasterKeyEntry.setPreGeneratedEncryptionKeys(preGeneratedEncryptionKeyEntriesList);

		return this.cryptoWrapperService.addUserMasterKeyAndPreGeneratedKeys(userMasterKeyEntry);
	}

	public List<Pair<Pair<LocalDateTime, LocalDateTime>, KeyPair>> generatePreGeneratedKeypairs(
		LocalDateTime startDate, Integer numberOfWeeks
	) {
		ArrayList<Pair<Pair<LocalDateTime, LocalDateTime>, KeyPair>> keyList = new ArrayList<>();
		for (int i = 0; i < (numberOfWeeks * 7) / PREGENERATED_KEYS_VALID_FOR_N_DAYS; i++) {
			LocalDateTime keyStartDate =
				startDate.plusDays((long) i * PREGENERATED_KEYS_VALID_FOR_N_DAYS);
			LocalDateTime keyEndDate =
				startDate.plusDays(((long) (i + 1) * PREGENERATED_KEYS_VALID_FOR_N_DAYS))
					.minusSeconds(1);

			keyList.add(new Pair<>(new Pair<>(keyStartDate, keyEndDate),
				CryptoWrapper.derivePreGeneratedKeypair(this.masterKeyPair.getPrivate(),
					keyStartDate,
					keyEndDate
				)
			));
		}
		return keyList;
	}

	public Map<Pair<LocalDateTime, LocalDateTime>, KeyPair> surrenderPreGeneratedKeyPairsForDates(
		final List<LocalDate> dates
	) {
		HashMap<Pair<LocalDateTime, LocalDateTime>, KeyPair> uniqueSurrenderedPreGeneratedKeyPairs =
			new HashMap<Pair<LocalDateTime, LocalDateTime>, KeyPair>();
		for (LocalDate date : dates) {
			Map<Pair<LocalDateTime, LocalDateTime>, KeyPair> preGenKeyPairsMap =
				this.preGeneratedKeyPairs.entrySet()
					.stream()
					.filter(x -> DateHelper.isBeforeOrEqual(x.getKey().getValue0(), date))
					.filter(x -> DateHelper.isAfterOrEqual(x.getKey().getValue1(), date))
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
			uniqueSurrenderedPreGeneratedKeyPairs.putAll(preGenKeyPairsMap);
		}
		return uniqueSurrenderedPreGeneratedKeyPairs;
	}
}
