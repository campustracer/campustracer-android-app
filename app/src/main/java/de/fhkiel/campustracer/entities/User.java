package de.fhkiel.campustracer.entities;

import android.util.Log;

import org.javatuples.Pair;

import java.security.KeyPair;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import de.fhkiel.campustracer.CampusTracerParameters;
import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.cryptowrapper.CryptoWrapperDataInterface;
import de.fhkiel.campustracer.cryptowrapper.ThresholdCryptoSystem;
import de.fhkiel.campustracer.entities.qrcode.LectureQRCode;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;
import de.fhkiel.campustracer.model.CheckInEntry;
import de.fhkiel.campustracer.model.KeysEnum;
import de.fhkiel.campustracer.model.KeystoreEntry;
import de.fhkiel.campustracer.model.RolesEnum;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;
import de.fhkiel.campustracer.service.AppService;
import lombok.Getter;


public class User {
	private static final String TAG = "User";
	protected final AppService appService = AppService.getInstance();

	protected final CryptoWrapperDataInterface cryptoWrapperService =
		AppService.getInstance().getCryptoWrapperService();
	@Getter
	protected final long userId;
	@Getter
	protected final KeyPair masterKeyPair; //used for signing

	private final Map<LocalDate, List<KeyPair>> derivedCheckInKeyPairs;
	// <"YYYY-MM-DD", List<KeyPairs>>, should be stored in persistent storage, otherwise
	// intraDayNonces would not work

	protected User(final long userId) {
		this.userId = userId;
		this.masterKeyPair = CryptoWrapper.generateKeypair();
		this.derivedCheckInKeyPairs = new HashMap<>();
	}

	// fromStorage
	protected User(
		long userId,
		KeyPair masterKeyPair,
		List<Pair<LocalDate, KeyPair>> derivedCheckInKeyPairsList
	) {
		this.userId = userId;
		this.masterKeyPair = masterKeyPair;
		this.derivedCheckInKeyPairs = new HashMap<>();
		for (Pair<LocalDate, KeyPair> derivedCheckInKeyPair : derivedCheckInKeyPairsList) {
			this.addCheckInKeyPair(derivedCheckInKeyPair.getValue0(),
				derivedCheckInKeyPair.getValue1()
			);
		}
	}

	public Boolean isStudent() {
		return (this instanceof Student);
	}

	public Boolean isLecturer() {
		return (this instanceof Lecturer);
	}

	public PublicKey getMasterPublicKey() {
		return this.masterKeyPair.getPublic();
	}

	public void addCheckInKeyPair(LocalDate date, KeyPair keypair) {

		if (this.derivedCheckInKeyPairs.containsKey(date)) {
			// add keys to existing date entry
			this.derivedCheckInKeyPairs.get(date).add(keypair);
			return;
		}
		// create new map-entry for date
		ArrayList<KeyPair> temp = new ArrayList<KeyPair>();
		temp.add(keypair);
		this.derivedCheckInKeyPairs.put(date, temp);
	}

	protected UserMasterKeyEntry generateUserMasterKeyEntry(
		LocalDateTime registryDate,
		PublicKey universityPubKey,
		List<PublicKey> trustedPartiesPubKeys
	) throws CryptoEncryptionException {
		//create User Map Entry for Mocking
		this.cryptoWrapperService.addUserEntryForMocking(new UserEntry(this.getUserId(),
			(this.isLecturer() ? "Lecturer" : "Student") + this.getUserId(),
			this.isLecturer() ? RolesEnum.LECTURER : RolesEnum.STUDENT,
			registryDate,
			new byte[]{0, 1, 2}
		));

		// Encrypt userMasterPrivKey with TPs and Uni PubKeys
		byte[] userMasterPrivKeyBytes =
			CryptoWrapper.serializePrivKey(this.masterKeyPair.getPrivate());

		ThresholdCryptoSystem threshCryptoSys = new ThresholdCryptoSystem(userMasterPrivKeyBytes,
			trustedPartiesPubKeys,
			trustedPartiesPubKeys.size()
		);
		byte[] userMasterPrivKeyEncryptedBytes =
			CryptoWrapper.encrypt(threshCryptoSys.toBytes(), universityPubKey);

		return UserMasterKeyEntry.createSigned(this.getUserId(),
			this.masterKeyPair.getPublic(),
			userMasterPrivKeyEncryptedBytes,
			registryDate,
			this.masterKeyPair.getPrivate(),
			null
		);
	}

	public void checkInWithQRCode(
		byte[] qrCodePayload, PublicKey universityPubKey, final LocalDate checkInDate
	) throws CryptoKeyDeserializationException, CryptoEncryptionException, ApiHttpException,
		CryptoDataDeserializationException {
		LectureQRCode lectureQRCode = LectureQRCode.fromBytes(qrCodePayload);

		// Decode QR-Code Object
		if (!checkInDate.equals(lectureQRCode.getLectureDate())) {
			throw new IllegalArgumentException(
				"Mismatch between LectureQRCode Date and given Check-In Date!");
		}

		// Derive a new Check-In KeyPair
		KeyPair newDerivedCheckInKeyPair = this.getNewCheckInKey(lectureQRCode.getLectureDate());
		this.addCheckInKeyPair(lectureQRCode.getLectureDate(), newDerivedCheckInKeyPair);
		// Generate & add UserAttendedLectureEntry
		this.appService.getApiService().addCheckInEntry(new CheckInEntry(this.userId,
			newDerivedCheckInKeyPair.getPublic(),
			Optional.of(newDerivedCheckInKeyPair.getPrivate()),
			this.userId,
			this.masterKeyPair.getPrivate(),
			lectureQRCode.getLecturePubKey(),
			lectureQRCode.getLecturePrivKeyEncryptedWithUniPubKey(),
			lectureQRCode.getLecturePrivKeyHashRandomized(),
			lectureQRCode.getLectureDate(),
			universityPubKey
		));

		// Write CheckIn Key to DB
		this.appService.getDatabaseService().addToKeystore(new KeystoreEntry(0,
			KeysEnum.CHECK_IN_KEYPAIR,
			LocalDateTime.of(lectureQRCode.getLectureDate(), LocalTime.now()),
			LocalDateTime.of(lectureQRCode.getLectureDate(), LocalTime.now()),
			null,
			newDerivedCheckInKeyPair.getPrivate(),
			newDerivedCheckInKeyPair.getPublic()
		));
	}

	/**
	 * Creates a new CheckInKey for a Date
	 * <p>
	 * Enforces that the first 25 keys have different random nonces (repeats until new unique
	 * key is
	 * found). If number of existing keys for that date >= 25, just create a new key with random
	 * nonce.
	 */
	private KeyPair getNewCheckInKey(LocalDate lectureDate) {
		SecureRandom srand = new SecureRandom();

		List<KeyPair> keyListForDate =
			this.derivedCheckInKeyPairs.getOrDefault(lectureDate, new ArrayList<>());
		int numExistingKeysForDate = keyListForDate.size();
		boolean keyNumOverRandmax =
			numExistingKeysForDate >= CampusTracerParameters.CHECK_IN_KEY_NONCE_RANDMAX;

		do {
			final KeyPair newCIK =
				CryptoWrapper.deriveCheckInKeypair(this.masterKeyPair.getPrivate(),
					lectureDate,
					srand.nextInt(CampusTracerParameters.CHECK_IN_KEY_NONCE_RANDMAX)
					// nextInt: number between 0 (incl) and N (excl) => 0 - 24
				);
			if (keyNumOverRandmax) {
				// 25 keys already, uniqueness not enforceable
				Log.w(TAG,
					"CIK COUNT >= "
						+ CampusTracerParameters.CHECK_IN_KEY_NONCE_RANDMAX
						+ ". IGNORE COLLISIONS!"
				);
				return newCIK;
			} else {
				// < 25 keys, enforce uniqueness
				// (repeat until a key is found that has not been used yet)
				long equalKeys = keyListForDate.stream()
					.filter(x -> Arrays.equals(CryptoWrapper.serializePubKey(x.getPublic()),
						CryptoWrapper.serializePubKey(newCIK.getPublic())
					))
					.count();

				// key has not been used before, return it
				if (equalKeys == 0) {
					Log.w(TAG, "NEW UNIQUE CIK FOUND!");
					return newCIK;
				}
				Log.w(TAG, "CIK COLLISION, REPEAT");
			}
		} while (true);
	}

	public List<KeyPair> surrenderCheckInKeyPairsForDates(final List<LocalDate> dates) {
		// return all possible CIKs (every date, every nonce (0-24))
		List<KeyPair> checkInKeyPairs = new ArrayList<>();
		for (LocalDate date : dates) {
			for (int i = 0; i < CampusTracerParameters.CHECK_IN_KEY_NONCE_RANDMAX; i++) {
				checkInKeyPairs.add(CryptoWrapper.deriveCheckInKeypair(this.masterKeyPair.getPrivate(),
					date,
					i
				));
			}
		}
		Log.w(TAG,
			"Surrender " + checkInKeyPairs.size() + " keys for " + dates.size() + " " + "days!"
		);
		return checkInKeyPairs;
	}
}

