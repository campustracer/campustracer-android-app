package de.fhkiel.campustracer.entities;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;


public class University {
	private static final University instance = new University();
	private KeyPair masterKeyPair;

	private University() {
	}

	public static University getInstance() {
		return University.instance;
	}

	public void generateKeyPair() {
		if (this.masterKeyPair == null) {
			this.masterKeyPair = CryptoWrapper.generateKeypair();
		}
		// directly initializing masterKeyPair in "public University()"
		// does not work, as setting the University Instance in
		// a static context cannot throw exceptions
	}

	public PublicKey getPublicKey() {
		return this.masterKeyPair.getPublic();
	}

	public PrivateKey getPrivateKey() {
		return this.masterKeyPair.getPrivate();
	}
}
