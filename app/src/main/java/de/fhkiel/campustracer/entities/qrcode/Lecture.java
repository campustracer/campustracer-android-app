package de.fhkiel.campustracer.entities.qrcode;

import java.io.Serializable;
import java.security.KeyPair;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.time.LocalDate;

import de.fhkiel.campustracer.CampusTracerParameters;
import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.helper.BytesPackage;
import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
public class Lecture implements Serializable {
	@Getter
	protected final LocalDate lectureDate;
	@Getter
	protected final PublicKey lecturePubKey;
	// no getter
	protected final byte[] lecturePrivKeyHash;
	@Getter
	protected final byte[] lecturePrivKeyEncryptedWithUniPubKey;
	@Getter
	protected final String checkInAuthKey;

	private static final SecureRandom SECURE_RANDOM = new SecureRandom();

	public Lecture(
		PublicKey universityPubKey, LocalDate lectureDate, String checkInAuthKey
	) throws CryptoEncryptionException {
		// KeyGen
		KeyPair lectureKeyPair = CryptoWrapper.generateKeypair();
		this.lectureDate = lectureDate;
		this.lecturePubKey = lectureKeyPair.getPublic();
		this.lecturePrivKeyHash =
			CryptoWrapper.hash(CryptoWrapper.serializePrivKey(lectureKeyPair.getPrivate()));
		this.lecturePrivKeyEncryptedWithUniPubKey =
			CryptoWrapper.encrypt(CryptoWrapper.serializePrivKey(lectureKeyPair.getPrivate()),
				universityPubKey
			);
		this.checkInAuthKey = checkInAuthKey;
	}

	public LectureQRCode getNextLectureQRCode() {
		return new LectureQRCode(this);
	}

	public byte[] getNextLecturePrivKeyHashRandomized() {
		if (this.lecturePrivKeyHash == null) {
			throw new IllegalArgumentException(
				"This QR-Code has been scanned and cannot be used to generate new codes!");
		}
		return CryptoWrapper.hash((new BytesPackage(this.lecturePrivKeyHash,
			BytesPackage.intToBytes(SECURE_RANDOM.nextInt(CampusTracerParameters.RANDOMIZED_LECTUREPRIVKEY_HASH_RANDMAX))
			// random int between 1 (inclusive) and N (exclusive) (prevents grouping)
		)).toBytes());
	}
}
