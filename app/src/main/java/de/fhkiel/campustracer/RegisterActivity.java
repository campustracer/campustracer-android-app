package de.fhkiel.campustracer;

import static de.fhkiel.campustracer.CampusTracerParameters.UNIVERSITY_PUBLIC_KEY_CREATED_AT;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import org.javatuples.Pair;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.entities.Lecturer;
import de.fhkiel.campustracer.entities.Student;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;
import de.fhkiel.campustracer.model.KeysEnum;
import de.fhkiel.campustracer.model.KeystoreEntry;
import de.fhkiel.campustracer.model.OptionsEnum;
import de.fhkiel.campustracer.model.PreGeneratedEncryptionKeyEntry;
import de.fhkiel.campustracer.model.RolesEnum;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;
import de.fhkiel.campustracer.ui.dialogs.InfoDialog;
import de.fhkiel.campustracer.ui.dialogs.LoaderDialog;
import de.fhkiel.campustracer.ui.loader.LoaderFragment;
import de.fhkiel.campustracer.ui.loader.LoaderViewModel;
import de.fhkiel.campustracer.ui.login.LoginFagmentHandlerInterface;
import de.fhkiel.campustracer.ui.login.LoginFragment;
import de.fhkiel.campustracer.ui.qr_display.QrCodeDisplayFragment;
import de.fhkiel.campustracer.ui.qr_display.QrCodeDisplayFragmentInterface;
import de.fhkiel.campustracer.ui.qr_scanner.QrScannerFragment;
import de.fhkiel.campustracer.ui.qr_scanner.QrScannerFragmentInterface;


/*
 1. Authenticate and get own role -> /User/userid
 2. Student/Lecturer: generate Keys
	 2a. Post /UserMasterKey/
 */
public class RegisterActivity extends AppCompatActivity
	implements LoginFagmentHandlerInterface, QrScannerFragmentInterface,
	QrCodeDisplayFragmentInterface {
	private static final String TAG = "RegisterActivity";
	private AppInitializer appInitializer;
	private LoaderViewModel mLoaderViewModel;

	@Nullable
	private UserState tempUserState;

	public RegisterActivity() {
		super(R.layout.activity_register2);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.appInitializer = AppInitializer.getInstance(this);
		this.getSupportActionBar().setTitle(R.string.title_login);
		this.mLoaderViewModel = new ViewModelProvider(this).get(LoaderViewModel.class);

		// Open Login Fragment
		if (savedInstanceState == null) {
			this.getSupportFragmentManager()
				.beginTransaction()
				.setReorderingAllowed(true)
				.replace(R.id.fragment_container_view, LoginFragment.class, null)
				.commit();
		}
	}

	@Override
	public void onLoginSuccess(UserState userState) {
		Log.w(TAG, "USER: " + userState.getUsername());
		this.closeSoftKeyboard();

		this.tempUserState = userState;

		this.performAfterLoginActionDependingOnRole();
	}

	private void performAfterLoginActionDependingOnRole() {

		switch (this.tempUserState.getRole()) {
			case STUDENT:
			case LECTURER:
				/*
				 *
				 * 0.) Update Contact Data Prompt
				 */

				final EditText contactDataEditText = new EditText(this);
				contactDataEditText.setInputType(InputType.TYPE_CLASS_PHONE);
				contactDataEditText.setHint(R.string.phone_number);
				contactDataEditText.setText("+49 1234 567 " + this.tempUserState.getUserId());

				new InfoDialog(this,
					R.string.update_contact_information,
					R.string.please_enter_phone_number
				).setView(contactDataEditText)
					.setPositiveButton(R.string.button_ok, (dialog0, which0) -> {
						dialog0.dismiss();
						String enteredPhoneNumber = contactDataEditText.getText().toString();

						new Thread(() -> {
							LoaderDialog contactDataLoaderDialog =
								new LoaderDialog(R.string.updating_contact_data, this);
							contactDataLoaderDialog.show();
							try {

								String encContactData =
									Base64.getEncoder().encodeToString(CryptoWrapper.encrypt(
										enteredPhoneNumber,
										CampusTracerParameters.getUniversityPublicKey()
									));
								this.appInitializer.getAppService()
									.getApiService()
									.putEncContactDataForUsername(this.tempUserState.getUsername(),
										encContactData
									);

								/*
								 * 1C.) Show Choice Dialog: Generate new Keys _OR_ Scan Master Key
								  QR-Code?
								 */
								contactDataLoaderDialog.dismiss();
								new InfoDialog(this,
									R.string.encryption_key_generation
								).setSingleChoiceItems(Arrays.asList(
									R.string.choice_generate_new_keys, // = 0
									R.string.choice_scan_master_qr_code // = 1
								)).setPositiveButton(R.string.button_ok, (dialog, whichButton) -> {
									// Callback when OK is pressed!
									int selectedPosition = ((AlertDialog) dialog).getListView()
										.getCheckedItemPosition();
									if (selectedPosition != -1) {
										dialog.dismiss();
									}
									Log.d(TAG, "SELECTED POS: " + selectedPosition);
									if (selectedPosition == 0) {
										this.registerWithKeyGeneration(); // Generate new
										// Keys
									} else {
										this.showQrCodeScannerFragment(); // Scan
										// MasterPrivKey
										// QR-Code
									}
								}).show();
							} catch (CryptoEncryptionException | ApiHttpException e) {
								e.printStackTrace();
								new InfoDialog(this, e).setPositiveButton(R.string.button_ok,
									(dialog200, which200) -> {
										dialog200.dismiss();
										// BACK TO LOGIN
										this.restartActivityAtLogin();
									}
								).show();
							} finally {
								contactDataLoaderDialog.dismiss();
							}
						}).start();
					})
					.show();
				this.runOnUiThread(() -> {
					// Fix Layout (EditText is originally too wide)
					float displayDensity = this.getResources().getDisplayMetrics().density;
					FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
						LinearLayout.LayoutParams.MATCH_PARENT,
						FrameLayout.LayoutParams.MATCH_PARENT
					);
					layoutParams.setMargins(Math.round(32 * displayDensity),
						0,
						Math.round(32 * displayDensity),
						0
					);
					contactDataEditText.setLayoutParams(layoutParams);
				});

				break;
			case UNIVERSITY:
				this.showQrCodeScannerFragment();
				break;
		}
	}

	private void showQrCodeScannerFragment() {
		this.runOnUiThread(() -> this.getSupportActionBar()
			.setTitle(R.string.choice_scan_master_qr_code));
		Bundle qrBundle = new Bundle();
		qrBundle.putString(QrScannerFragment.SCAN_MSG_KEY,
			this.getString(R.string.choice_scan_master_qr_code)
		);
		this.getSupportFragmentManager()
			.beginTransaction()
			.setReorderingAllowed(true)
			.replace(R.id.fragment_container_view, QrScannerFragment.class, qrBundle)
			.commit();

		// forwards to Callback this::onSuccessfulQrScan
	}

	@Override
	public void onSuccessfulQrScan(String scannedCode) {
		// Callback after QRCode Scanning
		// (when Keys are to be recreated from QR-Code)

		Log.d(TAG, "onSuccessfulScan RESULT: " + scannedCode);

		// check if scanned code is a PrivKey
		KeyPair masterKeyPair;
		try {
			PrivateKey masterPrivKey =
				CryptoWrapper.deserializePrivKey(Base64.getDecoder().decode(scannedCode));
			PublicKey masterPubKey = CryptoWrapper.calculatePubKeyFromPrivKey(masterPrivKey);
			masterKeyPair = new KeyPair(masterPubKey, masterPrivKey);

			this.registerWithScannedMasterKeyPair(masterKeyPair);

			Log.d(TAG, "SCANNED PRIVKEY IS VALID!");
		} catch (CryptoKeyDeserializationException e) {
			Log.e(TAG, "SCANNED PRIVKEY IS INVALID!");
			e.printStackTrace();

			new InfoDialog(this, e).setPositiveButton(R.string.retry, (dialog, which) -> {
				dialog.dismiss();
				this.showQrCodeScannerFragment(); // BACK TO QR-SCANNER FRAGMENT (new instance)
			})
				.setNegativeButton(R.string.button_abort, (dialog, which) -> {
					dialog.dismiss();
					this.restartActivityAtLogin(); // ABORT and recreate this Activity (back to
					// Login)
				})
				.replaceErrorResource(R.string.decryption_error_title, R.string.qr_scan_error)
				.replaceErrorResource(R.string.decryption_error_msg,
					R.string.qr_scan_fail_no_privkey
				)
				.show();
		}
	}

	private void registerWithScannedMasterKeyPair(KeyPair masterKeyPair) {
		this.showLoaderFragment();

		new Thread(() -> {
			if (this.tempUserState == null) {
				// Error Message if tempUserState missing (should not happen normally)
				this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.ERROR);
				new InfoDialog(this, R.string.error).setPositiveButton(R.string.button_ok,
					(dialog, which) -> {
						dialog.dismiss();
						this.restartActivityAtLogin();
					}
				).show();
			}

			/*
			 * PrivateKey was scanned and parsed successfully, now proceed depending on role
			 * - University:
			 * 		[X] check MKP^P against UK^P
			 * 	    [X] store MKP
			 * - Lecturer & Student:
			 * 		[X] check if this is the currently valid key pair
			 * 	    [X] store MKP
			 * - Student:
			 *      [X] pull preGenKeys from DB
			 *      [X] recreate preGenKeys^S
			 *      [X] store preGenKeyPairs
			 */

			try {
				KeystoreEntry masterKeystoreEntry;
				List<KeystoreEntry> recreatedPreGenKeystoreEntries = new ArrayList<>();

				if (this.tempUserState.getRole() == RolesEnum.UNIVERSITY) {
					// Check MKP^P against UK^P
					if (!Arrays.equals(CryptoWrapper.serializePubKey(masterKeyPair.getPublic()),
						CryptoWrapper.serializePubKey(CampusTracerParameters.getUniversityPublicKey())
					)) {
						Log.e(TAG, "University Pub Key mismatch!");
						//Show Error Symbol and AlertDialog
						this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.ERROR);
						this.showErrorDialogAndRedirectToQrCodeScannerFragment(R.string.error,
							R.string.uni_key_mismatch
						);
						this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.ERROR);
						return;
					}

					masterKeystoreEntry = new KeystoreEntry(0,
						KeysEnum.MASTER_KEYPAIR,
						UNIVERSITY_PUBLIC_KEY_CREATED_AT,
						UNIVERSITY_PUBLIC_KEY_CREATED_AT,
						null,
						masterKeyPair.getPrivate(),
						masterKeyPair.getPublic()
					);
				} else if (this.tempUserState.getRole() == RolesEnum.STUDENT
					|| this.tempUserState.getRole() == RolesEnum.LECTURER) {

					// CHECK IF THE MKP IS THE CURRENTLY VALID MKP! (PROHIBITS SCANNING OLD QRCODE)
					List<UserMasterKeyEntry> userMasterKeyEntryList =
						this.appInitializer.getAppService()
							.getApiService()
							.getCurrentlyValidMasterKeyEntriesByUserId(this.tempUserState.getUserId());
					if (userMasterKeyEntryList.isEmpty()) {
						this.showErrorDialogAndRedirectToLogin(R.string.error,
							R.string.no_master_key_in_backend
						);
						this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.ERROR);
						return;
					}
					if (userMasterKeyEntryList.size() > 1) {
						this.showErrorDialogAndRedirectToLogin(R.string.error,
							R.string.multiple_valid_master_keys_in_backend
						);
						this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.ERROR);
						return;
					}
					UserMasterKeyEntry userMasterKeyEntry = userMasterKeyEntryList.get(0);
					if (!Arrays.equals(CryptoWrapper.serializePubKey(masterKeyPair.getPublic()),
						CryptoWrapper.serializePubKey(userMasterKeyEntry.getMasterPubKey())
					)) {
						this.showErrorDialogAndRedirectToLogin(R.string.error,
							R.string.scanned_master_key_not_currently_valid
						);
						this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.ERROR);
						return;
					}
					if (!userMasterKeyEntry.verifySignature()) {
						this.showErrorDialogAndRedirectToLogin(R.string.error,
							R.string.umkentry_signature_mismatch
						);
						this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.ERROR);
						return;
					}
					masterKeystoreEntry = new KeystoreEntry(0,
						KeysEnum.MASTER_KEYPAIR,
						userMasterKeyEntry.getCreatedAt(),
						userMasterKeyEntry.getCreatedAt(),
						null,
						masterKeyPair.getPrivate(),
						masterKeyPair.getPublic()
					);

					/*
					//RECREATE CheckInKeys
					LocalDateTime startDate = userMasterKeyEntry.getCreatedAt();
					LocalDateTime endDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
					List<Pair<LocalDateTime, KeyPair>> checkInKeyPairList =
						ContactTracing.recreateDerivedCheckInKeyPairsWithDateAttribute
						(masterKeyPair
								.getPrivate(),
							DateHelper.getDatesListFromDateRange(startDate.toLocalDate(),
								endDate.toLocalDate()
							)
						);
					List<KeystoreEntry> checkInKeystoreList =
						checkInKeyPairList.stream().map(x -> new KeystoreEntry(0,
							KeysEnum.CHECK_IN_KEYPAIR,
							x.getValue0(),
							x.getValue0(),
							null,
							x.getValue1().getPrivate(),
							x.getValue1().getPublic()
						)).collect(Collectors.toList());
					for (KeystoreEntry ke : checkInKeystoreList) {
						this.appInitializer.getAppService().getDatabaseService().addToKeystore(ke);
					}
					*/
					if (this.tempUserState.getRole() == RolesEnum.STUDENT) {
						// Recreate PreGenKeys
						for (PreGeneratedEncryptionKeyEntry preGenKeyEntry :
							userMasterKeyEntry.getPreGeneratedEncryptionKeys()) {
							// Recreate Private Key
							KeyPair recreatedKeyPair = CryptoWrapper.derivePreGeneratedKeypair(
								masterKeyPair.getPrivate(),
								preGenKeyEntry.getValidFrom(),
								preGenKeyEntry.getValidTo()
							);

							// compare pub keys
							if (!Arrays.equals(
								CryptoWrapper.serializePubKey(recreatedKeyPair.getPublic()),
								CryptoWrapper.serializePubKey(preGenKeyEntry.getPreGeneratedPubKey())
							)) {
								this.showErrorDialogAndRedirectToLogin(R.string.error,
									R.string.error_pregenkey_recreate_mismatch
								);
								this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.ERROR);
								return;
							}
							if (!preGenKeyEntry.verifySignature(masterKeyPair.getPublic())) {
								this.showErrorDialogAndRedirectToLogin(R.string.error,
									R.string.error_pregen_masterkey_signature_mismatch
								);
								this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.ERROR);
								return;
							}
							recreatedPreGenKeystoreEntries.add(new KeystoreEntry(0,
								KeysEnum.PRE_GEN_KEYPAIR,
								userMasterKeyEntry.getCreatedAt(),
								preGenKeyEntry.getValidFrom(),
								preGenKeyEntry.getValidTo(),
								recreatedKeyPair.getPrivate(),
								recreatedKeyPair.getPublic()
							));
						}
					}
				} else {
					throw new IllegalStateException("UNKNOWN ROLES ENUM TYPE!");
				}

				// Store MasterKey
				long masterKeystoreEntryId = this.appInitializer.getAppService()
					.getDatabaseService()
					.addToKeystore(masterKeystoreEntry);

				this.appInitializer.getAppService()
					.getDatabaseService()
					.setOption(OptionsEnum.KEYSTORE_CURRENT_MASTER_KEY_ID,
						Long.toString(masterKeystoreEntryId)
					);

				if (this.tempUserState.getRole() == RolesEnum.STUDENT) {
					// store PreGenKeys
					for (KeystoreEntry preGenKeyStoreEntry : recreatedPreGenKeystoreEntries) {
						this.appInitializer.getAppService()
							.getDatabaseService()
							.addToKeystore(preGenKeyStoreEntry);
					}
				}

				// Commit temporary UserState to DB
				this.appInitializer.getAppService().setUserState(this.tempUserState);

				// show loader, wait 2s, show SUCCESS Image, wait 1s, forward to UserActivity
				Handler handler = new Handler(Looper.getMainLooper());
				handler.postDelayed(() -> {
					this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.SUCCESS);
					handler.postDelayed(this::forwardToUserActivity, 1000);
				}, 1000);
			} catch (ApiHttpException e) {
				this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.ERROR);
				e.printStackTrace();

				int errorTitleR = R.string.error;
				int errorMessageR = R.string.unexpected_error_msg;
				if (e.statusCode == 401) {
					errorTitleR = R.string.auth_error_title;
					errorMessageR = R.string.auth_error_msg;
				} else if (Boolean.TRUE.equals(e.isConnectionError())) {
					errorTitleR = R.string.network_error_title;
					errorMessageR = R.string.network_error_msg;
				}
				this.showErrorDialogAndRedirectToLogin(errorTitleR, errorMessageR);
			}
		}).start();
	}

	private void registerWithKeyGeneration() {
		this.showLoaderFragment();

		if (this.tempUserState == null) {
			// Error Message if tempUserState missing (should not happen normally)
			this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.ERROR);
			new InfoDialog(this, R.string.error, R.string.unexpected_error_msg).setPositiveButton()
				.show();
		}

		/*
		 * Generate new MasterPrivKeyEntry depending on role
		 * - University: DOES NOT APPLY
		 * - Lecturer: KeyGen, store MKP
		 * - Student: KeyGen, store MKP + preGenKeys
		 */

		new Thread(() -> {
			try {
				KeyPair masterKeyPair;
				UserMasterKeyEntry userMasterKeyEntry;
				List<KeystoreEntry> preGenKeystoreEntries = new ArrayList<>();

				if (this.tempUserState.getRole() == RolesEnum.LECTURER) {
					Lecturer lecturer = new Lecturer(this.tempUserState.getUserId());
					userMasterKeyEntry = lecturer.registerLecturer(LocalDateTime.now()
							.truncatedTo(ChronoUnit.SECONDS),
						CampusTracerParameters.getUniversityPublicKey(),
						CampusTracerParameters.getTrustedPartyPublicKeyList()
					);
					masterKeyPair = lecturer.getMasterKeyPair();
				} else if (this.tempUserState.getRole() == RolesEnum.STUDENT) {

					// MKP + PreGenKeys
					Student student = new Student(this.tempUserState.getUserId());
					userMasterKeyEntry =
						student.registerStudent(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
							CampusTracerParameters.getUniversityPublicKey(),
							CampusTracerParameters.getTrustedPartyPublicKeyList()
						);
					masterKeyPair = student.getMasterKeyPair();

					// preGenKeys
					for (Map.Entry<Pair<LocalDateTime, LocalDateTime>, KeyPair> preGenKeyMapEntry
						: student
						.getPreGeneratedKeyPairs()
						.entrySet()) {
						preGenKeystoreEntries.add(new KeystoreEntry(0,
							KeysEnum.PRE_GEN_KEYPAIR,
							userMasterKeyEntry.getCreatedAt(),
							preGenKeyMapEntry.getKey().getValue0(),
							preGenKeyMapEntry.getKey().getValue1(),
							preGenKeyMapEntry.getValue().getPrivate(),
							preGenKeyMapEntry.getValue().getPublic()
						));
					}
				} else {
					throw new IllegalStateException(
						"UNIVERSITY MAY NOT REGISTER WITH KEY GENERATION");
				}

				// Save MasterKeyPair To DB
				KeystoreEntry masterKeystoreEntry = new KeystoreEntry(0,
					KeysEnum.MASTER_KEYPAIR,
					userMasterKeyEntry.getCreatedAt(),
					userMasterKeyEntry.getCreatedAt(),
					userMasterKeyEntry.getInvalidatedAt(),
					masterKeyPair.getPrivate(),
					masterKeyPair.getPublic()
				);
				Long masterKeyId = this.appInitializer.getAppService()
					.getDatabaseService()
					.addToKeystore(masterKeystoreEntry);
				this.appInitializer.getAppService()
					.getDatabaseService()
					.setOption(OptionsEnum.KEYSTORE_CURRENT_MASTER_KEY_ID,
						Long.toString(masterKeyId)
					);

				if (this.tempUserState.getRole() == RolesEnum.STUDENT) {
					for (KeystoreEntry preGenEntry : preGenKeystoreEntries) {
						this.appInitializer.getAppService()
							.getDatabaseService()
							.addToKeystore(preGenEntry);
					}
				}

				// commit temporary UserState to database
				this.appInitializer.getAppService().setUserState(this.tempUserState);

				// Show Master Private Key QR-Code
				String qrCodePayload = Base64.getEncoder()
					.encodeToString(CryptoWrapper.serializePrivKey(masterKeyPair.getPrivate()));
				Handler handler = new Handler(Looper.getMainLooper());
				handler.postDelayed(() -> {
					this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.SUCCESS);
					handler.postDelayed(() -> this.showQrCodeDisplayFragment(qrCodePayload), 1000);
				}, 2000);
			} catch (CryptoEncryptionException e) {
				e.printStackTrace();
				this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.ERROR);
				this.showErrorDialogAndRedirectToKeyGen(R.string.error, R.string.keygen_exception);
			} catch (ApiHttpException e) {
				e.printStackTrace();

				int errorTitleR = R.string.error;
				int errorMessageR = R.string.unexpected_error_msg;
				if (e.statusCode == 401) {
					errorTitleR = R.string.auth_error_title;
					errorMessageR = R.string.auth_error_msg;
				} else if (Boolean.TRUE.equals(e.isConnectionError())) {
					errorTitleR = R.string.network_error_title;
					errorMessageR = R.string.network_error_msg;
				}
				this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.ERROR);
				this.showErrorDialogAndRedirectToKeyGen(errorTitleR, errorMessageR);
			}
		}).start();
	}

	private void showQrCodeDisplayFragment(String qrCodePayload) {
		this.runOnUiThread(() -> this.getSupportActionBar().setTitle(R.string.master_priv_key));

		Bundle qrCodeDisplayBundle = new Bundle();
		qrCodeDisplayBundle.putString(QrCodeDisplayFragment.QR_CODE_PAYLOAD_KEY, qrCodePayload);
		qrCodeDisplayBundle.putString(QrCodeDisplayFragment.TITLE_KEY,
			this.getString(R.string.your_master_priv_key)
		);
		qrCodeDisplayBundle.putString(QrCodeDisplayFragment.DESCRIPTION_KEY,
			this.getString(R.string.scan_master_key_description)
		);
		qrCodeDisplayBundle.putBoolean(QrCodeDisplayFragment.SHOW_SAVE_BUTTON_KEY, true);
		qrCodeDisplayBundle.putBoolean(QrCodeDisplayFragment.SHOW_CLOSE_BUTTON_KEY, true);

		this.getSupportFragmentManager()
			.beginTransaction()
			.setReorderingAllowed(true)
			.replace(R.id.fragment_container_view, QrCodeDisplayFragment.class,
				qrCodeDisplayBundle)
			.commit();
	}

	private void showLoaderFragment() {
		this.runOnUiThread(() -> this.getSupportActionBar()
			.setTitle(R.string.encryption_key_generation));
		this.getSupportFragmentManager()
			.beginTransaction()
			.setReorderingAllowed(true)
			.replace(R.id.fragment_container_view,
				new LoaderFragment(R.string.encryption_key_generation)
			)
			.commit();
		this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.LOADING);
	}

	private void forwardToUserActivity() {
		Intent intent = new Intent(this, UserActivity.class);
		this.finish();
		DownloadWorker.startOneTimeWorker(this.appInitializer.getWorkManager());
		this.startActivity(intent);
	}

	private void showErrorDialogAndRedirectToKeyGen(
		@StringRes int titleR, @StringRes int messageR
	) {
		new InfoDialog(this, titleR, messageR).setPositiveButton(R.string.retry,
			(dialog, which) -> {
				dialog.dismiss();
				this.registerWithKeyGeneration();
			}
		).setNegativeButton(R.string.button_abort, (dialog, which) -> {
			// ABORT and recreate this Activity (back to Login)
			dialog.dismiss();
			this.restartActivityAtLogin();
		}).show();
	}

	private void showErrorDialogAndRedirectToQrCodeScannerFragment(
		@StringRes int titleR, @StringRes int messageR
	) {
		new InfoDialog(this, titleR, messageR).setPositiveButton(R.string.retry,
			(dialog, which) -> {
				dialog.dismiss();
				// BACK TO QR-SCANNER FRAGMENT (new instance)
				this.showQrCodeScannerFragment();
			}
		).setNegativeButton(R.string.button_abort, (dialog, which) -> {
			dialog.dismiss();
			// ABORT and recreate this Activity (back to Login)
			this.restartActivityAtLogin();
		}).show();
	}

	private void showErrorDialogAndRedirectToLogin(
		@StringRes int titleR, @StringRes int messageR
	) {
		new InfoDialog(this, titleR, messageR).setPositiveButton(R.string.button_ok,
			(dialog, which) -> {
				dialog.dismiss();
				// BACK TO LOGIN
				this.restartActivityAtLogin();
			}
		).show();
	}

	private void restartActivityAtLogin() {
		// must also reset state
		this.tempUserState = null;
		this.finish();
		this.startActivity(this.getIntent());
	}

	private void closeSoftKeyboard() {

		View focusedView = this.getCurrentFocus();
		if (focusedView != null) {

			InputMethodManager manager =
				(InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
			manager.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
		}
	}

	@Override
	public void onQrCodeDisplayFragmentCloseButtonClick() {
		// Done with Register Activity
		this.forwardToUserActivity();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.restartActivityAtLogin();
	}
}