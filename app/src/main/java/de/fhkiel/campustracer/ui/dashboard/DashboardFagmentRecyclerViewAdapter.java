package de.fhkiel.campustracer.ui.dashboard;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.bouncycastle.util.encoders.Hex;

import java.util.Arrays;
import java.util.List;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.databinding.FragmentDashboardItemBinding;
import de.fhkiel.campustracer.helper.DateHelper;
import de.fhkiel.campustracer.model.KeystoreEntry;


public class DashboardFagmentRecyclerViewAdapter
	extends RecyclerView.Adapter<DashboardFagmentRecyclerViewAdapter.ViewHolder> {

	private final List<KeystoreEntry> mValues;

	public DashboardFagmentRecyclerViewAdapter(
		List<KeystoreEntry> items
	) {
		this.mValues = items;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new ViewHolder(FragmentDashboardItemBinding.inflate(LayoutInflater.from(parent.getContext()),
			parent,
			false
		));
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, int position) {
		holder.mItem = this.mValues.get(position);
		holder.mCheckInDate.setText(DateHelper.getISODateTimeString(holder.mItem.getCreatedAt()));
		String pkStr = Hex.toHexString(CryptoWrapper.serializePubKey(holder.mItem.getPublicKey()));
		int pkStrLen = pkStr.length() / 3;
		holder.mDashItemCheckInPubKey.setText(String.join("\n",
			Arrays.asList(pkStr.substring(0 + 0 * pkStrLen, 1 * pkStrLen),
				pkStr.substring(0 + 1 * pkStrLen, 2 * pkStrLen),
				pkStr.substring(0 + 2 * pkStrLen, pkStr.length())
			)
		));
	}

	@Override
	public int getItemCount() {
		return this.mValues.size();
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		public final TextView mCheckInDate;
		public final TextView mDashItemCheckInPubKey;

		public KeystoreEntry mItem;

		public ViewHolder(FragmentDashboardItemBinding binding) {
			super(binding.getRoot());
			this.mCheckInDate = binding.dashItemCheckInDate;
			this.mDashItemCheckInPubKey = binding.dashItemCheckInPubKey;
		}
	}
}