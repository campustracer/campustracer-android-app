package de.fhkiel.campustracer.ui.loader;

import androidx.annotation.StringRes;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


public class LoaderViewModel extends ViewModel {
	public enum LoaderViewStage {
		LOADING, SUCCESS, ERROR
	}

	private MutableLiveData<LoaderViewStage> mStage;
	private MutableLiveData<Integer> mMessageR;

	public LoaderViewModel() {
		this.mStage = new MutableLiveData<>();
		//this.mStage.setValue(LoaderViewStage.LOADING);
		this.mMessageR = new MutableLiveData<>();
		//this.mMessageR.setValue(R.string.loading_title);
	}

	public LiveData<LoaderViewStage> getStage() {
		return this.mStage;
	}

	public LiveData<Integer> getMessage() {
		return this.mMessageR;
	}

	public void postStage(LoaderViewStage newState) {
		this.mStage.postValue(newState);
	}

	public void postMessage(@StringRes int messageR) {
		this.mMessageR.postValue(messageR);
	}
}