package de.fhkiel.campustracer.ui.loader;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import de.fhkiel.campustracer.R;
import de.fhkiel.campustracer.databinding.FragmentLoaderBinding;


public class LoaderFragment extends Fragment {
	public static final String INITIAL_MESSAGE_KEY = "INITIAL_MESSAGE_KEY";
	private LoaderViewModel mLoaderViewModel;
	private FragmentLoaderBinding binding;
	private final @StringRes
	int initialMessageR;

	public LoaderFragment() {
		this.initialMessageR = R.string.please_wait;
	}

	public LoaderFragment(@StringRes int messageR) {
		// Required empty public constructor
		this.initialMessageR = messageR;
	}

	@Override
	public View onCreateView(
		LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState
	) {
		// Inflate the layout for this fragment

		this.binding = FragmentLoaderBinding.inflate(inflater, container, false);
		View root = this.binding.getRoot();
		this.binding.messageTextView.setText(this.initialMessageR);

		if (this.getArguments() != null) {
			if (this.getArguments().containsKey(INITIAL_MESSAGE_KEY)) {
				this.binding.messageTextView.setText(this.getArguments()
					.getInt(INITIAL_MESSAGE_KEY));
			}
		}
		return root;
	}

	@Override
	public void onViewCreated(
		@NonNull View view, @Nullable Bundle savedInstanceState
	) {
		super.onViewCreated(view, savedInstanceState);

		this.mLoaderViewModel =
			new ViewModelProvider(this.requireActivity()).get(LoaderViewModel.class);

		this.mLoaderViewModel.getStage().observe(this.getViewLifecycleOwner(), item -> {
			switch (item) {
				case LOADING:
					this.binding.progressBar.setVisibility(View.VISIBLE);
					this.binding.okImageView.setVisibility(View.GONE);
					this.binding.errorImageView.setVisibility(View.GONE);
					this.binding.statusTextView.setText(R.string.please_wait);
					break;
				case SUCCESS:
					this.binding.progressBar.setVisibility(View.GONE);
					this.binding.okImageView.setVisibility(View.VISIBLE);
					this.binding.errorImageView.setVisibility(View.GONE);
					this.binding.statusTextView.setText(R.string.success);
					break;
				case ERROR:
					this.binding.progressBar.setVisibility(View.GONE);
					this.binding.okImageView.setVisibility(View.GONE);
					this.binding.errorImageView.setVisibility(View.VISIBLE);
					this.binding.statusTextView.setText(R.string.error);
					break;
			}
		});

		this.mLoaderViewModel.getMessage().observe(this.getViewLifecycleOwner(), item -> {
			this.binding.messageTextView.setText(this.getString(item));
		});
		if (this.getArguments() != null) {
			LoaderViewModel.LoaderViewStage initialState =
				LoaderViewModel.LoaderViewStage.values()[this.getArguments()
					.getInt("initialState", 0)];
			this.mLoaderViewModel.postStage(initialState);
		} else {
			this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.LOADING);
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
	}
}