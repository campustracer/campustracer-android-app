package de.fhkiel.campustracer.ui.qr_display;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import lombok.Getter;


@Getter
public class QrCodeDisplayViewModel extends ViewModel {
	private MutableLiveData<String> mTitle;
	private MutableLiveData<String> mDescription;
	private MutableLiveData<String> mQrCodePayload;
	private MutableLiveData<Boolean> showSaveButton;
	private MutableLiveData<Boolean> showCloseButton;

	public QrCodeDisplayViewModel() {
		this.mTitle = new MutableLiveData<>();
		this.mDescription = new MutableLiveData<>();
		this.mQrCodePayload = new MutableLiveData<>();
		this.showSaveButton = new MutableLiveData<>();
		this.showCloseButton = new MutableLiveData<>();
		//this.showSaveButton.setValue(false);
	}

	public void setTitle(String title) {
		this.mTitle.postValue(title);
	}

	public void setDescription(String description) {
		this.mDescription.postValue(description);
	}

	public void setQrCodePayload(String qrCodePayload) {
		this.mQrCodePayload.postValue(qrCodePayload);
	}

	public void setShowSaveButton(Boolean showSaveButton) {
		this.showSaveButton.postValue(showSaveButton);
	}

	public void setShowCloseButton(Boolean showCloseButton) {
		this.showCloseButton.postValue(showCloseButton);
	}
}