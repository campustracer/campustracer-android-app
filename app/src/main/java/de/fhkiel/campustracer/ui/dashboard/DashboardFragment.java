package de.fhkiel.campustracer.ui.dashboard;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.bouncycastle.util.encoders.Hex;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import de.fhkiel.campustracer.AppInitializer;
import de.fhkiel.campustracer.R;
import de.fhkiel.campustracer.UserState;
import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.databinding.FragmentDashboardBinding;
import de.fhkiel.campustracer.helper.DateHelper;
import de.fhkiel.campustracer.model.KeystoreEntry;
import de.fhkiel.campustracer.model.RolesEnum;


public class DashboardFragment extends Fragment {
	private AppInitializer appInitializer;

	private DashboardViewModel dashboardViewModel;
	private FragmentDashboardBinding binding;
	private final int mColumnCount = 1;

	@Override
	public View onCreateView(
		@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState
	) {
		this.appInitializer = AppInitializer.getInstance(this.getActivity());
		this.dashboardViewModel = new ViewModelProvider(this).get(DashboardViewModel.class);

		this.binding = FragmentDashboardBinding.inflate(inflater, container, false);
		View root = this.binding.getRoot();

		// setup recyclerView
		Context context = root.getContext();
		RecyclerView recyclerView = root.findViewById(R.id.dashCheckInRecyclerView);
		if (this.mColumnCount <= 1) {
			recyclerView.setLayoutManager(new LinearLayoutManager(context));
		} else {
			recyclerView.setLayoutManager(new GridLayoutManager(context, this.mColumnCount));
		}
		recyclerView.addItemDecoration(new DividerItemDecoration(this.getActivity(),
			DividerItemDecoration.VERTICAL
		));

		// Populate Bindings
		Optional<UserState> userStateOptional = this.appInitializer.getAppService().getUserState();
		if (!userStateOptional.isPresent()) {
			throw new IllegalStateException("Missing UserState");
		}
		UserState userState = userStateOptional.get();
		this.binding.dashUsername.setText(this.getString(R.string.username_userid_string,
			userState.getUsername(),
			userState.getUserId()
		));
		this.binding.dashRole.setText(RolesEnum.getStringRes(userState.getRole()));
		Optional<KeystoreEntry> userMasterKeyOpt =
			this.appInitializer.getAppService().getDatabaseService().getMasterKeyPairFromKeystore();
		if (!userMasterKeyOpt.isPresent()) {
			throw new IllegalStateException("Missing " + "UserMasterKey");
		}

		String pkStr =
			Hex.toHexString(CryptoWrapper.serializePubKey(userMasterKeyOpt.get().getPublicKey()));
		int pkStrLen = pkStr.length() / 3;
		this.binding.dashMasterPubKey.setText(String.join("\n",
			Arrays.asList(pkStr.substring(0 + 0 * pkStrLen, 1 * pkStrLen),
				pkStr.substring(0 + 1 * pkStrLen, 2 * pkStrLen),
				pkStr.substring(0 + 2 * pkStrLen, pkStr.length())
			)
		));
		if (userState.getRole() == RolesEnum.UNIVERSITY
			|| userState.getRole() == RolesEnum.LECTURER) {
			this.binding.dashKeysValidUntilString.setText(R.string.unlimited);
		} else {
			// Student
			Optional<KeystoreEntry> mostValidPreGenKeyOptional =
				this.appInitializer.getAppService()
				.getDatabaseService()
				.getPreGenKeyPairsFromKeystore()
				.stream()
				.sorted((a, b) -> b.getValidTo().compareTo(a.getValidTo()))
				.findFirst();

			if (mostValidPreGenKeyOptional.isPresent()) {
				this.binding.dashKeysValidUntilString.setText(DateHelper.getISODateTimeString(
					Objects.requireNonNull(mostValidPreGenKeyOptional.get().getValidTo())));
			} else {
				this.binding.dashKeysValidUntilString.setText(R.string.unknown);
			}
		}

		// Populate RecyclerView (or alternative message)
		if (userState.getRole() != RolesEnum.UNIVERSITY) {
			List<KeystoreEntry> checkInKeystoreEntries = this.appInitializer.getAppService()
				.getDatabaseService()
				.getCheckInKeyPairsFromKeystore()
				.stream()
				.sorted((a, b) -> b.getCreatedAt().compareTo(a.getCreatedAt()))
				.collect(Collectors.toList());
			this.binding.dashCheckInContainer.setVisibility(View.VISIBLE);
			if (checkInKeystoreEntries.size() != 0) {
				// Data
				this.binding.dashNoCheckInKeysAlttext.setVisibility(View.GONE);
				this.binding.dashCheckInRecyclerView.setVisibility(View.VISIBLE);

				this.binding.dashCheckInRecyclerView.setAdapter(new DashboardFagmentRecyclerViewAdapter(
					checkInKeystoreEntries));
			} else {
				// No Data
				this.binding.dashNoCheckInKeysAlttext.setVisibility(View.VISIBLE);
				this.binding.dashCheckInRecyclerView.setVisibility(View.GONE);
			}
		} else {
			this.binding.dashCheckInContainer.setVisibility(View.INVISIBLE);
		}
		return root;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		this.binding = null;
	}
}