package de.fhkiel.campustracer.ui.infection_case_detail;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import de.fhkiel.campustracer.AppInitializer;
import de.fhkiel.campustracer.R;
import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.databinding.FragmentInfectionCaseDetailBinding;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoDecryptionException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;
import de.fhkiel.campustracer.helper.DateHelper;
import de.fhkiel.campustracer.helper.LongUtils;
import de.fhkiel.campustracer.model.InfectionCaseEntry;
import de.fhkiel.campustracer.model.KeystoreEntry;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.model.encdata.InfectionCaseEntryEncData;
import de.fhkiel.campustracer.ui.dialogs.LoaderDialog;


/**
 * A simple {@link Fragment} subclass. Use the {@link InfectionCaseDetailFragment#newInstance}
 * factory method to create an instance of this fragment.
 */
public class InfectionCaseDetailFragment extends Fragment {
	private static final String TAG = "InfectionCaseDetailFragment";

	private static final String INFECTION_CASE_ID_KEY = "INFECTION_CASE_ID";
	private static final String CONTACT_USERS_IDS_KEY = "CONTACT_USERS_IDS_KEY";

	private int mColumnCount = 1;

	private AppInitializer appInitializer;
	private FragmentInfectionCaseDetailBinding binding;
	private Long infectionCaseId;
	private List<Long> contactUserIds;

	private InfectionCaseEntryEncData infectionCaseEntryEncData;
	private List<UserEntry> userEntryList;
	private KeyPair uniMasterKeyPair;

	protected Boolean isImageFitToScreen = false;

	public InfectionCaseDetailFragment() {

	}

	public static InfectionCaseDetailFragment newInstance(
		long infectionCaseId, List<Long> contactUserIds
	) {
		InfectionCaseDetailFragment fragment = new InfectionCaseDetailFragment();
		Bundle args = new Bundle();
		args.putLong(INFECTION_CASE_ID_KEY, infectionCaseId);
		args.putLongArray(CONTACT_USERS_IDS_KEY, LongUtils.toArray(contactUserIds));
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.appInitializer = AppInitializer.getInstance(this.getActivity());

		if (this.getArguments() != null) {
			this.infectionCaseId = this.getArguments().getLong(INFECTION_CASE_ID_KEY);
			this.contactUserIds =
				LongUtils.toList(this.getArguments().getLongArray(CONTACT_USERS_IDS_KEY));

			Optional<KeystoreEntry> uniMKPKeystoreEntry = this.appInitializer.getAppService()
				.getDatabaseService()
				.getMasterKeyPairFromKeystore();
			if (!uniMKPKeystoreEntry.isPresent()) {
				// TODO: Handle error
				Log.e(TAG, "No master key pair in database");
				return;
			}
			this.uniMasterKeyPair = uniMKPKeystoreEntry.get().getKeyPair();
		}
	}

	@Override
	public View onCreateView(
		LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState
	) {

		this.binding = FragmentInfectionCaseDetailBinding.inflate(inflater, container, false);
		View root = this.binding.getRoot();

		// RecyclerView
		Context context = root.getContext();
		RecyclerView recyclerView = root.findViewById(R.id.infection_case_detail_recycler);
		if (this.mColumnCount <= 1) {
			recyclerView.setLayoutManager(new LinearLayoutManager(context));
		} else {
			recyclerView.setLayoutManager(new GridLayoutManager(context, this.mColumnCount));
		}
		recyclerView.addItemDecoration(new DividerItemDecoration(this.getActivity(),
			DividerItemDecoration.VERTICAL
		));

		new Thread(() -> {
			LoaderDialog loaderDialog =
				new LoaderDialog(R.string.loading_infection_case, this.getActivity());
			loaderDialog.show();

			try {

				// Load Case and Users
				InfectionCaseEntry infectionCaseEntry = this.appInitializer.getAppService()
					.getApiService()
					.getInfectionCaseEntryById(this.infectionCaseId);
				this.infectionCaseEntryEncData = InfectionCaseEntryEncData.getDecryptedPayload(
					infectionCaseEntry.getId(),
					infectionCaseEntry.getEncData(),
					infectionCaseEntry.getEncMedicalProof(),
					this.uniMasterKeyPair.getPrivate()
				);
				Optional<UserEntry> userEntryOpt = this.appInitializer.getAppService()
					.getCryptoWrapperService()
					.getUserEntryById(this.infectionCaseEntryEncData.getUserId());
				this.userEntryList = new ArrayList<>();
				for (Long contactUserId : this.contactUserIds) {
					Optional<UserEntry> cUserEntryOpt = this.appInitializer.getAppService()
						.getCryptoWrapperService()
						.getUserEntryById(contactUserId);
					cUserEntryOpt.ifPresent(cUserEntry -> this.userEntryList.add(cUserEntry));
				}
				// BIND TO VIEW
				this.getActivity().runOnUiThread(() -> {
					this.binding.icdCaseNumber.setText(String.valueOf(this.infectionCaseId));
					this.binding.icdCreatedAt.setText(DateHelper.getISODateString(infectionCaseEntry
						.getCreatedAt()));
					this.binding.icdUsername.setText(userEntryOpt.get().getUsername());
					this.binding.icdStartDate.setText(DateHelper.getISODateString(this.infectionCaseEntryEncData
						.getDateFrom()));
					this.binding.icdEndDate.setText(DateHelper.getISODateString(this.infectionCaseEntryEncData
						.getDateTo()));
					this.binding.icdNumContactPersons.setText(Long.toString(this.userEntryList.size()));
					this.binding.infectionCaseDetailRecycler.setAdapter(new InfectionCaseDetailRecyclerViewAdapter(
						this.userEntryList,
						this.uniMasterKeyPair
					));
					try {
						byte[] medicalProofBytes =
							CryptoWrapper.decrypt(infectionCaseEntry.getEncMedicalProof(),
								this.uniMasterKeyPair.getPrivate()
							);
						Bitmap medProofBitmap = BitmapFactory.decodeByteArray(medicalProofBytes,
							0,
							medicalProofBytes.length
						);

						this.binding.icdProofImage.setImageBitmap(medProofBitmap);
						this.binding.icdProofImageFullscreen.setImageBitmap(medProofBitmap);

						this.binding.icdProofImage.setOnClickListener(v -> {
							this.binding.icdProofImageFullscreenContainer.setVisibility(View.VISIBLE);
						});
						this.binding.icdProofImageFullscreenContainer.setOnClickListener(v -> {
							this.binding.icdProofImageFullscreenContainer.setVisibility(View.GONE);
						});
					} catch (CryptoDecryptionException | IllegalArgumentException e) {
						e.printStackTrace();
						this.binding.icdProofImage.setImageResource(R.drawable.ic_baseline_warning_24);
						this.binding.icdProofImage.setOnClickListener(v -> {
							this.getActivity()
								.runOnUiThread(() -> new AlertDialog.Builder(this.getActivity()).setTitle(
									R.string.error)
									.setMessage(R.string.image_invalid)
									.setPositiveButton(R.string.button_ok, null)
									.show());
						});
					}
				});
			} catch (CryptoDecryptionException | CryptoDataDeserializationException | CryptoKeyDeserializationException | ApiHttpException e) {
				if (e instanceof ApiHttpException) {
					// do http stuff
				}
				e.printStackTrace();
			} finally {
				loaderDialog.dismiss();
			}
		}).start();

		// Inflate the layout for this fragment
		return root;
	}
}