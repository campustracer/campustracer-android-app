package de.fhkiel.campustracer.ui.lecture;

public interface CreateLectureFragmentInterface {
	void onCreateLectureButtonClick();
}
