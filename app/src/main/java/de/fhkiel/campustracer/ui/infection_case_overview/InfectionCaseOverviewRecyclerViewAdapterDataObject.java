package de.fhkiel.campustracer.ui.infection_case_overview;

import java.time.LocalDate;

import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.model.encdata.InfectionCaseEntryEncData;
import lombok.AllArgsConstructor;
import lombok.Data;


@AllArgsConstructor
@Data
public class InfectionCaseOverviewRecyclerViewAdapterDataObject {
	private final long infectionCaseId;
	private final LocalDate createdAt;
	private final InfectionCaseEntryEncData infectionCaseEntryEncData;
	private final UserEntry userEntry;
}
