package de.fhkiel.campustracer.ui.dialogs;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Looper;
import android.view.View;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import de.fhkiel.campustracer.R;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.exceptions.CryptoBaseException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;


public class InfoDialog {
	private final AlertDialog.Builder dialogBuilder;
	private final Activity activity;

	private boolean positiveButtonSet = false;
	private boolean negativeButtonSet = false;

	private boolean cancelOnTouchOutside = false;

	private Optional<Exception> optionalException = Optional.empty();
	private final Map<Integer, Integer> errorMessages = new HashMap<>();

	/**
	 * @param activity Activity
	 * @param title    InfoDialogs title
	 */
	public InfoDialog(Activity activity, @StringRes int title) {
		this.activity = activity;
		this.dialogBuilder = new AlertDialog.Builder(activity);
		this.dialogBuilder.setTitle(title);
	}

	/**
	 * @param activity Activity
	 * @param title    InfoDialogs title
	 * @param message  InfoDialogs message
	 */
	public InfoDialog(Activity activity, @StringRes int title, @StringRes int message) {
		this(activity, title);
		this.dialogBuilder.setMessage(message);
	}

	/**
	 * @param activity Activity
	 * @param e        Exception that is used to set the title and message
	 */
	public InfoDialog(Activity activity, Exception e) {
		this.activity = activity;
		this.optionalException = Optional.of(e);
		this.dialogBuilder = new AlertDialog.Builder(activity);

		this.updateErrorMessages();
	}

	public InfoDialog setMessage(@StringRes int resId, Object... formatArgs) {
		this.dialogBuilder.setMessage(this.activity.getString(resId, formatArgs));
		return this;
	}

	public InfoDialog setPositiveButton(
		@StringRes int text, DialogInterface.OnClickListener listener
	) {
		if (this.positiveButtonSet) {
			throw new IllegalStateException("Positive button already set");
		}
		this.dialogBuilder.setPositiveButton(text, listener);
		this.positiveButtonSet = true;
		return this;
	}

	/**
	 * Default positive button that closes the dialog
	 */
	public InfoDialog setPositiveButton(@StringRes int text) {
		this.setPositiveButton(text, (dialog, which) -> dialog.dismiss());
		return this;
	}

	/**
	 * Default positive button with button_ok string that closes the dialog
	 */
	public InfoDialog setPositiveButton() {
		this.setPositiveButton(R.string.button_ok);
		return this;
	}

	public InfoDialog setNegativeButton(
		@StringRes int text, DialogInterface.OnClickListener listener
	) {
		if (this.negativeButtonSet) {
			throw new IllegalStateException("Negative button already set");
		}
		this.dialogBuilder.setNegativeButton(text, listener);
		this.negativeButtonSet = true;
		return this;
	}

	/**
	 * Default negative button that closes the dialog
	 */
	public InfoDialog setNegativeButton(@StringRes int text) {
		this.setNegativeButton(text, (dialog, which) -> dialog.dismiss());
		return this;
	}

	/**
	 * Default negative button with button_abort string that closes the dialog
	 */
	public InfoDialog setNegativeButton() {
		this.setNegativeButton(R.string.button_abort);
		return this;
	}

	public InfoDialog setSingleChoiceItems(List<Integer> items) {
		final CharSequence[] chars =
			items.stream().map(this.activity::getString).toArray(CharSequence[]::new);
		this.dialogBuilder.setSingleChoiceItems(chars, 0, null);
		return this;
	}

	public InfoDialog setView(View view) {
		this.dialogBuilder.setView(view);
		return this;
	}

	/**
	 * Allow closing dialog by clicking on the outside (default: false)
	 */
	public InfoDialog setCanceledOnTouchOutside() {
		this.cancelOnTouchOutside = true;
		return this;
	}

	public void show() {
		Runnable runShow = () -> {
			AlertDialog dialog = this.dialogBuilder.create();
			dialog.setCanceledOnTouchOutside(this.cancelOnTouchOutside);
			dialog.show();
		};
		if (Looper.getMainLooper().isCurrentThread()) {
			runShow.run();
		} else {
			this.activity.runOnUiThread(runShow);
		}
	}


	/*
	 * The following is just for making it possible to replace
	 * error messages in the default error handler
	 */

	/**
	 * Replace one string ressource with another to change a message in the dialog
	 *
	 * @param messageId  the regularly displayed message
	 * @param resourceId the message to display instead
	 */
	public InfoDialog replaceErrorResource(@StringRes int messageId, @StringRes int resourceId) {
		this.errorMessages.put(messageId, resourceId);
		this.updateErrorMessages();
		return this;
	}

	/**
	 * Get the currently set value for a string resource or the string resource itself
	 *
	 * @param messageId the message id we want
	 * @return the mapped message id or message id if none was set
	 */
	private int getErrorMessage(@StringRes int messageId) {
		Integer value = this.errorMessages.get(messageId);
		return value == null ? messageId : value;
	}

	/**
	 * Figure out what the error is and set messages accordingly
	 */
	private void updateErrorMessages() {
		if (this.optionalException.isPresent()) {
			Exception e = this.optionalException.get();
			@StringRes int errorTitle = this.getErrorMessage(R.string.error);
			@StringRes int errorMessage = this.getErrorMessage(R.string.unexpected_error_msg);
			if (e instanceof ApiHttpException) {
				if (((ApiHttpException) e).statusCode == 401) {
					errorTitle = this.getErrorMessage(R.string.auth_error_title);
					errorMessage = this.getErrorMessage(R.string.auth_error_msg);
				} else if (Boolean.TRUE.equals(((ApiHttpException) e).isConnectionError())) {
					errorTitle = this.getErrorMessage(R.string.network_error_title);
					errorMessage = this.getErrorMessage(R.string.network_error_msg);
				}
			} else if (e instanceof CryptoEncryptionException) {
				errorTitle = this.getErrorMessage(R.string.encryption_error_title);
				errorMessage = this.getErrorMessage(R.string.encryption_error_msg);
			} else if (e instanceof CryptoBaseException) {
				errorTitle = this.getErrorMessage(R.string.decryption_error_title);
				errorMessage = this.getErrorMessage(R.string.decryption_error_msg);
			}

			this.dialogBuilder.setTitle(errorTitle);
			this.dialogBuilder.setMessage(errorMessage);
		} else {
			throw new IllegalStateException("Use this only with error constructors!");
		}
	}
}
