package de.fhkiel.campustracer.ui.qr_scanner;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.budiyev.android.codescanner.AutoFocusMode;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.ScanMode;
import com.google.zxing.BarcodeFormat;

import java.util.Collections;

import de.fhkiel.campustracer.databinding.FragmentQrScannerBinding;


public class QrScannerFragment extends Fragment {
	private static final String TAG = "CheckInFragment";
	public static final String SCAN_MSG_KEY = "SCAN_MSG_KEY";
	public static final Integer CAMERA_PERMISSION_REQUEST_CODE = 1111; // arbitrary number

	private QrScannerViewModel mQrScannerViewModel;
	private FragmentQrScannerBinding binding;
	private CodeScanner codeScanner;
	private QrScannerFragmentInterface callingActivity;

	@Override
	public View onCreateView(
		@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState
	) {
		if (!(this.getActivity() instanceof QrScannerFragmentInterface)) {
			throw new IllegalArgumentException(
				"This QrScannerFragment must only be called by Activities implementing the "
					+ "QrScannerFragmentInterface!");
		}
		this.callingActivity = (QrScannerFragmentInterface) this.getActivity();

		if (ContextCompat.checkSelfPermission(this.getActivity(), Manifest.permission.CAMERA)
			!= PackageManager.PERMISSION_GRANTED) {
			// Permission is not granted, request it:
			ActivityCompat.requestPermissions(
				this.getActivity(),
				new String[]{Manifest.permission.CAMERA},
				CAMERA_PERMISSION_REQUEST_CODE
			);
		}

		this.mQrScannerViewModel = new ViewModelProvider(this).get(QrScannerViewModel.class);
		this.binding = FragmentQrScannerBinding.inflate(inflater, container, false);
		View root = this.binding.getRoot();
		if (this.getArguments() != null && this.getArguments().containsKey(SCAN_MSG_KEY)) {
			this.binding.scanMessage.setText(this.getArguments().getString(SCAN_MSG_KEY));
		}

		// set up code scanner
		final Activity activity = this.getActivity();
		if (activity == null) {
			Log.e(TAG, "Activity was null");
			return root;
		}
		this.codeScanner = new CodeScanner(activity, this.binding.checkInView);

		// Parameters (default values)
		this.codeScanner.setCamera(CodeScanner.CAMERA_BACK);
		this.codeScanner.setFormats(Collections.singletonList(BarcodeFormat.QR_CODE));
		this.codeScanner.setAutoFocusMode(AutoFocusMode.SAFE);
		this.codeScanner.setScanMode(ScanMode.SINGLE);
		this.codeScanner.setAutoFocusEnabled(true);
		this.codeScanner.setFlashEnabled(false);

		this.codeScanner.setDecodeCallback(result -> {
			Log.d(TAG, "QR scan result: " + result.getText());
			this.callingActivity.onSuccessfulQrScan(result.getText());
		});

		this.codeScanner.setErrorCallback(result -> activity.runOnUiThread(() -> {
			Log.e(TAG, "QR scan resulted in error: " + result.getMessage());
			Toast.makeText(activity, result.getMessage(), Toast.LENGTH_LONG).show();
		}));

		return root;
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		this.codeScanner.startPreview();
	}

	@Override
	public void onDestroyView() {
		this.codeScanner.releaseResources();
		super.onDestroyView();
		this.binding = null;
	}

	@Override
	public void onResume() {
		super.onResume();
		this.codeScanner.startPreview();
	}

	@Override
	public void onPause() {
		this.codeScanner.releaseResources();
		super.onPause();
	}
}