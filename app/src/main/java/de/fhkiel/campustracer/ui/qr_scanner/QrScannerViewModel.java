package de.fhkiel.campustracer.ui.qr_scanner;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


public class QrScannerViewModel extends ViewModel {
	private MutableLiveData<String> mText;

	public QrScannerViewModel() {
		this.mText = new MutableLiveData<>();
	}

	public LiveData<String> getText() {
		return this.mText;
	}
}