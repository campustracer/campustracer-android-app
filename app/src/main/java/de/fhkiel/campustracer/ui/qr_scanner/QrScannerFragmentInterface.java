package de.fhkiel.campustracer.ui.qr_scanner;

public interface QrScannerFragmentInterface {
	void onSuccessfulQrScan(String scannedCode);
}
