package de.fhkiel.campustracer.ui.lecture;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import de.fhkiel.campustracer.databinding.FragmentCreateLectureBinding;


public class CreateLectureFragment extends Fragment {

	private FragmentCreateLectureBinding binding;
	private CreateLectureFragmentInterface callingActivity;

	public static CreateLectureFragment newInstance() {
		return new CreateLectureFragment();
	}

	@Override
	public View onCreateView(
		@NonNull LayoutInflater inflater,
		@Nullable ViewGroup container,
		@Nullable Bundle savedInstanceState
	) {

		this.binding = FragmentCreateLectureBinding.inflate(inflater, container, false);

		View root = this.binding.getRoot();
		if (!(this.getActivity() instanceof CreateLectureFragmentInterface)) {
			throw new IllegalArgumentException(
				"This CreateLectureFragment must only be called by Activities implementing the "
					+ "CreateLectureFragmentInterface!");
		}

		this.callingActivity = (CreateLectureFragmentInterface) this.getActivity();

		this.binding.startLectureButton.setOnClickListener(view -> {
			this.callingActivity.onCreateLectureButtonClick();
		});
		return root;
	}
}