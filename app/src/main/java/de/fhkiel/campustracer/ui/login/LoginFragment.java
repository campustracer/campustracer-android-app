package de.fhkiel.campustracer.ui.login;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import de.fhkiel.campustracer.AppInitializer;
import de.fhkiel.campustracer.R;
import de.fhkiel.campustracer.UserState;
import de.fhkiel.campustracer.databinding.FragmentLoginBinding;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.exceptions.WorkerAlreadyRunningException;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.ui.dialogs.InfoDialog;
import de.fhkiel.campustracer.ui.dialogs.LoaderDialog;


public class LoginFragment extends Fragment {
	private static final String TAG = "LoginFragment";

	private AppInitializer appInitializer;

	private LoginViewModel loginViewModel;
	private FragmentLoginBinding binding;
	private LoginFagmentHandlerInterface callingActivity;

	public LoginFragment() {
		super();
	}

	@Override
	public View onCreateView(
		@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState
	) {
		if (!(this.getActivity() instanceof LoginFagmentHandlerInterface)) {
			throw new IllegalArgumentException(
				"This LoginFragment can must only be called by Activities implementing the "
					+ "LoginFragmentHandlerInterface!");
		}
		this.callingActivity = (LoginFagmentHandlerInterface) this.getActivity();

		this.appInitializer = AppInitializer.getInstance(this.getActivity());
		this.loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);

		this.binding = FragmentLoginBinding.inflate(inflater, container, false);
		View root = this.binding.getRoot();

		this.binding.loginButton.setOnClickListener(this::onLoginButtonClick);

		return root;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		this.binding = null;
	}

	public void onLoginButtonClick(View view) {
		String username = this.binding.loginName.getText().toString();
		String password = this.binding.loginPassword.getText().toString();

		if (username.isEmpty() || password.isEmpty()) {
			new InfoDialog(this.getActivity(),
				R.string.login_failed,
				R.string.login_empty
			).setPositiveButton(R.string.button_ok).show();
			return;
		}

		LoaderDialog loaderDialog =
			new LoaderDialog(R.string.login_in_progress, this.getActivity());
		loaderDialog.show();

		Thread t = new Thread(() -> {
			try {
				this.appInitializer.getAppService().authenticate(username, password);
				UserEntry userEntry =
					this.appInitializer.getAppService().getUserEntryByName(username);
				Thread.sleep(400);
				loaderDialog.dismiss();
				assert username.equals(userEntry.getUsername());
				this.callingActivity.onLoginSuccess(new UserState(userEntry.getId(),
					userEntry.getUsername(),
					password,
					userEntry.getRole()
				));
				this.appInitializer.startWorkers();
				Log.w(TAG, "LOGIN OK");
			} catch (ApiHttpException e) {
				loaderDialog.dismiss();
				Log.e(TAG, e.getMessage());
				e.printStackTrace();
				new InfoDialog(this.getActivity(), e).setNegativeButton(R.string.button_ok)
					.replaceErrorResource(R.string.auth_error_msg, R.string.login_invalid)
					.show();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (WorkerAlreadyRunningException ignore) {
			}
		});
		t.start(); // no joining, it would block current UI thread!
	}
}