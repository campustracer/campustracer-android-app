package de.fhkiel.campustracer.ui.login;

import de.fhkiel.campustracer.UserState;


public interface LoginFagmentHandlerInterface {
	void onLoginSuccess(UserState userState);
}
