package de.fhkiel.campustracer.ui.infection_case_overview;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import de.fhkiel.campustracer.AppInitializer;
import de.fhkiel.campustracer.R;
import de.fhkiel.campustracer.databinding.FragmentInfectionCaseOverviewBinding;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoDecryptionException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;
import de.fhkiel.campustracer.model.InfectionCaseEntry;
import de.fhkiel.campustracer.model.KeystoreEntry;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.model.encdata.InfectionCaseEntryEncData;
import de.fhkiel.campustracer.ui.dialogs.InfoDialog;
import de.fhkiel.campustracer.ui.dialogs.LoaderDialog;


/**
 * A fragment representing a list of Items.
 */
public class InfectionCaseOverviewFragment extends Fragment {
	private static final String TAG = "InfectionCaseOverviewFragment";
	private AppInitializer appInitializer;

	private FragmentInfectionCaseOverviewBinding binding;
	private InfectionCaseOverviewFragmentInterface callingActivity;

	private Optional<KeyPair> uniMasterKeyPair = Optional.empty();

	private static final String ARG_COLUMN_COUNT = "column-count";
	private int mColumnCount = 1;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the fragment (e.g. upon
	 * screen orientation changes).
	 */
	public InfectionCaseOverviewFragment() {
	}

	public static InfectionCaseOverviewFragment newInstance(int columnCount) {
		InfectionCaseOverviewFragment fragment = new InfectionCaseOverviewFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_COLUMN_COUNT, columnCount);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (this.getArguments() != null) {
			this.mColumnCount = this.getArguments().getInt(ARG_COLUMN_COUNT);
		}
	}

	@Override
	public View onCreateView(
		@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState
	) {
		if (!(this.getActivity() instanceof InfectionCaseOverviewFragmentInterface)) {
			throw new IllegalArgumentException(
				"This InfectionCaseOverviewFragment must only be called by Activities implementing"
					+ " the InfectionCaseOverviewFragmentInterface!");
		}
		this.callingActivity = (InfectionCaseOverviewFragmentInterface) this.getActivity();
		this.binding = FragmentInfectionCaseOverviewBinding.inflate(inflater, container, false);
		this.appInitializer = AppInitializer.getInstance(this.getActivity());
		View root = this.binding.getRoot();

		// setup recyclerView
		Context context = root.getContext();
		RecyclerView recyclerView = root.findViewById(R.id.ICList);
		if (this.mColumnCount <= 1) {
			recyclerView.setLayoutManager(new LinearLayoutManager(context));
		} else {
			recyclerView.setLayoutManager(new GridLayoutManager(context, this.mColumnCount));
		}
		recyclerView.addItemDecoration(new DividerItemDecoration(this.getActivity(),
			DividerItemDecoration.VERTICAL
		));

		this.refreshInfectionCases();
		this.binding.ICRefreshButton.setOnClickListener((View view) -> this.refreshInfectionCases());

		return root;
	}

	public void refreshInfectionCases() {
		Log.d(TAG, "Refreshing infection cases");
		if (!this.uniMasterKeyPair.isPresent()) {
			Optional<KeystoreEntry> uniMKPKeystoreEntry = this.appInitializer.getAppService()
				.getDatabaseService()
				.getMasterKeyPairFromKeystore();
			if (!uniMKPKeystoreEntry.isPresent()) {
				Log.e(TAG, "No master key pair in database");
				new InfoDialog(this.getActivity(),
					R.string.error,
					R.string.no_master_key_in_backend
				).setPositiveButton().show();
				return;
			}
			this.uniMasterKeyPair = Optional.of(uniMKPKeystoreEntry.get().getKeyPair());
		}
		KeyPair uniMasterKeyPair = this.uniMasterKeyPair.get();
		LoaderDialog loaderDialog =
			new LoaderDialog(R.string.ic_refresh_in_progress, this.getActivity());
		loaderDialog.show();

		Thread t = new Thread(() -> {
			this.appInitializer.getAppService().authenticate();
			try {
				List<InfectionCaseEntry> encList =
					this.appInitializer.getAppService().getInfectionCases();
				Log.d(TAG, "Got " + encList.size() + " infection cases");

				List<InfectionCaseOverviewRecyclerViewAdapterDataObject> decList =
					new ArrayList<>();
				for (InfectionCaseEntry infectionCaseEntry : encList) {
					try {

						InfectionCaseEntryEncData infectionCaseEntryEncData =
							InfectionCaseEntryEncData.getDecryptedPayload(infectionCaseEntry.getId(),
								infectionCaseEntry.getEncData(),
								infectionCaseEntry.getEncMedicalProof(),
								uniMasterKeyPair.getPrivate()
							);
						// Get UserEntry from DB
						Optional<UserEntry> userEntryOpt = this.appInitializer.getAppService()
							.getCryptoWrapperService()
							.getUserEntryById(infectionCaseEntryEncData.getUserId());
						if (!userEntryOpt.isPresent()) {
							throw new IllegalStateException("No User found for UserId "
								+ infectionCaseEntryEncData.getUserId());
						}
						decList.add(new InfectionCaseOverviewRecyclerViewAdapterDataObject(
							infectionCaseEntry.getId(),
							infectionCaseEntry.getCreatedAt(),
							infectionCaseEntryEncData,
							userEntryOpt.get()
						));
					} catch (IllegalArgumentException | IllegalStateException | CryptoDataDeserializationException | CryptoDecryptionException | CryptoKeyDeserializationException e) {
						Log.e(TAG,
							"Skipped InfectionCase #" + infectionCaseEntry.getId() + ", Reason:" + e
								.getClass()
								.getSimpleName()
						);
						e.printStackTrace();
						continue;
					}
				}
				if (decList.size() < encList.size()) {
					Log.e(TAG, "SOME ENTRIES WERE SKIPPED!");
					new InfoDialog(this.getActivity(),
						R.string.error,
						R.string.ic_entries_skipped
					).setPositiveButton().show();
				}
				Log.d(TAG, "Infection cases successfully decrypted!");

				if (this.getActivity() != null) {
					this.getActivity().runOnUiThread(() -> {
						if (decList.size() != 0) {
							// Data
							this.binding.icViewEmptyText.setVisibility(View.GONE);
							this.binding.ICList.setVisibility(View.VISIBLE);

							this.binding.ICList.setAdapter(new InfectionCaseOverviewRecyclerViewAdapter(decList,
								this.callingActivity
							));
						} else {
							// No Data
							this.binding.icViewEmptyText.setVisibility(View.VISIBLE);
							this.binding.ICList.setVisibility(View.GONE);
						}
					});
				}
				loaderDialog.dismiss();
			} catch (ApiHttpException e) {
				Log.e(TAG, "Error while decrypting infection cases");
				loaderDialog.dismiss();
				new InfoDialog(this.getActivity(), e).setPositiveButton().show();
			}
		});
		t.start();
	}
}