package de.fhkiel.campustracer.ui.qr_display;

public interface QrCodeDisplayFragmentInterface {
	void onQrCodeDisplayFragmentCloseButtonClick();
}
