package de.fhkiel.campustracer.ui;

import static androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_WEAK;
import static androidx.biometric.BiometricManager.Authenticators.DEVICE_CREDENTIAL;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;
import androidx.fragment.app.FragmentActivity;

import java.util.concurrent.Executor;

import de.fhkiel.campustracer.R;
import de.fhkiel.campustracer.ui.dialogs.InfoDialog;


/**
 * Authentication helper for Auth via Fingerprint/PIN/Password/Pattern
 */
public class DeviceAuthHelper {
	public static final String TAG = "DeviceAuthHelper";
	public static final int AUTH_SETUP_REQUEST_CODE = 22123;

	public static boolean checkDeviceAuthenticationSetUp(Activity context) {
		FingerprintManagerCompat fingerprintManager = FingerprintManagerCompat.from(context);
		KeyguardManager keyguardManager =
			(KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);

		if (fingerprintManager.isHardwareDetected()
			&& fingerprintManager.hasEnrolledFingerprints()) {
			return true;
		} else {
			return keyguardManager.isDeviceSecure();
		}
	}

	public interface OnAuthSuccessCallback {
		void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result);
	}

	/**
	 * Checks if the Device has set up an Authentication method, opens settings menu if not.
	 * <p>
	 * Note: The calling Activity SHOULD override the Activity::onActivityResult() callback and
	 * call
	 * onActivityResultAuthSetUpHandler() inside in order to create a re-prompting loop.
	 *
	 * @param context Application Context
	 */
	public static void requireDeviceAuthenticationSetUp(Activity context) {
		if (checkDeviceAuthenticationSetUp(context)) {
			return;
		}
		FingerprintManagerCompat fingerprintManager = FingerprintManagerCompat.from(context);
		KeyguardManager keyguardManager =
			(KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);

		// Fingerprint Hardware available, but no Fingerprint set up -> Setup Fingerprint
		if (fingerprintManager.isHardwareDetected()
			&& !fingerprintManager.hasEnrolledFingerprints()) {
			// Open Fingerprint Setup dialog!
			new InfoDialog(context,
				R.string.auth_method_required_title,
				R.string.fingerprint_auth_required_descr
			).setPositiveButton(R.string.button_ok, (dialog, which) -> {
				Intent enrollIntent;
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
					enrollIntent = new Intent(Settings.ACTION_BIOMETRIC_ENROLL);
				} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
					enrollIntent = new Intent(Settings.ACTION_FINGERPRINT_ENROLL);
				} else {
					enrollIntent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
				}
				context.startActivityForResult(enrollIntent, AUTH_SETUP_REQUEST_CODE);
			}).show();
		} else {
			Toast.makeText(context, "NO FINGERPRINT READER DETECTED!", Toast.LENGTH_LONG).show();
			// Fallback: PIN-Code only
			if (!keyguardManager.isDeviceSecure()) {
				new InfoDialog(context,
					R.string.auth_method_required_title,
					R.string.device_auth_required_descr
				).setPositiveButton(R.string.button_ok, (dialog, which) -> {
					Intent enrollIntent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
					context.startActivityForResult(enrollIntent, AUTH_SETUP_REQUEST_CODE);
				}).show();
			}
		}
	}

	/**
	 * Checks if an Auth Method has been set up, re-prompts if not.
	 * <p>
	 * Note: This method SHOULD be used inside the overridden onActivityResult() method inside any
	 * Activity using the requireDeviceAuthenticationSetUp() method. If used, it will re-prompt the
	 * settings menu if no proper authentication method was set up.
	 */
	public static void onActivityResultAuthSetUpHandler(
		int requestCode, int resultCode, @Nullable Intent data, Activity context
	) {
		if (requestCode == AUTH_SETUP_REQUEST_CODE && !checkDeviceAuthenticationSetUp(context)) {
			requireDeviceAuthenticationSetUp(context);
		}
	}

	public static void requireDeviceAuthentication(
		FragmentActivity context, OnAuthSuccessCallback onAuthSuccessCallback
	) {
		if (!checkDeviceAuthenticationSetUp(context)) {
			requireDeviceAuthenticationSetUp(context);
			return;
		}
		//NOTICE: Using androidx.biometric.BiometricPrompt compat library to satisfy API Level < 28
		Executor mainThreadExecutor = ContextCompat.getMainExecutor(context);

		BiometricPrompt biometricPrompt = new BiometricPrompt(context,
			mainThreadExecutor,
			new BiometricPrompt.AuthenticationCallback() {
				@Override
				public void onAuthenticationError(
					int errorCode, @NonNull CharSequence errString
				) {
					super.onAuthenticationError(errorCode, errString);
					Toast.makeText(context, R.string.device_auth_error, Toast.LENGTH_LONG).show();
				}

				@Override
				public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
					super.onAuthenticationSucceeded(result);
					onAuthSuccessCallback.onAuthenticationSucceeded(result);
				}

				@Override
				public void onAuthenticationFailed() {
					super.onAuthenticationFailed();
					Toast.makeText(context, R.string.device_auth_error, Toast.LENGTH_LONG).show();
				}
			}
		);

		BiometricPrompt.PromptInfo.Builder promptInfoB = new BiometricPrompt.PromptInfo.Builder();
		BiometricPrompt.PromptInfo promptInfo =
			promptInfoB.setTitle(context.getString(R.string.auth_required_title))
				.setDescription(context.getString(R.string.auth_required_desc))
				.setAllowedAuthenticators(BIOMETRIC_WEAK | DEVICE_CREDENTIAL)
				.build();
		biometricPrompt.authenticate(promptInfo);
	}
}
