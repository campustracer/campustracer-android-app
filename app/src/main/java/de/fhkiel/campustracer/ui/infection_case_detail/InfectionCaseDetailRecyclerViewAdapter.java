package de.fhkiel.campustracer.ui.infection_case_detail;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.util.List;

import de.fhkiel.campustracer.R;
import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.databinding.FragmentInfectionCaseDetailItemBinding;
import de.fhkiel.campustracer.exceptions.CryptoDecryptionException;
import de.fhkiel.campustracer.model.RolesEnum;
import de.fhkiel.campustracer.model.UserEntry;


public class InfectionCaseDetailRecyclerViewAdapter
	extends RecyclerView.Adapter<InfectionCaseDetailRecyclerViewAdapter.ViewHolder> {

	private final List<UserEntry> mValues;
	private KeyPair uniMasterKeyPair;

	public InfectionCaseDetailRecyclerViewAdapter(
		List<UserEntry> items, KeyPair uniMasterKeyPair
	) {
		this.mValues = items;
		this.uniMasterKeyPair = uniMasterKeyPair;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new ViewHolder(FragmentInfectionCaseDetailItemBinding.inflate(LayoutInflater.from(
			parent.getContext()), parent, false));
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, int position) {
		holder.mItem = this.mValues.get(position);
		holder.mUserIdView.setText(String.valueOf(holder.mItem.getId()));
		holder.mUsernameView.setText(holder.mItem.getUsername());
		holder.mRoleView.setText(holder.mItem.getRole() == RolesEnum.STUDENT
			? R.string.student
			: R.string.lecturer);
		try {
			String contactData = new String(
				CryptoWrapper.decrypt(holder.mItem.getEncContactData(),
					this.uniMasterKeyPair.getPrivate()
				),
				StandardCharsets.UTF_8
			);
			holder.mContactData.setText(contactData);
		} catch (CryptoDecryptionException | IllegalArgumentException e) {
			e.printStackTrace();
			holder.mContactData.setText(R.string.not_available);
		}
	}

	@Override
	public int getItemCount() {
		return this.mValues.size();
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		public final TextView mUserIdView;
		public final TextView mUsernameView;
		public final TextView mRoleView;
		public final TextView mContactData;

		public UserEntry mItem;

		public ViewHolder(FragmentInfectionCaseDetailItemBinding binding) {
			super(binding.getRoot());
			this.mUserIdView = binding.icdiUserId;
			this.mUsernameView = binding.icdiUsername;
			this.mRoleView = binding.icdiRole;
			this.mContactData = binding.icdiContactData;
			;
		}
	}
}