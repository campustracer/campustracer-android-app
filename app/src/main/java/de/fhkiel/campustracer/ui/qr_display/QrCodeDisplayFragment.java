package de.fhkiel.campustracer.ui.qr_display;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.zxing.WriterException;

import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import de.fhkiel.campustracer.R;
import de.fhkiel.campustracer.databinding.FragmentQrCodeDisplayBinding;
import de.fhkiel.campustracer.helper.DateHelper;
import de.fhkiel.campustracer.helper.QrCodeHelper;
import de.fhkiel.campustracer.ui.dialogs.InfoDialog;


public class QrCodeDisplayFragment extends Fragment {

	private QrCodeDisplayViewModel mViewModel;
	private FragmentQrCodeDisplayBinding binding;

	private QrCodeDisplayFragmentInterface callingActivity;

	public static final String TITLE_KEY = "TITLE_KEY";
	public static final String DESCRIPTION_KEY = "DESCRIPTION_KEY";
	public static final String QR_CODE_PAYLOAD_KEY = "IMAGE_KEY";
	public static final String SHOW_SAVE_BUTTON_KEY = "SHOW_SAVE_BUTTON_KEY";
	public static final String SHOW_CLOSE_BUTTON_KEY = "SHOW_CLOSE_BUTTON_KEY";
	private static final String TAG = "QrCodeDisplayFragment";

	public static QrCodeDisplayFragment newInstance() {
		return new QrCodeDisplayFragment();
	}

	@Override
	public View onCreateView(
		@NonNull LayoutInflater inflater,
		@Nullable ViewGroup container,
		@Nullable Bundle savedInstanceState
	) {
		if (!(this.getActivity() instanceof QrCodeDisplayFragmentInterface)) {
			throw new IllegalArgumentException(
				"This QrCodeDisplayFragment can must only be called by Activities implementing "
					+ "the QrCodeDisplayFragmentInterface!");
		}
		this.callingActivity = (QrCodeDisplayFragmentInterface) this.getActivity();

		this.binding = FragmentQrCodeDisplayBinding.inflate(inflater, container, false);
		this.binding.saveButton.setOnClickListener(this::onSaveButtonClick);
		this.binding.closeView.setOnClickListener(this::onCloseViewClick);

		//#

		// HANDLE ARGUMENTS
		Bundle argBundle = this.getArguments();
		if (argBundle != null) {
			if (argBundle.containsKey(TITLE_KEY)) {
				this.binding.titleTextView.setText(argBundle.getString(TITLE_KEY));
			}
			if (argBundle.containsKey(DESCRIPTION_KEY)) {
				this.binding.descriptionTextView.setText(argBundle.getString(DESCRIPTION_KEY));
			}
			if (argBundle.containsKey(SHOW_SAVE_BUTTON_KEY)) {
				this.binding.saveButton.setVisibility(argBundle.getBoolean(SHOW_SAVE_BUTTON_KEY)
					? View.VISIBLE
					: View.GONE);
			}
			if (argBundle.containsKey(SHOW_CLOSE_BUTTON_KEY)) {
				this.binding.closeView.setVisibility(argBundle.getBoolean(SHOW_CLOSE_BUTTON_KEY)
					? View.VISIBLE
					: View.GONE);
			}
			if (argBundle.containsKey(QR_CODE_PAYLOAD_KEY)) {
				try {
					this.updateQrCode(argBundle.getString(QR_CODE_PAYLOAD_KEY));
				} catch (WriterException e) {
					e.printStackTrace();
				}
			}
		}

		//#

		View root = this.binding.getRoot();
		return root;
	}

	@Override
	public void onViewCreated(
		@NonNull View view, @Nullable Bundle savedInstanceState
	) {
		super.onViewCreated(view, savedInstanceState);

		this.mViewModel =
			new ViewModelProvider(this.requireActivity()).get(QrCodeDisplayViewModel.class);

		// CREATE VIEW MODEL OBSERVERS

		this.mViewModel.getMTitle()
			.observe(this.getViewLifecycleOwner(), this.binding.titleTextView::setText);
		this.mViewModel.getMDescription()
			.observe(this.getViewLifecycleOwner(), this.binding.descriptionTextView::setText);
		this.mViewModel.getShowSaveButton().observe(this.getViewLifecycleOwner(),
			item -> this.binding.saveButton.setVisibility(item ? View.VISIBLE : View.GONE)
		);
		this.mViewModel.getShowCloseButton().observe(this.getViewLifecycleOwner(),
			item -> this.binding.closeView.setVisibility(item ? View.VISIBLE : View.GONE)
		);
		this.mViewModel.getMQrCodePayload().observe(this.getViewLifecycleOwner(), payload -> {
			try {
				this.updateQrCode(payload);
			} catch (WriterException e) {
				Log.e(TAG, "ERROR: QR CODE WRITER!");
				e.printStackTrace();
			}
		});

		// QRCode Payload
		Bundle argBundle = this.getArguments();
		if (argBundle != null) {
			if (argBundle.containsKey(QR_CODE_PAYLOAD_KEY)) {
				this.mViewModel.setQrCodePayload(argBundle.getString(QR_CODE_PAYLOAD_KEY));
			}
		}
	}

	private void updateQrCode(String payload) throws WriterException {
		this.binding.qrImageView.setImageBitmap(QrCodeHelper.createQrCodeBitmap(payload));
		this.binding.fingerprintTextView.setText(QrCodeHelper.getFingerprint(payload));
		this.binding.createdAtTextView.setText(DateHelper.getISODateTimeString(LocalDateTime.now()
			.truncatedTo(ChronoUnit.SECONDS)));
	}

	public void onSaveButtonClick(View view) {
		//this.mViewModel.setShowButton(false); -> apparently too slow, still showing in screenshot
		this.binding.saveButton.setVisibility(View.GONE);
		String filename =
			DateHelper.getISODateTimeString(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS))
				.replace(":", ".") + "_QRCode";
		Bitmap screenshot = this.takeScreenshot();
		try {
			String filePath = QrCodeHelper.saveBitmapToFile(screenshot,
				this.getActivity().getApplicationContext(),
				"Screenshots/",
				filename
			);
			new InfoDialog(this.getActivity(),
				R.string.image_saved,
				R.string.image_saved_to_screenshot_gallery
			).setPositiveButton().show();
			Log.w(TAG, "DONE SAVING FILE, PATH: " + filePath);
		} catch (FileNotFoundException e) {
			Log.e(TAG, "ERROR SAVING FILE: ");
			e.printStackTrace();
			new InfoDialog(this.getActivity(),
				R.string.error,
				R.string.error_saving_qr_image
			).setPositiveButton(R.string.open_permissions, (dialog, which) -> {
				dialog.dismiss();
				Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
				Uri uri = Uri.fromParts("package", this.getActivity().getPackageName(), null);
				intent.setData(uri);
				this.startActivity(intent);
			}).setNegativeButton().show();
		} finally {
			this.mViewModel.setShowSaveButton(true);
		}
	}

	public void onCloseViewClick(View view) {
		this.callingActivity.onQrCodeDisplayFragmentCloseButtonClick();
	}

	public Bitmap takeScreenshot() {
		View screenshotViewGroup = this.binding.qrCodeDisplayViewGroup;
		Bitmap bitmap = Bitmap.createBitmap(screenshotViewGroup.getWidth(),
			screenshotViewGroup.getHeight(),
			Bitmap.Config.ARGB_8888
		);
		Canvas canvas = new Canvas(bitmap);
		screenshotViewGroup.draw(canvas);
		return bitmap;
	}
}