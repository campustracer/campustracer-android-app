package de.fhkiel.campustracer.ui.infection_case_overview;

public interface InfectionCaseOverviewFragmentInterface {
	void onInfectionCaseOverviewClick(InfectionCaseOverviewRecyclerViewAdapterDataObject icObject);
}
