package de.fhkiel.campustracer.ui.infection_case_report;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.io.IOException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import de.fhkiel.campustracer.AppInitializer;
import de.fhkiel.campustracer.R;
import de.fhkiel.campustracer.databinding.FragmentInfectionCaseReportBinding;
import de.fhkiel.campustracer.entities.Student;
import de.fhkiel.campustracer.entities.User;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.helper.BytesPackage;
import de.fhkiel.campustracer.helper.DateHelper;
import de.fhkiel.campustracer.helper.FileHelper;
import de.fhkiel.campustracer.model.InfectionCaseEntry;
import de.fhkiel.campustracer.model.virtual.InfectionCaseCreateEntry;
import de.fhkiel.campustracer.ui.dashboard.DashboardFragment;
import de.fhkiel.campustracer.ui.dialogs.InfoDialog;
import de.fhkiel.campustracer.ui.dialogs.LoaderDialog;


public class InfectionCaseReportFragment extends Fragment {
	private static final String TAG = "InfectionCaseReportFragment";

	private AppInitializer appInitializer;
	private InfectionCaseReportViewModel mViewModel;
	private FragmentInfectionCaseReportBinding binding;
	ActivityResultLauncher<Intent> getFileIntent;
	byte[] medicalProof;
	Uri medicalProofUri;
	InfectionCaseEntry infectionCaseEntry;

	public static InfectionCaseReportFragment newInstance() {
		return new InfectionCaseReportFragment();
	}

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		this.getFileIntent =
			this.registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
				result -> {
					if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
						this.medicalProofUri = result.getData().getData();
						Log.d(TAG, "Got file: " + this.medicalProofUri);
						if (this.getActivity() != null) {
							try {
								this.medicalProof =
									BytesPackage.inputStreamToBytes(this.getActivity()
										.getContentResolver()
										.openInputStream(this.medicalProofUri));
								this.binding.ICFileName.setText(FileHelper.getFilename(this.getActivity(),
									this.medicalProofUri
								));
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
					if (this.medicalProof == null) {
						this.binding.ICFileName.setText(R.string.ic_file_name_error);
						Log.e(TAG, "Could not read file " + this.medicalProofUri);
					}
				}
			);
	}

	@Override
	public View onCreateView(
		@NonNull LayoutInflater inflater,
		@Nullable ViewGroup container,
		@Nullable Bundle savedInstanceState
	) {
		this.binding = FragmentInfectionCaseReportBinding.inflate(inflater, container, false);
		View root = this.binding.getRoot();
		this.appInitializer = AppInitializer.getInstance(this.getActivity());

		this.setDatePicker(LocalDate.now().minusWeeks(1), () -> this.binding.ICFromDate);
		this.setDatePicker(LocalDate.now(), () -> this.binding.ICToDate);

		this.binding.ICAddFileButton.setOnClickListener(this::addFileButtonListener);
		this.binding.ICSendButton.setOnClickListener(this::sendButtonListener);

		return root;
	}

	private interface GetEditTextFieldOp {
		EditText get();
	}

	private void setDatePicker(LocalDate date, GetEditTextFieldOp getEditTextField) {
		EditText editText = getEditTextField.get();
		editText.setText(DateHelper.getISODateString(date));
		final DatePickerDialog datePicker =
			new DatePickerDialog(this.getActivity(), (view, year, month, dayOfMonth) -> {
				LocalDate newDate = LocalDate.of(year, month + 1, dayOfMonth);
				editText.setText(DateHelper.getISODateString(newDate));
			}, date.getYear(), date.getMonthValue() - 1, date.getDayOfMonth());
		editText.setOnClickListener(v -> datePicker.show());
	}

	private void addFileButtonListener(View v) {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		final String[] acceptMimeTypes = {
			"application/pdf", "image/*",
		};
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		intent.setType("*/*");
		intent.putExtra(Intent.EXTRA_MIME_TYPES, acceptMimeTypes);
		this.getFileIntent.launch(intent);
	}

	private List<PrivateKey> getPreGeneratedKeys(
		User userObject, LocalDate fromDate, LocalDate toDate
	) {
		List<PrivateKey> preGenKeyPairs = new ArrayList<>();
		// Student: Also Surrender PreGen KeyPairs
		if (userObject.isStudent()) {
			Student student = (Student) userObject;
			preGenKeyPairs =
				student.surrenderPreGeneratedKeyPairsForDates(DateHelper.getDatesListFromDateRange(
					fromDate,
					toDate
				))
					.values()
					.stream()
					.map(KeyPair::getPrivate)
					.collect(Collectors.toList());
		}
		Log.d(TAG,
			String.format("Surrendered %d pre generated encryption keys", preGenKeyPairs.size())
		);
		return preGenKeyPairs;
	}

	private List<PrivateKey> getCheckInKeys(
		User userObject, LocalDate fromDate, LocalDate toDate
	) {
		List<PrivateKey> checkInKeyPairs =
			userObject.surrenderCheckInKeyPairsForDates(DateHelper.getDatesListFromDateRange(
				fromDate,
				toDate
			)).stream().map(KeyPair::getPrivate).collect(Collectors.toList());
		Log.d(TAG, String.format("Surrendered %d check-in keys", checkInKeyPairs.size()));
		return checkInKeyPairs;
	}

	private void sendButtonListener(View v) {
		if (this.medicalProof == null) {
			new InfoDialog(this.getActivity(),
				R.string.incomplete,
				R.string.ic_send_incomplete_msg
			).setNegativeButton(R.string.close).setCanceledOnTouchOutside().show();
		} else {
			new InfoDialog(this.getActivity(),
				R.string.confirm,
				R.string.ic_send_confirm_msg
			).setPositiveButton(R.string.send, (dialog, which) -> {
				LoaderDialog loaderDialog =
					new LoaderDialog(R.string.ic_send_in_progress, this.getActivity());
				loaderDialog.show();

				LocalDate fromDate =
					DateHelper.getLocalDateFromISODateString(this.binding.ICFromDate.getText()
						.toString());
				LocalDate toDate =
					DateHelper.getLocalDateFromISODateString(this.binding.ICToDate.getText()
						.toString());

				new Thread(() -> {
					try {
						Optional<User> optionalUserObj =
							this.appInitializer.getAppService().getUserObject();
						if (!optionalUserObj.isPresent()) {
							throw new IllegalStateException("Missing UserState Object");
						}
						User userObject = optionalUserObj.get();

						this.infectionCaseEntry = this.appInitializer.getAppService()
							.getApiService()
							.addInfectionCase(new InfectionCaseCreateEntry(userObject.getUserId(),
								userObject.getMasterPublicKey(),
								this.getPreGeneratedKeys(userObject, fromDate, toDate),
								this.getCheckInKeys(userObject, fromDate, toDate),
								fromDate,
								toDate,
								this.medicalProof
							));

						loaderDialog.dismiss();
						if (this.getActivity() != null) {
							this.getActivity().runOnUiThread(() -> {
								new InfoDialog(this.getActivity(),
									R.string.success,
									R.string.ic_send_success
								).setPositiveButton(R.string.close).show();
								this.getActivity()
									.getSupportFragmentManager()
									.beginTransaction()
									.setReorderingAllowed(true)
									.replace(R.id.nav_fragment_container_activity_user,
										DashboardFragment.class,
										new Bundle()
									)
									.commit();
							});
						}
					} catch (ApiHttpException | CryptoEncryptionException e) {
						e.printStackTrace();
						loaderDialog.dismiss();
						new InfoDialog(this.getActivity(), e).setPositiveButton(R.string.close)
							.show();
					}
				}).start();
				dialog.dismiss();
			}).setNegativeButton().show();
		}
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		this.mViewModel = new ViewModelProvider(this).get(InfectionCaseReportViewModel.class);
		// TODO: Use the ViewModel
	}
}