package de.fhkiel.campustracer.ui.dialogs;

import android.app.Activity;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;

import de.fhkiel.campustracer.R;


/**
 * The LoaderDialog is only shown if there were at least SHOW_AFTER_THRESHOLD_MS between show() and
 * dismiss(). If so it is shown for at least SHOW_AFTER_THRESHOLD_MS. This prevents unnecessary
 * and/or short loaders (flickering). The show/dismiss methods are also aware towards if they are
 * run on the UI thread and act accordingly, no need for wrapping calls in runOnUThread.
 */
public class LoaderDialog {
	private AlertDialog alertDialog;
	private final Activity showingActivity;
	private static final long SHOW_AFTER_THRESHOLD_MS = 300;
	private static final long SHOW_MINIMUM_MS = 1000;
	private volatile Boolean isDismissed = false;
	private volatile Boolean wasActuallyShown = false;
	private final Runnable delayedRunnable;
	private Boolean instanceWasUsed = false;
	private Looper looper;

	public LoaderDialog(@StringRes int messageR, Activity showingActivity) {
		this(messageR, showingActivity, false);
	}

	public LoaderDialog(@StringRes int messageR, Activity showingActivity, Boolean isDismissible) {
		this.showingActivity = showingActivity;
		AlertDialog.Builder loaderAlertDialogBuilder = new AlertDialog.Builder(showingActivity);
		loaderAlertDialogBuilder.setTitle(messageR).setView(R.layout.layout_loader_dialog);
		if (Boolean.TRUE.equals(isDismissible)) {
			loaderAlertDialogBuilder.setNegativeButton(R.string.button_dismiss, null);
		}
		this.showingActivity.runOnUiThread(() -> this.alertDialog =
			loaderAlertDialogBuilder.create());

		this.delayedRunnable = () -> {
			synchronized (this) {
				if (Boolean.TRUE.equals(this.isDismissed)) {
					return;
				}
				if (Looper.getMainLooper().isCurrentThread()) {

					this.alertDialog.show();
				} else {
					this.showingActivity.runOnUiThread(() -> {
						this.alertDialog.setCanceledOnTouchOutside(false);
						this.alertDialog.show();
					});
				}
				this.wasActuallyShown = true;
			}
		};
	}

	public synchronized void show() {
		if (Boolean.TRUE.equals(this.instanceWasUsed)) {
			throw new IllegalStateException("Instance's show() method can only be called once!");
		}
		this.instanceWasUsed = true;
		HandlerThread handlerThread = new HandlerThread("LoaderDiagHandlerThread");
		handlerThread.start();
		this.looper = handlerThread.getLooper();
		Handler handler = new Handler(this.looper);
		handler.postDelayed(this.delayedRunnable, SHOW_AFTER_THRESHOLD_MS);
	}

	public synchronized void dismiss() {
		if (this.isDismissed) {
			return; // noop, prevents double-execution
		}
		this.isDismissed = true;
		if (Boolean.FALSE.equals(this.instanceWasUsed)) {
			throw new IllegalStateException("Instance's show() method was not called before!");
		}
		synchronized (this) {
			if (Boolean.FALSE.equals(this.wasActuallyShown)) {
				Handler handler = new Handler(this.looper);
				handler.removeCallbacks(this.delayedRunnable);
				return;
			}
		}
		if (this.alertDialog.isShowing()) {
			if (Looper.getMainLooper().isCurrentThread()) {
				Thread t = new Thread(() -> {
					try {
						Thread.sleep(SHOW_MINIMUM_MS);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {
						this.alertDialog.dismiss();
					}
				});
				t.start();
			} else {
				try {
					Thread.sleep(SHOW_MINIMUM_MS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					this.showingActivity.runOnUiThread(this.alertDialog::dismiss);
				}
			}
		}
	}
}
