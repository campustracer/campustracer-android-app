package de.fhkiel.campustracer.ui.infection_case_overview;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.List;

import de.fhkiel.campustracer.R;
import de.fhkiel.campustracer.databinding.FragmentInfectionCaseOverviewItemBinding;
import de.fhkiel.campustracer.helper.DateHelper;
import de.fhkiel.campustracer.model.RolesEnum;


public class InfectionCaseOverviewRecyclerViewAdapter
	extends RecyclerView.Adapter<InfectionCaseOverviewRecyclerViewAdapter.ViewHolder> {

	private final List<InfectionCaseOverviewRecyclerViewAdapterDataObject> mValues;
	private final InfectionCaseOverviewFragmentInterface callingActivity;

	public InfectionCaseOverviewRecyclerViewAdapter(
		List<InfectionCaseOverviewRecyclerViewAdapterDataObject> items,
		InfectionCaseOverviewFragmentInterface callingActivity
	) {
		Collections.reverse(items);
		this.mValues = items;
		this.callingActivity = callingActivity;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new ViewHolder(FragmentInfectionCaseOverviewItemBinding.inflate(LayoutInflater.from(
			parent.getContext()), parent, false));
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, int position) {
		holder.mItem = this.mValues.get(position);
		holder.mIdView.setText(String.valueOf(holder.mItem.getInfectionCaseId()));
		holder.mUserTypeView.setText(holder.mItem.getUserEntry().getRole() == RolesEnum.STUDENT
			? R.string.student
			: (holder.mItem.getUserEntry().getRole() == RolesEnum.LECTURER
				? R.string.lecturer
				: R.string.error));
		holder.mUsernameView.setText(holder.mItem.getUserEntry().getUsername());
		holder.mStartDateView.setText(DateHelper.getISODateString(holder.mItem.getInfectionCaseEntryEncData()
			.getDateFrom()));
		holder.mEndDateView.setText(DateHelper.getISODateString(holder.mItem.getInfectionCaseEntryEncData()
			.getDateTo()));
		holder.mCreatedAtView.setText(DateHelper.getISODateString(holder.mItem.getCreatedAt()));
		holder.bindListener(holder.mItem, this.callingActivity);
	}

	@Override
	public int getItemCount() {
		return this.mValues.size();
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		public final TextView mIdView;
		public final TextView mUserTypeView;
		public final TextView mUsernameView;
		public final TextView mStartDateView;
		public final TextView mEndDateView;
		public final TextView mCreatedAtView;

		public InfectionCaseOverviewRecyclerViewAdapterDataObject mItem;

		public ViewHolder(FragmentInfectionCaseOverviewItemBinding binding) {
			super(binding.getRoot());
			this.mIdView = binding.itemNumber;
			this.mUserTypeView = binding.userTypeView;
			this.mUsernameView = binding.usernameView;
			this.mStartDateView = binding.startDateView;
			this.mEndDateView = binding.endDateView;
			this.mCreatedAtView = binding.createdAtView;
		}

		public void bindListener(
			InfectionCaseOverviewRecyclerViewAdapterDataObject item,
			InfectionCaseOverviewFragmentInterface callingActivity
		) {
			this.itemView.setOnClickListener(v -> callingActivity.onInfectionCaseOverviewClick(item));
		}

		@Override
		public String toString() {
			return super.toString() + " '" + this.mItem.getInfectionCaseEntryEncData()
				.getDescription() + "'";
		}
	}
}