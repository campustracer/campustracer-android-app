package de.fhkiel.campustracer.exceptions;

public class CryptoDataDeserializationException extends CryptoBaseException {
	public CryptoDataDeserializationException(Throwable e) {
		super(e);
	}

	public CryptoDataDeserializationException(String message) {
		super(message);
	}
}
