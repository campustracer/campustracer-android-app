package de.fhkiel.campustracer.exceptions;

public class CryptoDecryptionException extends CryptoBaseException {
	public CryptoDecryptionException(Throwable e) {
		super(e);
	}
}
