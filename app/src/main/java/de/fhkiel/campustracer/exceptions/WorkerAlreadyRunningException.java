package de.fhkiel.campustracer.exceptions;

public class WorkerAlreadyRunningException extends Exception {
	private final String tag;

	public WorkerAlreadyRunningException(String tag) {
		this.tag = tag;
	}
}
