package de.fhkiel.campustracer.exceptions;

public class CryptoEncryptionException extends CryptoBaseException {
	public CryptoEncryptionException(Throwable e) {
		super(e);
	}
}
