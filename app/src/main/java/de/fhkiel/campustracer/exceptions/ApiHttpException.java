package de.fhkiel.campustracer.exceptions;

import java.io.IOException;
import java.util.Optional;

import retrofit2.Response;


public class ApiHttpException extends Exception {
	public static final int CONNECTION_ERROR = 0;
	public final Optional<Response> responseObject;
	public final int statusCode;

	public <T> ApiHttpException(Response<T> responseObject) {
		super(responseObjectErrorBodyToString(responseObject));
		this.responseObject = Optional.of(responseObject);
		this.statusCode = responseObject.code();
	}

	// Wrap IOExceptions, etc.
	public ApiHttpException(Throwable e) {
		super("Wrapped-ApiHttpException: " + e.getClass().getSimpleName() + ": " + e.getMessage(),
			e
		);
		this.responseObject = Optional.empty();
		this.statusCode = CONNECTION_ERROR;
	}

	// custom message + statusCode
	public ApiHttpException(String message, int statusCode) {
		super(message);
		this.responseObject = Optional.empty();
		this.statusCode = statusCode;
	}

	// custom message + statusCode
	public <T> ApiHttpException(String message, Response<T> responseObject) {
		super(message);
		this.statusCode = 0;
		this.responseObject = Optional.of(responseObject);
	}

	public int getStatusCode() {
		return this.statusCode;
	}

	public Optional<Response> getResponseObject() {
		return this.responseObject;
	}

	public Boolean isConnectionError() {
		return this.statusCode == CONNECTION_ERROR;
	}

	// Handles the edge case where errorBody.string() throws an IOException.
	// This method is necessary because the message must be passed into super(),
	// which must be the first statement in the constructor.
	private static String responseObjectErrorBodyToString(Response resp) {
		String message = "ApiHttpException (" + resp.code() + "): ";
		try {
			message += resp.errorBody().string();
		} catch (IOException e) {
			message += e.getClass().getSimpleName() + ": " + e.getMessage();
		}
		return message;
	}
}
