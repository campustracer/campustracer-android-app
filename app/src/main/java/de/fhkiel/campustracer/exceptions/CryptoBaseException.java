package de.fhkiel.campustracer.exceptions;

public class CryptoBaseException extends Exception {
	public CryptoBaseException(Throwable e) {
		super(e);
	}

	public CryptoBaseException(String message) {
		super(message);
	}
}
