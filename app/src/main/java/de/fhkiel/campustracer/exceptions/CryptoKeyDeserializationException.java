package de.fhkiel.campustracer.exceptions;

public class CryptoKeyDeserializationException extends CryptoBaseException {
	public CryptoKeyDeserializationException(Throwable e) {
		super(e);
	}

	public CryptoKeyDeserializationException(String message) {
		super(message);
	}
}
