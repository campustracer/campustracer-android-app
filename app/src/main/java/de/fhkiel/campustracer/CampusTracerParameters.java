package de.fhkiel.campustracer;

import org.bouncycastle.util.encoders.Hex;

import java.security.PublicKey;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;
import lombok.SneakyThrows;


public class CampusTracerParameters {
	public static final Integer GENERATE_PREGENERATED_KEYS_FOR_N_WEEKS = 12;
	public static final Integer PREGENERATED_KEYS_VALID_FOR_N_DAYS = 7;
	public static final Integer RANDOMIZED_LECTUREPRIVKEY_HASH_RANDMAX = 100;
	public static final Integer CHECK_IN_KEY_NONCE_RANDMAX = 25;
	public static final String UNIVERSITY_PUBLIC_KEY_HEX =
		"0200c633aeb92cbc762998218ddf3fb46f5c9d54c47ccdb16562d4920b3ce0d7e6";
	// PRIVATE KEY: bd9bb9be353352309dda6e464e7ed74076a9401d8cbcd3b6cb207de4c8b6c7c7
	// SCAN  BASE64: vZu5vjUzUjCd2m5GTn7XQHapQB2MvNO2yyB95Mi2x8c=
	public static final LocalDateTime UNIVERSITY_PUBLIC_KEY_CREATED_AT =
		LocalDateTime.of(2021, 1, 1, 0, 0, 0);

	@SneakyThrows(CryptoKeyDeserializationException.class)
	public static PublicKey getUniversityPublicKey() {
		return CryptoWrapper.deserializePubKey(Hex.decode(UNIVERSITY_PUBLIC_KEY_HEX));
	}

	public static List<PublicKey> getTrustedPartyPublicKeyList() {
		return Collections.emptyList();
	}

	private CampusTracerParameters() {
	}
}
