package de.fhkiel.campustracer.service;

import android.content.Context;

import androidx.core.util.Pair;
import androidx.room.Room;

import java.security.PublicKey;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import de.fhkiel.campustracer.UserState;
import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.model.InfectionCaseEntry;
import de.fhkiel.campustracer.model.KeysEnum;
import de.fhkiel.campustracer.model.KeystoreEntry;
import de.fhkiel.campustracer.model.OptionEntry;
import de.fhkiel.campustracer.model.OptionsEnum;
import de.fhkiel.campustracer.model.RolesEnum;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;
import de.fhkiel.campustracer.model.virtual.UserMasterKeyRelationEntry;
import de.fhkiel.campustracer.repository.sqlite.SqliteDatabase;
import lombok.Getter;
import lombok.Setter;


public class DatabaseService {
	private static final String TAG = "DatabaseService";
	private static final String DB_NAME = "campus_tracer_db";

	@Getter
	@Setter
	private SqliteDatabase db;

	protected DatabaseService(Context context) {
		this.db = Room.databaseBuilder(context, SqliteDatabase.class, DB_NAME)
			.allowMainThreadQueries()
			.build();
	}

	/*
	 * OptionRepository
	 */
	public String getOption(OptionsEnum key) {
		OptionEntry option = this.getDb().optionRepository().getOption(key);
		return option == null ? null : option.getValue();
	}

	public void setOption(OptionsEnum key, String value) {
		this.getDb().optionRepository().setOption(new OptionEntry(key, value));
	}

	public Optional<UserState> getUserState() {
		try {
			Long userId = Long.valueOf(this.getOption(OptionsEnum.USER_ID));
			String username = this.getOption(OptionsEnum.USERNAME);
			String password = this.getOption(OptionsEnum.PASSWORD);
			RolesEnum role = RolesEnum.valueOf(this.getOption(OptionsEnum.ROLE));
			if (username != null && password != null) {
				return Optional.of(new UserState(userId, username, password, role));
			}
		} catch (Exception ignored) {
			// nothing happens here
		}
		return Optional.empty();
	}

	public void setUserState(UserState userState) {
		this.getDb()
			.optionRepository()
			.setOptions(Arrays.asList(new OptionEntry(
					OptionsEnum.USER_ID,
					userState.getUserId().toString()
				),
				new OptionEntry(OptionsEnum.USERNAME, userState.getUsername()),
				new OptionEntry(OptionsEnum.PASSWORD, userState.getPassword()),
				new OptionEntry(OptionsEnum.ROLE, userState.getRole().name())
			));
	}

	/*
	 * CheckInRepository
	 */
	public long[] addNewInfectionCaseEntries(List<InfectionCaseEntry> infectionCaseEntries) {
		return this.getDb().checkInRepository().addNewInfectionCaseEntries(infectionCaseEntries);
	}

	public List<InfectionCaseEntry> getAllInfectionCaseEntries() {
		return this.getDb().checkInRepository().getAllInfectionCaseEntries();
	}

	public void removeInfectionCaseEntry(InfectionCaseEntry infectionCaseEntry) {
		this.getDb().checkInRepository().removeInfectionCaseEntry(infectionCaseEntry);
	}

	/*
	 * KeyRepository
	 */

	public long addToKeystore(KeystoreEntry keystoreEntry) {
		return this.getDb().keyRepository().addToKeystore(keystoreEntry);
	}

	public void addToOrUpdateKeystore(KeystoreEntry keystoreEntry) {
		this.getDb().keyRepository().addToOrUpdateKeystore(keystoreEntry);
	}

	public void deleteFromKeystore(KeystoreEntry keystoreEntry) {
		this.getDb().keyRepository().deleteFromKeystore(keystoreEntry);
	}

	public LocalDateTime getDateTimeOfLatestKeyEntry() {
		return this.getDb().keyRepository().getDateTimeOfLatestKeyEntry();
	}

	public Pair<long[], long[]> addOrUpdateMasterKeyEntries(List<UserMasterKeyEntry> userMasterKeyEntries) {
		return this.getDb().keyRepository().addOrUpdateUserMasterKeyEntries(userMasterKeyEntries);
	}

	public void addUserMasterKeyAndPreGeneratedKeys(UserMasterKeyEntry userMasterKeyEntry) {
		//this.getDb().keyRepository().
	}

	/*
	 * UserRepository
	 */

	public LocalDateTime getDateTimeOfLatestUserEntry() {
		return this.getDb().userRepository().getDateTimeOfLatestUserEntry();
	}

	public long[] addUserEntries(List<UserEntry> userEntries) {
		return this.getDb().userRepository().addUserEntries(userEntries);
	}

	public Optional<KeystoreEntry> getMasterKeyPairFromKeystore() {
		OptionEntry masterKeyOption =
			this.db.optionRepository().getOption(OptionsEnum.KEYSTORE_CURRENT_MASTER_KEY_ID);
		if (masterKeyOption == null) {
			return Optional.empty();
		}
		KeystoreEntry keystoreEntry = this.db.keyRepository()
			.getKeystoreEntryById(Long.parseLong(masterKeyOption.getValue()));
		if (keystoreEntry == null) {
			return Optional.empty();
		}
		return Optional.of(keystoreEntry);
	}

	public List<KeystoreEntry> getPreGenKeyPairsFromKeystore() {
		return this.db.keyRepository().getKeystoreEntriesByType(KeysEnum.PRE_GEN_KEYPAIR);
	}

	public List<KeystoreEntry> getCheckInKeyPairsFromKeystore() {
		return this.db.keyRepository().getKeystoreEntriesByType(KeysEnum.CHECK_IN_KEYPAIR);
	}

	public void clearAllTables() {
		this.db.clearAllTables();
	}

	public Optional<UserMasterKeyRelationEntry> getUserMasterKeyRelationEntrieyByMasterPubKey(
		PublicKey pubKey
	) {
		return this.db.keyRepository()
			.getUserMasterKeyRelationEntryByMasterPubKey(Base64.getEncoder()
				.encodeToString(CryptoWrapper.serializePubKey(pubKey)));
	}
}
