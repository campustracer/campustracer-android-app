package de.fhkiel.campustracer.service;

import android.util.ArrayMap;
import android.util.Base64;
import android.util.Log;

import com.squareup.moshi.Moshi;

import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import de.fhkiel.campustracer.UserState;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.helper.moshiadapter.ByteArrayAdapter;
import de.fhkiel.campustracer.helper.moshiadapter.LocalDateAdapter;
import de.fhkiel.campustracer.helper.moshiadapter.LocalDateTimeAdapter;
import de.fhkiel.campustracer.helper.moshiadapter.PublicKeyAdapter;
import de.fhkiel.campustracer.helper.moshiadapter.RolesEnumAdapter;
import de.fhkiel.campustracer.model.CheckInEntry;
import de.fhkiel.campustracer.model.InfectionCaseEntry;
import de.fhkiel.campustracer.model.LectureAttendedByUserEntry;
import de.fhkiel.campustracer.model.UserAttendedLectureEntry;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;
import de.fhkiel.campustracer.model.virtual.InfectionCaseCreateEntry;
import de.fhkiel.campustracer.repository.ApiRepository;
import lombok.Getter;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;


public class ApiService {
	private static final String TAG = "ApiService";
	public static final String BASE_URL = "https://campustracer.q0w.org/api/";
	// private static final String BASE_URL = "http://127.0.0.1:8898/api/";

	@Getter
	private ApiRepository repo;
	private String authtoken;

	protected ApiService() {
		this.repo = ApiService.getRetrofit().create(ApiRepository.class);
	}

	public static Retrofit getRetrofit() {
		/* For API-Request-Debugging
		OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
		HttpLoggingInterceptor httpLogger = new HttpLoggingInterceptor();
		// set your desired log level
		httpLogger.setLevel(HttpLoggingInterceptor.Level.BASIC);
		httpClient.addInterceptor(httpLogger);
		*/
		Moshi moshi = new Moshi.Builder().add(byte[].class, new ByteArrayAdapter())
			.add(LocalDate.class, new LocalDateAdapter())
			.add(LocalDateTime.class, new LocalDateTimeAdapter())
			.add(PublicKey.class, new PublicKeyAdapter())
			.add(RolesEnumAdapter.class, new RolesEnumAdapter())
			.build();
		return new Retrofit.Builder().baseUrl(BASE_URL)
			.addConverterFactory(MoshiConverterFactory.create(moshi))
			//.client(httpClient.build()) // For API-Request-Debugging
			.build();
	}

	public void setApiRepository(ApiRepository apiRepository) {
		this.repo = apiRepository;
	}

	public void authenticate(UserState userState) {
		this.authenticate(userState.getUsername(), userState.getPassword());
	}

	public void authenticate(String username, String password) {
		byte[] authdata = (username + ":" + password).getBytes(StandardCharsets.UTF_8);
		this.authtoken = "Basic " + Base64.encodeToString(authdata, Base64.NO_WRAP);
	}

	/**
	 * Set the authtoken manually, for testing purposes only!
	 *
	 * @param authtoken
	 */
	public void setAuthtoken(String authtoken) {
		this.authtoken = authtoken;
	}

	//this would suppress http response codes, which we however need to handle in the api-calling
	// methods ...
	private <T> T responseHandler(Call<T> call) throws ApiHttpException {
		Response<T> response;
		try {
			response = call.execute();
		} catch (IOException e) {
			Log.e(TAG, e.getStackTrace().toString());
			throw new ApiHttpException(e);
		}
		if (!response.isSuccessful()) {
			throw new ApiHttpException(response);
		}
		return response.body();
	}

	private void requireAuthToken() throws ApiHttpException {
		if (this.authtoken == null) {
			Log.e(TAG, "Authentication token was not set but is required by called method!");
			throw new ApiHttpException("Missing service authentication", 401);
		}
	}

	public List<UserMasterKeyEntry> getUserMasterKeyEntriesSince(LocalDateTime dateTime)
		throws ApiHttpException {
		this.requireAuthToken();
		Call<List<UserMasterKeyEntry>> call =
			this.getRepo().getAllUserMasterKeysAndPreGeneratedKeys(this.authtoken,
				dateTime == null ? null : dateTime.format(DateTimeFormatter.ISO_DATE_TIME)
			);
		return this.responseHandler(call);
	}

	public UserMasterKeyEntry addUserMasterKeyAndPreGeneratedKeys(
		UserMasterKeyEntry userMasterKeyEntry
	) throws ApiHttpException {
		this.requireAuthToken();
		Call<UserMasterKeyEntry> call =
			this.getRepo().addUserMasterKeyAndPreGeneratedKeys(userMasterKeyEntry, this.authtoken);
		return this.responseHandler(call);
	}

	public List<UserEntry> getUserEntries(LocalDateTime sinceDateTime) throws ApiHttpException {
		this.requireAuthToken();
		Call<List<UserEntry>> call = this.getRepo().getUserEntries(this.authtoken,
			sinceDateTime == null ? null : sinceDateTime.format(DateTimeFormatter.ISO_DATE_TIME)
		);
		return this.responseHandler(call);
	}

	public UserEntry getUserEntryByName(String username) throws ApiHttpException {
		this.requireAuthToken();
		Call<UserEntry> call = this.getRepo().getUserEntryByName(username, this.authtoken);
		return this.responseHandler(call);
	}

	public UserEntry putEncContactDataForUsername(String username, String encContactData)
		throws ApiHttpException {
		this.requireAuthToken();

		/* build:
		{ "encContactData": "..." }
		 */
		Map<String, Object> jsonParams = new ArrayMap<>();
		//put something inside the map, could be null
		jsonParams.put("encContactData", encContactData);

		RequestBody requestBodyWithEncContactData =
			RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
				(new JSONObject(jsonParams)).toString()
			);

		Call<UserEntry> call = this.getRepo()
			.putEncContactDataForUsername(username, requestBodyWithEncContactData, this.authtoken);
		return this.responseHandler(call);
	}

	public List<UserMasterKeyEntry> getMasterKeyEntriesForAuthenticatedUser()
		throws ApiHttpException {
		this.requireAuthToken();
		Call<List<UserMasterKeyEntry>> call =
			this.getRepo().getAllMasterKeyEntriesForAuthenticatedUser(this.authtoken);
		return this.responseHandler(call);
	}

	public CheckInEntry addCheckInEntry(CheckInEntry checkInEntry) throws ApiHttpException {
		Call<CheckInEntry> call = this.getRepo().createCheckIn(checkInEntry);
		return this.responseHandler(call);
	}

	public List<UserMasterKeyEntry> getCurrentlyValidMasterKeyEntriesByUserId(Long userId)
		throws ApiHttpException {
		this.requireAuthToken();
		Call<List<UserMasterKeyEntry>> call =
			this.repo.getCurrentlyValidMasterKeyEntriesByUserId(this.authtoken, userId);
		return this.responseHandler(call);
	}

	public InfectionCaseEntry addInfectionCase(InfectionCaseCreateEntry infectionCaseCreateEntry)
		throws ApiHttpException {
		Call<InfectionCaseEntry> call = this.repo.addInfectionCase(infectionCaseCreateEntry);
		return this.responseHandler(call);
	}

	public List<InfectionCaseEntry> getInfectionCaseEntriesSinceId(long infectionCaseId)
		throws ApiHttpException {
		this.requireAuthToken();
		Call<List<InfectionCaseEntry>> call =
			this.getRepo().getInfectionCaseEntriesSinceId(this.authtoken, infectionCaseId);
		return this.responseHandler(call);
	}

	public InfectionCaseEntry getInfectionCaseEntryById(Long infectionCaseId)
		throws ApiHttpException {
		this.requireAuthToken();
		Call<InfectionCaseEntry> call =
			this.getRepo().getInfectionCaseEntryById(infectionCaseId, this.authtoken);
		return this.responseHandler(call);
	}

	public List<LectureAttendedByUserEntry> getLectureAttendedByUserEntriesByLecturePrivKeyHashes(
		List<String> privKeyHashes
	) throws ApiHttpException {
		this.requireAuthToken();
		Call<List<LectureAttendedByUserEntry>> call =
			this.repo.getLectureAttendedByUserEntriesByLecturePrivKeyHashes(this.authtoken,
				privKeyHashes
			);
		return this.responseHandler(call);
	}

	public List<UserAttendedLectureEntry> getUserAttendedLectureEntriesByCheckInPrivateKeyHashes(
		List<String> privKeyHashes
	) throws ApiHttpException {
		this.requireAuthToken();
		Call<List<UserAttendedLectureEntry>> call =
			this.repo.getUserAttendedLectureEntriesByCheckInPrivateKeyHashes(this.authtoken,
				privKeyHashes
			);
		return this.responseHandler(call);
	}

	public List<UserAttendedLectureEntry> getUserAttendedLectureEntriesByDates(
		LocalDateTime dateFrom, LocalDateTime dateTo
	) throws ApiHttpException {
		this.requireAuthToken();
		Call<List<UserAttendedLectureEntry>> call =
			this.repo.getUserAttendedLectureEntriesByDates(this.authtoken, dateFrom, dateTo);
		return this.responseHandler(call);
	}
}
