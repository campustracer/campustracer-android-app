package de.fhkiel.campustracer.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapperDataInterface;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.helper.DateHelper;
import de.fhkiel.campustracer.model.CheckInEntry;
import de.fhkiel.campustracer.model.LectureAttendedByUserEntry;
import de.fhkiel.campustracer.model.PreGeneratedEncryptionKeyEntry;
import de.fhkiel.campustracer.model.RolesEnum;
import de.fhkiel.campustracer.model.UserAttendedLectureEntry;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;
import de.fhkiel.campustracer.model.virtual.UserMasterKeyRelationEntry;
import de.fhkiel.campustracer.repository.sqlite.SqliteDatabase;
import kotlin.NotImplementedError;


/**
 * This services tries to get records from the database and fetches them from the API and adds them
 * to the database if they don't exists
 * <p>
 * All records will always be fetched from the database so that fields like auto incrementing keys
 * are populated correctly. ApiHttpExceptions are passed to the calling method.
 */
public class CryptoWrapperService implements CryptoWrapperDataInterface {
	private static final String TAG = "CachingService";
	private final ApiService apiService;
	private final DatabaseService dbService;
	private final SqliteDatabase db;

	protected CryptoWrapperService(ApiService apiService, DatabaseService databaseService) {
		this.apiService = apiService;
		this.dbService = databaseService;
		this.db = databaseService.getDb();
	}

	@Override
	public UserMasterKeyEntry addUserMasterKeyAndPreGeneratedKeys(UserMasterKeyEntry userMasterKeyEntry)
		throws ApiHttpException {
		return this.apiService.addUserMasterKeyAndPreGeneratedKeys(userMasterKeyEntry);
	}

	@Override
	public void addCheckInEntry(CheckInEntry checkInEntry) {
		// TODO: Api
		this.db.checkInRepository().addCheckInEntry(checkInEntry);
	}

	@Override
	public void addUserEntryForMocking(UserEntry userEntry) {
		// Only required for mocking.
		// Background: There was previously a UserMasterKeyEntry.isLecturer attribute,
		// which was later removed from the model. The MockDatabase however used this to determine
		// if UMK belongs to a Lecturer or Student. This method was added to the MockDB along
		// with a UserEntry List. Therefore, the MockDB can now build up a User list and determine
		// the role of a user this way now.
	}

	@Override
	public Optional<UserMasterKeyEntry> getCurrentlyValidUserMasterKeyEntryForUserId(long userId) {
		Optional<UserMasterKeyRelationEntry> key =
			this.db.keyRepository().getValidUserMasterKeyRelationEntryForUser(userId);
		if (!key.isPresent()) {
			return Optional.empty();
		}
		return Optional.of(key.get().toUserMasterKeyEntry());
	}

	@Override
	public List<UserMasterKeyEntry> getUserMasterKeyEntriesForUserIdAndDate(
		long userId, LocalDate date
	) {
		return this.db.keyRepository()
			.getUserMasterKeyRelationEntriesForUserAndDate(userId, date)
			.stream()
			.map(UserMasterKeyRelationEntry::getUserMasterKeyEntry)
			.collect(Collectors.toList());
	}

	@Override
	public Optional<UserEntry> getUserEntryById(long userId) {
		return this.db.userRepository().getUserEntryById(userId);
	}

	@Override
	public List<UserMasterKeyEntry> getStudentMasterKeyEntriesForStudentIdAndDate(
		long studentID, LocalDate date
	) {
		if (Boolean.TRUE.equals(this.isUserIdAStudent(studentID))) {
			return this.getUserMasterKeyEntriesForUserIdAndDate(studentID, date);
			//return this.getLecturerMasterKeyEntriesForLecturerIdAndDate(studentID, date);
		}
		return new ArrayList<>();
	}

	@Override
	public List<UserMasterKeyEntry> getLecturerMasterKeyEntriesForLecturerIdAndDate(
		long lecturerID, LocalDate date
	) {
		if (Boolean.TRUE.equals(this.isUserIdALecturer(lecturerID))) {
			return this.getUserMasterKeyEntriesForUserIdAndDate(lecturerID, date);
		}
		return new ArrayList<>();
	}

	@Override
	public Optional<PreGeneratedEncryptionKeyEntry> getCurrentlyValidPreGeneratedKeyEntryForUserIdAndDate(
		long userId, LocalDate date
	) {
		Optional<UserMasterKeyRelationEntry> key =
			this.db.keyRepository().getValidUserMasterKeyRelationEntryForUser(userId);

		if (!key.isPresent()) {
			return Optional.empty();
		}

		for (PreGeneratedEncryptionKeyEntry pgek : key.get()
			.getPreGeneratedEncryptionKeyEntries()) {
			if (DateHelper.isBeforeOrEqual(pgek.getValidFrom(), date) && DateHelper.isAfterOrEqual(pgek.getValidTo(),
				date
			)) {
				return Optional.of(pgek);
			}
		}
		return Optional.empty();
	}

	@Override
	public Optional<PreGeneratedEncryptionKeyEntry> getPreGeneratedKeyEntryForUserIdAndDate(
		long userId, LocalDate date
	) {
		throw new NotImplementedError("NOT IMPLEMENTED!");
		//return Optional.empty();
	}

	@Override
	public List<UserAttendedLectureEntry> getUserAttendedLectureEntriesByCheckInPrivateKeyHashes(
		List<byte[]> privKeyHashes
	) throws ApiHttpException {
		List<String> privKeyHashesStrings = privKeyHashes.stream()
			.map(x -> Base64.getEncoder().encodeToString(x))
			.collect(Collectors.toList());
		return this.apiService.getUserAttendedLectureEntriesByCheckInPrivateKeyHashes(
			privKeyHashesStrings);
	}

	@Override
	public List<UserAttendedLectureEntry> getUserAttendedLectureEntriesByDates(
		LocalDateTime dateFrom, LocalDateTime dateTo
	) throws ApiHttpException {
		return this.apiService.getUserAttendedLectureEntriesByDates(dateFrom, dateTo);
	}

	@Override
	public List<LectureAttendedByUserEntry> getLectureAttendedByUserEntriesByLecturePrivKeyHashes(
		List<byte[]> lecturePrivKeyHash
	) throws ApiHttpException {
		List<String> privKeyHashesStrings = lecturePrivKeyHash.stream()
			.map(x -> Base64.getEncoder().encodeToString(x))
			.collect(Collectors.toList());
		return this.apiService.getLectureAttendedByUserEntriesByLecturePrivKeyHashes(
			privKeyHashesStrings);
	}

	@Override
	public Boolean isUserIdALecturer(long userId) {
		Optional<UserEntry> userOpt = this.db.userRepository().getUserEntryById(userId);
		return !userOpt.isPresent() ? false : userOpt.get().getRole().equals(RolesEnum.LECTURER);
	}

	@Override
	public Boolean isUserIdAStudent(long userId) {
		Optional<UserEntry> userOpt = this.db.userRepository().getUserEntryById(userId);
		return !userOpt.isPresent() ? false : userOpt.get().getRole().equals(RolesEnum.STUDENT);
	}

	@Override
	public void updateContactInformationForUser(String contactInformation) {
		// TODO: Api
		throw new NotImplementedError("NOT IMPLEMENTED");
	}
}
