package de.fhkiel.campustracer.service;

import android.content.Context;

import org.javatuples.Pair;

import java.security.KeyPair;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import de.fhkiel.campustracer.UserState;
import de.fhkiel.campustracer.entities.Lecturer;
import de.fhkiel.campustracer.entities.Student;
import de.fhkiel.campustracer.entities.User;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.model.InfectionCaseEntry;
import de.fhkiel.campustracer.model.KeystoreEntry;
import de.fhkiel.campustracer.model.OptionsEnum;
import de.fhkiel.campustracer.model.RolesEnum;
import de.fhkiel.campustracer.model.UserEntry;
import lombok.Getter;


// SINGLETON
public class AppService {
	private static final String TAG = "AppService";

	private static AppService instance;

	@Getter
	private ApiService apiService;
	@Getter
	private DatabaseService databaseService;
	@Getter
	private CryptoWrapperService cryptoWrapperService;

	private AppService(Context context) {
		this.apiService = new ApiService();
		this.databaseService = new DatabaseService(context);
		this.cryptoWrapperService = new CryptoWrapperService(this.apiService,
			this.databaseService);
	}

	public static AppService getInstance(Context context) {
		if (AppService.instance == null) {
			AppService.instance = new AppService(context);
		}
		return AppService.instance;
	}

	public static AppService getInstance() {
		if (AppService.instance == null) {
			throw new IllegalStateException(
				"No instance created: did you forget to call getInstance()"
					+ " with context on the current activity or fragment?");
		}
		return AppService.instance;
	}

	/*
	 * UserState
	 */

	public Optional<UserState> getUserState() {
		return this.databaseService.getUserState();
	}

	public void setUserState(UserState userState) {
		this.databaseService.setUserState(userState);
	}

	public void authenticate() {
		this.getUserState().ifPresent(state -> this.apiService.authenticate(state));
	}

	public void authenticate(String username, String password) {
		this.apiService.authenticate(username, password);
	}

	public Optional<User> getUserObject() {
		Optional<UserState> userStateOptional = this.getUserState();
		if (!userStateOptional.isPresent()) {
			return Optional.empty();
		}

		UserState userState = userStateOptional.get();
		User userObject;
		if (userState.getRole() == RolesEnum.UNIVERSITY) {
			return Optional.empty();
		}

		// get master key pair
		Optional<KeystoreEntry> masterKeystoreOptional =
			this.getDatabaseService().getMasterKeyPairFromKeystore();

		// get check in key pairs
		List<KeystoreEntry> checkInKeystoreList =
			this.databaseService.getCheckInKeyPairsFromKeystore();
		if (!masterKeystoreOptional.isPresent()) {
			return Optional.empty();
		}
		List<Pair<LocalDate, KeyPair>> checkInKeyPairs = checkInKeystoreList.stream()
			.map(k -> new Pair<>(k.getCreatedAt().toLocalDate(), k.getKeyPair()))
			.collect(Collectors.toList());

		// create user Objects
		if (userState.getRole() == RolesEnum.LECTURER) {
			userObject = new Lecturer(userState.getUserId(),
				masterKeystoreOptional.get().getKeyPair(),
				checkInKeyPairs
			);
		} else if (userState.getRole() == RolesEnum.STUDENT) {

			// student also needs preGenKeyPairs
			List<KeystoreEntry> preGenKeystoreList =
				this.databaseService.getPreGenKeyPairsFromKeystore();
			Map<Pair<LocalDateTime, LocalDateTime>, KeyPair> preGenKeyPairs =
				preGenKeystoreList.stream()
					.collect(Collectors.toMap(k -> new Pair<>(k.getValidFrom(), k.getValidTo()),
						KeystoreEntry::getKeyPair
					));

			userObject = new Student(userState.getUserId(),
				masterKeystoreOptional.get().getKeyPair(),
				checkInKeyPairs,
				preGenKeyPairs
			);
		} else {
			return Optional.empty();
		}
		return Optional.of(userObject);
	}

	/*
	 * ApiService
	 */

	public UserEntry getUserEntryByName(String username) throws ApiHttpException {
		return this.apiService.getUserEntryByName(username);
	}
	
	/*
	 * Database and API
	 */
	public List<InfectionCaseEntry> getInfectionCases() throws ApiHttpException {
		long latestInfectionCase = 0;
		try {
			latestInfectionCase = Long.parseLong(this.getDatabaseService()
				.getOption(OptionsEnum.LATEST_INFECTION_CASE_ID));
		} catch (NumberFormatException ignore) {
		}
		System.out.println("INFECTION CASE ID: " + latestInfectionCase);

		List<InfectionCaseEntry> newInfectionCaseEntries =
			this.getApiService().getInfectionCaseEntriesSinceId(latestInfectionCase);
		if (!newInfectionCaseEntries.isEmpty()) {
			// add all new infection cases to database and update option
			this.getDatabaseService().addNewInfectionCaseEntries(newInfectionCaseEntries);
			Optional<InfectionCaseEntry> latestEntry = newInfectionCaseEntries.stream()
				.max(Comparator.comparingLong(InfectionCaseEntry::getId));
			latestEntry.ifPresent(infectionCaseEntry -> this.getDatabaseService()
				.setOption(OptionsEnum.LATEST_INFECTION_CASE_ID,
					Long.toString(infectionCaseEntry.getId())
				));
		}

		return this.getDatabaseService().getAllInfectionCaseEntries();
	}
}
