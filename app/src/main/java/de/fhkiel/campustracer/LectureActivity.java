package de.fhkiel.campustracer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.bouncycastle.util.encoders.Hex;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.fhkiel.campustracer.databinding.ActivityLectureBinding;
import de.fhkiel.campustracer.entities.Lecturer;
import de.fhkiel.campustracer.entities.User;
import de.fhkiel.campustracer.entities.qrcode.Lecture;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.model.PreGeneratedEncryptionKeyEntry;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.ui.DeviceAuthHelper;
import de.fhkiel.campustracer.ui.dialogs.InfoDialog;
import de.fhkiel.campustracer.ui.dialogs.LoaderDialog;
import de.fhkiel.campustracer.ui.qr_display.QrCodeDisplayFragment;
import de.fhkiel.campustracer.ui.qr_display.QrCodeDisplayFragmentInterface;
import de.fhkiel.campustracer.ui.qr_display.QrCodeDisplayViewModel;


public class LectureActivity extends AppCompatActivity implements QrCodeDisplayFragmentInterface {

	private static final String TAG = "LectureActivity";
	public static final String LECTURE_OBJECT_KEY = "LECTURE_OBJECT_KEY";
	public static final Long QR_UPDATE_INTERVAL_MS = 1000L;
	public static boolean isFirstSyncObserverCall = false;
	private ActivityLectureBinding binding;
	private AppInitializer appInitializer;
	private Lecture lecture;
	private Lecturer lecturer;
	private QrCodeDisplayViewModel mQrCodeDisplayViewModel;
	private int authFailCounter = 0;

	public LectureActivity() {
		super(R.layout.activity_lecture);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.binding = ActivityLectureBinding.inflate(this.getLayoutInflater());

		this.appInitializer = AppInitializer.getInstance(this);
		this.mQrCodeDisplayViewModel =
			new ViewModelProvider(this).get(QrCodeDisplayViewModel.class);

		Serializable serializedLecture = this.getIntent().getSerializableExtra(LECTURE_OBJECT_KEY);
		if (serializedLecture == null) {
			throw new IllegalArgumentException("MISSING LECTURE OBJECT!");
		}
		this.lecture = (Lecture) serializedLecture;
		Optional<User> userObjectOpt = this.appInitializer.getAppService().getUserObject();
		userObjectOpt.ifPresent(user -> this.lecturer = (Lecturer) user);

		if (savedInstanceState == null) {
			// QrCode Display Fragment
			Bundle qrCodeDisplayBundle = new Bundle();
			qrCodeDisplayBundle.putString(QrCodeDisplayFragment.TITLE_KEY,
				this.getString(R.string.lecture_check_in)
			);
			qrCodeDisplayBundle.putString(QrCodeDisplayFragment.DESCRIPTION_KEY,
				this.getString(R.string.scan_to_check_in)
			);
			qrCodeDisplayBundle.putBoolean(QrCodeDisplayFragment.SHOW_SAVE_BUTTON_KEY, false);
			qrCodeDisplayBundle.putBoolean(QrCodeDisplayFragment.SHOW_CLOSE_BUTTON_KEY, false);

			this.getSupportFragmentManager()
				.beginTransaction()
				.setReorderingAllowed(true)
				.replace(R.id.lecture_fragment_container,
					QrCodeDisplayFragment.class,
					qrCodeDisplayBundle
				)
				.commit();

			// Update QR-Code every second
			Handler handler = new Handler(this.getMainLooper());
			Runnable qrCodeUpdaterRunnable = new Runnable() {
				@Override
				public void run() {
					String qrPayload = Base64.getEncoder()
						.encodeToString((LectureActivity.this.lecture.getNextLectureQRCode()
							.toBytes()));
					LectureActivity.this.mQrCodeDisplayViewModel.setQrCodePayload(qrPayload);
					handler.postDelayed(this, QR_UPDATE_INTERVAL_MS);
				}
			};
			handler.post(qrCodeUpdaterRunnable);
		}

		// CHECK IF AUTHENTICATION IS AVAILABLE ON THIS DEVICE (Fingerprint/Password/PIN/Pattern)
		// PROMPT IF NOT SET UP! (THIS IS ALSO ALREADY DONE IN CreateLectureFragment
		DeviceAuthHelper.requireDeviceAuthenticationSetUp(this);

		// Get BottomNav View
		BottomNavigationView bottomNav = this.findViewById(R.id.nav_view);
		bottomNav.setOnNavigationItemSelectedListener((BottomNavigationView.OnNavigationItemSelectedListener) menuitem -> {
			Class<? extends Fragment> fragmentClass = null;
			Bundle argsBundle = new Bundle();
			Log.w(TAG, "SELECTED ITEM: " + menuitem.getItemId());
			switch (menuitem.getItemId()) {
				case R.id.navigation_quit_lecture:
					DeviceAuthHelper.requireDeviceAuthentication(this, result -> {
						this.leaveLectureActivity();
					});
					break;
				case R.id.navigation_check_in_no_app:

					this.openStudentCheckInDialog();

					break;
			}
			return false;
		});
	}

	private void openStudentCheckInDialog() {
		final EditText studIdEditText = new EditText(this);
		studIdEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
		studIdEditText.setHint(R.string.student_id);
		/*
		 SHOW DIALOG WITH STUDENTID FIELD
		*/
		new InfoDialog(this, R.string.title_checkin_no_app, R.string.checkin_no_app_descr).setView(
			studIdEditText).setPositiveButton(R.string.title_check_in, (dialog, which) -> {
			this.authFailCounter = 0;
			String enteredStudentId = studIdEditText.getText().toString();

			// Validate Input
			Pattern pattern = Pattern.compile("^([0-9]+)$");
			Matcher matcher = pattern.matcher(enteredStudentId);
			if (!matcher.matches()) {
				new InfoDialog(this,
					R.string.error,
					R.string.enter_a_valid_studentid
				).setPositiveButton(R.string.button_ok).show();
				return;
			}
			dialog.dismiss();

			// Require fingerprint auth (by lecturer)
			DeviceAuthHelper.requireDeviceAuthentication(this, result -> {
				Optional<UserEntry> studentUserEntryOptional = this.appInitializer.getAppService()
					.getCryptoWrapperService()
					.getUserEntryById(Long.parseLong(enteredStudentId));
				if (!studentUserEntryOptional.isPresent()) {
					// UserId not found!
					new InfoDialog(this, R.string.error).setMessage(R.string.studentid_not_found,
						enteredStudentId
					)
						.setNegativeButton(R.string.button_ok)
						.show();
					return;
				}

				// Confirm student identity dialog
				UserEntry studentUserEntry = studentUserEntryOptional.get();
				new InfoDialog(this,
					R.string.title_student_check_in
				).setMessage(R.string.check_student_details_message,
					Long.toString(studentUserEntry.getId()),
					studentUserEntry.getUsername()
				).setPositiveButton(R.string.button_ok, (dialog12, which12) -> {
					dialog12.dismiss();
					// Check if valid pregenkey exists for student
					Optional<PreGeneratedEncryptionKeyEntry> studentPregenKeyEntryOpt =
						this.appInitializer.getAppService()
							.getCryptoWrapperService()
							.getCurrentlyValidPreGeneratedKeyEntryForUserIdAndDate(Long.parseLong(
								enteredStudentId), this.lecture.getLectureDate());
					if (!studentPregenKeyEntryOpt.isPresent()) {
						// Student has no currently valid MK
						new InfoDialog(this,
							R.string.error,
							R.string.student_has_no_valid_masterkey
						).setPositiveButton(R.string.button_abort).show();
						return;
					}
					// Check in student
					this.checkInStudentWithoutApp(studentUserEntry);
				}).setNegativeButton(R.string.button_abort).show();
			});
		}).setNegativeButton(R.string.button_abort, (dialog, which) -> {
			if (this.authFailCounter++ == 5) {
				this.runOnUiThread(() -> Toast.makeText(this, new String(
					Hex.decode(
						"596f7520666f756e6420746865207365637265742045737465722045676721205765617220796f7572206d61736b20616e642073746179207361666521"),
					StandardCharsets.UTF_8
				), Toast.LENGTH_LONG).show());
			}
		}).show();

		this.runOnUiThread(() -> {
			// Fix Layout (EditText is originally too wide)
			float displayDensity = this.getResources().getDisplayMetrics().density;
			FrameLayout.LayoutParams layoutParams =
				new FrameLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
					FrameLayout.LayoutParams.MATCH_PARENT
				);
			layoutParams.setMargins(Math.round(32 * displayDensity),
				0,
				Math.round(32 * displayDensity),
				0
			);
			studIdEditText.setLayoutParams(layoutParams);
		});
	}

	private void checkInStudentWithoutApp(UserEntry studentUserEntry) {
		new Thread((
		) -> {
			try {
				long studentId = studentUserEntry.getId();
				this.lecturer.checkInStudentWithoutApp((studentId),
					this.lecture,
					CampusTracerParameters.getUniversityPublicKey()
				);
				new InfoDialog(this,
					R.string.title_student_check_in
				).setMessage(R.string.student_check_in_success, studentUserEntry.getUsername())
					.setPositiveButton(R.string.button_ok)
					.show();
			} catch (CryptoEncryptionException | ApiHttpException e) {
				e.printStackTrace();
				new InfoDialog(this, e).setPositiveButton(R.string.retry, (dialog, which) -> {
					dialog.dismiss();
					this.checkInStudentWithoutApp(studentUserEntry);
				}).setNegativeButton(R.string.button_abort).show();
			}
		}).start();
	}

	@Override
	protected void onActivityResult(
		int requestCode, int resultCode, @Nullable Intent data
	) {
		super.onActivityResult(requestCode, resultCode, data);
		DeviceAuthHelper.onActivityResultAuthSetUpHandler(resultCode, resultCode, data, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = this.getMenuInflater();
		inflater.inflate(R.menu.lecture_actionbar_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_quit_lecture:
				Activity that = this;
				DeviceAuthHelper.requireDeviceAuthentication(this, result -> {
					this.leaveLectureActivity();
				});
				break;
			case R.id.action_sync_backend:
				LoaderDialog dwLoader = new LoaderDialog(R.string.backend_sync, this, true);
				dwLoader.show();
				DownloadWorker.startOneTimeWorker(this.appInitializer.getWorkManager());
				DownloadWorker.getInitialSyncState().observe(this, syncState -> {
					if (!isFirstSyncObserverCall) {
						isFirstSyncObserverCall = true;
						// observer automatically emits current value when attaching
						return;
					}
					if (syncState == DownloadWorker.InitialSyncStateEnum.FAILED) {
						dwLoader.dismiss();
						new InfoDialog(LectureActivity.this,
							R.string.backend_sync,
							R.string.sync_error
						).setPositiveButton().show();
						isFirstSyncObserverCall = false;
						DownloadWorker.getInitialSyncState().removeObservers(LectureActivity.this);
					} else if (syncState == DownloadWorker.InitialSyncStateEnum.DONE) {
						dwLoader.dismiss();
						new InfoDialog(LectureActivity.this,
							R.string.backend_sync,
							R.string.backend_sync_complete_msg
						).setPositiveButton().show();
					} else {
						return;
					}
					isFirstSyncObserverCall = false;
					DownloadWorker.getInitialSyncState().removeObservers(LectureActivity.this);
				});
				break;
			default:
				return super.onOptionsItemSelected(item);
		}
		return true;
	}

	private void leaveLectureActivity() {
		new InfoDialog(this,
			R.string.quit_lecture_question,
			R.string.quit_lecture_question_text
		).setPositiveButton(R.string.stay)
			.setNegativeButton(R.string.quit_lecture, (dialog, which) -> {
				dialog.dismiss();
				this.finish();
				Intent intent = new Intent(this, UserActivity.class);
				this.startActivity(intent);
			})
			.show();
	}

	@Override
	public void onQrCodeDisplayFragmentCloseButtonClick() {
		return;
	}

	@Override
	public void onBackPressed() {
		// ONLY ALLOW BACK-BUTTON WHEN AUTHENTICATED W/ FINGERPRINT
		DeviceAuthHelper.requireDeviceAuthentication(this, result -> {
			//super.onBackPressed(); -> may lead back to Splashscreen?!
			this.leaveLectureActivity();
		});
	}
}