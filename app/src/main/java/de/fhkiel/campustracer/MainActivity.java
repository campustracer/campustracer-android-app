package de.fhkiel.campustracer;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import de.fhkiel.campustracer.databinding.ActivityMainBinding;


public class MainActivity extends AppCompatActivity {
	private static final String TAG = "MainActivity";

	private ActivityMainBinding binding;
	private AppInitializer appInitializer;
	private static final Handler splashscreenHandler = new Handler(Looper.getMainLooper());

	private static final long SPLASHSCREEN_DURATION = 2000; //ms

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.appInitializer = AppInitializer.getInstance(this);

		this.binding = ActivityMainBinding.inflate(this.getLayoutInflater());
		this.setContentView(this.binding.getRoot());

		if (savedInstanceState == null) {
			this.showSplashscreen();
		}
	}

	private void showSplashscreen() {
		splashscreenHandler.removeCallbacksAndMessages(null); // cancel pending postDelayed
		splashscreenHandler.postDelayed(() -> {
			// If no UserState -> redirect to RegisterActivity
			Log.w(TAG, "SPLASHSCREEN END; OPENING NEXT ACTIVITY");
			this.finish();
			if (!this.appInitializer.getAppService().getUserState().isPresent()) {
				this.startActivity(new Intent(this, RegisterActivity.class));
			} else {
				this.startActivity(new Intent(this, UserActivity.class));
			}
		}, SPLASHSCREEN_DURATION);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// save current state

	}

	@Override
	protected void onRestoreInstanceState(Bundle savedState) {
		super.onRestoreInstanceState(savedState);
		// restore state

	}
}