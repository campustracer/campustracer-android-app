package de.fhkiel.campustracer;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.javatuples.Pair;

import java.security.KeyPair;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.databinding.ActivityUserBinding;
import de.fhkiel.campustracer.entities.Lecturer;
import de.fhkiel.campustracer.entities.User;
import de.fhkiel.campustracer.entities.qrcode.Lecture;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoDecryptionException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;
import de.fhkiel.campustracer.model.KeystoreEntry;
import de.fhkiel.campustracer.model.PreGeneratedEncryptionKeyEntry;
import de.fhkiel.campustracer.model.RolesEnum;
import de.fhkiel.campustracer.model.encdata.InfectionCaseEntryEncData;
import de.fhkiel.campustracer.model.virtual.UserMasterKeyRelationEntry;
import de.fhkiel.campustracer.tracing.ContactTracing;
import de.fhkiel.campustracer.ui.DeviceAuthHelper;
import de.fhkiel.campustracer.ui.dashboard.DashboardFragment;
import de.fhkiel.campustracer.ui.dialogs.InfoDialog;
import de.fhkiel.campustracer.ui.dialogs.LoaderDialog;
import de.fhkiel.campustracer.ui.infection_case_detail.InfectionCaseDetailFragment;
import de.fhkiel.campustracer.ui.infection_case_overview.InfectionCaseOverviewFragment;
import de.fhkiel.campustracer.ui.infection_case_overview.InfectionCaseOverviewFragmentInterface;
import de.fhkiel.campustracer.ui.infection_case_overview.InfectionCaseOverviewRecyclerViewAdapterDataObject;
import de.fhkiel.campustracer.ui.infection_case_report.InfectionCaseReportFragment;
import de.fhkiel.campustracer.ui.lecture.CreateLectureFragment;
import de.fhkiel.campustracer.ui.lecture.CreateLectureFragmentInterface;
import de.fhkiel.campustracer.ui.loader.LoaderFragment;
import de.fhkiel.campustracer.ui.loader.LoaderViewModel;
import de.fhkiel.campustracer.ui.qr_scanner.QrScannerFragment;
import de.fhkiel.campustracer.ui.qr_scanner.QrScannerFragmentInterface;


public class UserActivity extends AppCompatActivity
	implements ActivityCompat.OnRequestPermissionsResultCallback, QrScannerFragmentInterface,
	CreateLectureFragmentInterface, InfectionCaseOverviewFragmentInterface {
	private static final String TAG = "UserActivity";

	private ActivityUserBinding binding;
	private AppInitializer appInitializer;
	private LoaderViewModel mLoaderViewModel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.appInitializer = AppInitializer.getInstance(this);

		this.mLoaderViewModel = new ViewModelProvider(this).get(LoaderViewModel.class);

		this.binding = ActivityUserBinding.inflate(this.getLayoutInflater());
		this.setContentView(this.binding.getRoot());

		// BOTTOM NAVIGATION
		// Load Default Fragment
		this.getSupportFragmentManager()
			.beginTransaction()
			.setReorderingAllowed(true)
			.replace(R.id.nav_fragment_container_activity_user, DashboardFragment.class, null)
			.commit();

		Optional<UserState> userStateOptional = this.appInitializer.getAppService().getUserState();
		if (!userStateOptional.isPresent()) {
			this.startActivity(new Intent(this, RegisterActivity.class));
			return;
		}

		// Get BottomNav View
		BottomNavigationView bottomNav = this.findViewById(R.id.nav_view);
		// Hide BottomNav Buttons according to Role
		List<Integer> disableMenuItems = new ArrayList<>();
		switch (userStateOptional.get().getRole()) {
			case STUDENT:
				disableMenuItems.add(R.id.navigation_infection_case_overview);
				disableMenuItems.add(R.id.navigation_new_lecture);
				break;
			case LECTURER:
				disableMenuItems.add(R.id.navigation_infection_case_overview);
				break;
			case UNIVERSITY:
				disableMenuItems.add(R.id.navigation_check_in);
				disableMenuItems.add(R.id.navigation_new_lecture);
				break;
		}
		disableMenuItems.forEach(i -> bottomNav.getMenu().findItem(i).setVisible(false));

		// BottomNav Listener
		bottomNav.setOnNavigationItemSelectedListener((BottomNavigationView.OnNavigationItemSelectedListener) menuitem -> {
			Class<? extends Fragment> fragmentClass = null;
			Bundle argsBundle = new Bundle();
			Log.w(TAG, "SELECTED ITEM: " + menuitem.getItemId());
			switch (menuitem.getItemId()) {
				case R.id.navigation_check_in:
					fragmentClass = QrScannerFragment.class;
					argsBundle.putString(QrScannerFragment.SCAN_MSG_KEY,
						this.getString(R.string.check_in_scan_message)
					);
					break;
				case R.id.navigation_infection_case_overview:
					fragmentClass = InfectionCaseOverviewFragment.class;
					break;
				case R.id.navigation_new_lecture:
					fragmentClass = CreateLectureFragment.class;
					break;
				case R.id.navigation_dashboard:
				default:
					fragmentClass = DashboardFragment.class;
			}
			return this.navigateToFragment(fragmentClass, argsBundle);
		});

		// DownloadWorker: Show LoaderDialog until first Sync is complete!
		if ((userStateOptional.get().getRole() == RolesEnum.UNIVERSITY
			|| userStateOptional.get().getRole() == RolesEnum.LECTURER)
			&& (DownloadWorker.getInitialSyncState().getValue()
			== DownloadWorker.InitialSyncStateEnum.RUNNING
			|| DownloadWorker.getInitialSyncState().getValue()
			== DownloadWorker.InitialSyncStateEnum.NOT_STARTED)) {
			LoaderDialog dwLoader = new LoaderDialog(R.string.backend_sync, this);
			dwLoader.show();
			DownloadWorker.getInitialSyncState().observe(this, syncState -> {
				if (syncState == DownloadWorker.InitialSyncStateEnum.FAILED) {
					dwLoader.dismiss();
					new InfoDialog(this,
						R.string.backend_sync,
						R.string.sync_error
					).setPositiveButton()
						.show();
				} else if (syncState == DownloadWorker.InitialSyncStateEnum.DONE) {
					dwLoader.dismiss();
				}
			});
		}
	}

	private boolean navigateToFragment(
		Class<? extends Fragment> fragmentClass, Bundle argsBundle
	) {
		this.getSupportFragmentManager()
			.beginTransaction()
			.setReorderingAllowed(true)
			.replace(R.id.nav_fragment_container_activity_user, fragmentClass, argsBundle)
			.commit();
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar

		MenuInflater inflater = this.getMenuInflater();
		inflater.inflate(R.menu.user_actionbar_menu, menu);

		// Hide Option Entries depending on Role
		Optional<UserState> userStateOptional = this.appInitializer.getAppService().getUserState();
		userStateOptional.ifPresent(userState -> {
			if (userState.getRole() == RolesEnum.UNIVERSITY) {
				MenuItem reportItem = menu.findItem(R.id.action_report_infection);
				reportItem.setVisible(false);
			}
		});

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
			case R.id.action_report_infection:
				return this.navigateToFragment(InfectionCaseReportFragment.class, new Bundle());
			case R.id.action_logout:
				new InfoDialog(this,
					R.string.logout_questionmark,
					R.string.logout_confirm_message
				).setPositiveButton(R.string.stay_logged_in)
					.setNegativeButton(R.string.logout_confirm_button, (dialog, which) -> {
						dialog.dismiss();
						this.performLogout();
					})
					.show();
				break;
			default:
				return super.onOptionsItemSelected(item);
		}
		return true;
	}

	public void performLogout() {
		// stop running workers
		this.appInitializer.stopWorkers();
		// clear all SQLite DB Tables (includes UserState)
		this.appInitializer.getAppService().getDatabaseService().clearAllTables();
		this.finish();
		this.startActivity(new Intent(this, RegisterActivity.class));
	}

	@Override
	// Check-In
	public void onSuccessfulQrScan(String scannedCode) {
		this.showLoaderFragment(R.string.title_check_in, R.string.checking_in_wait);
		this.performCheckIn(scannedCode);
	}

	private void performCheckIn(String scannedCode) {

		HandlerThread newThread = new HandlerThread(TAG + "CheckInHandlerThread");
		newThread.start();
		Handler handler = new Handler(newThread.getLooper());
		handler.post(() -> { // thread needed for api calls (must not be on UI thread!)
			try {
				Optional<User> userObjectOptional =
					this.appInitializer.getAppService().getUserObject();

				if (!userObjectOptional.isPresent()) {
					throw new IllegalStateException("ERROR, USERSTATE/USEROBJECT OPTIONAL IS "
						+ "EMPTY!");
				}
				User userObject = userObjectOptional.get();

				userObject.checkInWithQRCode(Base64.getDecoder().decode(scannedCode),
					CampusTracerParameters.getUniversityPublicKey(),
					LocalDate.now()
				);

				// show loader, wait 2s, show SUCCESS Image, wait 1s, forward
				handler.postDelayed(() -> {
					this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.SUCCESS);
					handler.postDelayed(() -> new InfoDialog(this,
						R.string.title_check_in,
						R.string.check_in_ok_message
					).setPositiveButton(R.string.button_ok).show(), 1000);
				}, 500);
			} catch (ApiHttpException | IllegalStateException | CryptoEncryptionException | CryptoDataDeserializationException | CryptoKeyDeserializationException e) {
				e.printStackTrace();
				this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.ERROR);
				new InfoDialog(this, e).setPositiveButton(R.string.retry, (dialog, which) -> {
					dialog.dismiss();
					this.findViewById(R.id.navigation_check_in).callOnClick();
				}).replaceErrorResource(R.string.error, R.string.title_check_in).show();
			}
		});
	}

	private void showLoaderFragment(
		@StringRes int actionBarTitle, @StringRes int loaderMessage
	) {
		this.runOnUiThread(() -> this.getSupportActionBar().setTitle(actionBarTitle));

		Bundle argsBundle = new Bundle();
		argsBundle.putInt(LoaderFragment.INITIAL_MESSAGE_KEY, loaderMessage);
		this.getSupportFragmentManager()
			.beginTransaction()
			.setReorderingAllowed(true)
			.replace(R.id.nav_fragment_container_activity_user, new LoaderFragment(loaderMessage))
			.commit();
	}

	@Override
	public void onCreateLectureButtonClick() {
		if (!DeviceAuthHelper.checkDeviceAuthenticationSetUp(this)) {
			DeviceAuthHelper.requireDeviceAuthenticationSetUp(this);
			return;
		}

		// Start LoaderFragment
		this.showLoaderFragment(R.string.new_lecture, R.string.creating_lecture);

		Optional<User> userObjectOptional = this.appInitializer.getAppService().getUserObject();
		if (!userObjectOptional.isPresent() || !userObjectOptional.get().isLecturer()) {
			throw new IllegalStateException("UserObject not loadable or is not a lecturer!");
		}
		Lecturer lecturer = (Lecturer) userObjectOptional.get();

		// needed because Api calls must not run on main thread!
		HandlerThread newThread = new HandlerThread("UserActivityCreateLectureThread");
		newThread.start();
		Handler handler = new Handler(newThread.getLooper());
		// Create Lecture

		// Lecturer Self-Check-In
		handler.postDelayed(() -> {
			this.mLoaderViewModel.postMessage(R.string.checking_in_wait);
			final Lecture lecture;
			try {
				lecture = new Lecture(CampusTracerParameters.getUniversityPublicKey(),
					LocalDate.now(),
					"AUTHKEY"
				);
				lecturer.checkInToLecture(lecture,
					CampusTracerParameters.getUniversityPublicKey());
				handler.postDelayed(() -> {
					this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.SUCCESS);
					handler.postDelayed(() -> {
						this.finish();
						Intent intent1 = new Intent(this, LectureActivity.class);
						Bundle iBundle = new Bundle();
						iBundle.putSerializable(LectureActivity.LECTURE_OBJECT_KEY, lecture);
						intent1.putExtras(iBundle);
						this.startActivity(intent1);
					}, 1000);
				}, 1000);
			} catch (ApiHttpException | CryptoEncryptionException | IllegalStateException | CryptoKeyDeserializationException | CryptoDataDeserializationException e) {
				e.printStackTrace();
				this.mLoaderViewModel.postStage(LoaderViewModel.LoaderViewStage.ERROR);
				new InfoDialog(this, e).setPositiveButton(R.string.retry, (dialog, which) -> {
					dialog.dismiss();
					this.findViewById(R.id.navigation_check_in).callOnClick();
				}).replaceErrorResource(R.string.error, R.string.title_check_in).show();
			}
		}, 1500);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// save current state

	}

	@Override
	protected void onRestoreInstanceState(Bundle savedState) {
		super.onRestoreInstanceState(savedState);
		// restore state

	}

	@Override
	public void onInfectionCaseOverviewClick(InfectionCaseOverviewRecyclerViewAdapterDataObject icObject) {
		new InfoDialog(this, R.string.infection_tracing_title).setMessage(
			R.string.infection_tracing_confirm,
			icObject.getInfectionCaseId()
		).setPositiveButton(R.string.button_ok, (dialog, which) -> {
			dialog.dismiss();
			this.performInfectionTracing(icObject);
		}).setNegativeButton(R.string.button_abort, (dialog, which) -> {
			dialog.dismiss();
		}).show();
	}

	private void performInfectionTracing(InfectionCaseOverviewRecyclerViewAdapterDataObject icObject) {

		new Thread(() -> {

			LoaderDialog loaderDialog = new LoaderDialog(R.string.ic_in_progress, this);
			loaderDialog.show();
			// START TRACING
			try {
				InfectionCaseEntryEncData infectionCase = icObject.getInfectionCaseEntryEncData();

				// PREPARE DATA FOR CONTACTTRACING-CONSTRUCTOR

				// CheckIn KeyPairs
				List<KeyPair> checkInKeyPairList = infectionCase.getCheckInPrivkeys()
					.stream()
					.map(priv -> new KeyPair(CryptoWrapper.calculatePubKeyFromPrivKey(priv),
						priv
					))
					.collect(Collectors.toList());
				List<KeyPair> preGenKeyPairList = infectionCase.getPreGenPrivKeys()
					.stream()
					.map(priv -> new KeyPair(CryptoWrapper.calculatePubKeyFromPrivKey(priv),
						priv
					))
					.collect(Collectors.toList());

				// UserMasterKey
				Optional<UserMasterKeyRelationEntry> umkRelEntryOpt =
					this.appInitializer.getAppService()
						.getDatabaseService()
						.getUserMasterKeyRelationEntrieyByMasterPubKey(infectionCase.getMasterPublicKey());
				if (!umkRelEntryOpt.isPresent()) {
					loaderDialog.dismiss();
					new InfoDialog(this,
						R.string.error,
						R.string.error_masterkey_not_found
					).setPositiveButton().show();
					Log.e(TAG,
						"The MasterKey provided in the Infection Case does not exist "
							+ "in the DB!"
					);
					return;
				}
				UserMasterKeyRelationEntry umkRelEntry = umkRelEntryOpt.get();
				List<PreGeneratedEncryptionKeyEntry> preGeneratedEncryptionKeyEntriesListFromDb =
					umkRelEntry.getPreGeneratedEncryptionKeyEntries();

				// recreate:  preGeneratedKeyPairsMap
				Map<Pair<LocalDateTime, LocalDateTime>, KeyPair> preGeneratedKeyPairsMap =
					new HashMap<>();
				for (KeyPair preGenKeyPair : preGenKeyPairList) {
					// find
					Optional<PreGeneratedEncryptionKeyEntry> preGenKeyFromDbOptional =
						preGeneratedEncryptionKeyEntriesListFromDb.stream()
							.filter(x -> Arrays.equals(
								CryptoWrapper.serializePubKey(x.getPreGeneratedPubKey()),
								CryptoWrapper.serializePubKey(preGenKeyPair.getPublic())
							))
							.findFirst();
					if (!preGenKeyFromDbOptional.isPresent()) {
						loaderDialog.dismiss();
						new InfoDialog(this,
							R.string.error,
							R.string.error_pregenkey_masterkey_mismatch
						).setPositiveButton().show();
						Log.e(TAG,
							"A preGenKey provided in the InfectionCase does not belong"
								+ " to the also provided MasterPubKey"
						);
						return;
					}
					// verify signature of preGenKey!
					if (Boolean.FALSE.equals(preGenKeyFromDbOptional.get()
						.verifySignature(infectionCase.getMasterPublicKey()))) {
						loaderDialog.dismiss();
						new InfoDialog(this,
							R.string.error,
							R.string.pubkey_pregenkey_signature_mismatch
						).setPositiveButton().show();
						Log.e(TAG,
							"Signature mismatch between PreGenKey and PublicKey, both "
								+ "provided in Infection Case!"
						);
						return;
					}

					// All OK, add to Map
					preGeneratedKeyPairsMap.put(new Pair<>(preGenKeyFromDbOptional.get()
						.getValidFrom(),
						preGenKeyFromDbOptional.get().getValidTo()
					), preGenKeyPair);
				}

				// Get Uni MasterKeyPair from DB
				Optional<KeystoreEntry> uniMKPKeystoreEntry = this.appInitializer.getAppService()
					.getDatabaseService()
					.getMasterKeyPairFromKeystore();
				if (!uniMKPKeystoreEntry.isPresent()) {
					loaderDialog.dismiss();
					new InfoDialog(this,
						R.string.error,
						R.string.no_umk_pair_in_db
					).setPositiveButton()
						.show();
					Log.e(TAG, "No master key pair in database");
					return;
				}
				KeyPair uniMasterKeyPair = uniMKPKeystoreEntry.get().getKeyPair();

				// Build ContactTracing Object
				ContactTracing contactTracing = new ContactTracing(infectionCase.getUserId(),
					infectionCase.getMasterPublicKey(),
					checkInKeyPairList,
					preGeneratedKeyPairsMap,
					infectionCase.getMedicalProof(),
					infectionCase.getDateFrom(),
					infectionCase.getDateTo()
				);

				// PERFORM TRACING
				List<Long> foundContactUserIds =
					contactTracing.performTracing(uniMasterKeyPair.getPrivate());

				Log.d(TAG,
					"TRACING DONE, FOUND USERIDS: " + foundContactUserIds.stream()
						.map(l -> Long.toString(l))
						.collect(Collectors.joining(", "))
				);
				loaderDialog.dismiss();

				// GOTO: InfectionCaseDetailFragment (and pass over ContactUserIDs)
				this.getSupportFragmentManager()
					.beginTransaction()
					.setReorderingAllowed(true)
					.replace(R.id.nav_fragment_container_activity_user,
						InfectionCaseDetailFragment.newInstance(icObject.getInfectionCaseId(),
							foundContactUserIds
						)
					)
					.commit();
			} catch (ApiHttpException | CryptoKeyDeserializationException | CryptoDecryptionException | CryptoDataDeserializationException | IllegalStateException e) {
				new InfoDialog(this, e).replaceErrorResource(R.string.unexpected_error_msg,
					R.string.tracing_error_msg
				)
					.setPositiveButton()
					.show();

				Log.e(TAG, "An error occurred while Tracing: " + e.getClass().getSimpleName());
				e.printStackTrace();
			} finally {
				loaderDialog.dismiss();
			}
		}).start();
	}

	@Override
	public void onBackPressed() {
		// do nothing
	}
}