package de.fhkiel.campustracer;

import java.io.Serializable;

import de.fhkiel.campustracer.model.RolesEnum;
import lombok.Getter;


@Getter
public class UserState implements Serializable {

	private final Long userId;
	private final String username;
	private final String password;
	private final RolesEnum role;

	public UserState(Long userId, String username, String password, RolesEnum role) {
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.role = role;
	}
}

