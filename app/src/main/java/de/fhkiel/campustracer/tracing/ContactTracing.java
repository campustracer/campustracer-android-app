package de.fhkiel.campustracer.tracing;

import android.util.Log;

import org.javatuples.Pair;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;

import de.fhkiel.campustracer.CampusTracerParameters;
import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.cryptowrapper.CryptoWrapperDataInterface;
import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoDecryptionException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;
import de.fhkiel.campustracer.helper.BytesPackage;
import de.fhkiel.campustracer.helper.DateHelper;
import de.fhkiel.campustracer.model.LectureAttendedByUserEntry;
import de.fhkiel.campustracer.model.PreGeneratedEncryptionKeyEntry;
import de.fhkiel.campustracer.model.UserAttendedLectureEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;
import de.fhkiel.campustracer.model.encdata.LectureAttendedByUserEncData;
import de.fhkiel.campustracer.model.encdata.UserAttendedLectureEncData;
import de.fhkiel.campustracer.service.AppService;
import lombok.NonNull;


public class ContactTracing {
	private static final String TAG = "ContactTracing";
	private static final CryptoWrapperDataInterface cryptoWrapperService =
		AppService.getInstance().getCryptoWrapperService();

	private final long studentId;
	private final PublicKey masterPubKey;
	private final Map<byte[], KeyPair> checkInKeyPairsMap = new HashMap<>();
	// Map<PrivKeyHash, KeyPair>
	private final Map<Pair<LocalDateTime, LocalDateTime>, KeyPair> preGeneratedKeyPairsMap =
		new HashMap<>(); // Map<WeekYearString, KeyPair>
	private final byte[] proofOfInfectionBlob; // PDF, JPEG, ... of Medical Notice
	private final LocalDate contagiousStartDate;
	private final LocalDate contagiousEndDate;
	public static final Integer CHECKIN_PRIVKEYHASH_QUERY_BATCH_SIZE = 10;
	public static final Integer PREGENERATED_PUBKEY_STUDENTATTENDEDLECTURE_QUERY_BATCH_SIZE = 10;

	// App-User Case
	public ContactTracing(
		long studentId,
		PublicKey masterPubKey,
		List<KeyPair> derivedCheckInKeyPairs,
		Map<Pair<LocalDateTime, LocalDateTime>, KeyPair> preGeneratedKeyPairsMap,
		// <WeekYearString, KeyPair>
		byte[] proofOfInfectionBlob,
		LocalDate contagiousStartDate,
		LocalDate contagiousEndDate
	) {
		this.studentId = studentId;

		this.masterPubKey = masterPubKey;

		for (KeyPair kp : derivedCheckInKeyPairs) {
			this.checkInKeyPairsMap.put(CryptoWrapper.hash(CryptoWrapper.serializePrivKey(kp.getPrivate())),
				kp
			);
		}
		this.preGeneratedKeyPairsMap.putAll(preGeneratedKeyPairsMap);
		this.contagiousStartDate = contagiousStartDate;
		this.contagiousEndDate = contagiousEndDate;
		this.proofOfInfectionBlob = proofOfInfectionBlob;
	}

	public static List<Pair<LocalDateTime, KeyPair>> recreateDerivedCheckInKeyPairsWithDateAttribute(
		PrivateKey masterPrivKey, List<LocalDate> contagiousDates
	) throws ApiHttpException {
		List<Pair<LocalDateTime, KeyPair>> derivedCheckInKeys = new ArrayList<>();
		// Recreate derivedCheckInKeys
		for (LocalDate date : contagiousDates) {
			int currentMatchCount;
			int lowerI = 0;
			// Iterate through intraDayNonces until no more entries are found for that day,
			// then proceed with next day
			while (true) {
				// For every day, query DB for recreated checkInPrivKeyHashes in
				// batches of 10 different intraDayNonces until a batch has zero matches in the DB
				// Build batch:
				HashMap<byte[], KeyPair> currentPrivKeyHashBatchMap =
					new HashMap<>(); //<checkInPrivKeyHash, checkInKeyPair>
				for (int i = 0; i < CHECKIN_PRIVKEYHASH_QUERY_BATCH_SIZE; i++) {
					int intraDayNonce = i + lowerI;
					KeyPair checkInKeyPair =
						CryptoWrapper.deriveCheckInKeypair(masterPrivKey, date, intraDayNonce);
					byte[] checkInPrivKeyHash =
						CryptoWrapper.hash(CryptoWrapper.serializePrivKey(checkInKeyPair.getPrivate()));
					currentPrivKeyHashBatchMap.put(checkInPrivKeyHash, checkInKeyPair);
				}
				// Query DB
				List<UserAttendedLectureEntry> privKeyDBMatches =
					cryptoWrapperService.getUserAttendedLectureEntriesByCheckInPrivateKeyHashes(new ArrayList<>(
						currentPrivKeyHashBatchMap.keySet()));
				currentMatchCount = privKeyDBMatches.size();
				if (currentMatchCount == 0) {
					break;
				}
				// filter batch for actual DB-matches
				for (Map.Entry<byte[], KeyPair> batchEntry :
					currentPrivKeyHashBatchMap.entrySet()) {
					for (UserAttendedLectureEntry ualEntry : privKeyDBMatches) {
						if (Arrays.equals(ualEntry.getCheckInPrivateKeyHash(),
							batchEntry.getKey()
						)) {
							// MATCH
							derivedCheckInKeys.add(new Pair<>(date.atStartOfDay(),
								batchEntry.getValue()
							));
						}
					}
				}
				/* LegacyCode:
				List<KeyPair> batchMatches = currentPrivKeyHashBatchMap.entrySet()
					.stream()
					.filter(x -> privKeyDBMatches.stream()
						.anyMatch(y -> Arrays.equals(x.getKey(), y.getCheckInPrivateKeyHash())))
					.map(Map.Entry::getValue)
					.collect(Collectors.toList());
				// Add matches to derivedCheckInKeys
				derivedCheckInKeys.addAll(batchMatches);
				*/
				lowerI += CHECKIN_PRIVKEYHASH_QUERY_BATCH_SIZE;
			}
		}
		return derivedCheckInKeys;
	}

	// keeps legacy method intact
	public static List<KeyPair> recreateDerivedCheckInKeyPairs(
		PrivateKey masterPrivKey, List<LocalDate> contagiousDates
	) throws ApiHttpException {
		return recreateDerivedCheckInKeyPairsWithDateAttribute(masterPrivKey,
			contagiousDates
		).stream()
			.map(Pair::getValue1)
			.collect(Collectors.toList());
	}

	private static Map<Pair<LocalDateTime, LocalDateTime>, KeyPair> recreateDerivedPreGeneratedKeyPairs(
		long studentId, KeyPair masterKeyPair, List<LocalDate> contagiousDates
	) {

		HashMap<Pair<LocalDateTime, LocalDateTime>, KeyPair> preGeneratedDerivedKeyPairsMap =
			new HashMap<>(); // <weekYearString, preGeneratedKeyPair>
		for (LocalDate date : contagiousDates) {
			Optional<PreGeneratedEncryptionKeyEntry> preGenKeyEntryOptional =
				cryptoWrapperService.getPreGeneratedKeyEntryForUserIdAndDate(studentId, date);
			if (!preGenKeyEntryOptional.isPresent()) {
				continue;
			}
			PreGeneratedEncryptionKeyEntry preGenKeyEntry = preGenKeyEntryOptional.get();
			KeyPair preGenKeyPair =
				CryptoWrapper.derivePreGeneratedKeypair(masterKeyPair.getPrivate(),
					preGenKeyEntry.getValidFrom(),
					preGenKeyEntry.getValidTo()
				);
			if (!Arrays.equals(CryptoWrapper.serializePubKey(preGenKeyPair.getPublic()),
				CryptoWrapper.serializePubKey(preGenKeyEntry.getPreGeneratedPubKey())
			)) {
				throw new IllegalStateException(
					"THE RECREATED PREGENERATED PUBKEY DOES NOT MATCH THE PUBKEY OF THE "
						+ "PREGENERATEDKEYS-TABLE!");
			}

			preGeneratedDerivedKeyPairsMap.put(new Pair<>(preGenKeyEntry.getValidFrom(),
				preGenKeyEntry.getValidTo()
			), preGenKeyPair);
		}
		return preGeneratedDerivedKeyPairsMap;
	}

	// Recreate KeyPairs from masterPrivKey (lost private key case)
	public static ContactTracing fromMasterPrivKey(
		long studentId,
		KeyPair masterKeyPair,
		List<LocalDate> contagiousDates,
		byte[] proofOfInfectionBlob
	) throws ApiHttpException {
		PrivateKey masterPrivKey = masterKeyPair.getPrivate();

		// Recreate derivedCheckInKeyPairs
		ArrayList<KeyPair> derivedCheckInKeyPairs =
			new ArrayList<>(recreateDerivedCheckInKeyPairs(masterPrivKey, contagiousDates));

		// Recreate preGeneratedDerivedKeys
		HashMap<Pair<LocalDateTime, LocalDateTime>, KeyPair> derivedPreGeneratedKeyPairs =
			new HashMap<>(recreateDerivedPreGeneratedKeyPairs(studentId,
				masterKeyPair,
				contagiousDates
			));

		contagiousDates.stream()
			.min(LocalDate::compareTo)
			.orElseThrow(IllegalArgumentException::new);
		// new ContactTracingTree
		return new ContactTracing(studentId,
			masterKeyPair.getPublic(),
			derivedCheckInKeyPairs,
			derivedPreGeneratedKeyPairs,
			proofOfInfectionBlob,
			contagiousDates.stream()
				.min(LocalDate::compareTo)
				.orElseThrow(IllegalArgumentException::new),
			contagiousDates.stream()
				.max(LocalDate::compareTo)
				.orElseThrow(IllegalArgumentException::new)
		);
	}

	// Recreate KeyPairs from masterPrivKey (lost private key case)
	public static List<ContactTracing> fromMasterPrivKeys(
		long studentId,
		List<KeyPair> masterKeyPairs,
		List<LocalDate> contagiousDates,
		byte[] proofOfInfectionBlob
	) throws ApiHttpException {
		ArrayList<ContactTracing> contactTracingTrees = new ArrayList<>();
		for (KeyPair masterKeyPair : masterKeyPairs) {
			contactTracingTrees.add(fromMasterPrivKey(studentId,
				masterKeyPair,
				contagiousDates,
				proofOfInfectionBlob
			));
		}
		return contactTracingTrees;
	}

	private List<Long> performKeyPairTracing(
		PrivateKey universityMasterPrivKey, Boolean checkInOrPreGeneratedKeyTracing
	) throws CryptoKeyDeserializationException, CryptoDecryptionException,
		CryptoDataDeserializationException, ApiHttpException {
		ArrayList<Long> contactStudentIds = new ArrayList<>();
		List<PrivateKey> lecturePrivateKeys = Boolean.TRUE.equals(checkInOrPreGeneratedKeyTracing)
			? this.traceLecturePrivateKeysFromCheckInKeyPairs(universityMasterPrivKey)
			: this.traceLecturePrivateKeysFromPreGeneratedKeyPairs(universityMasterPrivKey);
		for (PrivateKey lecturePrivateKey : lecturePrivateKeys) {
			List<Long> singleLectureContactStudentIDs =
				this.traceContactStudentIDsFromLecturePrivateKey(lecturePrivateKey,
					universityMasterPrivKey
				);
			contactStudentIds.addAll(singleLectureContactStudentIDs);
		}
		return contactStudentIds;
	}

	public List<Long> performCheckInKeyPairTracing(PrivateKey universityMasterPrivKey)
		throws CryptoKeyDeserializationException, CryptoDecryptionException,
		CryptoDataDeserializationException, ApiHttpException {
		return this.performKeyPairTracing(universityMasterPrivKey, true);
	}

	public List<Long> performPreGeneratedKeyPairTracing(PrivateKey universityMasterPrivKey)
		throws CryptoKeyDeserializationException, CryptoDecryptionException,
		CryptoDataDeserializationException, ApiHttpException {
		return this.performKeyPairTracing(universityMasterPrivKey, false);
	}

	public List<Long> performTracing(PrivateKey universityMasterPrivKey)
		throws CryptoKeyDeserializationException, CryptoDecryptionException,
		CryptoDataDeserializationException, ApiHttpException {
		TreeSet<Long> contactStudentIds =
			new TreeSet<>(); // set is used for automatic deduplication, treeset is an ordered
		// set
		contactStudentIds.addAll(this.performCheckInKeyPairTracing(universityMasterPrivKey));
		contactStudentIds.addAll(this.performPreGeneratedKeyPairTracing(universityMasterPrivKey));
		return new ArrayList<>(contactStudentIds);
	}

	public List<PrivateKey> traceLecturePrivateKeysFromCheckInKeyPairs(PrivateKey universityMasterPrivKey)
		throws CryptoDecryptionException, CryptoKeyDeserializationException,
		CryptoDataDeserializationException, ApiHttpException {
		ArrayList<PrivateKey> lecturePrivateKeyList = new ArrayList<>();
		List<byte[]> checkInKeyPairHashList = this.checkInKeyPairsMap.entrySet()
			.stream()
			.map(x -> x.getKey())
			.collect(Collectors.toList());

		List<UserAttendedLectureEntry> userAttendedLectureEntries =
			cryptoWrapperService.getUserAttendedLectureEntriesByCheckInPrivateKeyHashes(
				checkInKeyPairHashList);

		for (UserAttendedLectureEntry userAttendedLectureEntry : userAttendedLectureEntries) {
			if (!(DateHelper.isAfterOrEqual(userAttendedLectureEntry.getCreatedAt(),
				this.contagiousStartDate
			)
				&& DateHelper.isBeforeOrEqual(userAttendedLectureEntry.getCreatedAt(),
				this.contagiousEndDate
			))) {
				Log.w(TAG,
					"UALEntry not in Date Range, skipping! (id: "
						+ userAttendedLectureEntry.getId()
						+ ", date: "
						+ DateHelper.getISODateString(userAttendedLectureEntry.getCreatedAt())
						+ ")"
				);
				continue;
			}
			// find matching CIK
			Optional<Map.Entry<byte[], KeyPair>> checkInKeyPairOpt =
				this.checkInKeyPairsMap.entrySet()
					.stream()
					.filter(x -> Arrays.equals(x.getKey(),
						userAttendedLectureEntry.getCheckInPrivateKeyHash()
					))
					.findFirst();
			if (!checkInKeyPairOpt.isPresent()) {
				Log.w(
					TAG,
					"BACKEND RETURNED A UAL ENTRY THAT DOES NOT MATCH TO THE PROVIDED CIK-HASHES!"
				);
				continue;
			}
			KeyPair checkInKeyPair = checkInKeyPairOpt.get().getValue();

			// create object from bytes and decrypt
			UserAttendedLectureEncData userAttendedLectureEncData =
				UserAttendedLectureEncData.fromBytes(userAttendedLectureEntry.getEncData(),
					checkInKeyPair.getPrivate(),
					universityMasterPrivKey,
					this.studentId,
					userAttendedLectureEntry.getCreatedAt()
				);

			// Verify signature of Student MasterPubKey
			if (Boolean.FALSE.equals(userAttendedLectureEncData.isSignedBySameUser())) {
				throw new IllegalStateException(
					"CHECKIN-KEYPAIR-BASED USERATTENDEDLECTURE ENTRY HAS BEEN SIGNED BY "
						+ "DIFFERENT PERSON!");
			}
			if (Boolean.FALSE.equals(userAttendedLectureEncData.verifySignature(this.masterPubKey))) {
				throw new IllegalStateException(
					"SIGNATURE MISMATCH IN USERTATTENDEDLECTURE-PACKAGE!");
			}

			// Verify that UALPackage contents match with userAttendedLectureEntry (from DB)
			// and StudentID

			if (userAttendedLectureEncData.getLectureDate()
				.compareTo(userAttendedLectureEntry.getCreatedAt()) != 0) {
				throw new IllegalStateException(
					"DATE OF ENCRYPTED STUDENTATTENDEDLECTURE-PACKAGE DOES NOT MATCH "
						+ "STUDENTATTENDEDLECTURE-ENTRY FROM DATABASE!");
			}
			if (userAttendedLectureEncData.getUserId() != this.studentId) {
				throw new IllegalStateException(
					"ENCRYPTED STUDENTATTENDEDLECTURE-PACKAGE CONTAINS DIFFERENT STUDENTID "
						+ "THAN DECRYPTING STUDENT!");
			}

			// decryption of Enc(UK^P, LK^S) with University Priv Key
			PrivateKey lecturePrivateKey =
				userAttendedLectureEncData.getLecturePrivKey(universityMasterPrivKey);
			lecturePrivateKeyList.add(lecturePrivateKey);
		}
		return lecturePrivateKeyList;
	}

	public List<PrivateKey> traceLecturePrivateKeysFromPreGeneratedKeyPairs(PrivateKey universityMasterPrivKey)
		throws CryptoDecryptionException, CryptoKeyDeserializationException,
		CryptoDataDeserializationException, ApiHttpException {
		ArrayList<PrivateKey> lecturePrivateKeyList = new ArrayList<>();
		for (Map.Entry<Pair<LocalDateTime, LocalDateTime>, KeyPair> preGeneratedKeyPairSet :
			this.preGeneratedKeyPairsMap
			.entrySet()) {
			KeyPair preGeneratedKeyPair = preGeneratedKeyPairSet.getValue();
			Pair<LocalDateTime, LocalDateTime> dateRange = preGeneratedKeyPairSet.getKey();
			// filter out entires outside of contagious period!
			LocalDateTime validFrom =
				!DateHelper.isAfterOrEqual(dateRange.getValue0(), this.contagiousStartDate)
					? DateHelper.castLocalDateToLocalDateTime(this.contagiousStartDate, true)
					: dateRange.getValue0();
			LocalDateTime validTo =
				!DateHelper.isBeforeOrEqual(dateRange.getValue1(), this.contagiousEndDate)
					? DateHelper.castLocalDateToLocalDateTime(this.contagiousEndDate, false)
					: dateRange.getValue1();
			List<UserAttendedLectureEntry> userAttendedLectureEntriesForDates =
				cryptoWrapperService.getUserAttendedLectureEntriesByDates(validFrom, validTo);
			for (UserAttendedLectureEntry UALEntry : userAttendedLectureEntriesForDates) {
				if (!(DateHelper.isAfterOrEqual(UALEntry.getCreatedAt(), this.contagiousStartDate)
					&& DateHelper.isBeforeOrEqual(UALEntry.getCreatedAt(),
					this.contagiousEndDate
				))) {
					Log.w(TAG,
						"PGK-Tracing: UALEntry not in Date Range, skipping! (id: "
							+ UALEntry.getId()
							+ ", date: "
							+ DateHelper.getISODateString(UALEntry.getCreatedAt())
							+ ")"
					);
					continue;
				}
				if (Boolean.FALSE.equals(UserAttendedLectureEncData.isDecryptableWithPrivKey(UALEntry.getEncData(),
					preGeneratedKeyPair.getPrivate()
				))) {
					// not decryptable with this private key
					continue;
				}

				// unpack
				UserAttendedLectureEncData userAttendedLectureEncData =
					UserAttendedLectureEncData.fromBytes(UALEntry.getEncData(),
						preGeneratedKeyPair.getPrivate(),
						universityMasterPrivKey,
						this.studentId,
						UALEntry.getCreatedAt()
					);

				// SANITY CHECKS

				// Verify that a lecturer signed the Package
				Boolean isUserAStudent =
					cryptoWrapperService.isUserIdAStudent(userAttendedLectureEncData.getUserId());
				Boolean isSignerALecturer =
					cryptoWrapperService.isUserIdALecturer(userAttendedLectureEncData.getSignedById());
				if (Boolean.TRUE.equals(!isUserAStudent)
					|| Boolean.TRUE.equals(!isSignerALecturer)) {
					throw new IllegalStateException(
						"A PREGENERATED-BASED UALPackage CAN ONLY BE OWNED BY STUDENTS AND SIGNED "
							+ "BY LECTURERS!");
				}
				if (userAttendedLectureEncData.getLectureDate().compareTo(UALEntry.getCreatedAt())
					!= 0) {
					throw new IllegalStateException(
						"DATE OF ENCRYPTED STUDENTATTENDEDLECTURE-PACKAGE DOES NOT MATCH "
							+ "STUDENTATTENDEDLECTURE-ENTRY FROM DATABASE!");
				}
				if (userAttendedLectureEncData.getUserId() != this.studentId) {
					throw new IllegalStateException(
						"ENCRYPTED STUDENTATTENDEDLECTURE-PACKAGE CONTAINS DIFFERENT STUDENTID "
							+ "THAN DECRYPTING STUDENT!");
				}

				// Get Lecturer PublicKeyPairs

				List<PublicKey> signingPartyMasterPubKeys =
					cryptoWrapperService.getLecturerMasterKeyEntriesForLecturerIdAndDate(
						userAttendedLectureEncData.getSignedById(),
						userAttendedLectureEncData.getLectureDate()
					)
						.stream()
						.map(UserMasterKeyEntry::getMasterPubKey)
						.collect(Collectors.toList());

				// Verify Lecturers Signature
				boolean userAttendedLectureSignatureVerified = false;
				for (PublicKey signingPartyMasterPubKey : signingPartyMasterPubKeys) {
					if (Boolean.TRUE.equals(userAttendedLectureEncData.verifySignature(
						signingPartyMasterPubKey))) {
						Log.d(TAG,
							"VERIFIED STUDENTATTENDEDLECTURE-PACKAGE FOR "
								+ userAttendedLectureEncData.getUserId()
								+ " (Signature: Lecturer "
								+ userAttendedLectureEncData.getSignedById()
						);
						userAttendedLectureSignatureVerified = true;
						break;
					}
				}
				if (!userAttendedLectureSignatureVerified) {
					throw new IllegalStateException("COULD NOT VERIFY SIGNATURE");
				}

				// decryption of Enc(UK^P, LK^S) with University Priv Key
				PrivateKey lecturePrivateKey =
					userAttendedLectureEncData.getLecturePrivKey(universityMasterPrivKey);

				lecturePrivateKeyList.add(lecturePrivateKey);
			}
		}
		return lecturePrivateKeyList;
	}

	private List<Long> traceContactStudentIDsFromLecturePrivateKey(
		PrivateKey lecturePrivateKey, PrivateKey uniPrivKey
	) throws CryptoDecryptionException, CryptoDataDeserializationException, ApiHttpException {

		ArrayList<Long> contactStudentIds = new ArrayList<>();

		// generate all possible randomized LecturePrivateKeyHashes for Lookup: Hash( [ Hash(LK^S)
		// , rand(0,N) ])
		byte[] lecturePrivKeyHash =
			CryptoWrapper.hash(CryptoWrapper.serializePrivKey(lecturePrivateKey));
		ArrayList<byte[]> randomizedLecturePrivKeyHashes = new ArrayList<>();
		for (
			int i = 0; i < CampusTracerParameters.RANDOMIZED_LECTUREPRIVKEY_HASH_RANDMAX; i++
		) {
			randomizedLecturePrivKeyHashes.add(CryptoWrapper.hash((new BytesPackage(lecturePrivKeyHash,
				BytesPackage.intToBytes(i)
			)).toBytes()));
		}

		// lookup all randomized priv key hashes in LectureAttendedByUser Table (LABU)
		List<LectureAttendedByUserEntry> lectureAttendedByUserEntries =
			cryptoWrapperService.getLectureAttendedByUserEntriesByLecturePrivKeyHashes(
				randomizedLecturePrivKeyHashes);

		for (LectureAttendedByUserEntry LABUEntry : lectureAttendedByUserEntries) {

			LectureAttendedByUserEncData lectureAttendedByUserEncData =
				LectureAttendedByUserEncData.fromBytes(LABUEntry.getEncData(),
					lecturePrivateKey,
					uniPrivKey,
					LABUEntry.getCreatedAt()
				);

			// verify date = date
			if (lectureAttendedByUserEncData.getLectureDate()
				.compareTo(lectureAttendedByUserEncData.getLectureDate()) != 0) {
				throw new IllegalStateException(
					"DATE OF ENCRYPTED LECTUREATTENDEDBYSTUDENT-PACKAGE DOES NOT MATCH "
						+ "LECTUREATTENDEDBYSTUDENT-ENTRY FROM DATABASE!");
			}

			// get masterPubKeys of singing party (contact-StudentID OR Lecturer)
			@NonNull List<PublicKey> signingPartyMasterPubKeys;
			Boolean isSignerALecturer =
				cryptoWrapperService.isUserIdALecturer(lectureAttendedByUserEncData.getSignedById());
			Boolean isUserALecturer =
				cryptoWrapperService.isUserIdALecturer(lectureAttendedByUserEncData.getUserId());
			if (Boolean.TRUE.equals(isSignerALecturer)) {
				// SIGNER IS LECTURER
				// ALLOW: either lecturer self-signed, or signed for a student
				// DENY: signing for other lecturers
				if (Boolean.TRUE.equals(!isUserALecturer) || Boolean.TRUE.equals(
					lectureAttendedByUserEncData.isSignedBySameUser())) {
					// lookup LecturerID -> LecturerMasterPubKeys
					// lecturer is allowed to sign for every student
					signingPartyMasterPubKeys =
						cryptoWrapperService.getLecturerMasterKeyEntriesForLecturerIdAndDate(
							lectureAttendedByUserEncData.getSignedById(),
							lectureAttendedByUserEncData.getLectureDate()
						)
							.stream()
							.map(UserMasterKeyEntry::getMasterPubKey)
							.collect(Collectors.toList());
				} else {
					throw new IllegalStateException(
						"SIGNATURE ERROR: A LECTURER MAY ONLY SIGN FOR STUDENTS OR THEMSELVES, NOT"
							+ " OTHER LECTURERS!");
				}
			} else {
				// SIGNER IS STUDENT
				// ALLOW: Only self-signed
				if (Boolean.TRUE.equals(lectureAttendedByUserEncData.isSignedBySameUser())) {
					// lookup StudentID -> StudentMasterPubKeys

					List<UserMasterKeyEntry> studentMasterKeyEntriesForDate =
						cryptoWrapperService.getStudentMasterKeyEntriesForStudentIdAndDate(
							lectureAttendedByUserEncData.getUserId(),
							lectureAttendedByUserEncData.getLectureDate()
						);

					signingPartyMasterPubKeys = studentMasterKeyEntriesForDate.stream()
						.map(UserMasterKeyEntry::getMasterPubKey)
						.collect(Collectors.toList());
				} else {
					throw new IllegalStateException(
						"SIGNATURE ERROR: STUDENTS MY ONLY SIGN FOR THEMSELVES!");
				}
			}

			// verify signature
			boolean signatureVerified = false;
			for (PublicKey signingPartyMasterPubKey : signingPartyMasterPubKeys) {
				if (Boolean.TRUE.equals(lectureAttendedByUserEncData.verifySignature(
					signingPartyMasterPubKey))) {
					Log.d(TAG,
						"VERIFIED LECTUREATTENDEDBYUSER-PACKAGE FOR " + lectureAttendedByUserEncData
							.getUserId() + " (Signature: " + (Boolean.FALSE.equals(
							lectureAttendedByUserEncData.isSignedBySameUser())
							? ("Lecturer "
							+ lectureAttendedByUserEncData.getSignedById())
							: ("Student " + lectureAttendedByUserEncData.getUserId())) + ")"
					);
					signatureVerified = true;
					break;
				}
			}
			if (!signatureVerified) {
				throw new IllegalStateException("COULD NOT VERIFY SIGNATURE");
			}

			// valid entry -> found a contact-Student!
			contactStudentIds.add(lectureAttendedByUserEncData.getUserId());
		}
		return contactStudentIds;
	}
}

