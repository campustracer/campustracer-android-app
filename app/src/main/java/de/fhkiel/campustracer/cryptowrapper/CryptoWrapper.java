package de.fhkiel.campustracer.cryptowrapper;

import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.spec.ECPrivateKeySpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;
import org.bouncycastle.math.ec.ECConstants;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.util.encoders.Hex;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoDecryptionException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;
import de.fhkiel.campustracer.helper.BytesPackage;
import de.fhkiel.campustracer.helper.DateHelper;
import lombok.SneakyThrows;


public class CryptoWrapper {
	public static final String KEYPAIR_ALGORITHM = "EC";
	public static final String KEYPAIR_CURVENAME = "secp256k1";
	public static final String KEYPAIR_PBKDF_ALGORITHM = "PBKDF2WithHmacSHA256";
	public static final String KEYPAIR_PROVIDER = BouncyCastleProvider.PROVIDER_NAME;
	public static final String CIPHER_ALGORITHM = "ECIES";
	public static final String CIPHER_PROVIDER = BouncyCastleProvider.PROVIDER_NAME;
	public static final Boolean CIPHER_IS_HYBRID = true;
	public static final String SIGNATURE_ALGORITHM = "SHA256withECDSA";
	public static final String SIGNATURE_PROVIDER = BouncyCastleProvider.PROVIDER_NAME;
	public static final String HASH_ALGORITHM = "SHA-256";

	public static final Integer SYM_KEYGEN_KEY_LENGTH = 32; // 32 byte = 256 bit
	public static final String SYM_CIPHER_ALGORITHM = "AES/CBC/PKCS7Padding";
	public static final String SYM_KEYGEN_ALGORITHM = "AES";
	public static final Integer SYM_AES_IV_LENGTH = 16; // 16 bytes

	public static final SecureRandom SECURE_RANDOM = new SecureRandom();



	/*
	Algorithm Names:
	JCE:
		https://docs.oracle.com/javase/9/docs/specs/security/standard-names
		.html#keygenerator-algorithms
	BouncyCastle:
		https://flylib.com/books/en/1.274
		.1/appendix_b_algorithms_provided_by_the_bouncy_castle_provider.html
	 */

	private CryptoWrapper() {
	}

	@SneakyThrows({
		NoSuchAlgorithmException.class,
		NoSuchProviderException.class,
		InvalidAlgorithmParameterException.class
	})
	public static KeyPair generateKeypair() {
		ECParameterSpec ecSpec = ECNamedCurveTable.getParameterSpec(KEYPAIR_CURVENAME);

		KeyPairGenerator kpg = KeyPairGenerator.getInstance(KEYPAIR_ALGORITHM, KEYPAIR_PROVIDER);
		kpg.initialize(ecSpec, new SecureRandom());

		return kpg.generateKeyPair();
	}

	/**
	 * @param masterPrivKey users master private key
	 * @param date          date
	 * @param intraDayNonce intra-day nonce, beginning with 0
	 * @return
	 */
	public static KeyPair deriveCheckInKeypair(
		PrivateKey masterPrivKey, LocalDate date, Integer intraDayNonce
	) {
		String dateString = DateHelper.getISODateString(date);
		return deriveKeypair(dateString + "-" + intraDayNonce.toString(),
			serializePrivKey(masterPrivKey)
		);
	}

	/**
	 * @param masterPrivKey Student's master private key
	 * @return
	 */
	public static KeyPair derivePreGeneratedKeypair(
		PrivateKey masterPrivKey, LocalDateTime validFromDate, LocalDateTime validToDate
	) {
		String dateRangeString =
			DateHelper.getISODateTimeString(validFromDate) + "#" + DateHelper.getISODateTimeString(
				validToDate);
		return deriveKeypair(dateRangeString, serializePrivKey(masterPrivKey));
	}

	/**
	 * ENCRYPT/DECRYPT
	 */

	@SneakyThrows({
		NoSuchAlgorithmException.class,
		NoSuchPaddingException.class,
		NoSuchProviderException.class,
		InvalidKeyException.class
	})
	public static byte[] encrypt(
		byte[] cleartext, PublicKey pubkey
	) throws CryptoEncryptionException {
		Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM, CIPHER_PROVIDER);
		cipher.init(Cipher.ENCRYPT_MODE, pubkey);
		try {
			return cipher.doFinal(cleartext);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new CryptoEncryptionException(e);
		}
	}

	public static byte[] encrypt(
		String cleartext, PublicKey pubkey
	) throws CryptoEncryptionException {
		return encrypt(cleartext.getBytes(StandardCharsets.UTF_8), pubkey);
	}

	@SneakyThrows({
		NoSuchAlgorithmException.class,
		NoSuchProviderException.class,
		NoSuchPaddingException.class,
		InvalidKeyException.class
	})
	public static byte[] decrypt(
		byte[] ciphertext, PrivateKey privkey
	) throws CryptoDecryptionException {
		Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM, CIPHER_PROVIDER);
		try {
			cipher.init(Cipher.DECRYPT_MODE, privkey);
			return cipher.doFinal(ciphertext);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new CryptoDecryptionException(e);
		}
	}

	public static String decryptToString(
		byte[] ciphertext, PrivateKey privkey
	) throws CryptoDecryptionException {
		return new String(decrypt(ciphertext, privkey), StandardCharsets.UTF_8);
	}

	/**
	 * SIGN/VERIFY
	 */

	@SneakyThrows({
		NoSuchAlgorithmException.class,
		NoSuchProviderException.class,
		InvalidKeyException.class,
		SignatureException.class
	})
	public static byte[] sign(
		byte[] message, PrivateKey privkey
	) {
		Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM, SIGNATURE_PROVIDER);
		signature.initSign(privkey);
		signature.update(message);
		return signature.sign();
	}

	public static byte[] sign(
		String message, PrivateKey privkey
	) {
		return sign(message.getBytes(StandardCharsets.UTF_8), privkey);
	}

	@SneakyThrows({
		NoSuchAlgorithmException.class, NoSuchProviderException.class, InvalidKeyException.class
	})
	public static Boolean verify(
		byte[] message, byte[] signature, PublicKey pubkey
	) {
		Signature st = Signature.getInstance(SIGNATURE_ALGORITHM, SIGNATURE_PROVIDER);
		st.initVerify(pubkey);
		try {
			st.update(message);
			return st.verify(signature);
		} catch (SignatureException e) {
			return false;
		}
	}

	public static Boolean verify(
		String message, byte[] signature, PublicKey pubkey
	) {
		return verify(message.getBytes(StandardCharsets.UTF_8), signature, pubkey);
	}

	/**
	 * SERIALIZATION/DESERIALIZATION
	 */
	@SneakyThrows({
		NoSuchAlgorithmException.class, NoSuchProviderException.class,
		InvalidKeySpecException.class
	})
	public static byte[] serializePubKey(PublicKey pubKey) {
		KeyFactory keyFactory = KeyFactory.getInstance(KEYPAIR_ALGORITHM, KEYPAIR_PROVIDER);
		ECPublicKeySpec kspec = keyFactory.getKeySpec(pubKey, ECPublicKeySpec.class);
		return kspec.getQ().getEncoded(true);
		// encodes X and Y coordinates as zero-padded unsigned byte arrays and concatenates them
		// compression results in shorter output (about half as short)
		// explanation: https://stackoverflow.com/a/6687080
	}

	@SneakyThrows({
		NoSuchAlgorithmException.class, NoSuchProviderException.class,
		InvalidKeySpecException.class
	})
	public static byte[] serializePrivKey(PrivateKey privKey) {
		KeyFactory keyFactory = KeyFactory.getInstance(KEYPAIR_ALGORITHM, KEYPAIR_PROVIDER);
		ECPrivateKeySpec kspec = keyFactory.getKeySpec(privKey, ECPrivateKeySpec.class);
		BigInteger privateD = kspec.getD();
		int maxNLength = BigIntegers.getUnsignedByteLength(kspec.getParams().getN());

		// Zero-padded byte output ensures that the length of the output is always the same.
		// Thus, if D is stored encrypted, small D values (= shorter byte array) cannot be
		// identified from the ciphertext length
		// Encoding/decoding ECPoints works similar, where its ECFieldElements (X, Y) are
		// encoded/decoded to/from
		// byte[] Arrays with the same methods.
		// See: ECFieldElement::getEncoded(), ECCurve::decodePoint()
		return BigIntegers.asUnsignedByteArray(maxNLength, privateD);
	}

	@SneakyThrows({NoSuchAlgorithmException.class, NoSuchProviderException.class})
	public static PrivateKey deserializePrivKey(byte[] privKeyRaw)
		throws CryptoKeyDeserializationException {
		ECNamedCurveParameterSpec ecp = ECNamedCurveTable.getParameterSpec(KEYPAIR_CURVENAME);
		BigInteger privateD = BigIntegers.fromUnsignedByteArray(privKeyRaw);
		if (privateD.compareTo(ECConstants.ONE) <= 0 || (privateD.compareTo(ecp.getN()) >= 0)) {
			throw new CryptoKeyDeserializationException(
				"PrivateKey BigInteger representation is invalid!");
		}
		ECPrivateKeySpec ecPrivKeySpec = new ECPrivateKeySpec(privateD, ecp);

		KeyFactory keyFactory = KeyFactory.getInstance(KEYPAIR_ALGORITHM, KEYPAIR_PROVIDER);
		try {
			return keyFactory.generatePrivate(ecPrivKeySpec);
		} catch (InvalidKeySpecException e) {
			throw new CryptoKeyDeserializationException(e);
		}
	}

	@SneakyThrows({NoSuchAlgorithmException.class, NoSuchProviderException.class})
	public static PublicKey deserializePubKey(byte[] pubKeyRaw)
		throws CryptoKeyDeserializationException {
		ECNamedCurveParameterSpec ecp = ECNamedCurveTable.getParameterSpec(KEYPAIR_CURVENAME);
		ECPoint publicQ = ecp.getCurve().decodePoint(pubKeyRaw);
		// decodes 2-dimensional Point Q from byte array,
		// detects compression automatically from first byte

		ECPublicKeySpec ecPubKeySpec = new ECPublicKeySpec(publicQ, ecp);
		KeyFactory keyFactory = KeyFactory.getInstance(KEYPAIR_ALGORITHM, KEYPAIR_PROVIDER);
		try {
			return keyFactory.generatePublic(ecPubKeySpec);
		} catch (InvalidKeySpecException e) {
			throw new CryptoKeyDeserializationException(e);
		}
	}

	/**
	 * SERIALIZATION/DESERIALIZATION
	 */

	// generate Keypair based on PBKDF2withSHA256
	@SneakyThrows({
		NoSuchAlgorithmException.class, NoSuchProviderException.class,
		InvalidKeySpecException.class
	})
	private static KeyPair deriveKeypair(
		String passphrase, byte[] seed
	) {
		ECNamedCurveParameterSpec ecParameterSpec =
			ECNamedCurveTable.getParameterSpec(KEYPAIR_CURVENAME);

		/* KEYPAIRS ARE DERIVED BASED ON THE PBKDF2 ALGORITHM
		 * This method uses the PBKDF2 to generate a key with the same bit length as the maximum
		 * D value for the Curve (defined by N). D is the "Private Scalar", which is the secret
		 * number
		 * used to calculate the EC Private Key.
		 * HOWEVER: N is a large prime number und thus does not reach the maximum value of the bit
		 * array (N < 2^keyLength).
		 * Therefore, an edge case exists where the BigInteger value of the secret key generated
		 * by PBKDF2
		 * may be greater than N and thus invalid!
		 * This loop ensures that - if this edge case occures - additional iterations are added to
		 * the PBKDF2
		 * until its output represents a valid D value!
		 */
		int pbkdf2KeyLength = ecParameterSpec.getN().bitLength(); // = 256 for secp256k1
		BigInteger
			privateD; // D := Private Scalar := secret number used to calculate the ECPrivateKey
		SecretKey pbkdf2Key; // the result of the PBKDF2 will be used as private scalar value!
		int additionalIterations = 0;
		// prevent edge-case where D > N  (N occupies 256 bit but actual value is smaller!)
		// this way more iterations are added to the PBKDF until D <= N
		// See: https://crypto.stackexchange
		// .com/questions/30269/are-all-possible-ec-private-keys-valid
		do {
			pbkdf2Key =
				generatePBKDF2SecretKey(passphrase, seed, pbkdf2KeyLength, additionalIterations);
			assert (pbkdf2KeyLength == (pbkdf2Key.getEncoded().length * 8));
			privateD = BigIntegers.fromUnsignedByteArray(pbkdf2Key.getEncoded());
			additionalIterations++;
		} while (privateD.compareTo(ECConstants.ONE) < 0
			|| (privateD.compareTo(ecParameterSpec.getN()) >= 0)); //see ECDomainParameter.java

		// Get Curve Parameter Spec and Domain Parameters
		ECDomainParameters domainParams = new ECDomainParameters(ecParameterSpec.getCurve(),
			ecParameterSpec.getG(),
			ecParameterSpec.getN(),
			ecParameterSpec.getH(),
			ecParameterSpec.getSeed()
		);

		// generate PrivKey Parameters
		ECPrivateKeyParameters ecPrivateKey = new ECPrivateKeyParameters(privateD, domainParams);
		// calculate Q for PublicKey by multiplying PrivateKey's D with Curve's G
		ECPoint publicQ = domainParams.getG().multiply(privateD);
		// generate PubKey Parameters
		ECPublicKeyParameters ecPublicKey = new ECPublicKeyParameters(publicQ, domainParams);

		// Generate PrivateKey Object
		ECPrivateKeySpec ecprivkeyspec = new ECPrivateKeySpec(ecPrivateKey.getD(),
			ecParameterSpec);
		KeyFactory keyFactory = KeyFactory.getInstance(KEYPAIR_ALGORITHM, KEYPAIR_PROVIDER);
		PrivateKey privateKeyObj = keyFactory.generatePrivate(ecprivkeyspec);

		// Generate PublicKey Object
		ECPublicKeySpec ecpubkeyspec = new ECPublicKeySpec(ecPublicKey.getQ(), ecParameterSpec);
		PublicKey publicKeyObj = keyFactory.generatePublic(ecpubkeyspec);

		return new KeyPair(publicKeyObj, privateKeyObj);
	}

	@SneakyThrows({InvalidKeySpecException.class, NoSuchAlgorithmException.class})
	private static SecretKey generatePBKDF2SecretKey(
		String passphrase, byte[] seed, int keyLength, int additionalIterations
	) {
		// Generate a SecretKey with PBKDF2
		SecretKeyFactory skf = SecretKeyFactory.getInstance(KEYPAIR_PBKDF_ALGORITHM);
		PBEKeySpec pbekeyspec = new PBEKeySpec(passphrase.toCharArray(),
			seed,
			1000 + additionalIterations,
			keyLength /* in bit = 32 byte*/
		);
		// additionalIterations: may be needed if BitInteger value of Key > Curve Parameter N
		return skf.generateSecret(pbekeyspec);
		// PBKDF2 output matches with this online generator (non-JS): https://8gwifi.org/pbkdf.jsp
	}

	@SneakyThrows(NoSuchAlgorithmException.class)
	public static byte[] hash(byte[] data) {
		MessageDigest md = MessageDigest.getInstance(HASH_ALGORITHM);
		md.update(data);
		return md.digest();
	}

	public static String hmac(byte[] secretKey, byte[] message) {
		return Hex.toHexString(hash((new BytesPackage(secretKey, hash(message))).toBytes()));
	}

	public static String hmac(String secretKey, String message) {
		return hmac(secretKey.getBytes(StandardCharsets.UTF_8),
			message.getBytes(StandardCharsets.UTF_8)
		);
	}

	public static byte[] hash(String data) {
		return hash(data.getBytes(StandardCharsets.UTF_8));
	}

	public static byte[] randomHash() {
		SecureRandom rnd = new SecureRandom();
		byte[] payload = new byte[100];
		rnd.nextBytes(payload);
		return hash(payload);
	}

	// Symmetric Cryptography

	public static byte[] generateSymKey() {
		byte[] keyBytes = new byte[SYM_KEYGEN_KEY_LENGTH];
		SECURE_RANDOM.nextBytes(keyBytes);
		return keyBytes;
	}

	@SneakyThrows({
		NoSuchPaddingException.class,
		NoSuchAlgorithmException.class,
		NoSuchProviderException.class,
		InvalidKeyException.class
	})
	public static byte[] encryptSymmetric(
		byte[] plaintext, byte[] key
	) throws CryptoEncryptionException {
		Cipher cipher =
			Cipher.getInstance(SYM_CIPHER_ALGORITHM, BouncyCastleProvider.PROVIDER_NAME);
		byte[] ivBytes = new byte[SYM_AES_IV_LENGTH];
		SECURE_RANDOM.nextBytes(ivBytes);
		try {
			cipher.init(Cipher.ENCRYPT_MODE,
				new SecretKeySpec(key, SYM_CIPHER_ALGORITHM),
				new IvParameterSpec(ivBytes)
			);
			byte[] ciphertext = cipher.doFinal(plaintext);

			// combine ciphertext and IV into one byte array
			return (new BytesPackage(ivBytes, ciphertext)).toBytes();
		} catch (InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
			throw new CryptoEncryptionException(e);
		}
	}

	public static byte[] encryptSymmetric(
		String plaintext, byte[] key
	) throws CryptoEncryptionException {
		return encryptSymmetric(plaintext.getBytes(StandardCharsets.UTF_8), key);
	}

	@SneakyThrows({
		NoSuchPaddingException.class,
		NoSuchAlgorithmException.class,
		NoSuchProviderException.class,
		InvalidAlgorithmParameterException.class,
		InvalidKeyException.class
	})
	public static byte[] decryptSymmetric(
		byte[] ciphertextRaw, byte[] key
	) throws CryptoDecryptionException, CryptoDataDeserializationException {

		//split ciphertext in iv and real_ciphertext
		BytesPackage unpacked = BytesPackage.fromRaw(ciphertextRaw);
		byte[] iv = unpacked.get(0);
		byte[] ciphertext = unpacked.get(1);

		// decrypt
		Cipher cipher =
			Cipher.getInstance(SYM_CIPHER_ALGORITHM, BouncyCastleProvider.PROVIDER_NAME);
		cipher.init(Cipher.DECRYPT_MODE,
			new SecretKeySpec(key, SYM_CIPHER_ALGORITHM),
			new IvParameterSpec(iv)
		);
		try {
			return cipher.doFinal(ciphertext);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new CryptoDecryptionException(e);
		}
	}

	public static String decryptSymmetricToString(
		byte[] ciphertext, byte[] key
	) throws CryptoDecryptionException, CryptoDataDeserializationException {
		return new String(decryptSymmetric(ciphertext, key), StandardCharsets.UTF_8);
	}

	@SneakyThrows({
		NoSuchAlgorithmException.class, NoSuchProviderException.class,
		InvalidKeySpecException.class
	})
	public static PublicKey calculatePubKeyFromPrivKey(PrivateKey privKey) {
		ECNamedCurveParameterSpec ecParameterSpec =
			ECNamedCurveTable.getParameterSpec(KEYPAIR_CURVENAME);
		ECDomainParameters domainParams = new ECDomainParameters(ecParameterSpec.getCurve(),
			ecParameterSpec.getG(),
			ecParameterSpec.getN(),
			ecParameterSpec.getH(),
			ecParameterSpec.getSeed()
		);

		KeyFactory keyFactory = KeyFactory.getInstance(KEYPAIR_ALGORITHM, KEYPAIR_PROVIDER);
		ECPrivateKeySpec kspec = keyFactory.getKeySpec(privKey, ECPrivateKeySpec.class);
		BigInteger privateD = kspec.getD();

		ECPoint publicQ = domainParams.getG().multiply(privateD);
		// generate PubKey Parameters
		ECPublicKeyParameters ecPublicKey = new ECPublicKeyParameters(publicQ, domainParams);

		// Generate PublicKey Object
		ECPublicKeySpec ecpubkeyspec = new ECPublicKeySpec(ecPublicKey.getQ(), ecParameterSpec);

		return keyFactory.generatePublic(ecpubkeyspec);
	}
}



