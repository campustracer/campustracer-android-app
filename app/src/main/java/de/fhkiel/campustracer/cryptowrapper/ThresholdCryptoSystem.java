package de.fhkiel.campustracer.cryptowrapper;

import org.bouncycastle.util.BigIntegers;

import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoDecryptionException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;
import de.fhkiel.campustracer.helper.BytesPackage;
import lombok.Getter;


/*
General Principle:
Encrypt:
	- Create N secretKeys, encrypt encData with all secretKeys, encrypt keys with encrypterPubKeys
Decrypt:
   	- Parties decrypt their encrypted secretKeys
   	- if threshold reached: decrypt encDataEncrypted
 */
public class ThresholdCryptoSystem {
	// Prototype Implementation: Always N of N Keys needed!
	@Getter
	public final byte[] encDataEncrypted;
	public final Integer threshold;
	public final Map<PublicKey, byte[]> encryptedSymKeysMap = new HashMap<>();
	// encrypterPubKey, encSymKey
	public final Map<byte[], byte[]> decryptedSymKeysMap = new HashMap<>();
	// encryptedSymKey, decryptedSymKey

	public ThresholdCryptoSystem(
		byte[] encData, List<PublicKey> encrypterPublicKeys, Integer threshold
	) throws CryptoEncryptionException {
		if (encrypterPublicKeys.size() != threshold) {
			throw new IllegalArgumentException(
				"Thresholds have not been implemented yet! Always all parties are required for "
					+ "decryption - at least for now!");
		}
		ArrayList<byte[]> symKeys = new ArrayList<byte[]>();
		// generate SymKey for every encrypter
		for (PublicKey encrypterPubKey : encrypterPublicKeys) {
			byte[] newSymKey = CryptoWrapper.generateSymKey();
			symKeys.add(newSymKey);

			// encrypt SymKey for every encrypter
			byte[] encSymKey = CryptoWrapper.encrypt(newSymKey, encrypterPubKey);
			this.encryptedSymKeysMap.put(encrypterPubKey, encSymKey);
		}
		this.threshold = threshold;
		this.encDataEncrypted = this.encrypt(encData, symKeys);
	}

	private byte[] encrypt(byte[] encData, List<byte[]> symKeys) throws CryptoEncryptionException {
		// sort symKeys by BigInteger representation, Ascending  (later decryption in descending
		// order)
		List<byte[]> symKeysSortedAscending = symKeys.stream()
			.sorted(Comparator.comparing(BigIntegers::fromUnsignedByteArray))
			.collect(Collectors.toList());

		// encrypt encData with SymKeys
		byte[] newEncDataEncrypted = encData.clone();
		for (byte[] symKey : symKeysSortedAscending) {
			newEncDataEncrypted = CryptoWrapper.encryptSymmetric(newEncDataEncrypted, symKey);
		}
		return newEncDataEncrypted;
	}

	private byte[] decrypt() throws CryptoDecryptionException, CryptoDataDeserializationException {
		// sort symKeys by BigInteger representation, Descending  (encrypted in ascending order)
		List<byte[]> decSymKeysSortedDescending = this.decryptedSymKeysMap.values()
			.stream()
			.sorted((a, b) -> BigIntegers.fromUnsignedByteArray(a)
				.compareTo(BigIntegers.fromUnsignedByteArray(b)) * (-1))
			.collect(Collectors.toList());

		// decrypt encData with SymKeys
		byte[] encDataDecrypted = this.encDataEncrypted.clone();
		for (byte[] symKey : decSymKeysSortedDescending) {
			encDataDecrypted = CryptoWrapper.decryptSymmetric(encDataDecrypted, symKey);
		}
		return encDataDecrypted;
	}

	public Integer getThresholdCount() {
		return this.threshold;
	}

	public Integer getTotalCount() {
		return this.encryptedSymKeysMap.size();
	}

	public Boolean isDecryptable() {
		return this.decryptedSymKeysMap.size() >= this.getThresholdCount();
	}

	public byte[] toBytes() {
		ArrayList<byte[]> bytesList = new ArrayList<byte[]>();
		bytesList.add(BytesPackage.intToBytes(this.threshold));
		bytesList.add(this.encDataEncrypted);
		for (Map.Entry<PublicKey, byte[]> encSymKeyMap : this.encryptedSymKeysMap.entrySet()) {
			bytesList.add(CryptoWrapper.serializePubKey(encSymKeyMap.getKey()));
			bytesList.add(encSymKeyMap.getValue());
		}

		BytesPackage bytesPackage = new BytesPackage(bytesList);

		return bytesPackage.toBytes();
	}

	// fromBytes
	private ThresholdCryptoSystem(byte[] tcData)
		throws CryptoKeyDeserializationException, IOException, CryptoDataDeserializationException {
		BytesPackage bytesPackage = BytesPackage.fromRaw(tcData);
		if (bytesPackage.size() < 2 || bytesPackage.size() % 2 != 0) {
			throw new IOException("BytesPackage malformed!");
		}
		this.threshold = BytesPackage.bytesToInt(bytesPackage.get(0));
		this.encDataEncrypted = bytesPackage.get(1);
		for (int i = 2; i < bytesPackage.getList().size(); i += 2) {
			this.encryptedSymKeysMap.put(CryptoWrapper.deserializePubKey(bytesPackage.get(i)),
				bytesPackage.get(i + 1)
			);
		}
	}

	public static ThresholdCryptoSystem fromBytes(byte[] tcData)
		throws CryptoKeyDeserializationException, IOException, CryptoDataDeserializationException {
		return new ThresholdCryptoSystem(tcData);
	}

	public Boolean decryptPartial(PublicKey encrypterPubKey, PrivateKey encrypterPrivKey) {
		if (this.encryptedSymKeysMap.containsKey(encrypterPubKey)) {
			try {
				byte[] decryptedSymKey =
					CryptoWrapper.decrypt(this.encryptedSymKeysMap.get(encrypterPubKey),
						encrypterPrivKey
					);
				if (!this.decryptedSymKeysMap.containsKey(this.encryptedSymKeysMap.get(
					encrypterPubKey))) {
					this.decryptedSymKeysMap.put(this.encryptedSymKeysMap.get(encrypterPubKey),
						decryptedSymKey
					); // encryptedSymKey, decryptedSymKey
					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	public Optional<byte[]> decryptAll()
		throws CryptoDecryptionException, CryptoDataDeserializationException {
		if (Boolean.FALSE.equals(this.isDecryptable())) {

			System.out.println("AT LEAST "
				+ this.getThresholdCount()
				+ " of "
				+ this.getTotalCount()
				+ " Parties required!");
			return Optional.empty();
		}
		return Optional.of(this.decrypt());
	}
}
