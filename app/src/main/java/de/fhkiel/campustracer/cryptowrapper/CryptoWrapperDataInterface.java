package de.fhkiel.campustracer.cryptowrapper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.model.CheckInEntry;
import de.fhkiel.campustracer.model.LectureAttendedByUserEntry;
import de.fhkiel.campustracer.model.PreGeneratedEncryptionKeyEntry;
import de.fhkiel.campustracer.model.UserAttendedLectureEntry;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;


public interface CryptoWrapperDataInterface {

	/**
	 * ADD ENTRIES
	 */
	UserMasterKeyEntry addUserMasterKeyAndPreGeneratedKeys(UserMasterKeyEntry userMasterKeyEntry)
		throws ApiHttpException;

	void addCheckInEntry(CheckInEntry checkInEntry);

	void addUserEntryForMocking(UserEntry userEntry);

	/*
	 * GET UserMasterKeysEntry
	 */

	// Used by lecturers for student login
	Optional<UserMasterKeyEntry> getCurrentlyValidUserMasterKeyEntryForUserId(long userId);

	// main query for get{Student|Lecturer}MasterkeyEntriesBy{Student|Lecturer}IdAndDate
	List<UserMasterKeyEntry> getUserMasterKeyEntriesForUserIdAndDate(
		long userId, LocalDate date
	);

	Optional<UserEntry> getUserEntryById(long userId);

	/*
	 * GET UserMasterKeysEntry -> Student/Lecturer specializations (not necessarily needed in
	 * backend-side implementation)
	 */

	// for Student Signature Verification
	List<UserMasterKeyEntry> getStudentMasterKeyEntriesForStudentIdAndDate(
		long studentID, LocalDate date
	);

	// for Lecturer Signature Verification
	List<UserMasterKeyEntry> getLecturerMasterKeyEntriesForLecturerIdAndDate(
		long lecturerID, LocalDate date
	);

	/*
	 * GET PreGeneratedEncryptionKeyEntry
	 */

	// for Student Check-In via Lecturer
	Optional<PreGeneratedEncryptionKeyEntry> getCurrentlyValidPreGeneratedKeyEntryForUserIdAndDate(
		long userId, LocalDate date
	);

	// for Tracing / Recreating derived PreGenPrivKeys
	Optional<PreGeneratedEncryptionKeyEntry> getPreGeneratedKeyEntryForUserIdAndDate(
		long userId, LocalDate date
	);

	/*
	 * GET UserAttendedLectureEntry
	 */

	// for CheckIn-Key Tracing
	List<UserAttendedLectureEntry> getUserAttendedLectureEntriesByCheckInPrivateKeyHashes(
		List<byte[]> privKeyHashes
	) throws ApiHttpException;

	// for PreGen-Key Tracing
	List<UserAttendedLectureEntry> getUserAttendedLectureEntriesByDates(
		LocalDateTime dateFrom, LocalDateTime dateTo
	) throws ApiHttpException;

	/*
	 * GET LectureAttendedByUserEntry
	 */

	List<LectureAttendedByUserEntry> getLectureAttendedByUserEntriesByLecturePrivKeyHashes(
		List<byte[]> lecturePrivKeyHash
	) throws ApiHttpException;

	/*
	 * INFECTION CASES
	 */

	// missing

	/*
	 * Misc
	 */

	Boolean isUserIdALecturer(long userId);

	Boolean isUserIdAStudent(long userId);

	//String getTodaysCheckInAuthKey(long lecturerId);

	void updateContactInformationForUser(String contactInformation);
}
