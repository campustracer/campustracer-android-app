package de.fhkiel.campustracer.model.virtual;

import androidx.annotation.Nullable;
import androidx.room.PrimaryKey;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;


/**
 * For existing keys only update the invalidatedAt field
 */
@Data
@AllArgsConstructor
public class UserMasterKeyUpdateEntry {
	@PrimaryKey
	public long id;
	@Nullable
	private LocalDateTime invalidatedAt;
}
