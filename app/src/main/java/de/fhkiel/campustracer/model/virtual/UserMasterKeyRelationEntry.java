package de.fhkiel.campustracer.model.virtual;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

import de.fhkiel.campustracer.model.PreGeneratedEncryptionKeyEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;
import lombok.AllArgsConstructor;
import lombok.Data;


/**
 * This is not a database entity but only a wrapper class allowing for the relationship between
 * userMasterKeyEntry and preGeneratedEncryptionKeyEntry. The missing @Entity is intentional. See:
 * https://developer.android.com/reference/androidx/room/Relation
 */
@Data
@AllArgsConstructor
public class UserMasterKeyRelationEntry {
	@Embedded
	public UserMasterKeyEntry userMasterKeyEntry;

	@Relation(parentColumn = "id", entityColumn = "userMasterKeyId")
	public List<PreGeneratedEncryptionKeyEntry> preGeneratedEncryptionKeyEntries;

	public UserMasterKeyEntry toUserMasterKeyEntry() {
		this.userMasterKeyEntry.setPreGeneratedEncryptionKeys(this.preGeneratedEncryptionKeyEntries);
		return this.userMasterKeyEntry;
	}
}
