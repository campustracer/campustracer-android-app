package de.fhkiel.campustracer.model.encdata;

import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDate;

import de.fhkiel.campustracer.CampusTracerParameters;
import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoDecryptionException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.helper.BytesPackage;
import de.fhkiel.campustracer.helper.DateHelper;
import lombok.Getter;

/*
	= Enc(LK^P,
		[
			studentId,
			signedById,
			Sig(SP^S, [studentId, date])
		]
 */

public class LectureAttendedByUserEncData {
	@Getter
	private final long userId;
	@Getter
	private final long signedById;
	@Getter
	private final LocalDate lectureDate;
	@Getter
	private byte[] signature = null;

	public LectureAttendedByUserEncData(
		long userId, long signedById, LocalDate lectureDate
	) {
		this.userId = userId;
		this.signedById = signedById;
		this.lectureDate = lectureDate;
	}

	private BytesPackage createSignaturePayload() {
		return new BytesPackage(BytesPackage.longToBytes(this.userId),
			DateHelper.getISODateString(this.lectureDate).getBytes(StandardCharsets.UTF_8)
		);
	}

	public byte[] signAndEncryptToBytes(
		PublicKey lecturePubKey, PrivateKey signerPrivKey
	) throws CryptoEncryptionException {

		BytesPackage signaturePayload = this.createSignaturePayload();
		this.signature = CryptoWrapper.sign(signaturePayload.toBytes(), signerPrivKey);

		BytesPackage encDataPayload = new BytesPackage(BytesPackage.longToBytes(this.userId),
			BytesPackage.longToBytes(this.signedById),
			this.signature
		);
		return CryptoWrapper.encrypt(CryptoWrapper.encrypt(encDataPayload.toBytes(),
			lecturePubKey),
			CampusTracerParameters.getUniversityPublicKey()
		);
	}

	// decrypt with lecturePrivKey
	public static LectureAttendedByUserEncData fromBytes(
		byte[] encDataEncrypted,
		PrivateKey lecturePrivKey,
		PrivateKey uniPrivKey,
		LocalDate lectureDate
	) throws CryptoDecryptionException, CryptoDataDeserializationException {
		return new LectureAttendedByUserEncData(encDataEncrypted,
			lecturePrivKey,
			lectureDate,
			uniPrivKey
		);
	}

	// decrypting constructor
	private LectureAttendedByUserEncData(
		byte[] encDataEncrypted,
		PrivateKey lecturePrivKey,
		LocalDate lectureDate,
		PrivateKey uniPrivKey
	) throws CryptoDecryptionException, CryptoDataDeserializationException {
		BytesPackage encDataPayload =
			BytesPackage.fromRaw(CryptoWrapper.decrypt(CryptoWrapper.decrypt(encDataEncrypted,
				uniPrivKey
			), lecturePrivKey));
		this.userId = BytesPackage.bytesToLong(encDataPayload.get(0));
		this.signedById = BytesPackage.bytesToLong(encDataPayload.get(1));
		this.signature = encDataPayload.get(2);
		this.lectureDate = lectureDate;
	}

	public Boolean verifySignature(PublicKey signerPubKey) {
		if (this.signature == null) {
			throw new IllegalStateException("Package has no signature!");
		}
		BytesPackage signaturePayload = this.createSignaturePayload();
		return CryptoWrapper.verify(signaturePayload.toBytes(), this.signature, signerPubKey);
	}

	public Boolean isSignedBySameUser() {
		return this.userId == this.signedById;
	}
}
