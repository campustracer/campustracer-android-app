package de.fhkiel.campustracer.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.squareup.moshi.Json;

import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDateTime;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.helper.BytesPackage;
import de.fhkiel.campustracer.helper.DateHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@Entity(foreignKeys = {
	@ForeignKey(entity = UserMasterKeyEntry.class, parentColumns = "id", childColumns =
		"userMasterKeyId", onDelete = ForeignKey.CASCADE)
})

@EqualsAndHashCode
@AllArgsConstructor
public class PreGeneratedEncryptionKeyEntry {
	@PrimaryKey(autoGenerate = true)
	public long id;
	@ColumnInfo(index = true)
	public long userMasterKeyId;

	@NonNull
	@Json(name = "publicKey")
	private final PublicKey preGeneratedPubKey;
	@NonNull
	@Json(name = "signature")
	private final byte[] signatureByMasterPrivKey;
	@NonNull
	private final LocalDateTime validFrom; // inclusive
	@NonNull
	private final LocalDateTime validTo; // inclusive

	// createSigned Constructor
	private PreGeneratedEncryptionKeyEntry(
		PublicKey preGeneratedPubKey,
		LocalDateTime validFrom,
		LocalDateTime validTo,
		PrivateKey signingMasterPrivKey
	) {
		this.preGeneratedPubKey = preGeneratedPubKey;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.signatureByMasterPrivKey =
			CryptoWrapper.sign(this.getSignaturePayload(), signingMasterPrivKey);
	}

	// Constructor which creates the signature using masterPrivKey
	public static PreGeneratedEncryptionKeyEntry createSigned(
		PublicKey preGeneratedPubKey,
		LocalDateTime validFrom,
		LocalDateTime validTo,
		PrivateKey signingMasterPrivKey
	) {

		return new PreGeneratedEncryptionKeyEntry(preGeneratedPubKey,
			validFrom,
			validTo,
			signingMasterPrivKey
		);
	}

	public Boolean verifySignature(PublicKey masterPubKey) {

		return CryptoWrapper.verify(this.getSignaturePayload(),
			this.signatureByMasterPrivKey,
			masterPubKey
		);
	}

	private byte[] getSignaturePayload() {
		return new BytesPackage(CryptoWrapper.serializePubKey(this.preGeneratedPubKey),
			DateHelper.getISODateTimeString(this.validFrom).getBytes(StandardCharsets.UTF_8),
			DateHelper.getISODateTimeString(this.validTo).getBytes(StandardCharsets.UTF_8)
		).toBytes();
	}
}
