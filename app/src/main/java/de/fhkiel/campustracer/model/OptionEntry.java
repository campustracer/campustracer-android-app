package de.fhkiel.campustracer.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@Entity
@AllArgsConstructor
public class OptionEntry {
	@NonNull
	@PrimaryKey
	private final OptionsEnum key;
	private final String value;
}
