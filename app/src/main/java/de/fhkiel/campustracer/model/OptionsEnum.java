package de.fhkiel.campustracer.model;

public enum OptionsEnum {
	USER_ID, USERNAME, PASSWORD, ROLE, IS_REGISTERED, KEYSTORE_CURRENT_MASTER_KEY_ID,
	LATEST_INFECTION_CASE_ID
}
