package de.fhkiel.campustracer.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@Entity(indices = {@Index(value = {"username"}, unique = true)})
@AllArgsConstructor
public class UserEntry {
	@PrimaryKey
	public long id;

	@NonNull
	private String username;
	@NonNull
	private RolesEnum role;
	@NonNull
	private LocalDateTime changedAt;

	private byte[] encContactData;
}
