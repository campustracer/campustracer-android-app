package de.fhkiel.campustracer.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@Entity
@AllArgsConstructor
public class InfectionCaseEntry {
	@PrimaryKey
	private long id;
	@NonNull
	private LocalDate createdAt;
	@NonNull
	private byte[] encData;
	@Nullable
	@Ignore
	private byte[] encMedicalProof;

	public InfectionCaseEntry(long id, @NonNull LocalDate createdAt, @NonNull byte[] encData) {
		this.id = id;
		this.createdAt = createdAt;
		this.encData = encData;
	}
}
