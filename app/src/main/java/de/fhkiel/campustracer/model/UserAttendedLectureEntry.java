package de.fhkiel.campustracer.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@Entity
@AllArgsConstructor
public class UserAttendedLectureEntry {
	@PrimaryKey(autoGenerate = true)
	private final long id;

	@NonNull
	private final byte[] checkInPrivateKeyHash;
	@NonNull
	private final LocalDate createdAt;
	@NonNull
	private final byte[] encData;
}
