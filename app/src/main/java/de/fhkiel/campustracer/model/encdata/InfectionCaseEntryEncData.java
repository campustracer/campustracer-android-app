package de.fhkiel.campustracer.model.encdata;

import androidx.core.util.Pair;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDate;
import java.util.List;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoDecryptionException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;
import de.fhkiel.campustracer.helper.BytesPackage;
import de.fhkiel.campustracer.helper.DateHelper;
import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
public class InfectionCaseEntryEncData {
	@Getter
	long id; // API id, set to 0 for encrypting
	@Getter
	long userId;
	@Getter
	PublicKey masterPublicKey;
	@Getter
	List<PrivateKey> preGenPrivKeys;
	@Getter
	List<PrivateKey> checkInPrivkeys;
	@Getter
	LocalDate dateFrom;
	@Getter
	LocalDate dateTo;
	@Getter
	byte[] medicalProof;

	public String getDescription() {
		return "Infection case for user " + this.userId + " from " + DateHelper.getISODateString(
			this.dateFrom) + " to " + DateHelper.getISODateString(this.dateTo);
	}

	/**
	 * Encrypt the objects data for posting it to the API
	 *
	 * @param publicKey The public key to use for encryption
	 * @return Encrypted data as byte array
	 * @throws CryptoEncryptionException Something went wrong with encryption
	 */
	public Pair<byte[], byte[]> getEncryptedPayload(PublicKey publicKey)
		throws CryptoEncryptionException {
		return new Pair<>(CryptoWrapper.encrypt(new BytesPackage(
			BytesPackage.longToBytes(this.userId),
			CryptoWrapper.serializePubKey(this.masterPublicKey),
			BytesPackage.privateKeyListToBytes(this.preGenPrivKeys),
			BytesPackage.privateKeyListToBytes(this.checkInPrivkeys),
			BytesPackage.stringToBytes(DateHelper.getISODateString(this.dateFrom)),
			BytesPackage.stringToBytes(DateHelper.getISODateString(this.dateTo))
		).toBytes(), publicKey), CryptoWrapper.encrypt(this.medicalProof, publicKey));
	}

	/**
	 * Decrypt infection case entry to InfectionCaseEntryEncData class
	 *
	 * @param id              the APIs entry id
	 * @param encData         the encData to be decrypted
	 * @param encMedicalProof the encMedicalProof to be decrypted
	 * @param privateKey      the private key to use in decryption
	 * @return Object with all decrypted fields
	 * @throws CryptoDataDeserializationException Deserialized array has wrong size
	 * @throws CryptoKeyDeserializationException  ?
	 * @throws CryptoDecryptionException          ?
	 */
	public static InfectionCaseEntryEncData getDecryptedPayload(
		long id, byte[] encData, byte[] encMedicalProof, PrivateKey privateKey
	) throws CryptoDataDeserializationException, CryptoKeyDeserializationException,
		CryptoDecryptionException {
		List<byte[]> ds =
			BytesPackage.fromRaw(CryptoWrapper.decrypt(encData, privateKey)).getList();
		if (ds.size() != 6) {
			throw new CryptoDataDeserializationException(
				"Deserialized infection case array has wrong size");
		}
		return new InfectionCaseEntryEncData(
			id,
			BytesPackage.bytesToLong(ds.get(0)),
			CryptoWrapper.deserializePubKey(ds.get(1)),
			BytesPackage.bytesToPrivateKeyList(ds.get(2)),
			BytesPackage.bytesToPrivateKeyList(ds.get(3)),
			DateHelper.getLocalDateFromISODateString(BytesPackage.bytesToString(ds.get(4))),
			DateHelper.getLocalDateFromISODateString(BytesPackage.bytesToString(ds.get(5))),
			encMedicalProof == null ? null : CryptoWrapper.decrypt(encMedicalProof, privateKey)
		);
	}
}
