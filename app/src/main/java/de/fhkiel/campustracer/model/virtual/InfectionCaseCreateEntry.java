package de.fhkiel.campustracer.model.virtual;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDate;
import java.util.List;

import de.fhkiel.campustracer.CampusTracerParameters;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.model.encdata.InfectionCaseEntryEncData;
import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class InfectionCaseCreateEntry {
	@NonNull
	private byte[] encData;
	@NonNull
	private byte[] encMedicalProof;

	public InfectionCaseCreateEntry(
		long userId,
		PublicKey userMasterPubKey,
		List<PrivateKey> preGenPrivKeys,
		List<PrivateKey> checkInPrivKeys,
		LocalDate dateFrom,
		LocalDate dateTo,
		byte[] medicalProof
	) throws CryptoEncryptionException {
		Pair<byte[], byte[]> encrypted = new InfectionCaseEntryEncData(
			0,
			userId,
			userMasterPubKey,
			preGenPrivKeys,
			checkInPrivKeys,
			dateFrom,
			dateTo,
			medicalProof
		).getEncryptedPayload(CampusTracerParameters.getUniversityPublicKey());
		this.encData = encrypted.first;
		this.encMedicalProof = encrypted.second;
	}
}
