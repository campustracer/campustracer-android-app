package de.fhkiel.campustracer.model;

public enum KeysEnum {
	CHECK_IN_AUTH_KEY, MASTER_KEYPAIR, CHECK_IN_KEYPAIR, PRE_GEN_KEYPAIR
}
