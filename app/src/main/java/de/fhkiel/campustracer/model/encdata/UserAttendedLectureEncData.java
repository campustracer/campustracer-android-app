package de.fhkiel.campustracer.model.encdata;

import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDate;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.exceptions.CryptoDataDeserializationException;
import de.fhkiel.campustracer.exceptions.CryptoDecryptionException;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.exceptions.CryptoKeyDeserializationException;
import de.fhkiel.campustracer.helper.BytesPackage;
import de.fhkiel.campustracer.helper.DateHelper;
import lombok.Getter;

/*
 EncData = Enc(EP^P, [
				  Enc(UK^P, LK^S),
				  Enc(UK^P, [
					signedById,
					Sig(SP^S, [
					  Enc(UK^P, LK^S), StudentID, Date
					])
				  ])
				])
 */

public class UserAttendedLectureEncData {
	@Getter
	private final byte[] encLecturePrivKey;
	@Getter
	private final long userId;
	@Getter
	private final long signedById;
	@Getter
	private final LocalDate lectureDate;
	@Getter
	private byte[] signature = null;

	// generated encrypted and signed ticket
	public UserAttendedLectureEncData(
		byte[] encLecturePrivKey, long userId, long signedById, LocalDate lectureDate
	) {
		this.encLecturePrivKey = encLecturePrivKey;
		this.userId = userId;
		this.signedById = signedById;
		this.lectureDate = lectureDate;
	}

	private BytesPackage createSignaturePayload() {
		return new BytesPackage(this.encLecturePrivKey,
			BytesPackage.longToBytes(this.userId),
			DateHelper.getISODateString(this.lectureDate).getBytes(StandardCharsets.UTF_8)
		);
	}

	public byte[] signAndEncryptToBytes(
		PublicKey derivedPubKey, PublicKey universityPubKey, PrivateKey signerPrivKey
	) throws CryptoEncryptionException {

		BytesPackage signaturePayload = this.createSignaturePayload();
		// create signature
		this.signature = CryptoWrapper.sign(signaturePayload.toBytes(), signerPrivKey);

		// inner encryption with UK^P
		byte[] innerUniPayload0Encrypted = this.encLecturePrivKey; // = Enc(UK^P, LK^S)
		BytesPackage innerUniPayload1 =
			new BytesPackage(BytesPackage.longToBytes(this.signedById), this.signature);
		byte[] innerUniPayload1Encrypted =
			CryptoWrapper.encrypt(innerUniPayload1.toBytes(), universityPubKey);
		/*
		=	Enc(UK^P, [	signedById,	
						Sig(SP^S, [	  Enc(UK^P, LK^S), StudentID, Date	]) 
					  ])
		 */
		BytesPackage innerUniPayloadCombined =
			new BytesPackage(innerUniPayload0Encrypted, innerUniPayload1Encrypted);
		return CryptoWrapper.encrypt(innerUniPayloadCombined.toBytes(), derivedPubKey);
	}

	public static Boolean isDecryptableWithPrivKey(
		byte[] encDataEncrypted, PrivateKey derivedPrivKey
	) {
		try {
			CryptoWrapper.decrypt(encDataEncrypted, derivedPrivKey);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// decrypt with derivedPrivKey and universityPrivKey
	public static UserAttendedLectureEncData fromBytes(
		byte[] encDataEncrypted,
		PrivateKey derivedPrivKey,
		PrivateKey universityPrivKey,
		long studentId,
		LocalDate lectureDate
	) throws CryptoDecryptionException, CryptoDataDeserializationException {
		return new UserAttendedLectureEncData(encDataEncrypted,
			derivedPrivKey,
			universityPrivKey,
			studentId,
			lectureDate
		);
	}

	private UserAttendedLectureEncData(
		byte[] encDataEncrypted,
		PrivateKey derivedPrivKey,
		PrivateKey universityPrivKey,
		long userId,
		LocalDate lectureDate
	) throws CryptoDecryptionException, CryptoDataDeserializationException {
		BytesPackage innerUniPayloadCombined =
			BytesPackage.fromRaw(CryptoWrapper.decrypt(encDataEncrypted, derivedPrivKey));
		this.encLecturePrivKey = innerUniPayloadCombined.get(0);
		byte[] innerUniPayload1Encrypted = innerUniPayloadCombined.get(1);
		BytesPackage innerUniPayload1 = BytesPackage.fromRaw(CryptoWrapper.decrypt(
			innerUniPayload1Encrypted,
			universityPrivKey
		));
		this.signedById = BytesPackage.bytesToLong(innerUniPayload1.get(0));
		this.signature = innerUniPayload1.get(1);
		this.userId = userId;
		this.lectureDate = lectureDate;
	}

	// decrypt & get encLecturePrivKey
	public PrivateKey getLecturePrivKey(PrivateKey universityPrivKey)
		throws CryptoDecryptionException, CryptoKeyDeserializationException {
		return CryptoWrapper.deserializePrivKey(CryptoWrapper.decrypt(this.encLecturePrivKey,
			universityPrivKey
		));
	}

	public Boolean verifySignature(PublicKey signerPubKey) {
		if (this.signature == null) {
			throw new IllegalStateException("Package has no signature!");
		}
		BytesPackage signaturePayload = this.createSignaturePayload();
		return CryptoWrapper.verify(signaturePayload.toBytes(), this.signature, signerPubKey);
	}

	public Boolean isSignedBySameUser() {
		return this.userId == this.signedById;
	}
}
