package de.fhkiel.campustracer.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@Entity
@AllArgsConstructor
public class LectureAttendedByUserEntry {
	@PrimaryKey(autoGenerate = true)
	private final long id;

	@NonNull
	private final byte[] lecturePrivateKeyHash;
	@NonNull
	private final LocalDate createdAt;
	@NonNull
	private final byte[] encData;
}
