package de.fhkiel.campustracer.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.squareup.moshi.Json;

import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDateTime;
import java.util.List;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.helper.BytesPackage;
import de.fhkiel.campustracer.helper.DateHelper;
import de.fhkiel.campustracer.model.virtual.UserMasterKeyRelationEntry;
import de.fhkiel.campustracer.model.virtual.UserMasterKeyUpdateEntry;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UserMasterKeyEntry {
	@PrimaryKey
	public long id;

	@Nullable
	private long userId;

	@NonNull
	@Json(name = "publicKey")
	private PublicKey masterPubKey;
	@NonNull
	@Json(name = "encPrivateKey")
	private byte[] encryptedMasterPrivKey;
	@NonNull
	private byte[] signature; // of [studentID, createdAt]
	@NonNull
	private LocalDateTime createdAt;
	@Nullable
	private LocalDateTime invalidatedAt;
	@Nullable
	@Ignore
	private List<PreGeneratedEncryptionKeyEntry> preGeneratedEncryptionKeys;

	//createSigned
	private UserMasterKeyEntry(
		long userId,
		PublicKey masterPubKey,
		byte[] encryptedMasterPrivKey,
		LocalDateTime createdAt,
		PrivateKey signingMasterPrivKey,
		@Nullable List<PreGeneratedEncryptionKeyEntry> preGeneratedEncryptionKeys
	) {
		this.userId = userId;
		this.masterPubKey = masterPubKey;
		this.encryptedMasterPrivKey = encryptedMasterPrivKey;
		this.createdAt = createdAt;
		this.invalidatedAt = null;
		this.signature = CryptoWrapper.sign(this.getSignaturePayload(), signingMasterPrivKey);
		this.preGeneratedEncryptionKeys = preGeneratedEncryptionKeys;
	}

	// Constructor which creates the signature using masterPrivKey
	public static UserMasterKeyEntry createSigned(
		long userId,
		PublicKey masterPubKey,
		byte[] encryptedMasterPrivKey,
		LocalDateTime registryDate,
		PrivateKey signingMasterPrivKey,
		List<PreGeneratedEncryptionKeyEntry> preGeneratedEncryptionKeyEntries
	) {

		return new UserMasterKeyEntry(
			userId,
			masterPubKey,
			encryptedMasterPrivKey,
			registryDate,
			signingMasterPrivKey,
			preGeneratedEncryptionKeyEntries
		);
	}

	public UserMasterKeyRelationEntry toUserMasterKeyRelationEntry() {
		return new UserMasterKeyRelationEntry(this, this.preGeneratedEncryptionKeys);
	}

	public UserMasterKeyUpdateEntry toUserMasterKeyUpdateEntry() {
		return new UserMasterKeyUpdateEntry(this.id, this.invalidatedAt);
	}

	public Boolean verifySignature() {
		return CryptoWrapper.verify(this.getSignaturePayload(), this.signature, this.masterPubKey);
	}

	public byte[] getSignaturePayload() {
		return new BytesPackage(
			BytesPackage.longToBytes(this.userId),
			CryptoWrapper.serializePubKey(this.masterPubKey),
			this.encryptedMasterPrivKey,
			DateHelper.getISODateTimeString(this.createdAt).getBytes(StandardCharsets.UTF_8)
		).toBytes();
	}
}
