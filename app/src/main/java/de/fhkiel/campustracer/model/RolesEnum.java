package de.fhkiel.campustracer.model;

import androidx.annotation.StringRes;

import de.fhkiel.campustracer.R;


public enum RolesEnum {
	STUDENT, LECTURER, UNIVERSITY;

	public static @StringRes
	int getStringRes(RolesEnum en) {
		switch (en) {
			case STUDENT:
				return R.string.student;
			case LECTURER:
				return R.string.lecturer;
			case UNIVERSITY:
				return R.string.university;
			default:
				throw new IllegalArgumentException("Enum Entry unknown");
		}
	}
}
