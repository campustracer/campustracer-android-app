package de.fhkiel.campustracer.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@Entity
@AllArgsConstructor
public class KeystoreEntry {
	@PrimaryKey(autoGenerate = true)
	private long id;

	@NonNull
	private KeysEnum type;

	@NonNull
	private LocalDateTime createdAt;

	@NonNull
	private LocalDateTime validFrom;

	@Nullable
	private LocalDateTime validTo;

	@NonNull
	private PrivateKey privateKey;

	@NonNull
	private PublicKey publicKey;

	public KeyPair getKeyPair() {
		return new KeyPair(this.publicKey, this.privateKey);
	}
}
