package de.fhkiel.campustracer.model;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDate;
import java.util.Optional;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.exceptions.CryptoEncryptionException;
import de.fhkiel.campustracer.model.encdata.LectureAttendedByUserEncData;
import de.fhkiel.campustracer.model.encdata.UserAttendedLectureEncData;
import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@Entity
@AllArgsConstructor
public class CheckInEntry {
	@PrimaryKey(autoGenerate = true)
	private long id;

	@NonNull
	private final LocalDate createdAt;

	@NonNull
	private final byte[] checkInPrivateKeyHash;
	@NonNull
	private final byte[] encUserAttendedLecture;

	@NonNull
	private final byte[] lecturePrivateKeyHash;
	@NonNull
	private final byte[] encLectureAttendedByUser;

	public CheckInEntry(
		long userId,
		PublicKey studentDerivedPubKey,
		Optional<PrivateKey> studentDerivedPrivKey,
		long signedById,
		PrivateKey signerMasterPrivKey,
		PublicKey lecturePubKey,
		byte[] lecturePrivKeyEncryptedWithUniPubKey,
		byte[] lecturePrivKeyHashRandomized,
		LocalDate checkInDate,
		PublicKey universityPubKey
	) throws CryptoEncryptionException {
		/*
		 * UserAttendedLecture
		 */

		UserAttendedLectureEncData userAttendedLecture = new UserAttendedLectureEncData(
			lecturePrivKeyEncryptedWithUniPubKey,
			userId,
			signedById,
			checkInDate
		);

		boolean isSignedByLecturer = !(userAttendedLecture.isSignedBySameUser());

		if (studentDerivedPrivKey.isPresent() == isSignedByLecturer) {
			throw new IllegalArgumentException(
				"A CheckInKeyPair UAL Entry cannot be signed by a Lecturer!");
		}
		byte[] encryptedAndSignedUserAttendedLecture = userAttendedLecture.signAndEncryptToBytes(
			studentDerivedPubKey,
			universityPubKey,
			signerMasterPrivKey
		);

		byte[] derivedKeyHash;
		// regular case: student checks in with app -> derived check-in key -> hash
		// no-app-case: use preGenerated pub key for encryption, create random hash as there
		// is no preGenerated priv key available during check-in
		derivedKeyHash =
			studentDerivedPrivKey.map(privateKey -> CryptoWrapper.hash(CryptoWrapper.serializePrivKey(
				privateKey))).orElseGet(CryptoWrapper::randomHash);

		/*
		 * LectureAttendedByUser
		 */

		LectureAttendedByUserEncData lectureAttendedByUserEncData =
			new LectureAttendedByUserEncData(userId, signedById, checkInDate);
		byte[] encryptedAndSignedLectureAttendedByUserEncData =
			lectureAttendedByUserEncData.signAndEncryptToBytes(lecturePubKey, signerMasterPrivKey);

		/*
		 * Set fields
		 */

		this.createdAt = checkInDate;

		this.checkInPrivateKeyHash = derivedKeyHash;
		this.encUserAttendedLecture = encryptedAndSignedUserAttendedLecture;

		this.lecturePrivateKeyHash = lecturePrivKeyHashRandomized;
		this.encLectureAttendedByUser = encryptedAndSignedLectureAttendedByUserEncData;
	}

	public Pair<UserAttendedLectureEntry, LectureAttendedByUserEntry> split() {
		return new Pair<>(new UserAttendedLectureEntry(0,
			this.checkInPrivateKeyHash,
			this.createdAt,
			this.encUserAttendedLecture
		),
			new LectureAttendedByUserEntry(0,
				this.lecturePrivateKeyHash,
				this.createdAt,
				this.encLectureAttendedByUser
			)
		);
	}
}
