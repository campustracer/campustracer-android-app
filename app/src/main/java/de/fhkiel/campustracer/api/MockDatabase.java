package de.fhkiel.campustracer.api;

import androidx.core.util.Pair;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapperDataInterface;
import de.fhkiel.campustracer.helper.DateHelper;
import de.fhkiel.campustracer.model.CheckInEntry;
import de.fhkiel.campustracer.model.LectureAttendedByUserEntry;
import de.fhkiel.campustracer.model.PreGeneratedEncryptionKeyEntry;
import de.fhkiel.campustracer.model.RolesEnum;
import de.fhkiel.campustracer.model.UserAttendedLectureEntry;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;
import lombok.Getter;


public class MockDatabase implements CryptoWrapperDataInterface {

	private static final MockDatabase instance = new MockDatabase();

	@Getter
	private Map<Long, UserEntry> userEntryMap = new HashMap<>();
	@Getter
	private List<UserMasterKeyEntry> userMasterKeysTable = new ArrayList<>();
	@Getter
	private List<UserAttendedLectureEntry> userAttendedLectureTable = new ArrayList<>();
	@Getter
	private List<LectureAttendedByUserEntry> lectureAttendedByUserTable = new ArrayList<>();

	private MockDatabase() {
	}

	public static MockDatabase getInstance() {
		return instance;
	}

	public static void clearInstance() {
		instance.userEntryMap = new HashMap<>();
		instance.userMasterKeysTable = new ArrayList<>();
		instance.userAttendedLectureTable = new ArrayList<>();
		instance.lectureAttendedByUserTable = new ArrayList<>();
	}

	/*
	 * ADD ENTRIES
	 */

	@Override
	public UserMasterKeyEntry addUserMasterKeyAndPreGeneratedKeys(UserMasterKeyEntry userMasterKeyEntry) {
		// TODO: Backend: Verify Signature
		// TODO: Backend: UPDATE UserMasterKeysEntries SET invalidatedAt = entry.createdAt WHERE
		//  userID = entry.userID this User!
		this.userMasterKeysTable.add(userMasterKeyEntry);
		return userMasterKeyEntry;
	}

	@Override
	public void addCheckInEntry(CheckInEntry checkInEntry) {
		Pair<UserAttendedLectureEntry, LectureAttendedByUserEntry> checkInPair =
			checkInEntry.split();
		this.userAttendedLectureTable.add(checkInPair.first);
		this.lectureAttendedByUserTable.add(checkInPair.second);
	}

	@Override
	public void addUserEntryForMocking(UserEntry userEntry) {
		this.userEntryMap.putIfAbsent(userEntry.getId(), userEntry);
	}

	/*
	 * GET UserMasterKeysEntry
	 */

	// Used by lecturers for student login
	@Override
	public Optional<UserMasterKeyEntry> getCurrentlyValidUserMasterKeyEntryForUserId(long userId) {
		List<UserMasterKeyEntry> mks = this.userMasterKeysTable.stream()
			.filter(x -> x.getUserId() == userId)
			.filter(x -> x.getInvalidatedAt() == null)
			.collect(Collectors.toList());
		if (mks.size() == 1) {
			// validate
			if (Boolean.FALSE.equals(mks.get(0).verifySignature())) {
				throw new IllegalStateException("Signature mismatch in UserMasterKey-Table!");
			}
			return Optional.of(mks.get(0));
		}
		if (mks.size() > 1) {
			throw new IllegalStateException(
				"User has more than one currently valid UserMasterKey-Table entry!");
		}
		return Optional.empty();
	}

	// main query for get{Student|Lecturer}MasterkeyEntriesBy{Student|Lecturer}IdAndDate
	@Override
	public List<UserMasterKeyEntry> getUserMasterKeyEntriesForUserIdAndDate(
		long userId, LocalDate date
	) {

		List<UserMasterKeyEntry> userMasterKeyEntriesForDate =
			this.userMasterKeysTable.stream()
				.filter(x -> x.getUserId() == userId)
				//verified
				.filter(x -> ((DateHelper.isBeforeOrEqual(x.getCreatedAt(), date))
					&& (x.getInvalidatedAt() == null
					|| DateHelper.isAfterOrEqual(x.getInvalidatedAt(), date))))
				.collect(Collectors.toList());

		// verify all entries
		for (UserMasterKeyEntry userMasterKeyEntry : userMasterKeyEntriesForDate) {
			if (Boolean.FALSE.equals(userMasterKeyEntry.verifySignature())) {
				throw new IllegalStateException("Could not verify UserMasterKey Signature!");
			}
		}
		return userMasterKeyEntriesForDate;
	}

	@Override
	public Optional<UserEntry> getUserEntryById(long userId) {
		UserEntry userEntry = this.userEntryMap.get(userId);
		if (userEntry == null) {
			return Optional.empty();
		} else {
			return Optional.of(userEntry);
		}
	}

	/*
	 * GET UserMasterKeysEntry -> Student/Lecturer specializations (not necessarily needed in
	 * backend-side implementation)
	 */

	// for Student Signature Verification
	@Override
	public List<UserMasterKeyEntry> getStudentMasterKeyEntriesForStudentIdAndDate(
		long studentID, LocalDate date
	) {
		if (!this.isUserIdAStudent(studentID)) {
			return new ArrayList<>();
		}
		return this.getUserMasterKeyEntriesForUserIdAndDate(studentID, date)
			.stream()
			.collect(Collectors.toList());
	}

	// for Lecturer Signature Verification
	@Override
	public List<UserMasterKeyEntry> getLecturerMasterKeyEntriesForLecturerIdAndDate(
		long lecturerID, LocalDate date
	) {
		if (!this.isUserIdALecturer(lecturerID)) {
			return new ArrayList<>();
		}
		return this.getUserMasterKeyEntriesForUserIdAndDate(lecturerID, date)
			.stream()
			.collect(Collectors.toList());
	}

	/**
	 * GET PreGeneratedEncryptionKeyEntry
	 */

	// for Student Check-In via Lecturer
	@Override
	public Optional<PreGeneratedEncryptionKeyEntry> getCurrentlyValidPreGeneratedKeyEntryForUserIdAndDate(
		long userId, LocalDate date
	) {

		// first get currently valid UserMasterKeyEntry for userId, then get PreGenKeyEntry for
		// date
		Optional<UserMasterKeyEntry> validMasterkeyEntryOpt =
			this.getCurrentlyValidUserMasterKeyEntryForUserId(userId);
		if (!validMasterkeyEntryOpt.isPresent()) {
			return Optional.empty();
		}

		UserMasterKeyEntry currentlyValidMasterKeyEntry = validMasterkeyEntryOpt.get();
		if (currentlyValidMasterKeyEntry.getPreGeneratedEncryptionKeys() == null) {
			return Optional.empty();
		}

		List<PreGeneratedEncryptionKeyEntry> preGeneratedKeyEntries =
			currentlyValidMasterKeyEntry.getPreGeneratedEncryptionKeys()
				.stream()
				//verified
				.filter(x -> DateHelper.isBeforeOrEqual(x.getValidFrom(), date))
				.filter(x -> DateHelper.isAfterOrEqual(x.getValidTo(), date))
				.collect(Collectors.toList());
		if (preGeneratedKeyEntries.isEmpty()) {
			return Optional.empty();
		}
		if (preGeneratedKeyEntries.size() > 1) {
			throw new IllegalStateException(
				"User has more than one currently valid PreGeneratedKey-Table entry!");
		}
		PreGeneratedEncryptionKeyEntry preGeneratedKeyEntry = preGeneratedKeyEntries.get(0);
		if (Boolean.FALSE.equals(preGeneratedKeyEntry.verifySignature(currentlyValidMasterKeyEntry.getMasterPubKey()))) {
			throw new IllegalStateException("SIGNATURE MISMATCH IN PREGENERATEDKEYS TABLE");
		}
		return Optional.of(preGeneratedKeyEntry);
	}

	// for Tracing / Recreating derived PreGenPrivKeys
	@Override
	public Optional<PreGeneratedEncryptionKeyEntry> getPreGeneratedKeyEntryForUserIdAndDate(
		long userId, LocalDate date
	) {
		List<UserMasterKeyEntry> userMasterKeyEntries =
			this.getUserMasterKeyEntriesForUserIdAndDate(userId, date);
		if (userMasterKeyEntries.size() > 1) {
			throw new IllegalStateException(
				"THERE WERE MULTIPLE USERMASTERPUBKEYS FOUND FOR THE SAME MASTERPUBKEY + DATE "
					+ "COMBINATION!");
		} else if (userMasterKeyEntries.isEmpty()) {
			return Optional.empty();
		}
		UserMasterKeyEntry userMasterKeyEntry = userMasterKeyEntries.get(0);
		if (Boolean.FALSE.equals(userMasterKeyEntry.verifySignature())) {
			throw new IllegalStateException("SIGNATURE MISMATCH IN USERMASTERKEYS TABLE");
		}

		if (userMasterKeyEntry.getPreGeneratedEncryptionKeys() == null) {
			return Optional.empty();
		}

		List<PreGeneratedEncryptionKeyEntry> preGeneratedKeyEntries =
			userMasterKeyEntry.getPreGeneratedEncryptionKeys()
				.stream()
				.filter(x -> DateHelper.isBeforeOrEqual(x.getValidFrom(), date))
				.filter(x -> DateHelper.isAfterOrEqual(x.getValidTo(), date))
				.collect(Collectors.toList());
		if (preGeneratedKeyEntries.size() > 1) {
			throw new IllegalStateException(
				"THERE WERE MULTIPLE PREGENKEYENTRIES FOUND FOR THE SAME MASTERPUBKEY + DATE "
					+ "COMBINATION!");
		} else if (preGeneratedKeyEntries.isEmpty()) {
			return Optional.empty();
		} else {
			PreGeneratedEncryptionKeyEntry preGeneratedKeyEntry = preGeneratedKeyEntries.get(0);
			if (Boolean.FALSE.equals(preGeneratedKeyEntry.verifySignature(userMasterKeyEntry.getMasterPubKey()))) {
				throw new IllegalStateException("SIGNATURE MISMATCH IN PREGENERATEDKEYS TABLE");
			}
			return Optional.of(preGeneratedKeyEntry);
		}
	}

	/**
	 * GET UserAttendedLectureEntry
	 */

	// for CheckIn-Key Tracing
	@Override
	public List<UserAttendedLectureEntry> getUserAttendedLectureEntriesByCheckInPrivateKeyHashes(
		List<byte[]> privKeyHashes
	) {
		return this.userAttendedLectureTable.stream()
			.filter(x -> privKeyHashes.stream()
				.anyMatch(y -> Arrays.equals(y, x.getCheckInPrivateKeyHash())))
			.collect(Collectors.toList());
	}

	// for PreGen-Key Tracing
	@Override
	public List<UserAttendedLectureEntry> getUserAttendedLectureEntriesByDates(
		LocalDateTime dateFrom, LocalDateTime dateTo
	) {
		return this.userAttendedLectureTable.stream()
			.filter(x -> DateHelper.getDatesListFromDateRange(
				dateFrom.toLocalDate(),
				dateTo.toLocalDate()
			).stream().anyMatch(y -> y.isEqual(x.getCreatedAt())))
			.collect(Collectors.toList());
	}

	/**
	 * GET LectureAttendedByUserEntry
	 */

	@Override
	public List<LectureAttendedByUserEntry> getLectureAttendedByUserEntriesByLecturePrivKeyHashes(
		List<byte[]> lecturePrivKeyHash
	) {
		return this.lectureAttendedByUserTable.stream()
			.filter(x -> lecturePrivKeyHash.stream()
				.anyMatch(y -> Arrays.equals(y, x.getLecturePrivateKeyHash())))
			.collect(Collectors.toList());
	}

	/*
	 * INFECTION CASES
	 */

	// TODO: Create Infection Case
	// TODO: Get Infection Case
	// TODO: Check for new Infection Cases

	/*
	 * MISC
	 */

	@Override
	public Boolean isUserIdALecturer(long userId) {
		UserEntry userEntry = this.userEntryMap.get(userId);
		if (userEntry == null) {
			return false;
		}
		return userEntry.getRole() == RolesEnum.LECTURER;
	}

	@Override
	public Boolean isUserIdAStudent(long userId) {
		UserEntry userEntry = this.userEntryMap.get(userId);
		if (userEntry == null) {
			return false;
		}
		return userEntry.getRole() == RolesEnum.STUDENT;
	}

	/*
	@Override
	public String getTodaysCheckInAuthKey(long lecturerId) {
		// TODO: IMPLEMENT AUTHKEY STUFF
		return this.todaysCheckInAuthKey;
	} */

	@Override
	public void updateContactInformationForUser(String contactInformation) {
		//TODO: IMPLEMENT
	}
}
