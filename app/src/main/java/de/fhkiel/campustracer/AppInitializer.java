package de.fhkiel.campustracer;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.work.WorkManager;

import java.security.Security;

import de.fhkiel.campustracer.exceptions.WorkerAlreadyRunningException;
import de.fhkiel.campustracer.service.AppService;
import lombok.Getter;


// Initializer Singleton
public class AppInitializer {
	private static final String TAG = "AppInitializer";
	private static AppInitializer instance = null;

	@Getter
	private final AppService appService;
	@Getter
	private final WorkManager workManager;

	private AppInitializer(Context context) {
		// Initialize BouncyCastle
		Security.removeProvider("BC"); // remove BouncyCastle shipped with Android Studio (very
		// old, shrunk down version)
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

		this.appService = AppService.getInstance(context);

		/*
		 * Start the background worker
		 */
		this.workManager = WorkManager.getInstance(context);
		try {
			this.startWorkers();
		} catch (WorkerAlreadyRunningException e) {
			Log.e(TAG, "Couldn't start worker - start() worker was already executed");
		}
	}

	/**
	 * Start periodic and one time workers This will throw an IllegalStateException if workers
	 * already started
	 */
	public void startWorkers() throws WorkerAlreadyRunningException {
		DownloadWorker.startInitialWorker(this.workManager);
		DownloadWorker.startPeriodicWorker(this.workManager);
	}

	/**
	 * Stop all workers
	 */
	public void stopWorkers() {
		DownloadWorker.stopWorkers(this.workManager);
	}

	public static AppInitializer getInstance(Activity activity) {
		if (instance == null) {
			instance = new AppInitializer(activity.getApplicationContext());
		}
		return instance;
	}

	public static AppInitializer getInstance(Context context) {
		if (instance == null) {
			instance = new AppInitializer(context);
		}
		return instance;
	}

	public static AppInitializer getInstance() {
		if (instance == null) {
			throw new IllegalStateException(
				"No instance created: did you forget to call getInstance()"
					+ " with context on the current activity or fragment?");
		}
		return instance;
	}
}
