package de.fhkiel.campustracer;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.lifecycle.MutableLiveData;
import androidx.work.BackoffPolicy;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.exceptions.WorkerAlreadyRunningException;
import de.fhkiel.campustracer.model.RolesEnum;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;
import de.fhkiel.campustracer.service.ApiService;
import de.fhkiel.campustracer.service.DatabaseService;
import lombok.Getter;


public class DownloadWorker extends Worker {
	public enum InitialSyncStateEnum {
		NOT_STARTED, RUNNING, DONE, FAILED;

		public static Boolean isLegalNextState(
			InitialSyncStateEnum current, InitialSyncStateEnum next
		) {
			switch (current) {
				case NOT_STARTED:
					return true;
				case FAILED:
					if (next == RUNNING) {
						return true;
					}
					break;
				case RUNNING:
					if (next == DONE || next == FAILED) {
						return true;
					}
					break;
				case DONE:
					return false;
			}
			return false;
		}

		public static void postNextStateIfLegal(
			MutableLiveData<InitialSyncStateEnum> initSyncState, InitialSyncStateEnum nextState
		) {
			Boolean legalnext = isLegalNextState(initSyncState.getValue(), nextState);
			if (Boolean.TRUE.equals(legalnext)) {
				initSyncState.postValue(nextState);
			}
		}
	}

	public static final String TAG = "DownloadWorker";
	public static final String TAG_PERIODIC = "PeriodicDownloadWorker";
	public static final String TAG_INITIAL = "InitialDownloadWorker";
	public static final String TAG_ONE_TIME = "OneTimeDownloadWorker";
	public static final long PERIODIC_INTERVAL_MILLIS = 15 * 60 * 1000L;
	public static final long PERIODIC_INITIAL_DELAY_MILLIS = 3 * 60 * 1000L;
	// must be >= PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS
	public static final long WORK_REQUEST_BACKOFF_TIME_MILLIS = WorkRequest.MIN_BACKOFF_MILLIS;
	private static boolean oneTimeWorkerStarted = false;
	private static boolean periodicWorkerStarted = false;
	private static boolean workerIsRunning = false;

	@Getter
	private static final MutableLiveData<InitialSyncStateEnum> initialSyncState =
		new MutableLiveData<>(InitialSyncStateEnum.NOT_STARTED);

	private final AppInitializer appInitializer;
	private final ApiService apiService;
	private final DatabaseService databaseService;

	public DownloadWorker(Context context, WorkerParameters params) {
		super(context, params);
		// Assert that intervals are within limits (does not happen automatically,
		// at least not for PERIODIC_INTERVAL_MILLIS)
		assert (DownloadWorker.PERIODIC_INTERVAL_MILLIS
			>= PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS);
		assert (DownloadWorker.WORK_REQUEST_BACKOFF_TIME_MILLIS >= WorkRequest.MIN_BACKOFF_MILLIS
			&& DownloadWorker.WORK_REQUEST_BACKOFF_TIME_MILLIS <= WorkRequest.MAX_BACKOFF_MILLIS);

		this.appInitializer = AppInitializer.getInstance();
		this.apiService = this.appInitializer.getAppService().getApiService();
		this.databaseService = this.appInitializer.getAppService().getDatabaseService();
	}

	private static Constraints getConstraints() {
		return new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build();
	}

	protected static void startPeriodicWorker(WorkManager workManager)
		throws WorkerAlreadyRunningException {
		if (DownloadWorker.periodicWorkerStarted) {
			throw new WorkerAlreadyRunningException(DownloadWorker.TAG_PERIODIC);
		}
		PeriodicWorkRequest periodicSyncDataWork =
			new PeriodicWorkRequest.Builder(DownloadWorker.class,
				PERIODIC_INTERVAL_MILLIS,
				TimeUnit.MILLISECONDS
			).addTag(DownloadWorker.TAG_PERIODIC)
				.setConstraints(DownloadWorker.getConstraints())
				// setting a backoff on case the work needs to retry
				.setBackoffCriteria(BackoffPolicy.LINEAR,
					WorkRequest.MIN_BACKOFF_MILLIS,
					TimeUnit.MILLISECONDS
				)
				.setInitialDelay(PERIODIC_INITIAL_DELAY_MILLIS, TimeUnit.MILLISECONDS)
				.build();
		workManager.enqueueUniquePeriodicWork(DownloadWorker.TAG,
			ExistingPeriodicWorkPolicy.KEEP,
			periodicSyncDataWork
		);
		DownloadWorker.periodicWorkerStarted = true;
	}

	// For Synchronization requested by User!
	public static Boolean startOneTimeWorker(WorkManager workManager) {
		try {
			initialSyncState.postValue(InitialSyncStateEnum.NOT_STARTED);
			if (workerIsRunning) {
				throw new WorkerAlreadyRunningException(DownloadWorker.TAG_INITIAL
					+ " requested by User");
			}
			OneTimeWorkRequest oneTimeRunRequest =
				new OneTimeWorkRequest.Builder(DownloadWorker.class).setInitialDelay(0,
					TimeUnit.MILLISECONDS
				)
					.addTag(DownloadWorker.TAG_ONE_TIME)
					.setConstraints(DownloadWorker.getConstraints())
					.build();
			workManager.enqueue(oneTimeRunRequest);
			return true;
		} catch (WorkerAlreadyRunningException e) {
			e.printStackTrace();
			Log.w(TAG, "startOneTimeWorker called, but worker already running!");
		}
		return false;
	}

	protected static void startInitialWorker(WorkManager workManager)
		throws WorkerAlreadyRunningException {
		if (DownloadWorker.oneTimeWorkerStarted) {
			throw new WorkerAlreadyRunningException(DownloadWorker.TAG_INITIAL);
		}
		OneTimeWorkRequest initialRunRequest =
			new OneTimeWorkRequest.Builder(DownloadWorker.class).setInitialDelay(0,
				TimeUnit.MILLISECONDS
			)
				.addTag(DownloadWorker.TAG_INITIAL)
				.setConstraints(DownloadWorker.getConstraints())
				.build();
		workManager.enqueue(initialRunRequest);
		DownloadWorker.oneTimeWorkerStarted = true;
	}

	protected static void stopWorkers(WorkManager workManager) {
		workManager.cancelAllWorkByTag(DownloadWorker.TAG_PERIODIC);
		DownloadWorker.periodicWorkerStarted = false;
		workManager.cancelAllWorkByTag(DownloadWorker.TAG_INITIAL);
		DownloadWorker.oneTimeWorkerStarted = false;
		workManager.cancelAllWorkByTag(DownloadWorker.TAG_ONE_TIME);
		DownloadWorker.getInitialSyncState().postValue(InitialSyncStateEnum.NOT_STARTED);
		Log.d(TAG, "Workers stopped");
	}

	private void checkStopped() {
		if (this.isStopped()) {
			throw new IllegalStateException("Worker stopped");
		}
	}

	@NonNull
	@Override
	public Result doWork() {
		workerIsRunning = true;
		try {
			this.checkStopped(); // we check before every time intensive task like db or api access
			Optional<UserState> userState = this.appInitializer.getAppService().getUserState();
			Log.i(TAG, "Starting worker");
			if (userState.isPresent() && (userState.get().getRole() == RolesEnum.LECTURER
				|| userState.get().getRole() == RolesEnum.UNIVERSITY)) {
				long startNs = System.nanoTime();
				this.checkStopped();
				InitialSyncStateEnum.postNextStateIfLegal(initialSyncState,
					InitialSyncStateEnum.RUNNING
				);
				this.appInitializer.getAppService().getApiService().authenticate(userState.get());
				Log.d(TAG, "Starting synchronization");
				this.checkStopped();

				// Import Users
				List<UserEntry> newUsers =
					this.apiService.getUserEntries(this.databaseService.getDateTimeOfLatestUserEntry());
				Log.d(TAG, String.format("Got %d new users", newUsers.size()));
				long[] addedUserIds = this.databaseService.addUserEntries(newUsers);
				Log.d(TAG, String.format("Added %d new users", addedUserIds.length));
				this.checkStopped();

				// Import Master/PreGenKeys
				List<UserMasterKeyEntry> newKeys =
					this.apiService.getUserMasterKeyEntriesSince(this.databaseService.getDateTimeOfLatestKeyEntry());
				Log.d(TAG, String.format("Got %d new UserMasterKeys", newKeys.size()));
				this.checkStopped();
				Pair<long[], long[]> keyIdPair =
					this.databaseService.addOrUpdateMasterKeyEntries(newKeys);
				Log.d(TAG,
					String.format("Added %d new UserMasterKeys and updated %d existing " + "keys",
						keyIdPair.first.length,
						keyIdPair.second.length
					)
				);
				long endNs = System.nanoTime();

				Log.d(TAG,
					"Synchronization completed after "
						+ (endNs - startNs) / (1000d * 1000d * 1000d)
						+ " seconds!"
				);
				InitialSyncStateEnum.postNextStateIfLegal(initialSyncState,
					InitialSyncStateEnum.DONE
				);
			} else if (!userState.isPresent()) {
				// User is not logged in yet, try again...
				Log.d(TAG, "UserState not present yet, trying again shortly ...");
				return Result.retry();
			}
		} catch (ApiHttpException e) {
			e.printStackTrace();
			InitialSyncStateEnum.postNextStateIfLegal(initialSyncState,
				InitialSyncStateEnum.FAILED
			);
			return Result.retry();
		} catch (IllegalStateException e) {
			Log.d(TAG, "Worker was stopped, will try again if not removed from WorkManager");
			InitialSyncStateEnum.postNextStateIfLegal(initialSyncState,
				InitialSyncStateEnum.FAILED
			);
			return Result.retry();
		} finally {
			workerIsRunning = false; // notice: finally is even executed after return!
		}

		return Result.success();
	}
}
