package de.fhkiel.campustracer.repository.sqlite;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.util.Log;

import androidx.core.util.Pair;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.Security;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import de.fhkiel.campustracer.cryptowrapper.CryptoWrapper;
import de.fhkiel.campustracer.model.PreGeneratedEncryptionKeyEntry;
import de.fhkiel.campustracer.model.RolesEnum;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;
import de.fhkiel.campustracer.model.virtual.UserMasterKeyRelationEntry;


public class KeyRepositoryTest {
	private static final String TAG = "KeyRepositoryTest";
	private KeyRepository keyRepo;
	private UserRepository userRepo;
	private SqliteDatabase db;
	private KeyPair key;
	private UserEntry student;
	private UserEntry fellow;
	private UserEntry lecturer;

	@Before
	public void setUp() throws Exception {
		Context context = ApplicationProvider.getApplicationContext();
		this.db = Room.inMemoryDatabaseBuilder(context, SqliteDatabase.class).build();
		this.keyRepo = this.db.keyRepository();
		this.userRepo = this.db.userRepository();

		// add users
		long id = this.userRepo.addUserEntry(new UserEntry(
			0,
			"Student10000",
			RolesEnum.STUDENT,
			LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
			new byte[]{1, 2}
		));
		this.student = this.userRepo.getUserEntryById(id).get();
		id = this.userRepo.addUserEntry(new UserEntry(
			0,
			"Student10001",
			RolesEnum.STUDENT,
			LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
			new byte[]{1, 2}
		));
		this.fellow = this.userRepo.getUserEntryById(id).get();
		id = this.userRepo.addUserEntry(new UserEntry(
			0,
			"Lecturer100",
			RolesEnum.LECTURER,
			LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
			new byte[]{1, 2}
		));
		this.lecturer = this.userRepo.getUserEntryById(id).get();

		// Initialize BouncyCastle
		Security.removeProvider("BC");
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

		// Use same keypair for everything
		this.key = CryptoWrapper.generateKeypair();
	}

	@After
	public void tearDown() throws Exception {
		this.db.close();
	}

	public UserMasterKeyEntry createUserMasterKey(int id) {
		UserMasterKeyEntry key = UserMasterKeyEntry.createSigned(
			this.student.getId(),
			this.key.getPublic(),
			this.key.getPublic().getEncoded(),
			LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
			this.key.getPrivate(),
			IntStream.range(0, 3).mapToObj(i -> PreGeneratedEncryptionKeyEntry.createSigned(
				this.key.getPublic(),
				LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
				LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
				this.key.getPrivate()
			)).collect(Collectors.toList())
		);
		key.setId(id);
		return key;
	}

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	public void storeMasterStudentKey() {
		UserMasterKeyEntry key = this.createUserMasterKey(1);
		long keyId = this.keyRepo.addUserMasterKeyEntry(key);

		UserMasterKeyEntry fetchedKey =
			this.keyRepo.getUserMasterKeyRelationEntry(keyId).toUserMasterKeyEntry();

		assertThat(key.getMasterPubKey(), equalTo(fetchedKey.getMasterPubKey()));

		assertThat(key.getPreGeneratedEncryptionKeys(), notNullValue());
		assertThat(fetchedKey.getPreGeneratedEncryptionKeys(), notNullValue());
		assertThat(
			key.getPreGeneratedEncryptionKeys().size(),
			equalTo(fetchedKey.getPreGeneratedEncryptionKeys().size())
		);

		// now with failure bc of same id
		UserMasterKeyEntry key2 = this.createUserMasterKey(1);
		this.exception.expect(SQLiteConstraintException.class);
		this.exception.expectMessage("UNIQUE constraint failed: UserMasterKeyEntry.id");
		this.keyRepo.addUserMasterKeyEntry(key2);
	}

	@Test
	public void getValidUserMasterKeyRelationEntryForUserTest() {
		UserMasterKeyEntry key = this.createUserMasterKey(999);
		key.setInvalidatedAt(null);
		long keyId = this.keyRepo.addUserMasterKeyEntry(key);
		Log.d(TAG, "KeyId: " + keyId);
		Optional<UserMasterKeyRelationEntry> userMasterKeyRelationEntryOptional =
			this.keyRepo.getValidUserMasterKeyRelationEntryForUser(this.student.getId());
		assertThat(userMasterKeyRelationEntryOptional.isPresent(), equalTo(true));
	}

	@Test
	public void storeMultipleMasterStudentKeys() {
		List<UserMasterKeyEntry> keys =
			IntStream.range(0, 5).mapToObj(this::createUserMasterKey).collect(Collectors.toList());

		Pair<long[], long[]> keyIdPair = this.keyRepo.addOrUpdateUserMasterKeyEntries(keys);
		assertThat(keyIdPair.first.length + keyIdPair.second.length, equalTo(keys.size()));

		// if key with same id is inserted only the invalidatedAt field should be updated
		UserMasterKeyEntry updateKey = keys.get(0);
		byte[] oldSignature = updateKey.getSignature();

		updateKey.setInvalidatedAt(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
		updateKey.setSignature("HalloWelt".getBytes(StandardCharsets.UTF_8));

		this.keyRepo.addOrUpdateUserMasterKeyEntries(Collections.singletonList(updateKey));
		UserMasterKeyEntry fetchedKey =
			this.keyRepo.getUserMasterKeyRelationEntry(0L).toUserMasterKeyEntry();
		assertThat(fetchedKey.getInvalidatedAt(), notNullValue());
		assertThat(fetchedKey.getSignature(), equalTo(oldSignature));
	}

	@Test
	public void deleteMasterKey() {
		UserMasterKeyEntry key = this.createUserMasterKey(1);
		long keyid = this.keyRepo.addUserMasterKeyEntry(key);

		List<PreGeneratedEncryptionKeyEntry> pgeks =
			this.keyRepo.getPreGeneratedEncryptionKeyEntries(keyid);
		assertThat(pgeks.size(), equalTo(3));

		this.keyRepo.deleteUserMasterKeyEntry(key);

		// check that related PGEKs are also deleted
		pgeks = this.keyRepo.getPreGeneratedEncryptionKeyEntries(keyid);
		assertThat(pgeks.size(), equalTo(0));
	}
}