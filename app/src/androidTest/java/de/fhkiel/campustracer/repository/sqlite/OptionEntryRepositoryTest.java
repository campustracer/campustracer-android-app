package de.fhkiel.campustracer.repository.sqlite;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import android.content.Context;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import de.fhkiel.campustracer.model.OptionEntry;
import de.fhkiel.campustracer.model.OptionsEnum;


public class OptionEntryRepositoryTest {
	private OptionRepository repo;
	private SqliteDatabase db;

	@Before
	public void setUp() throws Exception {
		Context context = ApplicationProvider.getApplicationContext();
		this.db = Room.inMemoryDatabaseBuilder(context, SqliteDatabase.class).build();
		this.repo = this.db.optionRepository();

		this.repo.setOption(new OptionEntry(OptionsEnum.USERNAME, "Student10001"));
		this.repo.setOption(new OptionEntry(OptionsEnum.ROLE, "STUDENT"));
		this.repo.setOption(new OptionEntry(OptionsEnum.IS_REGISTERED, "True"));
	}

	@After
	public void tearDown() throws Exception {
		this.db.close();
	}

	@Test
	public void getAll() {
		List<OptionEntry> options = this.repo.getAll();
		assertThat(options.size(), equalTo(3));
	}

	@Test
	public void getOption() {
		OptionEntry role = this.repo.getOption(OptionsEnum.ROLE);
		assertThat(role.getValue(), equalTo("STUDENT"));
	}

	@Test
	public void setOption() {
		this.repo.setOption(new OptionEntry(OptionsEnum.IS_REGISTERED, "False"));
		OptionEntry hasKeys = this.repo.getOption(OptionsEnum.IS_REGISTERED);
		assertThat(hasKeys.getValue(), equalTo("False"));
	}
}