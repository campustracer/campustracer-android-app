package de.fhkiel.campustracer.repository;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import de.fhkiel.campustracer.helper.DateHelper;
import de.fhkiel.campustracer.model.CheckInEntry;
import de.fhkiel.campustracer.model.InfectionCaseEntry;
import de.fhkiel.campustracer.model.LectureAttendedByUserEntry;
import de.fhkiel.campustracer.model.RolesEnum;
import de.fhkiel.campustracer.model.UserAttendedLectureEntry;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.model.UserMasterKeyEntry;
import de.fhkiel.campustracer.model.virtual.InfectionCaseCreateEntry;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.Calls;


public class MockApiRepository implements ApiRepository {
	private final BehaviorDelegate<ApiRepository> delegate;
	private final List<CheckInEntry> checkInEntries = new ArrayList<>();
	private final List<UserEntry> userEntries = new ArrayList<>();
	private final List<UserMasterKeyEntry> userMasterKeyEntries = new ArrayList<>();

	public MockApiRepository(BehaviorDelegate<ApiRepository> delegate) {
		this.delegate = delegate;
	}

	public void mockAddUserEntry(UserEntry userEntry) {
		this.userEntries.add(userEntry);
	}

	private Call<Object> getError(Integer code, String errorMessage) {
		final ResponseBody responseBody =
			ResponseBody.create(MediaType.parse("application/json"), errorMessage);
		return Calls.response(Response.error(code, responseBody));
	}

	/**
	 * Parse the mock basicAuth string to an UserEntry
	 *
	 * @param basicAuth "UserID|UserName|UserRole|CreatedAt"
	 * @return The parsed UserEntry
	 */
	public UserEntry userEntryFromBasicAuth(String basicAuth) {
		String[] parts = basicAuth.split("\\|");
		if (parts.length != 4) {
			throw new IllegalArgumentException("Invalid mock authString");
		}
		int id = Integer.parseInt(parts[0]);
		RolesEnum role = RolesEnum.valueOf(parts[2]);
		LocalDateTime createdAt = DateHelper.getLocalDateTimeFromISODateTimeString(parts[3]);
		return new UserEntry(id,
			parts[1],
			role,
			createdAt,
			"EncContactData".getBytes(StandardCharsets.UTF_8)
		);
	}

	/**
	 * Create a basicAuth string from an UserEntry
	 *
	 * @param userEntry The UserEntry
	 * @return "UserID|UserName|UserRole|CreatedAt"
	 */
	public String basicAuthFromUserEntry(UserEntry userEntry) {
		return String.format("%d|%s|%s|%s",
			userEntry.getId(),
			userEntry.getUsername(),
			userEntry.getRole().name(),
			DateHelper.getISODateTimeString(userEntry.getChangedAt())
		);
	}

	@Override
	public Call<List<UserMasterKeyEntry>> getAllUserMasterKeysAndPreGeneratedKeys(
		String basicAuth, String sinceDateTime
	) {
		List<UserMasterKeyEntry> result;
		if (sinceDateTime != null) {
			LocalDateTime since = DateHelper.getLocalDateTimeFromISODateTimeString(sinceDateTime);
			result = this.userMasterKeyEntries.stream()
				.filter(e -> e.getCreatedAt().isAfter(since) || (e.getInvalidatedAt() != null
					&& e.getInvalidatedAt().isAfter(since)))
				.collect(Collectors.toList());
		} else {
			result = this.userMasterKeyEntries;
		}
		return this.delegate.returningResponse(result)
			.getAllUserMasterKeysAndPreGeneratedKeys(basicAuth, sinceDateTime);
	}

	@Override
	public Call<UserMasterKeyEntry> addUserMasterKeyAndPreGeneratedKeys(
		UserMasterKeyEntry userMasterKeyEntry, String basicAuth
	) {
		UserEntry user = this.userEntryFromBasicAuth(basicAuth);
		if (userMasterKeyEntry.getUserId() != user.getId()) {
			return this.delegate.returning(this.getError(400, "Wrong user id"))
				.addUserMasterKeyAndPreGeneratedKeys(userMasterKeyEntry, basicAuth);
		}
		this.userMasterKeyEntries.add(userMasterKeyEntry);
		return this.delegate.returningResponse(userMasterKeyEntry)
			.addUserMasterKeyAndPreGeneratedKeys(userMasterKeyEntry, basicAuth);
	}

	@Override
	public Call<CheckInEntry> createCheckIn(CheckInEntry checkInEntry) {
		this.checkInEntries.add(checkInEntry);
		return this.delegate.returningResponse(checkInEntry).createCheckIn(checkInEntry);
	}

	@Override
	public Call<List<UserMasterKeyEntry>> getAllMasterKeyEntriesForAuthenticatedUser(String basicAuth) {
		UserEntry user = this.userEntryFromBasicAuth(basicAuth);
		return this.delegate.returningResponse(this.userMasterKeyEntries.stream()
			.filter(k -> k.getUserId() == user.getId()))
			.getAllMasterKeyEntriesForAuthenticatedUser(basicAuth);
	}

	@Override
	public Call<List<UserMasterKeyEntry>> getAllMasterKeyEntriesByUserId(
		String basicAuth, Integer userId
	) {
		return this.delegate.returningResponse(this.userMasterKeyEntries.stream()
			.filter(k -> k.getUserId() == userId))
			.getAllMasterKeyEntriesByUserId(basicAuth, userId);
	}

	@Override
	public Call<List<UserMasterKeyEntry>> getCurrentlyValidMasterKeyEntriesByUserId(
		String basicAuth, Long userId
	) {
		return this.delegate.returningResponse(this.userMasterKeyEntries.stream()
			.filter(k -> k.getUserId() == userId)
			.filter(i -> i.getInvalidatedAt() == null))
			.getCurrentlyValidMasterKeyEntriesByUserId(basicAuth, userId);
	}

	@Override
	public Call<List<UserEntry>> getUserEntries(
		String basicAuth, String sinceDateTime
	) {
		return this.delegate.returningResponse(this.userEntries)
			.getUserEntries(basicAuth, sinceDateTime);
	}

	@Override
	public Call<UserEntry> getUserEntryByName(String username, String basicAuth) {
		Optional<UserEntry> user =
			this.userEntries.stream().filter(u -> u.getUsername().equals(username)).findFirst();
		if (user.isPresent()) {
			return this.delegate.returningResponse(user.get())
				.getUserEntryByName(username, basicAuth);
		}
		return this.delegate.returning(this.getError(404, "Not Found"))
			.getUserEntryByName(username, basicAuth);
	}

	@Override
	public Call<UserEntry> putEncContactDataForUsername(
		String username, RequestBody requestBodyWithEncContactData, String basicAuth
	) {
		return null;
	}

	@Override
	public Call<InfectionCaseEntry> getInfectionCaseEntryById(
		Long infectionCaseId, String basicAuth
	) {
		return null;
	}

	@Override
	public Call<List<InfectionCaseEntry>> getInfectionCaseEntriesSinceId(
		String basicAuth, Long sinceId
	) {
		return null;
	}

	@Override
	public Call<InfectionCaseEntry> addInfectionCase(InfectionCaseCreateEntry infectionCaseCreateEntry) {
		return null;
	}

	@Override
	public Call<List<LectureAttendedByUserEntry>> getLectureAttendedByUserEntriesByLecturePrivKeyHashes(
		String basicAuth, List<String> privKeyHashes
	) {
		return null;
	}

	@Override
	public Call<List<UserAttendedLectureEntry>> getUserAttendedLectureEntriesByCheckInPrivateKeyHashes(
		String basicAuth, List<String> privKeyHashes
	) {
		return null;
	}

	@Override
	public Call<List<UserAttendedLectureEntry>> getUserAttendedLectureEntriesByDates(
		String authtoken, LocalDateTime dateFrom, LocalDateTime dateTo
	) {
		return null;
	}
}
