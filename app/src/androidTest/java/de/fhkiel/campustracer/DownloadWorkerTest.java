package de.fhkiel.campustracer;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import android.content.Context;
import android.util.Log;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.work.Configuration;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import androidx.work.testing.SynchronousExecutor;
import androidx.work.testing.WorkManagerTestInitHelper;

import org.junit.Before;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import de.fhkiel.campustracer.exceptions.ApiHttpException;
import de.fhkiel.campustracer.model.RolesEnum;
import de.fhkiel.campustracer.model.UserEntry;
import de.fhkiel.campustracer.repository.ApiRepository;
import de.fhkiel.campustracer.repository.MockApiRepository;
import de.fhkiel.campustracer.repository.sqlite.SqliteDatabase;
import de.fhkiel.campustracer.service.ApiService;
import de.fhkiel.campustracer.service.AppService;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;


public class DownloadWorkerTest {
	Context context;
	Executor executor;

	AppInitializer appInitializer;
	AppService appService;
	MockApiRepository api;
	SqliteDatabase db;

	@Before
	public void setUp() throws Exception {
		this.context = ApplicationProvider.getApplicationContext();
		this.executor = Executors.newSingleThreadExecutor();
		this.appInitializer = AppInitializer.getInstance(this.context);
		this.appService = this.appInitializer.getAppService();

		// set api
		MockRetrofit mockRetrofit = new MockRetrofit.Builder(ApiService.getRetrofit()).build();
		BehaviorDelegate<ApiRepository> delegate = mockRetrofit.create(ApiRepository.class);
		this.api = new MockApiRepository(delegate);
		this.appService.getApiService().setApiRepository(this.api);

		// set database
		this.db = Room.inMemoryDatabaseBuilder(this.context, SqliteDatabase.class).build();
		this.appService.getDatabaseService().setDb(this.db);

		// set worker config
		Configuration config = new Configuration.Builder().setMinimumLoggingLevel(Log.DEBUG)
			.setExecutor(new SynchronousExecutor())
			.build();

		// Initialize WorkManager for instrumentation tests.
		WorkManagerTestInitHelper.initializeTestWorkManager(this.context, config);
	}

	private UserEntry getStudentEntry(int id, String username) {
		return this.getUserEntry(id, username, RolesEnum.STUDENT);
	}

	private UserEntry getLecturerEntry(int id, String username) {
		return this.getUserEntry(id, username, RolesEnum.LECTURER);
	}

	private UserEntry getUserEntry(int id, String username, RolesEnum role) {
		return new UserEntry(id,
			username,
			role,
			LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
			"encContactData".getBytes(StandardCharsets.UTF_8)
		);
	}

	private UserState userStatefromEntry(UserEntry userEntry) {
		return new UserState(userEntry.getId(),
			userEntry.getUsername(),
			"secret",
			userEntry.getRole()
		);
	}

	@Test
	public void testSetUp() throws ApiHttpException {
		UserEntry user = this.getStudentEntry(0, "TestStudent");
		this.api.mockAddUserEntry(user);
		this.appService.getApiService().setAuthtoken(this.api.basicAuthFromUserEntry(user));
		List<UserEntry> result = this.appService.getApiService().getUserEntries(null);
		assertThat(result.size(), equalTo(1));
	}

	@Test
	public void testDownloadWorker() throws InterruptedException, ExecutionException {
		UserEntry lecturer = this.getLecturerEntry(1, "Lecturer");
		this.api.mockAddUserEntry(lecturer);
		this.db.userRepository().addUserEntry(lecturer);
		this.appService.setUserState(this.userStatefromEntry(lecturer));
		this.appService.getApiService().setAuthtoken(this.api.basicAuthFromUserEntry(lecturer));

		assertThat(this.db.userRepository().getAllUserEntries().size(), equalTo(1));
		for (int i = 2; i < 7; i++) {
			this.api.mockAddUserEntry(this.getStudentEntry(i, "Student" + i));
		}

		OneTimeWorkRequest request = new OneTimeWorkRequest.Builder(DownloadWorker.class).build();
		WorkManager workManager = WorkManager.getInstance(this.context);
		workManager.enqueue(request).getResult().get();
		WorkInfo workInfo = workManager.getWorkInfoById(request.getId()).get();
		Data outputData = workInfo.getOutputData();
		assertThat(workInfo.getState(), is(WorkInfo.State.SUCCEEDED));

		// check that the download worker added all new entries
		assertThat(this.db.userRepository().getAllUserEntries().size(), equalTo(6));
	}
}
